<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ReferralPlans;
use App\Models\Plan;
use Carbon\Carbon;
use CommonHelper;
use Mail;
use DB;
use NotificationHelper;
use App\Models\User;
use App\Models\PaymentHistory;
use Srmklive\PayPal\Services\PayPal as PayPalClient;
use Illuminate\Support\Facades\Storage;

class ReferralPlanActive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'referralplanactive:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Referral Plan Active for user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = Carbon::now()->format('Y-m-d');

        $user_data = User::select('id','email','phone','country_code','plan_id','plan_type','expiration_date')->with(['referralplan','paymenthistory'=>function($q) {
                        $q->where('mode', 'paypal')->orderBy('id','desc');
                    }])
                    ->where('parent_id', 0)->where('status',1)
                    ->whereHas('referralplan', function($q) {
                        $q->where('referral_plans.status', 0);
                    })
                    ->where('expiration_date', $date)
                    ->get();
                    
        $payment_history = array();

        if(!empty($user_data)){

        $user_ids = $user_data->pluck('id');
    
            foreach ($user_data as $key => $value) {

                $provider = new PayPalClient;
                $provider->getAccessToken();
              
                $plan_data = json_decode($value->paymenthistory->response,true);

                $subscription_id = $plan_data['subscription_id'];

                $response = $provider->suspendSubscription($subscription_id, 'Referral Plan Active');

                $payment_history[] = array(
                    'user_id' => $value['id'],
                    'mode' => "",
                    'plan_id' => $value['plan_id'],
                    'type' => $value['plan_type'],
                    'response' => ''
                );
            }
            
            ReferralPlans::whereIn('user_id',$user_ids)->update(['status'=>1]);        
            User::whereIn('id',$user_ids)->update(['expiration_date'=>Carbon::parse($date)->addDays('30')->format('Y-m-d')]);
        }
        
        if(!empty($payment_history)){
            PaymentHistory::insert($payment_history);
        }
        
        
        return true;
    }
}