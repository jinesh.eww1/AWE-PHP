<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Notification;
use Carbon\Carbon;
use CommonHelper;
use Mail;
use DB;
use App\Mail\ReminderSubscriptionMail;
use NotificationHelper;

class ReminderSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remindersubscription:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder Subscription for all users.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $date = Carbon::now()->addDays(CommonHelper::ConfigGet('reminder_mail_of_subscription'));
        $date_Ymd = $date->format('Y-m-d');

        $today = Carbon::now();
        $today_Ymd = $today->format('Y-m-d');

        $user_data = User::select('id','email','phone','country_code','plan_id','plan_type','expiration_date', DB::raw("CONCAT(country_code,'',phone) as country_code_phone") )->where('parent_id', 0)
                            ->where('status',1)
                            ->where('expiration_date','>=', $today_Ymd)
                            ->where('expiration_date','<=',$date_Ymd)
                            ->with(['devices'])
                            ->get();

        if(count($user_data) > 0){
            $number = $user_data->pluck('country_code_phone');

            $title = "Upcoming Subscription Renewal";
            $type = 'upcoming_subscription_renewal'; 
            $notification_arr['type'] = $type;
            $support_email = CommonHelper::ConfigGet('from_email');

            foreach ($user_data as $key => $value) {

                $expiration_date_obj = Carbon::parse($value->expiration_date);
                $expiration_date_dFy = $expiration_date_obj->format('d F, Y');
                $expiration_date_Ymd = $expiration_date_obj->format('Y-m-d');
                if($expiration_date_Ymd == $today_Ymd){
                    $expiration_date_dFy = 'today';
                }else{
                    $expiration_date_dFy = 'on '.$expiration_date_dFy;
                }

                $email_description = "<p>This is is just reminder. Your Upcoming Subscription Plan will renew automatically ".$expiration_date_dFy.".</p><p>We will automatically process your next payment ".$expiration_date_dFy.". You do not need to take any action, we just want to let you know.</p><p>If you have any questions, please contact <a href = 'mailto: ".$support_email."'>".$support_email."</a></p>";
                $description = "Your current subscription plan will be renew ".$expiration_date_dFy.".";

                Mail::to($value->email)->send(new ReminderSubscriptionMail($title, $email_description, config('app.address1'), config('app.address2')));

                if(isset($value->devices) && $value->devices->token != '' && $value->notification == 1){
                    NotificationHelper::send($value->devices->token, $title, $description, $type, $notification_arr);
                }

                $notification_data[] = array(
                        'user_id' => $value->id,
                        'title' => $title,
                        'message' => $description,
                        'type' => $type
                    );
            }

            Notification::insert($notification_data);

            if(!empty($number)){

                $message = "Your current subscription plan will expire soon.";
                foreach ($number as $key => $value) {
                    NotificationHelper::send_sms($message, $value);                    
                }
            }

        }

        return true;
    }
}