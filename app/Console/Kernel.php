<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //Commands\AutoRejectCron::class,
        Commands\DeleteUser::class,
        Commands\ReminderSubscription::class,
        Commands\DeleteChunkFile::class,
        Commands\ReferralPlanActive::class,
        Commands\ReferralPlanDeactive::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('deleteuser:cron')->daily();
        $schedule->command('remindersubscription:cron')->daily();
        $schedule->command('deletechunkfile:cron')->daily(); //dailyAt('4:00');
        $schedule->command('referralplanactive:cron')->daily(); //dailyAt('23:55');
        $schedule->command('referralplandeactive:cron')->daily(); //dailyAt('24:00');

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}