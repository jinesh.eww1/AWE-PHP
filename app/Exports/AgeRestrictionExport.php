<?php

namespace App\Exports;


use App\Models\AgeRatings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class AgeRestrictionExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return AgeRatings::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'Name',
            'For Children',
            'Total Content',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($age): array
    {
        return [
            $age->id,
            $age->name,
            $age->children ? 'Yes' : 'No',
            $age->content->count() > 0 ? $age->content->count() : '0',
        ];
    }
}
