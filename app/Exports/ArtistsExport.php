<?php

namespace App\Exports;

use App\Models\Artist;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ArtistsExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Artist::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'Cast Type',
            'Name',
            'Total Content',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($artist): array
    {
        return [
            $artist->id,
            $artist->castType->name,
            $artist->name,
            $artist->content_cast->count() > 0 ? $artist->content_cast->count() : '0',
        ];
    }
}
