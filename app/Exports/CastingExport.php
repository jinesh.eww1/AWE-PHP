<?php

namespace App\Exports;

use App\Models\CastTypes;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CastingExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return CastTypes::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'Name',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($cast): array
    {
        return [
            $cast->id,
            $cast->name,
        ];
    }
}
