<?php

namespace App\Exports;

use App\Models\Content;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ContentsExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Content::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'Name',
            'Is Free',
            'Content Type',
            'Status',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($content): array
    {
        if ($content->content_type == 1) {
            $content_type = "Movie";
        } elseif ($content->content_type == 2) {
            $content_type = "Tv Show";            
        } else {
            $content_type = "-";            
        }

        if($content->status == 1){
            $status = 'Active';
        }elseif($content->status == 2){
            $status = 'Deactivated';
        }else{
            $status = 'Upcoming';
        }
        
        return [
            $content->id,
            $content->name,
            $content->is_free ? 'Yes' : 'No',
            $content_type,
            $status,
        ];
    }
}
