<?php

namespace App\Exports;

use App\Models\Tag;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ContenttagsExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Tag::with('content_tags')->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Name',
            'Total Content'
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($tag): array
    {
        return [
            $tag->id,
            $tag->name,
            $tag->content_tags->count() > 0 ? $tag->content_tags->count() : '0',
        ];
    }
}
