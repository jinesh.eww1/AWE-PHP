<?php

namespace App\Exports;

use App\Models\Faq;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class FaqsExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Faq::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'Question',
            'Status',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($faq): array
    {
        if($faq->status == 0){
            $faq_status =  "Inactive";
        }else{
            $faq_status = "Active";
        }

        return [
            $faq->id,
            $faq->question,
            $faq_status,
        ];
    }
}
