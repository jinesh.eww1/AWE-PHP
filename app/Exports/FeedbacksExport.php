<?php

namespace App\Exports;

use App\Models\Feedback;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class FeedbacksExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Feedback::with('user')->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'User',
            'Question',
            'Feedback',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($feedback): array
    {
        $full_name = '';
        if(isset($feedback->user) && !empty($feedback->user)){
            $first_name = !empty($feedback->user->first_name) ? $feedback->user->first_name:'';
            $last_name = !empty($feedback->user->last_name) ? $feedback->user->last_name:'';
            $full_name = $first_name.' '.$last_name;
        }

        $questions = '';
        if($feedback->faq){
            $questions = !empty($feedback->faq->question) ? $feedback->faq->question:'';
        }

        return [
            $feedback->id,
            $full_name,
            $questions,
            $feedback->description,
        ];
    }
}
