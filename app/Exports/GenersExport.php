<?php

namespace App\Exports;

use App\Models\Genre;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class GenersExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Genre::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'Name',
            'Total Associated Content',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($genre): array
    {
        return [
            $genre->id,
            $genre->name,
            $genre->content->count() > 0 ? $genre->content->count() : '0',
        ];
    }
}
