<?php

namespace App\Exports;

use App\Models\MultiLanguageKey;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class MultiLanguageManagementExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return MultiLanguageKey::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'Key',
            'Group',
            'Name',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($multi_language_management): array
    {
        return [
            $multi_language_management->id,
            $multi_language_management->language_key,
            $multi_language_management->group,
            $multi_language_management->multi_language_value->language_value,
        ];
    }
}
