<?php

namespace App\Exports;

use App\Models\PaymentHistory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PaymentHistoryExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return PaymentHistory::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'User',
            'Mode',
            'Plan',
            'Type',
            'Transaction Date & Time',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($payment_history): array
    {
        $full_name = '';
        if($payment_history->users){
            $first_name = !empty($payment_history->users->first_name)?$payment_history->users->first_name:'';
            $last_name = !empty($payment_history->users->last_name)?$payment_history->users->last_name:'';
            $full_name = $first_name.' '.$last_name;
        }

        if ($payment_history->type == 1) {
            $plan_duration = "Monthly";
        } elseif ($payment_history->type == 2) {
            $plan_duration = "Yearly";            
        } else {
            $plan_duration = "-";            
        }

        return [
            $payment_history->id,
            $full_name,
            $payment_history->mode,
            $payment_history->plan->name,
            $plan_duration,
            $payment_history->created_at,
        ];
    }
}
