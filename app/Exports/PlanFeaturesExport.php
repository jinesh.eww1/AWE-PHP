<?php

namespace App\Exports;

use App\Models\Attribute;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PlanFeaturesExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Attribute::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'Name',
            'Value',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($plan_features): array
    {
        return [
            $plan_features->id,
            $plan_features->alias,
            $plan_features->value,
        ];
    }
}
