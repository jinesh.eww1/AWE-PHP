<?php

namespace App\Exports;

use App\Models\Plan;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PlanManagmentExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Plan::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'Name',
            'Monthly Price',
            'Yearly Price',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($plan_managment): array
    {
        return [
            $plan_managment->id,
            $plan_managment->name,
            '$'.' '.$plan_managment->monthly_price,
            '$'.' '.$plan_managment->yearly_price,
        ];
    }
}
