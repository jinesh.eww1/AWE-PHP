<?php

namespace App\Exports;

use App\Models\Season;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SeasonsExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Season::with('content_data')->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Content Name',
            'Name',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($season): array
    {
        if($season->content_data->name){
            $content_name = $season->content_data->name;
        }else{
            $content_name = '';
        }
        
        return [
            $season->id,
            $content_name,
            $season->season,
        ];
    }
}
