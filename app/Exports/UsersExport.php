<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

use App\Models\User;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use DB;
class UsersExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::select('users.*', 'users_group.group_id')->join('users_group', 'users.id', '=', 'users_group.user_id')->where('users_group.group_id',4)->where('users.parent_id',0)->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'First Name',
            'Last Name',
            'Email',
            'Status',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($user): array
    {
        if ($user->status == 0) {
            $status = "Pending";
        } elseif ($user->status == 1) {
            $status = "Active";            
        } elseif ($user->status == 2 && !is_null($user->deactivated_time)){
            $status = "Restore";            
        }else {
            $status = "Inactive";            
        }
        
        return [
            $user->id,
            $user->first_name,
            $user->last_name,
            $user->email,
            $status
        ];
    }
}
