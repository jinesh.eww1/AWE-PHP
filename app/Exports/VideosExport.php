<?php

namespace App\Exports;

use App\Models\Video;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class VideosExport implements FromCollection,WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Video::with('season','content')->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Content Name',
            'Season Name',
            'Video Name',
        ];
    }

    /**
     * @var Invoice $invoice
     */
    public function map($video): array
    {
        if(!empty($video->season)){
            $season_name = $video->season->season;
        }else{
            $season_name =  '';
        }

        if(!empty($video->content->name)){
            $content_name = $video->content->name;
        }else{
            $content_name = '';
        }   

        return [
            $video->id,
            $content_name,
            $season_name,
            $video->episode_name ? $video->episode_name : '',
        ];
    }

}
