<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use Image;
use Carbon\Carbon;
use Storage;
use App\Models\User;
use App\Models\Genre;
use Illuminate\Http\JsonResponse;
use Pion\Laravel\ChunkUpload\Exceptions\UploadFailedException;
use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

use App\Models\Notification;
use App\Models\MultiLanguageKey;
use App\Models\MultiLanguageValue;

class CommonHelper
{
    public static function resize($dir,$name,$w=300,$h=300)
    {
        $img = Image::make(public_path($dir.$name));
        $img->resize($w,$h, function ($constraint) {
            $constraint->aspectRatio();
        });
        $name = explode('.',$name);
        $new_path = public_path($dir.$name[0].'_'.$w.'x'.$h.".jpg");
        $img->save($new_path);
        return $new_path;
    }

    public static function docsresize($dir,$name,$w=3000,$h=3000)
    {
        $img = Image::make(public_path($dir.$name));
        $img->resize($w,$h, function ($constraint) {
            $constraint->aspectRatio();
        });
        $name = explode('.',$name);
        // dd($name);
        $new_path = public_path($dir.$name[0].'_'.$w.'x'.$h);
        $img->save($new_path);
        return $new_path;
    }

    public static function imageUpload($image,$dir = "images/users")
    {

        $name = time().rand(000, 999).'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path($dir);
        $image->move($destinationPath, $name);

        Self::resize($dir.'/',$name);
        return $name;
    }

    public static function imageUploadByLink($image_link, $dir = "images/users/")
    {

        $extension = pathinfo($image_link, PATHINFO_EXTENSION);

        $exte = array('jpeg','png','jpg','gif','svg');

        if(in_array($extension,$exte))
        {
            $filename = time().rand(000, 999).'.'.$extension;
            $full_path = $dir.$filename;
            $file = file_get_contents($image_link);
            $image=file_put_contents($full_path, $file);


            $data['image_full_link'] = url($full_path);
            $data['image_name'] = $filename;
            $data['status'] = TRUE;
            $data['message'] = 'Image fetched.';
        }else{
            $data['status'] = FALSE;
            $data['message'] = 'Only image link allowed.';
        }
        return $data;
    }

    public static function docsUpload($image,$dir = "images/users")
    {

        $name = time().rand(000, 999).'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path($dir);
        $image->move($destinationPath, $name);

        //Self::docsresize($dir.'/',$name);
        return $name;
    }

    public static function imageCatUpload($image,$dir = "images/category")
    {

        $name = time().rand(000, 999).'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path($dir);
        $image->move($destinationPath, $name);

        Self::resize($dir.'/',$name);
        return $name;
    }

    public static function s3Upload($image,$folder)
    {
        // $request->validate([
        //     'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        // ]);

        $path = Storage::disk('s3')->put($folder, $image);
        // return  $path = Storage::disk('s3')->url($path);
        
        return  str_replace($folder.'/','',$path);

    }

    public static function ConvertDate($date="",$formate="")
    {
 			// echo Carbon::createFromFormat('d-m-Y',date('Y-m-d'));exit;       
    }
     public static function ConfigSet($key_array)
    {
        foreach ($key_array as $key => $value) {
            $update['value']=$value;
            \App\Models\SystemConfig::wherePath($key)->update($update);
        }
        return true;
    }

    public static function ConfigGet($key)
    {
        $config = \App\Models\SystemConfig::wherePath($key)->first();
        return $config->value;
    }

    public static function RemovePhoneFormat($key)
    {
        $string = str_replace(array( '(+', ')',' ' , '-'), '', $key);
        return $string;
    }
    public static function SetPhoneFormat($key,$country_code)
    {
        // $string = '(+1'.substr($key, 0, 0).') '.substr($key,0);
        // $string = '+1 '.substr($key,-10, -7).'-'.substr($key,-7, -4).'-'.substr($key, -4);
        // return $string;
        $string =  !empty($country_code)?$country_code."(".substr($key, 0, 3).")".substr($key, 3, 3)."-".substr($key,6):"(".substr($key, 0, 3).")".substr($key, 3, 3)."-".substr($key,6);
        return $string;
    }


    public static function file_upload($request)
    {   
        $receiver = new FileReceiver("file", $request, HandlerFactory::classFromRequest($request));
        
        if ($receiver->isUploaded() === false) {
            throw new UploadMissingFileException();
        }
    
        $save = $receiver->receive();

        if ($save->isFinished()) {
            return Self::saveFile($save->getFile());
            //return CommonHelper::saveFile($save->getFile());

        }

        $handler = $save->handler();

        return response()->json([
            "done" => $handler->getPercentageDone(),
            'status' => true
        ]);

    }

    
    /**
     * Saves the file
     *
     * @param UploadedFile $file
     *
     * @return JsonResponse
     */
    public static function saveFile(UploadedFile $file)
    {
        $fileName = Self::createFilename($file);
        $disk = Storage::disk('s3');
        $disk->putFileAs('video', $file, $fileName);
        $mime = str_replace('/', '-', $file->getMimeType());
        unlink($file->getPathname());

        return response()->json([
            'path' => $disk->url("video/".$fileName),
            'name' => $fileName,
            'mime_type' =>$mime
        ]);

    }

    /**
     * Create unique filename for uploaded file
     * @param UploadedFile $file
     * @return string
     */
    public static function createFilename(UploadedFile $file)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = md5(time()). rand(10000,99999) . "." . $extension;
        return $filename;
    }

    public static function VttsaveFile(UploadedFile $file, $dir)
    {
        $fileName = Self::createFilename($file);
        $disk = Storage::disk('s3');
        $disk->putFileAs($dir, $file, $fileName);
        $mime = str_replace('/', '-', $file->getMimeType());
        unlink($file->getPathname());

        //return $disk->url("subtitles/".$fileName);
        return $dir."/".$fileName;

    }

    public static function random_number()
    {
        $number = rand(10000,99999);
        
        $check_number = User::where('reference_code','LIKE','%'.$number)->count();
        
        if($check_number>0){
            Self::random_number();
        }

        return $number;
        
    }

    public static function multi_language($group,$key=NULL)
    {
        $multilanguage = MultiLanguageKey::with(['multi_language_value']);
        if(!empty($group)){
            $multilanguage = $multilanguage->where('group',$group);
            $multilanguage = $multilanguage->get();
        }
        if(!empty($key)){
            $multilanguage = $multilanguage->where('language_key',$key);
            $multilanguage = $multilanguage->first();
        }
        
        return $multilanguage;
    }

    public static function total_likes_count_format($digit)
    {
        if ($digit >= 1000000000) {
            return round($digit/ 1000000000, 1). 'G';
        }
        if ($digit >= 1000000) {
            return round($digit/ 1000000, 1).'M';
        }
        if ($digit >= 1000) {
            return round($digit/ 1000, 1). 'K';
        }
        return (string)$digit;

    }

}
