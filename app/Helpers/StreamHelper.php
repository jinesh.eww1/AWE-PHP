<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
/*use FFMpeg;
use \FFMpeg\Format\Video\X264;
use Illuminate\Support\Facades\Storage;
use ProtoneMedia\LaravelFFMpeg\Exporters\HLSExporter;*/

use Aws\ElasticTranscoder\ElasticTranscoderClient;
use Aws\MediaConvert\MediaConvertClient;

class StreamHelper{

    public static function convert($video_name, $subTitles = array()){

        $hls_64k_audio_preset_id = '1351620000001-200071';
        $hls_0400k_preset_id     = '1351620000001-200050';
        $hls_0600k_preset_id     = '1351620000001-200040';
        $hls_1000k_preset_id     = '1351620000001-200030';
        $hls_1500k_preset_id     = '1351620000001-200020';
        $hls_2000k_preset_id     = '1351620000001-200010';

        $hls_presets = array(
          //'hlsAudio' => $hls_64k_audio_preset_id,
          'hls0400k' => $hls_0400k_preset_id,
          'hls0600k' => $hls_0600k_preset_id,
          'hls1000k' => $hls_1000k_preset_id,
          'hls1500k' => $hls_1500k_preset_id,
          'hls2000k' => $hls_2000k_preset_id,
        );

        $segment_duration = '2';
        $output_key_prefix = 'elastic-transcoder-samples/output/hls/';

        $credentials = array (
            'region' => 'us-west-1',
            'version' => 'latest',
            'default_caching_config' => '/tmp',
            'credentials' => [
                'key' => "AKIARVM6NJ7QNKOY2D6F",
                'secret' => "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"
            ]
        );

        $transcoder_client = ElasticTranscoderClient::factory($credentials);

        $pipeline_id = '1651235106655-rqdnqj';
        $input_key = $S3_file = 'video/'.$video_name;

        

        $CaptionSources = array();
        if(count($subTitles) > 0){
            foreach ($subTitles as $key => $value) {
                $sub_title = array(
                    'Key' => 'video/'.$value['file_name'],
                    'Language' => $value['language'],
                    'Label' => $value['Label'],
                );
                array_push($CaptionSources, $sub_title);
            }

            $input = array(
                'Key' => $input_key,
                'InputCaptions' => [
                        'MergePolicy' => 'MergeOverride',
                        'CaptionSources' => $CaptionSources
                ]
            );

        }else{
            $input = array(
                'Key' => $input_key
            );

        }

        //Setup the job outputs using the HLS presets.
        $output_key = hash('sha256', utf8_encode($input_key));

        // Specify the outputs based on the hls presets array specified.
        $outputs = array();

        $name =  strstr($video_name, '.', true);

        if(count($subTitles) > 0){

            $CaptionFormats = array();
            $captions = array(
                'Format' => 'webvtt',
                'Pattern' => 'webvtt/'.$name.'-{language}'
            );
            array_push($CaptionFormats, $captions);
            
        }


        

        foreach ($hls_presets as $prefix => $preset_id) {

            if(count($subTitles) > 0){
                array_push($outputs, array(
                        'Key' => "$prefix/$output_key", 
                        'PresetId' => $preset_id, 
                        'SegmentDuration' => $segment_duration,
                        'Captions' => [ "CaptionFormats" =>  $CaptionFormats]
                    )
                );
            }else{
                array_push($outputs, array(
                        'Key' => "$prefix/$output_key", 
                        'PresetId' => $preset_id, 
                        'SegmentDuration' => $segment_duration
                    )
                );
            }
            
        }

        // Setup master playlist which can be used to play using adaptive bitrate.
        $playlist = array(
            'Name' => 'hls_' . $output_key,
            'Format' => 'HLSv3',
            'OutputKeys' => array_map(function($x) { return $x['Key']; }, $outputs)
        );

        // Create the job.
        $create_job_request = array(
                'PipelineId' => $pipeline_id,
                'Input' => $input,
                'Outputs' => $outputs,
                'OutputKeyPrefix' => "$output_key_prefix$output_key/",
                'Playlists' => array($playlist)
        );

        try {
            $create_job_result = $transcoder_client->createJob($create_job_request)->toArray();

            $response['status'] = TRUE;
            $response['data'] = $create_job_result["Job"];

        } catch (AwsException $e) {
            // output error message if fails
           // echo $e->getMessage() . "\n";
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return $response;

    }

	public static function convert_prev_not_working($video_name, $subTitles = array()){

		$credentials = array (
            'region' => 'us-west-1',
            'version' => 'latest',
            'credentials' => [
            'key' => "AKIARVM6NJ7QNKOY2D6F",
            'secret' => "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"
        ]);
        $AWSClient = ElasticTranscoderClient::factory ($credentials);

        $hls_64k_audio_preset_id = '1351620000001-200071';
        $hls_0400k_preset_id     = '1351620000001-200050';
        $hls_0600k_preset_id     = '1351620000001-200040';
        $hls_1000k_preset_id     = '1351620000001-200030';
        $hls_1500k_preset_id     = '1351620000001-200020';
        $hls_2000k_preset_id     = '1351620000001-200010';

        $hls_presets = array(
          'hlsAudio' => $hls_64k_audio_preset_id,
          'hls0400k' => $hls_0400k_preset_id,
          'hls0600k' => $hls_0600k_preset_id,
          'hls1000k' => $hls_1000k_preset_id,
          'hls1500k' => $hls_1500k_preset_id,
          'hls2000k' => $hls_2000k_preset_id,
        );

        $segment_duration = '2';

        $output_key_prefix = 'elastic-transcoder-samples/output/hls/';
        $pipeline_id = '1651235106655-rqdnqj';
        $S3_file = 'video/'.$video_name;

        $outputs = [];
        
        $name =  strstr($video_name, '.', true);

        if(count($subTitles) > 0){
            $CaptionFormats = array();
            $captions = array(
                'Format' => 'webvtt',
                'Pattern' => 'webvtt/'.$name.'-{language}'
            );
            array_push($CaptionFormats, $captions);
        }
        
        foreach ($hls_presets as $prefix => $preset_id) {

            if(count($subTitles) > 0){
                $temp = array(
                    'Key' => $prefix . '/' . $name, 
                    'PresetId' => $preset_id, 
                    'SegmentDuration' => $segment_duration,
                    'Captions' => [ "CaptionFormats" =>  $CaptionFormats]
                );
            }else{
                $temp = array(
                    'Key' => $prefix . '/' . $name, 
                    'PresetId' => $preset_id, 
                    'SegmentDuration' => $segment_duration
                );
            }
            array_push( $outputs, $temp);
        }

        $playlist = [
            'Name' => 'hls_' . $name,
            'Format' => 'HLSv3',
            'OutputKeys' => array_map(function($x) { return $x['Key']; }, $outputs)
          ];


        $Input = array();
        $Input['Key'] = $S3_file;

        if(count($subTitles) > 0){
            $CaptionSources = array();

            foreach ($subTitles as $key => $value) {
                $sub_title = array(
                    'Key' => 'video/'.$value["file_name"],
                    'Language' => $value["language"],
                    'Label' => $value["Label"],
                );
                array_push($CaptionSources, $sub_title);
            }

            $Input['InputCaptions'] = array(
                'MergePolicy' => 'Override',
                'CaptionSources' => $CaptionSources
            );
        }


        try {
            $create_job_result = $AWSClient->createJob([
                'PipelineId' => $pipeline_id,
                'Input' => $Input,
                'Outputs' => $outputs,
                'OutputKeyPrefix' => $output_key_prefix,
                'Playlists' => [ $playlist ],
            ]);

            $response['status'] = TRUE;
            $response['data'] = $create_job_result["Job"];

        } catch (AwsException $e) {
            // output error message if fails
           // echo $e->getMessage() . "\n";
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return $response;
	}

	public static function convert_old(){

		try{

			/*$encryptionKey = HLSExporter::generateEncryptionKey();
			dd($encryptionKey);*/

			$lowBitrate = (new X264)->setKiloBitrate(250);
			$midBitrate = (new X264)->setKiloBitrate(500);
			$highBitrate = (new X264)->setKiloBitrate(1000);

			$data = FFMpeg::fromDisk('s3')
			    ->open('video/645219dc49d7079e184b9eb8c9d94213.mp4')
			    ->exportForHLS()
			    ->setSegmentLength(10) // optional
			    ->setKeyFrameInterval(48) // optional
			    ->addFormat($lowBitrate)
			    ->addFormat($midBitrate)
			    ->addFormat($highBitrate)
			    ->toDisk('s3')
			    ->save('m3u8/645219dc49d7079e184b9eb8c9d94213.m3u8');
			$response['status']  = TRUE;
			$response['data']  = $data;

		}catch (Exception $e){
            $response['message']  = $e->getMessage();
            $response['status']  = FALSE;

        }
        dd($response);
		

	}
}