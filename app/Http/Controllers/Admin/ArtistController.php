<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use App\Models\Artist;
use App\Models\CastTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\ContentCast;
use DataTables;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       if ($request->ajax())
       {
            $data = Artist::with(['castType','content_cast'])->get();
            return Datatables::of($data)
            ->editColumn('action', function ($row){
				$btn = '<a title="Edit" href="'.route('admin.artist.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
				$btn .= '<a title="View" href="'.route('admin.artist.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				$btn .= '<a title="Delete" href="'.route('admin.artist.destroy', $row['id']).'" data-url="artist" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
				return $btn;
			})
            ->editColumn('name', function ($row){
                return $row['name'];
            })
            ->addColumn('cast_name', function ($row) {
                return  $row->castType->name;
            })
            ->editColumn('total_content', function ($row){
                return count($row['content_cast']);
            })
			->rawColumns([ 'action', 'cast_name','total_content'])
            ->make(true);
       }
       else
       {
           $columns = [
            ['data' => 'id','name' => 'id','title' => "Id"], 
            ['data' => 'cast_name','name' => 'cast_name', 'title' => __("Cast Type")],
            ['data' => 'name','name' => 'name', 'title' => __("Name")],
            ['data' => 'total_content','name' => 'total_content', 'title' => __("Total Content")],
            ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
           $params['dateTableFields'] = $columns;
           $params['dateTableUrl'] = route('admin.artist.index');
           $params['dateTableTitle'] = "Artist Management";
           $params['dataTableId'] = time();
           $params['addUrl'] = route('admin.artist.create');
           return view('admin.pages.artist.index',$params);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
	{
        $castTypeRecord = CastTypes::select('id', 'name')->get();
        $params['castTypeRecord'] = $castTypeRecord;
		$params['pageTittle'] = "Add Artist" ;
		$params['backUrl'] = route('admin.artist.index');
		return view('admin.pages.artist.post',$params);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
	{	
		$request->validate([
            'cast_type_id' => 'required',
			'name_en' => ['required', 
            function ($attribute, $value, $fail) use ($request) {
                $str = '{"en":"'.$value.'%';
                    $artist = Artist::where('cast_type_id', $request->cast_type_id)->where('name','LIKE', $str)->first();
                    if($artist) {
                        $fail("Name english already exists.");
                    }
                 } ],
            'name_hi' => 'required',
            'name_fr' => 'required',
            'name_md' => 'required',
            'name_sp' => 'required'
		]);

		$category = Artist::create([
			'cast_type_id' => $request->cast_type_id,
			'name' => [
                'en' => $request->name_en,
                'hi' => $request->name_hi,
                'fr' => $request->name_fr,
                'md' => $request->name_md,
                'sp' => $request->name_sp
            ]
		]);

		// redirect
		return redirect()->route('admin.artist.index')->with('success','Artist created successfully.');
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
	{
		$params['pageTittle'] = "View Artist" ;
		$artistCastValue = Artist::where(['id' => $id])->with(['castType','content_cast.content','content_cast' => function($q){
                                $q->orderBy('content_id','desc');
                            }])->first();
		$params['backUrl'] = route('admin.artist.index');
		return view('admin.pages.artist.view',$params)->with('artistCastValue', $artistCastValue);
	}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
	{
		$params['pageTittle'] = "Edit Artist";
		$artistValue = Artist::where(['id' => $id])->with('castType')->first();
        $params['castTypeRecord'] = CastTypes::select('id', 'name')->get();
		$params['backUrl'] = route('admin.artist.index');
		return view('admin.pages.artist.put',$params)->with('artistValue', $artistValue);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
	{
		$request->validate([
            'cast_type_id' => 'required',
			'name_en' => ['required', 
            function ($attribute, $value, $fail) use ($request, $id) {
                $str = '{"en":"'.$value.'%';
                    $artist = Artist::where('cast_type_id', $request->cast_type_id)->where('name','LIKE', $str)->where('id', '!=', $id)->first();
                    if($artist) {
                        $fail("Name english already exists.");
                    }
                 } ],
            'name_hi' => 'required',
            'name_fr' => 'required',
            'name_md' => 'required',
            'name_sp' => 'required'
		]);
        
		$artistValue['cast_type_id'] = $request->cast_type_id;
		$artistValue['name'] = array(
                'en' => $request->name_en,
                'hi' => $request->name_hi,
                'fr' => $request->name_fr,
                'md' => $request->name_md,
                'sp' => $request->name_sp
            );

		Artist::whereId($id)->update($artistValue);
		return redirect()->route('admin.artist.index')->with('success','Artist updated successfully');
	}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
	{
        $model = Artist::find($id);
        $model->delete();

        ContentCast::where('artist_id',$id)->delete();

        return redirect()->route('admin.artist.index')->with('success','Artist deleted successfully');
	}
}
