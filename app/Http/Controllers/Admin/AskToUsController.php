<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Models\AskToUs;
use DataTables;
use Illuminate\Support\Facades\Auth;

class AskToUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            
       if ($request->ajax())
       {
            $data = AskToUs::with(['user'])->orderBy('id','desc')->get();

            return Datatables::of($data)
            ->editColumn('user_name', function ($row){
               if(!empty($row['user']['id'])){
                    return '<a href="'.route('admin.user.show', $row['user']['id']) . '" class="mr-2">'.$row['user']['first_name']." ".$row['user']['last_name'].'</a>';
               }else{
                return '';
               }
                
            })
            ->editColumn('question', function ($row){

                $out = strlen($row['question']) > 50 ? substr($row['question'],0,50)."..." : $row['question'];
                
                return $out; 
            })
            ->editColumn('action', function ($row){
                $btn ='<a title="View" href="'.route('admin.ask-to-us.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
                $btn .= '<a title="Delete" href="'.route('admin.ask-to-us.destroy', $row['id']).'" data-url="ask-to-us" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
                
                return $btn;
            })
            ->rawColumns(['action','id','user_name','question'])
            ->make(true);
       }
       else
       {
           $columns = [
               ['data' => 'id','name' => 'id','title' => "Id"],
               ['data' => 'user_name', 'name' => 'user_name','title' => __("User Name"),'searchable'=>true ], 
               ['data' => 'question', 'name' => 'question','title' => __("Question"),'searchable'=>true ], 
               ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
           $params['dateTableFields'] = $columns;
           $params['dateTableUrl'] = route('admin.ask-to-us.index');
           $params['dateTableTitle'] = "Ask to us";
           $params['dataTableId'] = time();
           return view('admin.pages.ask-to-us.index',$params);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function show($id)
    {
        $params['pageTittle'] = "View Ask to us";
        $params['asktous'] = AskToUs::with(['user'])->find($id);
        $params['backUrl'] = route('admin.ask-to-us.index');

        return view('admin.pages.ask-to-us.view',$params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        AskToUs::whereId($id)->delete();
        return redirect()->route('admin.ask-to-us.index')
                        ->with('success','Ask to us deleted successfully');
    }


}
