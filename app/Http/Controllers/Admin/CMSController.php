<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Issue;
use DataTables;
use App\Models\User;
use App\Models\SystemConfig;
use NotificationHelper;

class CMSController extends Controller
{        

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function terms_conditions(Request $request)
    {
        $type_val = request()->input('type');        
        if ($type_val == 'terms_conditions')
        {
            $update['value'] = request()->input('description');
            SystemConfig::where('path','terms_conditions')->update($update);
            return redirect()->route('admin.terms_conditions')->with('success', 'terms conditions updated successfully.');
        }

        $params['type'] = "Terms Conditions";
        $params['type_val'] = "terms_conditions";
        $params['pageTittle'] = "Terms Conditions";
        // $params['backUrl'] = route('admin.canteen.index');
        $params['breadcrumb_name'] = 'all';
        $params['data'] = SystemConfig::where('path','terms_conditions')->value('value');
        return view('admin.pages.cms.index', $params);
    }

    public function privacy_policy(Request $request)
    {
        $type_val = request()->input('type');
        if ($type_val == 'privacy_policy')
        {
            $update['value'] = request()->input('description');
            SystemConfig::where('path','privacy_policy')->update($update);    
            return redirect()->route('admin.privacy_policy')->with('success', 'privacy policy updated successfully.');
        }

        $params['type'] = "Privacy Policy";
        $params['type_val'] = "privacy_policy";
        $params['pageTittle'] = "Privacy Policy";        
        $params['breadcrumb_name'] = 'all';
        $params['data'] = SystemConfig::where('path','privacy_policy')->value('value');
        return view('admin.pages.cms.index', $params);
    }   


    public function loyalty_reward_terms_condition(Request $request)
    {
        $type_val = request()->input('type');        
        if ($type_val == 'loyalty_reward_terms_condition')
        {
            $update['value'] = request()->input('description');
            SystemConfig::where('path','loyalty_reward_terms_condition')->update($update);
            return redirect()->route('admin.loyalty_reward_terms_condition')->with('success', 'Loyalty reward terms conditions updated successfully.');
        }

        $params['type'] = "Loyalty Reward Terms Conditions";
        $params['type_val'] = "loyalty_reward_terms_condition";
        $params['pageTittle'] = "Loyalty Reward Terms Conditions";
        // $params['backUrl'] = route('admin.canteen.index');
        $params['breadcrumb_name'] = 'all';
        $params['data'] = SystemConfig::where('path','loyalty_reward_terms_condition')->value('value');
        return view('admin.pages.cms.index', $params);
    }

}
