<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Currency;
use DataTables;
use Illuminate\Support\Facades\Auth;

class CountryController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		

	   if ($request->ajax())
		{
			$data = Country::with(['currency'])->get();
			
			return Datatables::of($data)
			->addIndexColumn()
			->addColumn('currency', function ($row) {
                return  $row->currency->code;
            })
			->editColumn('action', function ($row){
				$btn = '<a title="Edit" href="'.route('admin.country.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
				$btn .= '<a title="View" href="'.route('admin.country.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				//$btn .= '<a title="Delete" href="'.route('admin.country.destroy', $row['id']).'" data-url="country" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
				return $btn;
			})
			->rawColumns(['stripe_supported','paypal_supported', 'action'])
			->make(true);
	   }
	   else
	   {
		   $columns = [
			   ['data' => 'id','name' => 'id','title' => "Id"], 
			   ['data' => 'country_name','name' => 'country_name', 'title' => "Country"],
			   //['data' => 'country_code','name' => 'country_code', 'title' => "Country Code"],
			   ['data' => 'currency','name' => 'currency', 'title' => "Currency"],
			   ['data' => 'dialing_code','name' => 'dialing_code', 'title' => "Dialing Code"],
			   ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
		   $params['dateTableFields'] = $columns;
		   $params['dateTableUrl'] = route('admin.country.index');
		   $params['dateTableTitle'] = "Country Management";
		   $params['dataTableId'] = time();
		   $params['addUrl'] = route('admin.country.create');
		   return view('admin.pages.country.index',$params);
	   }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$params['pageTittle'] = "Add Country" ;
        $params['currency'] = Currency::all();
		$params['backUrl'] = route('admin.country.index');

		return view('admin.pages.country.post',$params);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		
		$request->validate([
			'currency_id' => 'required',
			'dialing_code' => 'required',
			'country_name' => ['required', 
            function ($attribute, $value, $fail) use ($request) {
                    $country_name = Country::where('country_name','LIKE', $value)->first();
                    if($country_name) {
                        $fail("Country name already exists.");
                    }
                 } ]
		]);
		 
		$country = Country::create([
			'currency_id' => $request->currency_id,
			'dialing_code' => $request->dialing_code,
			'country_name' => $request->country_name
		]);

		// redirect
		return redirect()->route('admin.country.index')->with('success','Country created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{	
		$params['pageTittle'] = "View Country" ;
		$params['country'] = Country::with(['currency'])->find($id);
		$params['backUrl'] = route('admin.country.index');

		return view('admin.pages.country.view',$params);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$params['pageTittle'] = "Edit Country";
		$params['country'] = Country::find($id);
		$params['currency'] = Currency::all();
		$params['backUrl'] = route('admin.country.index');
		return view('admin.pages.country.put',$params);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	
		$request->validate([
			'currency_id' => 'required',
			'dialing_code' => 'required',
			'country_name' => ['required', 
            function ($attribute, $value, $fail) use ($request, $id) {
                    $country_name = Country::where('country_name','LIKE', $value)->where('id', '!=', $id)->first();
                    if($country_name) {
                        $fail("Country name already exists.");
                    }
                 } ]

		]);

		$country['currency_id'] = $request->currency_id;
		$country['dialing_code'] = $request->dialing_code;
		$country['country_name'] = $request->country_name;
		
		Country::whereId($id)->update($country);
	
		return redirect()->route('admin.country.index')
						->with('success','Country updated successfully');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
	
		Country::whereId($id)->delete();   

		$data['status'] = TRUE;        
		$data['message'] = 'Deleted';
        echo json_encode($data);
	}
}
