<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Models\Currency;
use DataTables;
use Illuminate\Support\Facades\Auth;

class CurrencyController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		

	   if ($request->ajax())
		{
			$data = Currency::all();
			
			return Datatables::of($data)
			->addIndexColumn()
			->editColumn('stripe_supported', function ($row){
				if($row['stripe_supported'] == 1){
					return '<i class="fa fa-check text-success" aria-hidden="true"></i>';
				}else{
					return '<i class="fa fa-times text-danger" aria-hidden="true"></i>';
				}
			})
			->editColumn('paypal_supported', function ($row){
				if($row['paypal_supported'] == 1){
					return '<i class="fa fa-check text-success" aria-hidden="true"></i>';
				}else{
					return '<i class="fa fa-times text-danger" aria-hidden="true"></i>';
				}
			})
			->editColumn('action', function ($row){
				$btn = '<a title="Edit" href="'.route('admin.currency.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
				$btn .= '<a title="View" href="'.route('admin.currency.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				//$btn .= '<a title="Delete" href="'.route('admin.currency.destroy', $row['id']).'" data-url="currency" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
				return $btn;
			})
			->rawColumns(['stripe_supported','paypal_supported', 'action'])
			->make(true);
	   }
	   else
	   {
		   $columns = [
			   ['data' => 'id','name' => 'id','title' => "Id"], 
			   ['data' => 'code','name' => 'code', 'title' => "Code"],
			   ['data' => 'stripe_supported','name' => 'stripe_supported', 'title' => "Stripe"],
			   ['data' => 'paypal_supported','name' => 'paypal_supported', 'title' => "Paypal"],
			   ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
		   $params['dateTableFields'] = $columns;
		   $params['dateTableUrl'] = route('admin.currency.index');
		   $params['dateTableTitle'] = "Currency Management";
		   $params['dataTableId'] = time();
		   $params['addUrl'] = route('admin.currency.create');
		   return view('admin.pages.currency.index',$params);
	   }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$params['pageTittle'] = "Add Currency" ;
		$params['backUrl'] = route('admin.currency.index');

		return view('admin.pages.currency.post',$params);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		
		$request->validate([
			'code' => 'required'
		]);
		 
		$currency = Currency::create([
			'code' => $request->code
		]);

		// redirect
		return redirect()->route('admin.currency.index')->with('success','Currency created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{	
		$params['pageTittle'] = "View Currency" ;
		$params['currency'] = Currency::find($id);

		$params['backUrl'] = route('admin.currency.index');

		return view('admin.pages.currency.view',$params);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$params['pageTittle'] = "Edit Currency";
		$params['currency'] = Currency::find($id);
		$params['backUrl'] = route('admin.currency.index');
		return view('admin.pages.currency.put',$params);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	
		$request->validate([
			'code' => 'required'
		]);

		$currency['code'] = $request->code;
		
		Currency::whereId($id)->update($currency);
	
		return redirect()->route('admin.currency.index')
						->with('success','Currency updated successfully');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
	
			$result = Currency::with(['content_currencys'])->whereId($id)->first();

			if($result){
				if(count($result['content_currencys'])==0){
					Currency::whereId($id)->delete();            
					$data['status'] = TRUE;        
					$data['message'] = 'Deleted';
				}else{
					$data['status'] = FALSE;        
					$data['message'] = 'You cannot delete '.$result["name"].' currency as '.count($result['content_currencys']).' content(s) already assigned to this currency';
				}
			}else{
				$data['status'] = FALSE;        
				$data['message'] = 'Currency not found. Please refresh page.';
			}
		
        echo json_encode($data);
	}

	public function active_deactive_currency()
	{
		if($_POST['table'] == 'currency'){
			if($_POST['status'] == 0){
				$status = 1;
			}else{
				$status = 0;
			}
			Currency::where('id', $_POST['id'])->update(['status' => $status]);
		}
		echo $status;
	}
}
