<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;
use App\Models\Content;
use App\Models\Video;
use App\Models\Plan;
use App\Models\Chat;
use App\Models\ActivityLog;
use App\Models\PaymentHistory;
use CommonHelper;
use PaymentHelper;
use DB;

class DashboardController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $login_id =  Auth::user()->id;
        $params['users'] = User::select('users.*')->join('users_group', 'users.id', '=', 'users_group.user_id')->where('users_group.group_id',4)->where('parent_id', 0)->count();

        $main_user = User::select('users.id','users.parent_id','users.plan_id')->join('users_group', 'users.id', '=', 'users_group.user_id')->where('users.parent_id',0)->where('users.status',1)->where('users_group.group_id',4)->get();

        $sub_user = $non_sub_user = $total_sub = $total_non_sub = 0;
        if(!empty($main_user) && count($main_user)>0){
            $total_main_user = count($main_user);
            foreach ($main_user as $key => $value) {
                if(!empty($value->plan_id) && $value->plan_id != NULL){
                    $sub_user = $sub_user+1;
                }else{
                    $non_sub_user = $non_sub_user+1;
                }
            }
            
            if($sub_user != 0){
                $total_sub = round(($sub_user*100)/$total_main_user);
            }

            if($non_sub_user != 0){
                $total_non_sub = round(($non_sub_user*100)/$total_main_user);
            }
        }
        
        $params['total_sub_count'] = $sub_user;
        $params['total_non_sub_count'] = $non_sub_user;
        $params['total_sub'] = $total_sub;
        $params['total_non_sub'] = $total_non_sub;


        $all_device = User::select('devices.*')->join('users_group', 'users.id', '=', 'users_group.user_id')->join('devices', 'users.id', '=', 'devices.user_id')->where('users_group.group_id', '=', '4')->get();

        $android_device = $ios_device = $total_android_device = $total_ios_device = 0;
        if(!empty($all_device) && count($all_device)>0){
            $all_device_count = count($all_device);
            foreach ($all_device as $key => $value) {
                if(strtolower($value->type) == strtolower('android')){
                    $android_device = $android_device+1;
                }elseif (strtolower($value->type) == strtolower('ios')) {
                    $ios_device = $ios_device+1;
                }
            }

            if($android_device != 0){
                $total_android_device = round(($android_device*100)/$all_device_count);
            }

            if($ios_device != 0){
                $total_ios_device = round(($ios_device*100)/$all_device_count);
            }

        }
        
        $params['total_android_device'] = $total_android_device;
        $params['total_ios_device'] = $total_ios_device;


        $params['content'] = Content::count();
        $params['video'] = Video::count();
        $params['plan'] = Plan::where('status',1)->count();

        $params['most_viewed'] = ActivityLog::select('*',DB::raw("count('content_id') as 'total_content'"))->with(['content'])->groupBy('content_id')->orderBy('total_content','desc')
            ->whereHas('content', function($q) use($request) {
                    $q->where('content.id', '!=', NULL);
                })->limit(10)->get();
        
        $from = date('Y-m-d H:i:s');
        $to = date('Y-m-d H:i:s',strtotime('-1 Year'));

        $params['lineChart'] = User::select(DB::raw("COUNT(*) as count"),DB::raw('CONCAT(MONTHNAME(users.created_at)," ",YEAR(users.created_at)) as monthyear'),DB::raw('max(users.created_at) as createdAt'))->where('users.parent_id',0)->where('users.status',1)
        ->whereDate('users.created_at', "<=",$from)
        ->whereDate('users.created_at', ">=",$to)
        ->groupBy('monthyear')
        ->orderBy('createdAt')
        ->get()->toArray();

        for ($i = 0; $i < 12; $i++) {
            $months[] = date("F Y", strtotime( date( 'Y-m-01' )." -$i months"));
        }

        
        $months = array_reverse($months);
        $templineChart = array(); $i = $j = 0;

        foreach ($months as $key => $value) {
            if(!empty($params['lineChart']) && count($params['lineChart'])>0){
                foreach ($params['lineChart'] as $lkey => $lvalue) {

                    if(in_array($lvalue['monthyear'],$months) && $lvalue['monthyear'] == $value){
                        $templineChart[$key]['count'] = $lvalue['count'];
                        $templineChart[$key]['month_name'] = $lvalue['monthyear'];
                        break;
                    }else{
                        $templineChart[$key]['count'] = 0;
                        $templineChart[$key]['month_name'] = $value;
                    }
                } 
            }
        }

        $params['lineChart']  = $templineChart;
        
        $params['pie_chart'] = array(
            'Android' => $total_android_device,
            'iOS' => $total_ios_device, );

        $params['earning'] = PaymentHistory::sum('amount');

        $payment = PaymentHistory::where('transaction_id', '!=', '')->get();

        return view('admin.pages.dashboard',$params);
    }

    public function s3bucket() {

        return view('admin.pages.s3bucket');
    }

    public function support_chat() {
        
        $params['recent_chat'] = Chat::with(['sender_user','receiver_user'])->join(DB::raw(
                '(
                SELECT
                    LEAST(sender_id, receiver_id) AS sender_id,
                    GREATEST(sender_id, receiver_id) AS receiver_id,
                    MAX(id) AS max_id
                FROM chat
                GROUP BY
                    LEAST(sender_id, receiver_id),
                    GREATEST(sender_id, receiver_id)
            ) AS m2'
            ), fn($join) => $join
                ->on(DB::raw('LEAST(chat.sender_id, chat.receiver_id)'), '=', 'm2.sender_id')
                ->on(DB::raw('GREATEST(chat.sender_id, chat.receiver_id)'), '=', 'm2.receiver_id')
                ->on('chat.id', '=', 'm2.max_id'))
            ->where('chat.sender_id', Auth::user()->id)
            ->orWhere('chat.receiver_id', Auth::user()->id)
            ->orderByDesc('chat.id')->get();

        if(isset($_GET['user'])){

            if(!empty($params['recent_chat']) && count($params['recent_chat'])>0){
                foreach ($params['recent_chat'] as $key => $value) {
                    if($value->sender_id == $_GET['user'] && $value->receiver_id == '1'){
                        $user_id = $value->sender_id;
                    }else if($value->receiver_id == $_GET['user'] && $value->sender_id == '1'){
                        $user_id = $value->receiver_id;
                    }
                }
                $params['chat_history'] = Chat::with(['sender_user'])->where('sender_id',$user_id)->orWhere('receiver_id',$user_id)->get();
            }

        }else if(!empty($params['recent_chat']) && count($params['recent_chat'])>0){

            if($params['recent_chat'][0]->sender_id != 1){
                $user_id = $params['recent_chat'][0]->sender_id;
            }else{
                $user_id = $params['recent_chat'][0]->receiver_id;
            }
            
            $params['chat_history'] = Chat::with(['sender_user'])->where('sender_id',$user_id)->orWhere('receiver_id',$user_id)->get();
            
        }
        return view('admin.pages.support_chat',$params);
    }

    public function get_chat_history_data(Request $request)
    {   
        if(!empty($request->id)){
        
        $data = Chat::with(['sender_user'])->where('sender_id',$request->id)->orWhere('receiver_id',$request->id)->get();
            return response()->json(['status'=>true,'data' => $data,'message'=>'chat history data']);
        }else{
            return response()->json(['status'=>false,'data' => array(),'message'=>'No data found']);
        }
    }

    public function reply_chat(Request $request)
    {   
        if(!empty($request->user_id) && !empty($request->message_val)){
        
            Chat::create([
                'sender_id'=>Auth::user()->id,
                'receiver_id'=>$request->user_id,
                'message'=>$request->message_val
            ]);

            return response()->json(['status'=>true]);
        }else{
            return response()->json(['status'=>false]);
        }
    }

}