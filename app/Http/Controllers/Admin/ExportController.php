<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ArtistsExport;
use App\Exports\AgeRestrictionExport;
use App\Exports\CastingExport;
use App\Exports\ContentsExport;
use App\Exports\FaqsExport;
use App\Exports\FeedbacksExport;
use App\Exports\GenersExport;
use App\Exports\MultiLanguageManagementExport;
use App\Exports\PaymentHistoryExport;
use App\Exports\PlanFeaturesExport;
use App\Exports\PlanManagmentExport;
use App\Exports\SeasonsExport;
use App\Exports\ContenttagsExport;
use App\Exports\UsersExport;
use App\Exports\VideosExport;



class ExportController extends Controller
{
    /***
     * exports the tags
     */
    public function contenttags($type)
    {
        $makeLink = 'tags-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new ContenttagsExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new ContenttagsExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new ContenttagsExport, $makeLink . '.pdf');
        }
    }

    /***
     * exports the users
     */
    public function users($type)
    {
        $makeLink = 'users-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new UsersExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new UsersExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new UsersExport, $makeLink . '.pdf');
        }
    }

    /***
     * exports the genres
     */
    public function genres($type)
    {
        $makeLink = 'genres-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new GenersExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new GenersExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new GenersExport, $makeLink . '.pdf');
        }
    }

    /***
     * exports the casts
     */
    public function casting($type)
    {
        $makeLink = 'cast-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new CastingExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new CastingExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new CastingExport, $makeLink . '.pdf');
        }
    }

    /***
     * exports the artist
     */
    public function artists($type)
    {
        $makeLink = 'artist-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new ArtistsExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new ArtistsExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new ArtistsExport, $makeLink . '.pdf');
        }
    }

    /***
     * exports the age restriction
     */
    public function age($type)
    {
        $makeLink = 'agerestriction-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new AgeRestrictionExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new AgeRestrictionExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new AgeRestrictionExport, $makeLink . '.pdf');
        }
    }

    /***
     * exports the plan managment
     */
    public function plan_managment($type)
    {
        $makeLink = 'plan_managment-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new PlanManagmentExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new PlanManagmentExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new PlanManagmentExport, $makeLink . '.pdf');
        }
    }

    /***
     * exports the plan features
     */
    public function plan_features($type)
    {
        $makeLink = 'plan_features-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new PlanFeaturesExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new PlanFeaturesExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new PlanFeaturesExport, $makeLink . '.pdf');
        }
    }

    /***
     * exports the payment history
     */
    public function payment_records($type)
    {
        $makeLink = 'payment_history-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new PaymentHistoryExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new PaymentHistoryExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new PaymentHistoryExport, $makeLink . '.pdf');
        }
    }

    /***
     * exports the contents
     */
    public function contents($type)
    {
        $makeLink = 'contents-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new ContentsExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new ContentsExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new ContentsExport, $makeLink . '.pdf');
        }
    }

    /***
     * exports the seasons
     */
    public function seasons($type)
    {
        $makeLink = 'seasons-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new SeasonsExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new SeasonsExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new SeasonsExport, $makeLink . '.pdf');
        }
    }

    /***
     * exports the videos
     */
    public function videos($type)
    {
        $makeLink = 'videos-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new VideosExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new VideosExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new VideosExport, $makeLink . '.pdf');
        }
    }

    /***
     * exports the faqs
     */
    public function faqs($type)
    {
        $makeLink = 'faqs-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new FaqsExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new FaqsExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new FaqsExport, $makeLink . '.pdf');
        }
    }

    /***
     * exports the feedbacks
     */
    public function feedbacks($type)
    {
        $makeLink = 'feedbacks-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new FeedbacksExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new FeedbacksExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new FeedbacksExport, $makeLink . '.pdf');
        }
    }


    /***
     * exports the multi language management
     */
    public function multi_language_management($type)
    {
        $makeLink = 'multi_language_management-' . Carbon::now()->toDateTimeString();
        if ($type == "excel") {
            return Excel::download(new MultiLanguageManagementExport, $makeLink . '.xlsx');
        } else if ($type == "csv") {
            return Excel::download(new MultiLanguageManagementExport, $makeLink . '.csv');
        } else if ($type == "pdf") {
            return Excel::download(new MultiLanguageManagementExport, $makeLink . '.pdf');
        }
    }
    
}
