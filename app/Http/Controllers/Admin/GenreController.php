<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Models\Genre;
use App\Models\Content;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\Auth;

class GenreController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		

	   if ($request->ajax())
		{
			$data = Genre::with(['content_genres'])->get();
			
			return Datatables::of($data)
			->addIndexColumn()
			->editColumn('action', function ($row){
				$btn = '<a title="Edit" href="'.route('admin.genre.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
				$btn .= '<a title="View" href="'.route('admin.genre.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				$btn .= '<a title="Delete" href="'.route('admin.genre.destroy', $row['id']).'" data-url="genre" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
				return $btn;
			})
			->editColumn('name', function ($row){
				return $row['name'];
			})
			->editColumn('total_content', function ($row){
				return count($row['content_genres']);
			})
			->rawColumns(['name','total_content', 'action'])
			->make(true);
	   }
	   else
	   {
		   $columns = [
			   ['data' => 'id','name' => 'id','title' => "Id"], 
			   ['data' => 'name','name' => 'name', 'title' => __("Name")],
			   ['data' => 'total_content','name' => 'total_content', 'title' => "Total Associated Content"],
			   ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
		   $params['dateTableFields'] = $columns;
		   $params['dateTableUrl'] = route('admin.genre.index');
		   $params['dateTableTitle'] = "Genre Management";
		   $params['dataTableId'] = time();
		   $params['addUrl'] = route('admin.genre.create');
		   return view('admin.pages.genre.index',$params);
	   }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$params['pageTittle'] = "Add Genre" ;
		$params['content'] = Content::select('id','name')->where('status','!=',2)->get();
		$params['backUrl'] = route('admin.genre.index');

		return view('admin.pages.genre.post',$params);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		
		$request->validate([
			'name_en' => ['required', 
            function ($attribute, $value, $fail) {
                $str = '{"en":"'.$value.'%';
                    $category = Genre::where('name','LIKE', $str)->first();
                    if($category) {
                        $fail("Name english already exists.");
                    }
                 } ],
			'name_hi' => 'required',
			'name_fr' => 'required',
			'name_md' => 'required',
			'name_sp' => 'required'
		]);
		 
		$genre = Genre::create([
			'name' => [
					'en' => $request->name_en,
	      			'hi' => $request->name_hi,
	      			'fr' => $request->name_fr,
	      			'md' => $request->name_md,
	      			'sp' => $request->name_sp
				]
		]);

		// redirect
		return redirect()->route('admin.genre.index')->with('success','Genre created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{	
		$params['pageTittle'] = "View Genre" ;
		$params['genre'] = Genre::with(['content_genres.content','content_genres' => function($q){
							    $q->orderBy('content_id','desc');
							}])->find($id);

		$params['backUrl'] = route('admin.genre.index');

		return view('admin.pages.genre.view',$params);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$params['pageTittle'] = "Edit Genre";
		$params['genre'] = Genre::find($id);
		$params['content'] = Content::select('id','name')->where('status','!=',2)->get();
		$params['backUrl'] = route('admin.genre.index');
		return view('admin.pages.genre.put',$params);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	
		$request->validate([
			'name_en' => ['required', 
            function ($attribute, $value, $fail) use ($id) {
                $str = '{"en":"'.$value.'%';
                    $CastTypes = Genre::where('name','LIKE', $str)->where('id', '!=', $id)->first();
                    if($CastTypes) {
                        $fail("Name english already exists.");
                    }
                 } ],
			'name_hi' => 'required',
			'name_fr' => 'required',
			'name_md' => 'required',
			'name_sp' => 'required'
		]);

		$genre['name'] =array(
				'en' => $request->name_en,
      			'hi' => $request->name_hi,
      			'fr' => $request->name_fr,
      			'md' => $request->name_md,
      			'sp' => $request->name_sp
			);
		
		Genre::whereId($id)->update($genre);
	
		return redirect()->route('admin.genre.index')
						->with('success','Genre updated successfully');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
	
			$result = Genre::with(['content_genres'])->whereId($id)->first();

			if($result){
				if(count($result['content_genres'])==0){
					Genre::whereId($id)->delete();            
					$data['status'] = TRUE;        
					$data['message'] = 'Deleted';
				}else{
					$data['status'] = FALSE;        
					$data['message'] = 'You cannot delete '.$result["name"].' genre as '.count($result['content_genres']).' content(s) already assigned to this genre';
				}
			}else{
				$data['status'] = FALSE;        
				$data['message'] = 'Genre not found. Please refresh page.';
			}
		
        echo json_encode($data);
	}

	public function active_deactive_genre()
	{
		if($_POST['table'] == 'genre'){
			if($_POST['status'] == 0){
				$status = 1;
			}else{
				$status = 0;
			}
			Genre::where('id', $_POST['id'])->update(['status' => $status]);
		}
		echo $status;
	}
}
