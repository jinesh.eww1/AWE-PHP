<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\MultiLanguageKey;
use App\Models\MultiLanguageValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;
use App\Helpers\CommonHelper;

class MultiLanguageKeyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {  
       if ($request->ajax())
       {    
            $data = MultiLanguageKey::with(['multi_language_value'])->get();
       
            return Datatables::of($data)
            ->editColumn('action', function ($row){
				$btn = '<a title="Edit" href="'.route('admin.multilanguage.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
				$btn .= '<a title="View" href="'.route('admin.multilanguage.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				$btn .= '<a title="Delete" href="'.route('admin.multilanguage.destroy', $row['id']).'" data-url="multilanguage" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
				return $btn;
			})
            ->editColumn('name', function ($row){
                return $row['multi_language_value']['language_value'];
            })
			->rawColumns(['action'])
            ->make(true);
       }
       else
       {
           $columns = [
            ['data' => 'id','name' => 'id','title' => "Id"], 
            ['data' => 'language_key','name' => 'language_key', 'title' => __("Key")],
            ['data' => 'group','name' => 'group', 'title' => __("Group")],
            ['data' => 'name','name' => 'name', 'title' => __("Name")],
            ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
           $params['dateTableFields'] = $columns;
           $params['dateTableUrl'] = route('admin.multilanguage.index');
           $params['dateTableTitle'] = "Multi Language Management";
           $params['dataTableId'] = time();
           $params['addUrl'] = route('admin.multilanguage.create');
           return view('admin.pages.multi_language_key.index',$params);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
	{
		$params['pageTittle'] = "Add Multi Language Data" ;
		$params['backUrl'] = route('admin.multilanguage.index');
        $params['group'] = MultiLanguageKey::GroupBy('group')->get()->pluck('group')->toArray();
		return view('admin.pages.multi_language_key.post',$params);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
	{	
		$request->validate([
			'language_key' => 'required|unique:multi_language_key',
            'language_value_en' => 'required',
		]);

        $insert_id = MultiLanguageKey::insertGetId([
            'language_key'=>$request->language_key,
            'group'=>!empty($request->group)?$request->group:'',
        ]);

        if($insert_id){
            $language_value = [
                    'en' => $request->language_value_en,
                    'hi' => $request->language_value_hi,
                    'fr' => $request->language_value_fr,
                    'md' => $request->language_value_md,
                    'sp' => $request->language_value_sp,
                ];

            MultiLanguageValue::create([
                'multi_language_key_id'=>$insert_id,
                'language_value'=>$language_value,
            ]);

        }

        return redirect()->route('admin.multilanguage.index')->with('success','Multi Language Data created successfully.');
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
	{
        $params['pageTittle'] = "View Multi Language Data" ;
		$params['multilanguage'] = MultiLanguageKey::with(['multi_language_value'])->find($id);
		$params['backUrl'] = route('admin.multilanguage.index');
		return view('admin.pages.multi_language_key.view',$params);
	}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
	{
		$params['pageTittle'] = "Edit Multi Language Data";
		$params['multilanguage'] = MultiLanguageKey::with(['multi_language_value'])->find($id);
		$params['backUrl'] = route('admin.multilanguage.index');
        $params['group'] = MultiLanguageKey::GroupBy('group')->get()->pluck('group')->toArray();
		return view('admin.pages.multi_language_key.put',$params);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
	{
		$request->validate([
            'language_key' => 'required|unique:multi_language_key,language_key,'. $id .'',
            'language_value_en' => 'required'
		]);

		MultiLanguageKey::where('id',$id)->update([
            'language_key'=>$request->language_key,
            'group'=>!empty($request->group)?$request->group:'',
        ]);

        $language_value = [
                    'en' => $request->language_value_en,
                    'hi' => $request->language_value_hi,
                    'fr' => $request->language_value_fr,
                    'md' => $request->language_value_md,
                    'sp' => $request->language_value_sp,
                ];

        MultiLanguageValue::where('multi_language_key_id',$id)->update([
            'language_value'=>$language_value,
        ]);

		return redirect()->route('admin.multilanguage.index')->with('success','Multi Language Data updated successfully');
	}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
	{	
        MultiLanguageKey::whereId($id)->delete();
        return redirect()->route('admin.multilanguage.index')->with('success','Multi Language data deleted successfully');
	}
}
