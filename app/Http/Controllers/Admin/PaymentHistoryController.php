<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use App\Models\PaymentHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Carbon\Carbon;

class PaymentHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

       if ($request->ajax())
       {
            $data = PaymentHistory::with(['users','plan'])->get();
            return Datatables::of($data)
            ->editColumn('action', function ($row){
				$btn = '<a title="View" href="'.route('admin.payment_history.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				return $btn;
			})
            ->editColumn('name', function ($row){
                if($row['users']){
                    $first_name = !empty($row['users']['first_name'])?$row['users']['first_name']:'';
                    $last_name = !empty($row['users']['last_name'])?$row['users']['last_name']:'';
                    return '<a target="_blank" href="'.route('admin.user.show', $row['users']['id']).'" class="mr-2">'.$first_name.' '.$last_name.'</a>';
                }else{
                    return "-";
                }
                
            })
            ->editColumn('mode', function ($row){
                return ucfirst($row['mode']);
            })
            ->editColumn('plan_name', function ($row){
                $plan_name = '<a target="_blank" href="'.route('admin.plan.show',$row['plan']['id']).'" class="mr-2">'.ucfirst($row['plan']['name']).'</a>';
                return $plan_name;
            })
            ->editColumn('date', function ($row){                    
                return Carbon::parse($row['created_at'])->format('d F Y h:i A');
            })
            ->editColumn('type', function ($row){
                if($row['type'] == 1){
                    return "Monthly";
                }elseif($row['type'] == 2){
                    return "Yearly";
                }else{
                    return '';
                }
            })
			->rawColumns(['name','mode','action', 'plan_name'])
            ->make(true);
       }
       else
       {
           $columns = [
            ['data' => 'id','name' => 'id','title' => "Id"], 
            ['data' => 'name','name' => 'name', 'title' => "User"],
            ['data' => 'mode','name' => 'mode','title' => "Mode"], 
            ['data' => 'plan_name','name' => 'plan_name','title' => "Plan"], 
            ['data' => 'type','name' => 'type','title' => "Type"], 
            ['data' => 'date','name' => 'date','title' => "Transaction DateTime"], 
            ['data' => 'action','name' => 'action', 'title' => "View",'searchable'=>false,'orderable'=>false]];
           $params['dateTableFields'] = $columns;
           $params['dateTableUrl'] = route('admin.payment_history.index');
           $params['dateTableTitle'] = "Payment History";
           $params['dataTableId'] = time();
           return view('admin.pages.payment_history.index',$params);
       }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
	{
		$params['pageTittle'] = "View Payment History" ;
		$params['payment_history'] = PaymentHistory::with(['users','plan'])->find($id);
		$params['backUrl'] = route('admin.payment_history.index');
		return view('admin.pages.payment_history.view',$params);
	}

}
