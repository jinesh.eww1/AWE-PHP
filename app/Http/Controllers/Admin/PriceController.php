<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Models\Plan;
use App\Models\Country;
use App\Models\Price;
use DataTables;
use Illuminate\Support\Facades\Auth;

use Srmklive\PayPal\Services\PayPal as PayPalClient;
use App\Traits\StripeTraits;

class PriceController extends Controller
{
	use StripeTraits;
	public function __construct()
    {

		$this->provider = new PayPalClient;
		$this->provider = \PayPal::setProvider();
		$config = \Config::get('paypal');
		$this->provider->setApiCredentials($config);
		$this->provider->getAccessToken();
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		

	   if ($request->ajax())
		{
			$data = Price::with(['plan', 'country'])->get();
			
			return Datatables::of($data)
			->addIndexColumn()
			->addColumn('plan_id', function ($row) {
                return  $row->plan->name;
            })
            ->addColumn('country_id', function ($row) {
                return  $row->country->country_name."|".$row->country->currency->code;
            })
            ->addColumn('monthly_price', function ($row) {
                return  $row->country->currency->symbol." ".$row->monthly_price;
            })
            ->addColumn('yearly_price', function ($row) {
                return  $row->country->currency->symbol." ".$row->yearly_price;
            })
			->editColumn('action', function ($row){
				//$btn = '<a title="Edit" href="'.route('admin.price.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
				$btn = '<a title="View" href="'.route('admin.price.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				//$btn .= '<a title="Delete" href="'.route('admin.price.destroy', $row['id']).'" data-url="country" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
				return $btn;
			})
			->rawColumns(['stripe_supported','paypal_supported', 'action'])
			->make(true);
	   }
	   else
	   {
		   $columns = [
			   ['data' => 'id','name' => 'id','title' => "Id"], 
			   ['data' => 'plan_id','name' => 'plan_id', 'title' => "plan"],
			   ['data' => 'country_id','name' => 'country_id', 'title' => "Country"],
			   ['data' => 'monthly_price','name' => 'monthly_price', 'title' => "Monthly Price"],
			   ['data' => 'yearly_price','name' => 'yearly_price', 'title' => "Yearly Price"],
			   ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
		   $params['dateTableFields'] = $columns;
		   $params['dateTableUrl'] = route('admin.price.index');
		   $params['dateTableTitle'] = "Price Management";
		   $params['dataTableId'] = time();
		   $params['addUrl'] = route('admin.price.create');
		   return view('admin.pages.price.index',$params);
	   }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{

		/*$frequency = 'year'; //year
		$result = $this->createPlan('prod_LH7CuBkpmjN9Jb', $frequency, 'USD', 90);
		dd($result);*/

		/*$plan = $this->provider->showPlanDetails("P-1UK812774H700863VMMVM2UI");
		dd($plan);*/

		/*$product_name = $plan_name = "BasicMonthlyUS44";
		$price = 2;
		$duration = 30;
		$response = $this->provider->addProduct($product_name, $product_name, 'SERVICE', 'SOFTWARE')->addCustomPlan($plan_name, $plan_name, $price, 'DAY', $duration);
		dd($response);*/

		$params['pageTittle'] = "Add Price";
		$params['country'] = Country::all();
		$params['plan'] = Plan::all();

		//dd($params);
		$params['backUrl'] = route('admin.price.index');

		return view('admin.pages.price.post',$params);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		
		$request->validate([
			'plan_id' => 'required',
			'monthly_price' => 'required',
			'yearly_price' => ['required', 
				function ($attribute, $value, $fail) use ($request) {
                    if($value < $request->monthly_price) {
                        $fail("Yearly price should be greater than monthly price.");
                    }
                 }
				],
			'country_id' => ['required', 
            function ($attribute, $value, $fail) use ($request) {
                    $exist = Price::where('plan_id', $request->plan_id)->where('country_id', $request->country_id)->first();
                    if($exist) {
                        $fail("Price already exists for this country.");
                    }
                 } ]
		]);

		$plan = Plan::find($request->plan_id);
		$country = Country::find($request->country_id);

		//Stripe monthly plan create
		$default_monthly_price = $plan->default_monthly_price;
		$default_yearly_price = $plan->default_yearly_price;
		$currency_code = $country->currency->code;

		$monthly = $this->createPlan($plan->stripe_product_id, 'month', $currency_code, $request->monthly_price, $country->country_name);
		if($monthly['status'] == TRUE){
			$stripe_monthly_id = $monthly['response']->id;
			$yearly = $this->createPlan($plan->stripe_product_id, 'year', $currency_code, $request->yearly_price, $country->country_name);
			if($yearly['status'] == TRUE){
				$stripe_yearly_id = $yearly['response']->id;

				$Price = Price::create([
					'plan_id' => $request->plan_id,
					'country_id' => $request->country_id,
					'monthly_price' => $request->monthly_price,
					'yearly_price' => $request->yearly_price,
					'stripe_monthly_id' => $stripe_monthly_id,
					'stripe_yearly_id' => $stripe_yearly_id
				]);

				return redirect()->route('admin.price.index')->with('success','Price created successfully.');

			}else{
				return redirect()->route('admin.price.index')->with('error',$yearly['message']);
			}
		}else{
			return redirect()->route('admin.price.index')->with('error',$monthly['message']);
		}
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{	
		$params['pageTittle'] = "View Price" ;
		$params['price'] = Price::with(['plan', 'country'])->find($id);
		$params['backUrl'] = route('admin.price.index');

		return view('admin.pages.price.view',$params);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$params['pageTittle'] = "Edit Country";
		$params['price'] = Price::with(['country', 'plan'])->find($id);

		$params['country'] = Country::find($params['price']->country_id);

		$params['backUrl'] = route('admin.price.index');
		return view('admin.pages.price.put',$params);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	
		$request->validate([
			'monthly_price' => 'required',
			'yearly_price' => ['required', 
				function ($attribute, $value, $fail) use ($request) {
                    if($value < $request->monthly_price) {
                        $fail("Yearly price should be greater than monthly price.");
                    }
                 }
				],

		]);

		$price = Price::find($id);

		$currency_code = $price->country->currency->code;

		$monthly = $this->updatePrice($price->stripe_monthly_id, $currency_code, $request->monthly_price);
		dd($monthly);
		
		Country::whereId($id)->update($country);
	
		return redirect()->route('admin.price.index')
						->with('success','Price updated successfully');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
	
		Country::whereId($id)->delete();   

		$data['status'] = TRUE;        
		$data['message'] = 'Deleted';
        echo json_encode($data);
	}
}
