<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Issue;
use DataTables;
use App\Models\OrdersDate;
use App\Models\User;
use App\Models\Devices;
use App\Mail\SendMail;
use App\Models\School;
use App\Models\Notification;
use Mail;
use NotificationHelper;

class SendMailController extends Controller
{        

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  
    public function customer(Request $request)
    {
        // $request->validate([
        //     'title' => 'required',
        //     'description' => 'required',
        //     'user_ids'=>'required'            
        // ]);
        

        $title=request()->input('title');
        $description=request()->input('description');
        $user_ids = request()->input('user_ids');
        $user_type = request()->input('user_type');

        if (isset($user_type) &&  isset($title))
        {
            $title = request()->input('title');
            $description = request()->input('description');        
            if ($user_type == 'select') {
                $data = User::whereIn('id', $user_ids)->groupBy('email')->get();
            } else
            {
                $data = User::select('users.*', 'users_group.group_id')->join('users_group', 'users.id', '=', 'users_group.user_id')->where('users_group.group_id', '=', '4')->groupBy('email')->get();
            }
            
            if(!empty($data)){
                $emails = $data->pluck('email');
                Mail::to($emails)->send(new SendMail($title,$description, config('app.address1'), config('app.address2')));
            }

            return redirect()->route('admin.mail.customer')->with('success', 'Email sent successfully.');                    
        }

        $params['type'] = "Customer";
        $params['pageTittle'] = "Send Mail to Customer";        
        // $params['backUrl'] = route('admin.canteen.index');
        $params['breadcrumb_name'] = 'all';
        $params['users'] = User::select('users.*', 'users_group.group_id')->join('users_group', 'users.id', '=', 'users_group.user_id')->where('users_group.group_id', '=', '4')->where('users.parent_id', 0)->get();
        return view('admin.pages.mail.index', $params);

    }



    public function customer_notification(Request $request)
    {
        $title = request()->input('title');
        $description = request()->input('description');
        $user_ids = request()->input('user_ids');
        $user_type = request()->input('user_type');

        if (isset($user_type) &&  isset($title))
        {

            $title = request()->input('title');
            $description = request()->input('description');        
            if ($user_type == 'select') {
                $data = User::with(['all_devices'])->where('status', 1)->where(function($query) use ($user_ids) {
                            $query->whereIn('id', $user_ids)
                                ->orWhere('parent_id', $user_ids);
                        })->get();
            } else
            {
                $data = User::with(['all_devices'])->where('status', 1)->get();
            }

            if(count($data) > 0){

                $devices = [];
                $user_ids = $data->pluck('id')->toArray();
                foreach ($data as $key => $value) {
                    if($value->notification == 1 && count($value->all_devices)){
                        foreach ($value->all_devices as $dkey => $dvalue) {
                            if(in_array($dvalue->user_id, $user_ids) && $dvalue->token != '' && !is_null($dvalue->token) && (!in_array($dvalue->token, $devices))){
                                    array_push($devices, $dvalue->token);
                            }
                        }
                    }
                }

                $notification_data = array();
                $type = 'custom';
                $notification_arr['type'] = $type;

                foreach ($user_ids as $key => $value) {
                    
                    $notification_data[] = array(
                            'user_id' => $value,
                            'title' => $title,
                            'message' => $description,
                            'type' => $type
                        );
                }
                
                if(!empty($notification_data)){
                    Notification::insert($notification_data);
                }

                if(!empty($devices)){
                    foreach ($devices as $key => $value) {
                        NotificationHelper::send($value, $title, $description, "custom", $notification_arr);
                    }
                }

                return redirect()->route('admin.notification.customer')->with('success', 'Notification sent successfully to '.count($user_ids).' user(s).');    

            }else{
                return redirect()->route('admin.notification.customer')->with('error', 'No any user found.');    
            }

                            
        }

        $params['type'] = "Customer";
        $params['pageTittle'] = "Send Notification to Customer";        
        $params['breadcrumb_name'] = 'all';
        $params['users'] = User::select('users.*', 'users_group.group_id')->join('users_group', 'users.id', '=', 'users_group.user_id')->where('users_group.group_id', '=', '4')->where('users.status', 1)->where('users.parent_id', 0)->get();
        return view('admin.pages.mail.customer_notification', $params);

    }

}
