<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str; 
use App\Models\UsersGroup;
use App\Models\Devices;
use App\Models\Group;
use App\Models\User;
use App\Models\AppleDetail;
use Carbon\Carbon;
use CommonHelper;
use DataTables;
use DB;
use Mail;
use App\Mail\AccountDeleteMail;
// use App\Traits\DatatableTraits;

class UserController extends Controller {
    // use DatatableTraits;

    public function index(Request $request) 
    {

        // event(new UserNotify(User::find(1)));
        if ($request->ajax()) {
            //    $data = User::select('*')->where('id','!=',1)->get();
            $data = User::select('users.*', 'users_group.group_id')->join('users_group', 'users.id', '=', 'users_group.user_id')->where('users_group.group_id',4)->where('users.parent_id',0);
            

            if(!empty($request->status) || $request->status != ''){
                $data = $data->where('users.status',$request->status);                 
            }

            $data = $data->get();

            $users_ids = $data->pluck('id')->toArray();
            
            $users_count = User::select('id',DB::raw("count('email') as 'total_email'"))->whereIn('id',$users_ids)->orWhereIn('parent_id',$users_ids)->groupBy('email')->orderBy('id','asc')->get()->toArray();

            $datat = Datatables::of($data);

            /*if ($request->has('status')) {
                $datat->filter(function ($instance) use ($request) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        if ($request->get('status') == "") {
                            return true;
                        }
                        dd($row);
                        return $row['status'] == $request->get('status') ? true : false;
                    });
                    
                });
            }*/

            return $datat->addIndexColumn()
            ->editColumn('first_name', function ($row)
            {
                return strlen($row['first_name']) > 50 ? substr($row['first_name'],0,50)."..." : $row['first_name'];
            })
            ->editColumn('last_name', function ($row)
            {
                return strlen($row['last_name']) > 50 ? substr($row['last_name'],0,50)."..." : $row['last_name'];
            })
            ->editColumn('user_count', function ($row) use($users_count)
            {
                foreach ($users_count as $key => $value) {

                    if($value['id'] == $row['id']){
                        return $value['total_email'];
                    }
                }
            })
            ->editColumn('action', function ($row)
            {
               
               $btn = '<a title="View" href="'.route('admin.user.show', $row['id']) . '" class="mr-2"><i class="fa fa-eye"></i></a>';
               $btn .= '<a title="Edit" href="' . route('admin.user.edit', $row['id']) . '" class="mr-2"><i class="fa fa-edit"></i></a>';
               return $btn;
            })
            ->editColumn('status', function ($row) {

                    if ($row['status'] == 0) {
                        return '<span class="badge bg-soft-warning text-warning">Pending</span>';
                    } elseif ($row['status'] == 1) {
                        return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '" data-token="' . csrf_token() . '"   class="btn btn-success btn-xs waves-effect waves-light" data-table="users" data-status="' . $row['status'] . '">Active</button>';
                    } elseif ($row['status'] == 2 && !is_null($row['deactivated_time'])){
                        return '<button onclick="restore_account(this);" data-id="' . $row['id'] . '" data-token="' . csrf_token() . '"  class="btn btn-warning btn-xs waves-effect waves-light" data-table="users" data-status="' . $row['status'] . '">Restore</button>';
                    }else {
                        return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '" data-token="' . csrf_token() . '"  class="btn btn-danger btn-xs waves-effect waves-light" data-table="users" data-status="' . $row['status'] . '">Inactive</button>';
                    }
                })
                ->rawColumns(['status','user_type' ,'action', 'group'])
                ->make(true);

        } else {
            $columns = [
                ['data' => 'id', 'name' => 'id', 'title' => "Id"],
                ['data' => 'first_name', 'name' => 'first_name', 'title' => __("First Name")],
                ['data' => 'last_name', 'name' => 'last_name', 'title' => __("Last Name")],
                ['data' => 'email', 'name' => 'email', 'title' => __("Email")],
                ['data' => 'user_count', 'title' => __("Accounts"), 'searchable' => false],
                ['data' => 'status', 'title' => __("Status"), 'searchable' => false],
                ['data' => 'action', 'name' => 'action', 'title' => "Action", 'searchable' => false, 'orderable' => false]];
            $groups = Group::get();
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = route('admin.users.index');
            $params['dateTableTitle'] = "User Management";
            $params['dataTableId'] = time();
            $params['groups'] = $groups;
            return view('admin.pages.users.index', $params);
        }
    }

      public function show($id) {
        $params['pageTittle'] = "View User";

        $user = User::with(['language','plan','usergenres.genre','useragerestriction.ageratings','userrestrictedcontent.content','mylist.content','subuser.usergenres.genre','activity_log.content.video','parent','used_refferal','paymenthistory'=> function($q) {
                $q->orderBy('id', 'desc');
            },'paymenthistory.users','paymenthistory.plan'])->where('id', $id)->first();
        
        if(!$user){
             return redirect()->route('admin.users.index')->with('error','User not found');
        }
        $user_group = UsersGroup::where('user_id', $id)->first();

        $params['user'] = $user->toArray();

        $sub_user_ids = array();
        if(isset($params['user']['subuser']) && count($params['user']['subuser'])){
            $sub_user_ids = array_column($params['user']['subuser'], 'id');
        }
        array_push($sub_user_ids, $id);

        $params['user_group'] = $user_group->toArray();

        $params['devices'] = Devices::whereIn('user_id',$sub_user_ids)->where('token', '!=', '')->orderBy('id','desc')->get();
        $params['backUrl'] = route('admin.users.index');
        return view('admin.pages.users.view', $params);

    }

    public function edit($id)
    {
        
        $params['pageTittle'] = "Edit User";
        $params['user'] = User::find($id);
        $params['backUrl'] = route('admin.users.index');
        return view('admin.pages.users.put',$params);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'image' => 'mimes:jpeg,png,jpg|max:2048',
            // 'email' => 'required|email|unique:users,email,'.$id,
            // 'phone' => 'required|min:14|max:16|unique:users,phone,'.$id,
        ]);        

        if(isset($request->image) && $request->image != ''){
            //$dir ="images/users";
            //$image = CommonHelper::imageUpload($request->image,$dir);
            
            $dir = env('AWS_S3_MODE')."/users"; 
            $image = CommonHelper::s3Upload($request->image, $dir);   
            $user['profile_picture'] = $image;
        }
        
        $user['first_name'] = $request->first_name;
        $user['last_name'] = $request->last_name;
        // $user['email'] = $request->email;
        // $user['phone'] = $request->phone;

        User::whereId($id)->update($user);

        return redirect()->route('admin.users.index')
                        ->with('success','User updated successfully');
    }


    public function destroy($id)
    {
        $user = User::where('id', $id)->first();                
        if (isset($user)) {
            $user->revokeAllTokens();
        }
        User::whereId($id)->delete();
        AppleDetail::whereEmail($user->email)->delete();
        return redirect()->route('admin.store.index')
                        ->with('success','User deleted successfully');
    }



    public function active_deactive() {
        if ($_POST['table'] == 'users') {
            if ($_POST['status'] == 1) {
                $status = 2;
            } else {
                $status = 1;
            }

            $user = User::where('id', $_POST['id'])->first();
            if (isset($user)) {
                if($status == '2'){
                    // $devices = Devices::where('user_id', $_POST['id'])->update([
                    //     'name' => '',
                    //     'token' => '',
                    //     'type' => '',
                    // ]);
                    $user->revokeAllTokens();
                }
                User::where('id', $_POST['id'])->update(['status' => $status]);

                if($user->deactivated_time != NULL){
                    User::where('id', $_POST['id'])->update(['deactivated_time'=>NULL]);

                    $reactive_user_ids = User::where('parent_id', $_POST['id'])->where('deactivated_time','=',NULL)->get()->pluck('id')->toArray();   
                    if(!empty($reactive_user_ids)){
                        User::whereIn('id', $reactive_user_ids)->update(['status' => 1]);
                    }

                }
            }
        }
        echo $status;
    }

    public function restore_account() {
        
        $user = User::find($_POST['id']);
        if ($user) {
            if($user->status == '2'){
                $user->deactivated_time = NULL;
                $user->status = 1;
                $user->save();

                $reactive_user_ids = User::where('parent_id', $_POST['id'])->get()->pluck('id')->toArray();   
                if(!empty($reactive_user_ids)){
                    User::whereIn('id', $reactive_user_ids)->update(['status' => 1]);
                }

                $title = "Account Restored";
                $description = "<p>Your account has been restored. You can access your account.</p>";

                Mail::to($user->email)->send(new AccountDeleteMail($title,$description, config('app.address1'), config('app.address2')));

                $response['status'] = TRUE;
                $response['message'] = 'Restored & Activated.';

            }else{
                $response['status'] = FALSE;
                $response['message'] = 'Something went wrong.';
            }
        }else{
            $response['status'] = FALSE;
            $response['message'] = 'User not found';
        }
        echo json_encode($response);
    }

}
