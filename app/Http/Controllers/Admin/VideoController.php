<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\Season;
use App\Models\Video;
use App\Models\Language;
use App\Models\VideoSubtitle;
use App\Models\MediaCovertLogs;
use App\Models\Audio;
use DataTables;
use Storage;
use Illuminate\Support\Facades\Auth;
set_time_limit(0);

use App\Jobs\MediaConvert;
use App\Traits\MediaCovertJob;

class VideoController extends Controller
{

    use MediaCovertJob;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {

        if ($request->ajax())
        {

            $data = Video::with(['content','season'])->whereHas('content')->get();
            
            return Datatables::of($data)
            ->editColumn('action', function ($row){

                if($row['content']['content_type'] == 2){
                    $btn = '<a title="Edit" href="'.route('admin.video.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
                }else{
                    $btn = '<i class="fa fa-edit mr-2" title="Movies are non-editable."></i>';
                }
                
                
                $btn .= '<a title="View" href="'.route('admin.video.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
                $btn .= '<a title="Delete" href="'.route('admin.video.destroy', $row['id']).'" data-url="video" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
                
                return $btn;
            })
            ->editColumn('episode_name', function ($row){
                return $row['episode_name'];
            })
            ->editColumn('season_name', function ($row){
                
                if(!empty($row['season'])){
                    return '<a target="_blank" href="'.route('admin.season.show',$row['season']['id']).'" class="mr-2">'.$row['season']['season'].'</a>';
                }else{
                    return '';
                }

            })
            ->editColumn('content_name', function ($row){

                if(!empty($row['content']['name'])){
                    $content_name = strlen($row['content']['name']) > 50 ? substr($row['content']['name'],0,50)."..." : $row['content']['name'];
                  return '<a target="_blank" href="'.route('admin.content.show',$row['content']['id']).'" class="mr-2">'.$content_name.'</a>';
                }else{
                    $content_name = '';
                  return '';
                }   
                
            })
            ->rawColumns(['action','id','episode_name','content_name','season_name'])
            
            ->make(true);
        }
        else
        {
           $columns = [
               ['data' => 'id','name' => 'id','title' => "Id"],
               ['data' => 'content_name', 'name' => 'content_name','title' => __("Content Name"),'searchable'=>true ], 
               ['data' => 'season_name', 'name' => 'season_name','title' => __("Season Name"),'searchable'=>true ], 
               ['data' => 'episode_name', 'name' => 'episode_name','title' => __("Video Name"),'searchable'=>true ], 
               ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
           $params['dateTableFields'] = $columns;
           $params['dateTableUrl'] = route('admin.video.index');
           $params['dateTableTitle'] = "Video Management";
           $params['dataTableId'] = time();
           $params['addUrl'] = route('admin.video.create');
           return view('admin.pages.video.index',$params);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $content_data = Content::select('id', 'name')->orderBy('id', 'DESC')->get();
        $params['content_data'] = $content_data;

        $params['pageTittle'] = "Add Video" ;
        $params['backUrl'] = route('admin.video.index');

        //dd($params); 

        return view('admin.pages.video.post',$params);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'content_id' => 'required',
            'thumbnail_image' => 'mimes:jpeg,png,jpg|max:2048',
            'video_path' => 'required'
        ]);
        //dd($request->all());

        if($request->content_type == 2){
            $request->validate([
                'episode_name_en' => 'required',
                'episode_name_md' => 'required',
                'episode_name_hi' => 'required',
                'episode_name_sp' => 'required',
                'episode_name_fr' => 'required',
                'episode_synopsis_en' => 'required',
                'episode_synopsis_md' => 'required',
                'episode_synopsis_hi' => 'required',
                'episode_synopsis_sp' => 'required',
                'episode_synopsis_fr' => 'required',
            ]);

            $episode_name =array(
                'en' => $request->episode_name_en,
                'md' => $request->episode_name_md,
                'hi' => $request->episode_name_hi,
                'sp' => $request->episode_name_sp,
                'fr' => $request->episode_name_fr
            );

            $episode_synopsis =array(
                'en' => $request->episode_synopsis_en,
                'md' => $request->episode_synopsis_md,
                'hi' => $request->episode_synopsis_hi,
                'sp' => $request->episode_synopsis_sp,
                'fr' => $request->episode_synopsis_fr
            );

        }else{
            $episode_name = $episode_synopsis = '';
        }

        //dd($request->all());

        $dir = "video";
        if(!empty($request->thumbnail_image)) {
            $thumbnail = CommonHelper::s3Upload($request->thumbnail_image,$dir);
            $thumbnail_image = $thumbnail;
        }else{
            $thumbnail_image = '';
        }

        $video = Video::create([
            'content_id' => $request->content_id,
            'season_id' => !empty($request->season_id)?$request->season_id:0,
            'episode_name' => $episode_name,
            'episode_synopsis' => $episode_synopsis,
            'video' => $request->video_full_link,
            'thumbnail_image' => $thumbnail_image,
            'duration' => '0',
            'duration_seconds' => '0'
        ]);

        $subTitles = [];
        if(!empty($request->subtitle_en) || !empty($request->subtitle_md) || !empty($request->subtitle_hi) || !empty($request->subtitle_sp) || !empty($request->subtitle_fr)){
            $dir = "subtitles";
            $subtitle_insert_data = [];
            if(!empty($request->subtitle_en)){
                $subtitle_en_file = CommonHelper::VttsaveFile($request->subtitle_en,$dir);
                $subtitle_en = array(
                            'NameModifier' => 'caption_en',
                            'LanguageCode' => 'ENG',
                            'LanguageDescription' => 'English Original',
                            'CaptionSourceFile' => $subtitle_en_file
                          );
                array_push($subTitles, $subtitle_en);
                $temp = array(
                    'video_id' => $video->id,
                    'language_id' => 1,
                );
                array_push($subtitle_insert_data, $temp);
            }

            if(!empty($request->subtitle_md)){
                $subtitle_md_file = CommonHelper::VttsaveFile($request->subtitle_md,$dir);
                $subtitle_md = array(
                            'NameModifier' => 'caption_md',
                            'LanguageCode' => 'ZHO',
                            'LanguageDescription' => 'Chinese',
                            'CaptionSourceFile' => $subtitle_md_file
                          );
                array_push($subTitles, $subtitle_md);
                $temp = array(
                    'video_id' => $video->id,
                    'language_id' => 2,
                );
                array_push($subtitle_insert_data, $temp);
            }

            if(!empty($request->subtitle_hi)){
                $subtitle_hi_file = CommonHelper::VttsaveFile($request->subtitle_hi,$dir);
                $subtitle_hi = array(
                            'NameModifier' => 'caption_hi',
                            'LanguageCode' => 'HIN',
                            'LanguageDescription' => 'Hindi',
                            'CaptionSourceFile' => $subtitle_hi_file
                          );
                array_push($subTitles, $subtitle_hi);
                $temp = array(
                    'video_id' => $video->id,
                    'language_id' => 3,
                );
                array_push($subtitle_insert_data, $temp);
            }

            if(!empty($request->subtitle_fr)){
                $subtitle_fr_file = CommonHelper::VttsaveFile($request->subtitle_fr,$dir);
                $subtitle_fr = array(
                            'NameModifier' => 'caption_fr',
                            'LanguageCode' => 'FRA',
                            'LanguageDescription' => 'French',
                            'CaptionSourceFile' => $subtitle_fr_file
                          );
                array_push($subTitles, $subtitle_fr);
                $temp = array(
                    'video_id' => $video->id,
                    'language_id' => 4,
                );
                array_push($subtitle_insert_data, $temp);
            }

            if(!empty($request->subtitle_sp)){
                $subtitle_sp_file = CommonHelper::VttsaveFile($request->subtitle_sp,$dir);
                $subtitle_sp = array(
                            'NameModifier' => 'caption_sp',
                            'LanguageCode' => 'SPA',
                            'LanguageDescription' => 'Spanish',
                            'CaptionSourceFile' => $subtitle_sp_file
                          );
                array_push($subTitles, $subtitle_sp);
                $temp = array(
                    'video_id' => $video->id,
                    'language_id' => 5,
                );
                array_push($subtitle_insert_data, $temp);
            }

            if(count($subtitle_insert_data) > 0){
                VideoSubtitle::insert($subtitle_insert_data);
            }
        }

        $audioSelector = [];
        if(!empty($request->audio_en) || !empty($request->audio_md) || !empty($request->audio_hi) || !empty($request->audio_sp) || !empty($request->audio_fr)){
            $dir = "audio";

            $audio_insert_data = [];
            if(!empty($request->audio_en)){
                $audio_en_file = CommonHelper::VttsaveFile($request->audio_en, $dir);
                $audio_en = array(
                              'NameModifier' => 'audio_en',
                              'LanguageCode' => 'ENG',
                              'StreamName' => 'English Original',
                              'ExternalAudioFileInput' => $audio_en_file
                            );

                array_push($audioSelector, $audio_en);
                $temp = array(
                    'video_id' => $video->id,
                    'language_id' => 1,
                );
                array_push($audio_insert_data, $temp);
            }

            if(!empty($request->audio_md)){
                $audio_md_file = CommonHelper::VttsaveFile($request->audio_md, $dir);
                $audio_md = array(
                              'NameModifier' => 'audio_md',
                              'LanguageCode' => 'ZHO',
                              'StreamName' => 'Chinese',
                              'ExternalAudioFileInput' => $audio_md_file
                            );
                array_push($audioSelector, $audio_md);
                $temp = array(
                    'video_id' => $video->id,
                    'language_id' => 2,
                );
                array_push($audio_insert_data, $temp);
            }

            if(!empty($request->audio_hi)){
                $audio_hi_file = CommonHelper::VttsaveFile($request->audio_hi, $dir);
                $audio_hi = array(
                            'NameModifier' => 'audio_hi',
                            'LanguageCode' => 'HIN',
                            'StreamName' => 'Hindi',
                            'ExternalAudioFileInput' => $audio_hi_file
                          );
                array_push($audioSelector, $audio_hi);
                $temp = array(
                    'video_id' => $video->id,
                    'language_id' => 3,
                );
                array_push($audio_insert_data, $temp);
            }

            if(!empty($request->audio_fr)){
                $audio_fr_file = CommonHelper::VttsaveFile($request->audio_fr,$dir);
                $audio_fr = array(
                            'NameModifier' => 'audio_fr',
                            'LanguageCode' => 'FRA',
                            'StreamName' => 'French',
                            'ExternalAudioFileInput' => $audio_fr_file
                          );
                array_push($audioSelector, $audio_fr);
                $temp = array(
                    'video_id' => $video->id,
                    'language_id' => 4,
                );
                array_push($audio_insert_data, $temp);
            }

            if(!empty($request->audio_sp)){
                $audio_sp_file = CommonHelper::VttsaveFile($request->audio_sp,$dir);
                $audio_sp = array(
                            'NameModifier' => 'audio_sp',
                            'LanguageCode' => 'SPA',
                            'StreamName' => 'Spanish',
                            'ExternalAudioFileInput' => $audio_sp_file
                          );
                array_push($audioSelector, $audio_sp);
                $temp = array(
                    'video_id' => $video->id,
                    'language_id' => 5,
                );
                array_push($audio_insert_data, $temp);
            }

            if(count($audio_insert_data) > 0){
                Audio::insert($audio_insert_data);
            }
            
        }

        $filename = str_replace('uppyfiles/', '', $request->video_path);
        $filename = str_replace('.mp4', '', $filename);

        MediaConvert::dispatch($video->id, $request->video_path, $filename, $subTitles, $audioSelector);

        return redirect()->route('admin.video.index')->with('success','Video created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        /*$client = Storage::disk('s3')->getDriver()->getAdapter()->getClient();
$bucket = \Config::get('filesystems.disks.s3.bucket');

$command = $client->getCommand('GetObject', [
    'Bucket' => $bucket,
    'Key' => 'videoFiles/test63/bandhan.m3u8'  // file name in s3 bucket which you want to access
]);



$request = $client->createPresignedRequest($command, '+1 minutes');


// Get the actual presigned-url
 $presignedUrl = (string)$request->getUri();
 dd($request->getUri());
 dd($presignedUrl);

    exit;

        $subTitles = [];
        $subtitle_en = array(
                    'NameModifier' => 'caption_en',
                    'LanguageCode' => 'ENG',
                    'LanguageDescription' => 'English Original',
                    'CaptionSourceFile' => 'testing/bandhan_en.vtt'
                  );

        $subtitle_md = array(
                            'NameModifier' => 'caption_md',
                            'LanguageCode' => 'ZHO',
                            'LanguageDescription' => 'Chinese',
                            'CaptionSourceFile' => 'testing/bandhan_arabic.vtt'
                          );

        array_push($subTitles, $subtitle_en);
        array_push($subTitles, $subtitle_md);

        $audioSelector = [];

        $audio_en = array(
                              'NameModifier' => 'audio_fr',
                              'LanguageCode' => 'FRA',
                            'StreamName' => 'French',
                              'ExternalAudioFileInput' => 'testing/Trimmed_song.mp3'
                            );

        array_push($audioSelector, $audio_en);

        MediaConvert::dispatch(18, "testing/bandhan.mp4", "test63", $subTitles, $audioSelector);
        exit;*/

        $params['pageTittle'] = "View Video";
        $params['video'] = $video = Video::with(['content', 'convert_log'])->find($id);
        $job = $this->getJobStatus($video['convert_log']['job_id']);
        if(isset($job) && $job['status'] == TRUE){
            $params['job_status'] = $job['result']['Job']['Status'];
        }else{
            $params['job_status'] = "FAILED";
        }

        $params['season'] = Season::find($params['video']['season_id']);
        $params['backUrl'] = route('admin.video.index');
        
        //dd($params);

        return view('admin.pages.video.view',$params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $params['pageTittle'] = "Edit Video";
        $params['video'] = Video::with(['content'])->find($id);
        $content_data = Content::select('id', 'name')->orderBy('id', 'DESC')->get();
        $params['content_data'] = $content_data;
        
        $season_data = Season::select('id', 'season')->where('content_id',$params['video']->content_id)->orderBy('id', 'DESC')->get();
        $params['season_data'] = $season_data;


        $params['backUrl'] = route('admin.video.index');
        return view('admin.pages.video.put',$params);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {  

        if($request->content_type == 2){
            $request->validate([
                'episode_name_en' => 'required',
                'episode_name_md' => 'required',
                'episode_name_hi' => 'required',
                'episode_name_sp' => 'required',
                'episode_name_fr' => 'required',
                'episode_synopsis_en' => 'required',
                'episode_synopsis_md' => 'required',
                'episode_synopsis_hi' => 'required',
                'episode_synopsis_sp' => 'required',
                'episode_synopsis_fr' => 'required',
            ]);
            
            $episode_name =array(
                'en' => $request->episode_name_en,
                'md' => $request->episode_name_md,
                'hi' => $request->episode_name_hi,
                'sp' => $request->episode_name_sp,
                'fr' => $request->episode_name_fr
            );

            $episode_synopsis =array(
                'en' => $request->episode_synopsis_en,
                'md' => $request->episode_synopsis_md,
                'hi' => $request->episode_synopsis_hi,
                'sp' => $request->episode_synopsis_sp,
                'fr' => $request->episode_synopsis_fr
            );

        }else{
            $episode_name = $episode_synopsis = '';
        }

            
            $update_data = array(
                'season_id' => !empty($request->season_id)?$request->season_id:0,
                'episode_name' => $episode_name,
                'episode_synopsis' => $episode_synopsis
            );

        $dir = "video";
        if(!empty($request->thumbnail_image)) {
            
            $request->validate([
                'thumbnail_image' => 'image|mimes:jpeg,png,jpg|max:2048'
            ]);

            $thumbnail = CommonHelper::s3Upload($request->thumbnail_image,$dir);
            $update_data['thumbnail_image'] = $thumbnail;
        }

        $video = Video::where('id', $id)->update($update_data);
            
        return redirect()->route('admin.video.index')->with('success','Video updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $video = Video::find($id);
        $content_id = $video->content_id;
        $video = $video->delete();
        $video_count=Video::where('content_id',$content_id)->count();
        $msg = "";
        if($video_count < 1){
            Content::where('id',$content_id)->update(['status'=>2]);
            $msg = 'and content marked as deactivated.';
        }

        return redirect()->route('admin.video.index')
                        ->with('success','Video deleted successfully'.$msg);
    }

    public function active_deactive_product()
    {
       if($_POST['table'] == 'video'){
            if($_POST['status'] == 0){
                $status = 1;
            }else{
                $status = 0;
            }
            Video::where('id', $_POST['id'])->update(['status' => $status]);
        }
        echo $status; 
    }

    public function get_season_data()
    {
        if(!empty($_GET['id'])){
        
        $video_content = Video::select('id','content_id')->with(['content'])->where('content_id',$_GET['id'])->get()->toArray();
        
        if(!empty($video_content) && $video_content[0]['content']['content_type'] == 1){
            return response()->json(['status'=>false,'data' => array(),'message'=>'Video already exist']);
        }else{
            $data = Content::with(['season'])->where('id',$_GET['id'])->first();
            return response()->json(['status'=>true,'data' => $data,'message'=>'Season data']);
            
        }
            
        }else{
            return response()->json(['status'=>false,'data' => array(),'message'=>'']);
        }
    }


    public function chunk_file_upload(Request $request)
    {
        return CommonHelper::file_upload($request);
    }

}