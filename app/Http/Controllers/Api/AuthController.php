<?php

namespace App\Http\Controllers\Api;



use App\Http\Controllers\Controller;

use App\Models\AppVersion;

use App\Models\Devices;

use App\Models\Banner;

use App\Models\Orders;

use App\Models\User;

use App\Models\UsersGroup;
use App\Models\Language;

use App\Traits\ApiResponser;
use App\Traits\StripeTraits;
use App\Models\Notification;
use App\Models\IntroScreenText;

use CommonHelper;
use NotificationHelper;

use Illuminate\Auth\Events\Registered;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Password;

use Illuminate\Support\Facades\Validator;

use Carbon\Carbon;

use App\Http\Resources\UserResource;

use Mail;
use App\Mail\BulkMail;
use App\Mail\OTPMail;
use App\Models\Plan;

use DB;

use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;

class AuthController extends Controller {

    use ApiResponser, StripeTraits;



    public function login(Request $request) {
        

        $validator = Validator::make(request()->all(), [
            'email' => 'required',
            'password' => 'required',
            'device_name' => 'required',
            'device_token' => 'required',
            'device_type' => 'required',
            'language_id' => '',
        ]);

        if (!$validator->fails()) {

            // Added for email-phone login  Start
            $login = request()->input('email');

            if(is_numeric($login)){
                $field = 'phone';

                $validator = Validator::make(request()->all(), [
                    'country_code' => 'required',
                ]);

                if ($validator->fails()) {
                    return $this->errorResponse($validator->messages(), true);
                }

                $user_data = array($field => $request->email, 'password' => $request->password,'country_code'=>$request->country_code);

            } elseif (filter_var($login, FILTER_VALIDATE_EMAIL)) {
                $field = 'email';

                $user_data = array($field => $request->email, 'password' => $request->password);
            } else{
                return response([
                    'status' => false,
                    'message' =>  __('message.api.email_phone_incorrect_format')
                ]);
            }

            // Added for email-phone login  End

            if (!auth()->attempt($user_data)) {
                return $this->errorResponse(__('message.api.account_not_exist'));
            }

            $user = auth()->user();

            if($user->isActive()){

                if (in_array($user->hasRole(), [4])) {

                    $accessToken = auth()->user()->createToken('authToken')->accessToken;
                    $user->token = $accessToken;

                    if(isset($request->language_id) && $request->language_id != ''){
                        $update_data = array('language_id' => $request->language_id);
                        User::whereId($user->id)->update($update_data);
                        $user->language_id = $request->language_id;
                    }

                    return (new UserResource($user))->additional([
                        'status' => true,
                        'message' =>  __('message.api.login_success')
                    ]);

                }else {

                    return $this->errorResponse(__('Entered email address or phone number does not match with the records'));

                }

            }else{
                return $this->errorResponse(__('message.api.account_deactivated'));
            }
        }

        return $this->errorResponse($validator->messages(), true);

    }

    public function register(Request $request) {

        $validator = Validator::make(request()->all(), [
            'first_name' => 'required|max:25',
            'last_name' => 'required|max:25',
            'password' => 'required|confirmed|min:8',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|unique:users,phone|min:7|max:15|gt:0',            
            'device_name' => 'required',
            'device_token' => 'required',
            'device_type' => 'required',
            'country_code' => 'required|starts_with:+',
            'currency_code' => 'required',
            'referral_code' => ['', 
            function ($attribute, $value, $fail) {
                if($value != ''){
                    $refer_user = User::where('reference_code',$value)->where('status',1)->first();
                    if($refer_user) {
                        if($refer_user->referral_expiration_date < Carbon::now()->format('Y-m-d')){
                            $fail( __('message.referral_expired'));
                        }
                    }else{
                        $fail( __('message.referral_invalid'));
                    }
                    
                } } ]
            ],
        [
            'email.unique' => 'This email address is already registered',
            'email.required' => 'Please enter email',
            'email.email' => 'Please enter a valid email'

        ]);


        if (!$validator->fails()) {

            if(isset($request->referral_code) && !empty($request->referral_code)){

                $used_referral_code = User::where('used_referral_code',$request->referral_code)->count();

                if($used_referral_code>0){
                    return response([
                        'status' => false,
                        'message' => __('message.referral_used')
                    ]);
                }
            }

            $user_data = [];
            $user_data['password'] = Hash::make($request->password);
            $user_data['first_name'] = $request->first_name;
            $user_data['last_name'] = $request->last_name;
            $user_data['email'] = $request->email;
            $user_data['country_code'] = $request->country_code;
            $user_data['currency_code'] = !empty($request->currency_code)?$request->currency_code:'USD';
            $user_data['phone'] = $request->phone;
            $user_data['phone_verified'] = !empty($request->phone)?1:0;
            $user_data['status'] = 1;
            $user_data['reference_code'] = "AWE".CommonHelper::random_number();
            $user_data['referral_expiration_date'] = Carbon::now()->addDays(CommonHelper::ConfigGet('referral_code_expiration_days'))->format('Y-m-d');
            $user_data['used_referral_code'] = !empty($request->referral_code)?$request->referral_code:'';

            $user_id = User::insertGetId($user_data); 

            UsersGroup::create([
                'user_id' => $user_id,
                'group_id' => '4',
            ]);

            $user = User::find($user_id);
            $result = $this->createCustomer($user);

            if($result['status'] == TRUE){
                $user->stripe_id = $result['data']->id;
                $user->save();
                
                if(isset($request->referral_code) && !empty($request->referral_code)){

                     $refer_user = User::where('reference_code',$request->referral_code)->with(['devices'])->where('status',1)->first();
                    
                    if($refer_user){

                        if(!empty($refer_user->plan_id)){
                            $refer_user->expiration_date = Carbon::createFromFormat('Y-m-d', $refer_user->expiration_date)->addDays(30);
                        }else{
                            $refer_user->plan_id = 1;
                            $refer_user->expiration_date = Carbon::now()->addDays(30);
                        }

                        $refer_user->save();

                        $type = 'referral_accepted';
                        $notification_arr['type'] = $type;

                        $title = "Referral Bonus";
                        $description = "Congratulations! ".$request->first_name." ".$request->last_name." used your referral code. Your current plan is extended by 30 days.";

                        if($refer_user->devices){
                            if($refer_user->devices->token != '' && $refer_user->notification == 1){
                                NotificationHelper::send($refer_user->devices->token, $title, $description, $type, $notification_arr);
                            }
                            
                        }
                        $notification = array(
                            'user_id' => $refer_user->id,
                            'title' => $title,
                            'message' => $description,
                            'type' => $type
                        );

                        Notification::insert($notification);

                    }
                    
                }

                /* Devices::create([
                     'user_id' => $user_id,
                     'name' => $request->device_name,
                     'token' => $request->device_token,
                     'type' => $request->device_type,
                ]); */

                $user = User::find($user_id);
                $accessToken = $user->createToken('authToken')->accessToken;
                $user->token = $accessToken;

                event(new Registered($user));
                Mail::to($request->email)->send(new BulkMail(config('app.name'), $request->first_name, $request->email, config('app.address1'), config('app.address2')));

                return (new UserResource($user))->additional([
                    'status' => true,
                    'message' => __('message.api.register_success')
                ]);

            }else{

                $user->delete();
                UsersGroup::where('user_id', $user_id)->delete();
                return response([
                    'status' => false,
                    'message' => $result['message']
                ]);
            }

        }

        return $this->errorResponse($validator->messages(), true);

    }



    public function forgot_password(Request $request) {

        $validator = Validator::make(request()->all(), [
            'email' => 'required',
        ]);

        if (!$validator->fails()) {

            $login = request()->input('email');

            if(is_numeric($login)){
                $field = 'phone';

                $validator = Validator::make(request()->all(), [
                    'country_code' => 'required',
                ]);

                if ($validator->fails()) {
                    return $this->errorResponse($validator->messages(), true);
                }

                $user = User::wherePhone($request->email)->where('country_code',$request->country_code)->first();
                //dd($user);
                if($user){

                    if($user->status == 1){

                        $data['otp'] = rand(1000,9999);
                        $message = "Your AWE otp is ".$data['otp'];
                        $result = NotificationHelper::send_sms($message, $user->country_code.$user->phone);
                        if($result['status'] == TRUE){
                            $data['user_id'] = $user->id;
                            return response([
                                'status' => true,
                                'message' =>  __('message.api.otp_sent_success'),
                                'data' => $data
                            ]);
                        }else{
                            return response([
                                'status' => false,
                                'message' => __('message.invalid_phone')
                            ]);
                        }

                    }else{
                        return response([
                            'status' => false,
                            'message' => __('message.api.account_deactivated'),
                        ]);
                    }
                    
                }else{
                    return response([
                        'status' => false,
                        'message' => __('message.phone_email_not_registered'),
                    ]);
                }
                
            } elseif (filter_var($login, FILTER_VALIDATE_EMAIL)) {
                $field = 'email';
                $user = User::whereEmail($request->email)->first();

                if($user){

                    if($user->status == 1){

                        if(!empty($user->social_type) && !empty($user->social_id)){
                            return response([
                                'status' => false,
                                'message' => __('message.social_account_not_forgot_password'),
                            ]);
                        }

                        $status = Password::sendResetLink(
                            $request->only('email')
                        );

                        if ($status == Password::RESET_LINK_SENT) {
                            return response([
                                'status' => true,
                                'message' => __('message.change_password_link_sent')
                            ]);
                        }else{
                            return $this->errorResponse( __('message.email_address_not_registered'));
                        }

                    }else{
                        return response([
                            'status' => false,
                            'message' => __('message.api.account_deactivated'),
                        ]);
                    }
                    
                }else{
                    return response([
                        'status' => false,
                        'message' => __('message.phone_email_not_registered')
                    ]);
                }
                

            } else{
                return response([
                    'status' => false,
                    'message' => __('message.api.email_phone_incorrect_format'),
                ]);
            }

        }

        return $this->errorResponse($validator->messages(), true);

    }

    public function init($type = "", $app_version = "",Request $request) {

        if(isset($request->user_id) && !empty($request->user_id)){
            $language = User::select('language.id','language.type','language.image')
                ->join('language', 'users.language_id', '=', 'language.id')
                ->where('users.id', $request->user_id)->where('users.status', 1)->first();
        }else{
            $language = language::select('id','type','image')->first();
        }

        if ($app_version != "" && $type != "") {

            $system_version = AppVersion::whereType($type)->first();

            $current_version = (Int) str_replace('.', '', $system_version->version);

            $app_version = (Int) str_replace('.', '', $app_version);

            $data['force_update'] = false;
            $data['downloads'] = true;
            $data['intro_screen_text'] = IntroScreenText::all();
            if ($system_version->maintenance == 1) {

                $data['status'] = false;
                $data['update'] = false;
                $data['maintenance'] = true;
                $data['language'] = $language;
                $data['message'] = __('message.api.under_maintenance');

                return response($data);

            } else if ($app_version < $current_version && $system_version->force_update == 0) {

                $data['status'] = false;
                $data['update'] = true;
                $data['force_update'] = false;
                $data['maintenance'] = false;
                $data['language'] = $language;
                $data['message'] = __('message.api.new_version_available');

                return response($data);

            } else if ($app_version < $current_version && $system_version->force_update == 1) {

                $data['status'] = false;
                $data['update'] = true;
                $data['force_update'] = true;
                $data['maintenance'] = false;
                $data['language'] = $language;
                $data['message'] = __('message.api.new_version_available');

                return response($data);

            } else {

                $data['status'] = true;
                $data['update'] = false;
                $data['maintenance'] = false;
                $data['language'] = $language;
                $data['message'] = __('Success');

                return response($data);

            }

        }

        return $this->errorResponse(__('message.api.missing_app_version'));

    }

    public function send_otp(Request $request)
    {
        $validator = Validator::make(request()->all(), [
           // 'email' => 'email:rfc,dns|unique:users,email',
            'phone' => 'required|numeric|unique:users,phone',
            'country_code' => 'required'
        ]);

        if (!$validator->fails()) {

            $data['otp'] = rand(1000,9999);
            $message = "Your AWE otp is ".$data['otp'];

            $result = NotificationHelper::send_sms($message, $request['country_code'].$request['phone']);
            if($result['status'] == TRUE){
                return response([
                    'status' => true,
                    'message' => __('message.api.otp_sent_success'),
                    'data' => $data
                ]);
            }else{
                return response([
                            'status' => false,
                            'message' => __('message.invalid_phone')
                        ]);
            }
            

        }

        return $this->errorResponse($validator->messages(), true);

    }

    public function send_otp_email(Request $request)
    {
        $validator = Validator::make(request()->all(), [
           'email' => 'email:rfc,dns|unique:users,email,NULL,id,deleted_at,NULL',
        ]);

        if (!$validator->fails()) {

            $data['otp'] = rand(1000,9999);

            $subject = "Verify Your OTP : ".$data['otp'];
            $message = $data['otp'];

            Mail::to($request['email'])->send(new OTPMail($subject, $message, config('app.address1'), config('app.address2')));

            return response([
                'status' => true,
                'message' => __('message.api.otp_sent_success'),
                'data' => $data
            ]);

        }

        return $this->errorResponse($validator->messages(), true);

    }


    public function test_upload(Request $request) {

        if ($request->hasFile('profile_picture')) {
            $dir = "images/users";
            $profile_picture = CommonHelper::s3Upload($request->file('profile_picture'), $dir);
            $update_data['profile_picture'] = $profile_picture;
        }

    }

}

