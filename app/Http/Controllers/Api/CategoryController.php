<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\UserGenres;
use App\Models\Genre;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Api\CategoryResource;

class CategoryController extends Controller {

    use ApiResponser;


    public function index() {

        $validator = Validator::make(request()->all(), [
            'user_id' => 'required'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }

        $genre = Genre::wherehas("content",function ($query){
                                    $query->where('status',1);
                                  })->where('status',1)->get();
        return CategoryResource::collection($genre)->additional([
            'status' => true,
            'message' => ''
        ]);

    }


    public function add(Request $request) {

        $validator = Validator::make(request()->all(), [
            'user_id' => 'required',
            'genres_id' => 'required',
        ]);


        if (!$validator->fails()) {
            if (User::whereId($request->user_id)->where('status','1')->first()) {

                if($request['genres_id'] == ''){
                    UserGenres::where('user_id',$request->user_id)->delete();
                }else{
                    $genres_id = explode(",", $request['genres_id']);
                    $genres_id_insert_array = array();

                    $genres_data = Genre::whereIn('id',$genres_id)->get();
                    
                    if(count($genres_data) == count($genres_id)){

                        foreach ($genres_id as $key => $value) {
                            $genres_id_insert_array[$key] = array(
                                'user_id' => $request->user_id,
                                'genre_id' => $value
                            );
                        }

                        UserGenres::where('user_id',$request->user_id)->delete();
                        UserGenres::insert($genres_id_insert_array);

                        return response([
                            'status' => true,
                            'message' => __('message.category_data_added')
                        ]);

                    }else{
                        return response([
                            'status' => false,
                            'message' => __('message.category_not_found')
                        ]);
                    }

                }
                   

            } else {
                return response([
                    'status' => false,
                    'message' => __('message.user_not_found'), //'Video not found',
                ]);
            }
        }
        return $this->errorResponse($validator->messages(), true);

    }




}
