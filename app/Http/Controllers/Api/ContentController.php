<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Content;
use App\Models\Genre;
use App\Models\ContentGenres;
use App\Models\FavoriteVideo;
use App\Models\SystemConfig;
use App\Models\Video;
use App\Models\UserGenres;
use App\Models\UserRestrictedContent;
use App\Models\UserAgeRestriction;
use App\Models\AgeRatings;
use App\Models\ActivityLog;
use App\Models\PaymentHistory;
use App\Models\PlanAttribute;
use App\Models\Streaming;
use CommonHelper;
use Validator;
use App; 
use DB;
use App\Http\Resources\Api\ContentResource;
use App\Http\Resources\Api\VideoResource;
use App\Http\Resources\Api\PlayResource;
use App\Http\Resources\Api\MainContentResource;
use Carbon\Carbon;

class ContentController extends Controller
{
	public function index(Request $request){

		$validator = Validator::make(request()->all(), [
            'content_type' => 'required|digits_between:1,2',
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }
        $user = auth()->user();
        
        if($request->content_type == 1){
        	$adv_content_data = CommonHelper::ConfigGet('movie_adv_content_id');
        }else{
        	$adv_content_data = CommonHelper::ConfigGet('adv_content_id');
    	}

        $restricted_content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
        $user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();
        $kids_age_ids = AgeRatings::where('children',1)->get()->pluck('id')->toArray();

        $content_type = $request->content_type;

			$main = Content::with(['genres', 'ageratings'])
	        ->select('id','poster','is_free', 'slug', 'name', 'synopsis', 'trailer', 'content_type', 'detail_poster', 'year', 'age_rating_id')
	        ->whereHas('genres', function($q) use($request) {
	       		$q->where('genre.status', 1);
			})
			->whereNotIn('id',$restricted_content_ids)
			->whereNotIn('age_rating_id',$user_age_restriction_ids)
	        ->where('status','!=',2)
	        ->where('content_type',$content_type)
	        ->orderBy('content.id','desc');

        if($user->is_child == 1){
        	$main = $main->whereIn('age_rating_id',$kids_age_ids);
        }else{
        	if(isset($adv_content_data) && !empty($adv_content_data) && !in_array($adv_content_data, $restricted_content_ids)){

				$content_data = Content::find($adv_content_data);

				if($content_data->content_type == $content_type && !in_array($content_data->age_rating_id, $user_age_restriction_ids)){
					$main = $main->where('id',$content_data->id)->where('content_type',$content_type);
				}else{
					$main = $main->where('content_type',$content_type);
				}
	        }
        }
        
        $main_data = $main->first();
        if($main_data){
        	$content['main'] = new MainContentResource($main_data);
        }else{
        	$content['main'] = [];
        }
        

		if($user->is_child == 1){
			$genre_data = Genre::with(['content' => function($q) use($content_type,$restricted_content_ids,$user_age_restriction_ids,$kids_age_ids){
							    $q->whereNotIn('content.id',$restricted_content_ids)->whereNotIn('age_rating_id',$user_age_restriction_ids)->where('content.content_type', $content_type)->whereIn('age_rating_id',$kids_age_ids)->where('status','!=',2)->orderBy('content.is_free','desc');
							}])->with(['user_genres' => function($q) use($request){
							    $q->where('user_genres.user_id', $request->user()->id)->orderBy('user_genres.id','asc');
							}])->where('status', 1)->get();
		}else{
			$genre_data = Genre::with(['content' => function($q) use($content_type,$restricted_content_ids,$user_age_restriction_ids){
							    $q->whereNotIn('content.id',$restricted_content_ids)->whereNotIn('age_rating_id',$user_age_restriction_ids)->where('content.content_type', $content_type)->where('status','!=',2)->orderBy('content.is_free','desc');
							}])->with(['user_genres' => function($q) use($request){
							    $q->where('user_genres.user_id', $request->user()->id)->orderBy('user_genres.id','asc');
							}])->where('status', 1)->get();
		}

	
		if(count($genre_data)>0){

			//return $genre_data;
			$content['category'] = array();

			$preferred_languages_array = $general_array = $temp_preferred_languages_array = array();

			foreach ($genre_data as $key => $value) {

					if(!empty($value['content']) && count($value['content'])>0){
						$content['category'][$key]['id'] = $value['id'];
						$content['category'][$key]['name'] = $value['name'];
					}

					if(!empty($value->user_genres) && $value->user_genres->genre_id == $value['id']){

						$temp_preferred_languages_array['id'] = $value['id'];
						$temp_preferred_languages_array['name'] = $value['name'];
						$temp_preferred_languages_array['content'] = array();
						$temp_preferred_languages_array['view_more'] = FALSE;

						if(!empty($value['content']) && count($value['content'])>0){

							$temp_content_array = $content_array = array();

							foreach ($value['content'] as $ckey => $cvalue) {

							
								if(count($content_array) <= 5) {
									
									$temp_content_array['id'] = $cvalue['id'];
									$temp_content_array['poster'] = $cvalue['poster'];
									$temp_content_array['is_free'] = $cvalue['is_free'];
									$temp_content_array['slug'] = $cvalue['slug'];

									array_push($content_array,$temp_content_array);
								}else{
									$temp_preferred_languages_array['view_more'] = TRUE;
								}


							}
							$temp_preferred_languages_array['content'] = $content_array;

							array_push($preferred_languages_array,$temp_preferred_languages_array);

						}else{
							unset($temp_preferred_languages_array);
						}

					}else{
						$temp_preferred_languages_array['id'] = $value['id'];
						$temp_preferred_languages_array['name'] = $value['name'];
						$temp_preferred_languages_array['content'] = array();
						$temp_preferred_languages_array['view_more'] = FALSE;


						if(!empty($value['content']) && count($value['content'])>0){

							$temp_content_array = $content_array = array();

							foreach ($value['content'] as $ckey => $cvalue) {

							
								if(count($content_array) <= 5) {
									
									$temp_content_array['id'] = $cvalue['id'];
									$temp_content_array['poster'] = $cvalue['poster'];
									$temp_content_array['is_free'] = $cvalue['is_free'];
									$temp_content_array['slug'] = $cvalue['slug'];

									array_push($content_array,$temp_content_array);
								}else{
									$temp_preferred_languages_array['view_more'] = TRUE;
								}


							}
							$temp_preferred_languages_array['content'] = $content_array;
							
							array_push($general_array,$temp_preferred_languages_array);

						}else{
							unset($temp_preferred_languages_array);
						}
						
					}
			}
		}

		$content['category'] = array_values($content['category']);

		//dd($content);
		
		$most_viewed = ActivityLog::select('*',DB::raw("count('content_id') as 'total_content'"))->with(['content' => function($q) use($content_type,$restricted_content_ids,$user_age_restriction_ids,$kids_age_ids){
							    $q->whereNotIn('content.id',$restricted_content_ids)->whereNotIn('age_rating_id',$user_age_restriction_ids)->where('content.content_type', $content_type)->whereIn('age_rating_id',$kids_age_ids)->where('content.status','!=',2)->whereNotNull('content.id')->orderBy('content.id','desc');
							}])
							->wherehas('content',function ($query){
                                    $query->where('status','!=',2);
                                  })
							->groupBy('content_id')->orderBy('total_content','desc')->limit(10)->get();
		if(!empty($most_viewed) && count($most_viewed)>0){
			$i=0;

			$recommendation[0] = array(
				'id' => 0,
				'name' => 'Recommended',
				'content' => [],
				'view_more' => FALSE

			);

			
			foreach ($most_viewed as $key => $value) {
				if(!empty($value['content'])){
					$temp['id'] = $value['content']['id'];
					$temp['poster'] = $value['content']['poster'];
					$temp['is_free'] = $value['content']['is_free'];
					$temp['slug'] = $value['content']['slug'];

					array_push($recommendation[0]['content'], $temp);

				}
			}
		}else{
			$recommendation = array();
		}


		//return $preferred_languages_array;

		$content['data'] = array_merge($recommendation, $preferred_languages_array,$general_array);

		if($content){
			return response([
                'status' => true,
                'message' => '',
                'data' => $content,
                'app_string' => __('message.home')
            ]);
        }else{
            return response([
                'status' => false,
                'message' => __('message.no_data_found'),
                'app_string' => __('message.home')
            ]);
        }

	}

	public function view_all(Request $request){
		
		$validator = Validator::make(request()->all(), [
            'genres_id' => 'required',
            'content_type' => 'required|digits_between:1,2'
        ]);

		if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }

        $user = auth()->user();

		$content_type = $request->content_type;
		
		$restricted_content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
		$user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();
		$kids_age_ids = AgeRatings::where('children',1)->get()->pluck('id')->toArray();


        $content = Content::with(['genres'])
        ->select('id','poster','is_free', 'slug')
        ->whereHas('genres', function($q) use($request) {
       		$q->where('genre.id', $request->genres_id);
       		$q->where('genre.status', 1);
		})
		->whereNotIn('id',$restricted_content_ids)
		->whereNotIn('age_rating_id',$user_age_restriction_ids)
        ->where('status','!=',2)
        ->where('content_type',$content_type)
        ->orderBy('content.is_free','desc');

        if($user->is_child == 1){
        	$content = $content->whereIn('age_rating_id',$kids_age_ids);
        }
        
        $content = $content->paginate(16);

		if($content){
			return response([
                'status' => true,
                'message' => '',
                'data' => $content
            ]);
        }else{
            return response([
                'status' => false,
                'message' => __('message.no_data_found')
            ]);
        }

	}

	public function category_vise_content(Request $request){
		
		$validator = Validator::make(request()->all(), [
            'genres_id' => 'required'
        ]);

		if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }

        $genre = Genre::find($request->genres_id);
		$adv_content_data = $genre->content_id;

		$restricted_content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
		$user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

		$user = auth()->user();

		if(isset($user->is_child) && !empty($user->is_child) && $user->is_child == 1){

			$main = Content::with(['genres','ageratings'])
		        ->select('id','poster','is_free', 'slug', 'name', 'synopsis', 'trailer', 'content_type')
		        ->whereHas('genres', function($q) use($request) {
		       		$q->where('genre.status', 1);
				})
				->whereHas('ageratings', function($q) use($request) {
		       		$q->where('children',1);
				})
				->whereNotIn('id',$restricted_content_ids)
				->whereNotIn('age_rating_id',$user_age_restriction_ids)
		        ->where('status','!=',2);

	        $main = $main->orderBy('id','desc')->first();
	        $data['main'] = new MainContentResource($main);

	        $data['recommendation'] = Content::with(['genres','ageratings'])
	        ->select('id','poster','is_free', 'slug')
	        ->whereHas('genres', function($q) use($request) {
	       		$q->where('genre.id', $request->genres_id);
	       		$q->where('genre.status', 1);
			})
			->whereHas('ageratings', function($q) use($request) {
		    	$q->where('children',1);
			})
			->whereNotIn('id',$restricted_content_ids)
			->whereNotIn('age_rating_id',$user_age_restriction_ids)
	        ->where('status','!=',2)
	        ->orderBy('content.id','desc')
        	->get();
        	

		}else{

			$main = Content::with(['genres'])
		        ->select('id','poster','is_free', 'slug', 'name', 'synopsis', 'trailer', 'content_type')
		        ->whereHas('genres', function($q) use($request) {
		       		$q->where('genre.status', 1)->where('genre.id', $request->genres_id);
				})
				->whereNotIn('id',$restricted_content_ids)
				->whereNotIn('age_rating_id',$user_age_restriction_ids)
		        ->where('status','!=',2);
	        
	        $main = $main->orderBy('id','desc')->first();

	        $data['main'] = new MainContentResource($main);

	        $data['recommendation'] = Content::with(['genres'])
	        ->select('id','poster','is_free', 'slug')
	        ->whereHas('genres', function($q) use($request) {
	       		$q->where('genre.id', $request->genres_id);
	       		$q->where('genre.status', 1);
			})
			->whereNotIn('id',$restricted_content_ids)
			->whereNotIn('age_rating_id',$user_age_restriction_ids)
	        ->where('status','!=',2)
	        ->orderBy('content.id','desc')
        	->get();

		}

		
		return response([
                'status' => true,
                'message' => '',
                'data' => $data,
                'app_string' => __('message.home')
            ]);

	}

	public function details($id,Request $request){
		
		$restricted_content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
		$user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

		$user = auth()->user();
		$kids_age_ids = AgeRatings::where('children',1)->get()->pluck('id')->toArray();


		$content = Content::with(['ageratings','video.video_subtitle','content_cast.artist','content_genres.genre','content_tags.tags'])
		//->where('id',$id)

		->where(function($query) use ($id){
	        $query->where('id',$id)
	        ->orWhere('slug',$id);
	    })

		->whereNotIn('id',$restricted_content_ids)
		->whereNotIn('age_rating_id',$user_age_restriction_ids)
		->where('status','!=',2);
		
		if($user->is_child == 1){
			$content = $content->whereHas('ageratings', function($q) use($request) { $q->where('children',1); });
		}

		$content = $content->first();
		
		$content_data = $temp_content_array = array();
		if(isset($content) && !empty($content->content_genres)){

			if(isset($request->search) && $request->search == 1){
				if(!empty($content)){
					$content->search = $content->search+1;
					$content->save();
				}
			}
			
			$genre_ids = array(); $content_id = '';
			foreach ($content->content_genres as $key => $value) {
				$genre_ids[] = $value->genre_id;
				$content_id = $value->content_id;
			}

			if($user->is_child == 1){
				
				$more_like_this_data = ContentGenres::with(['content' => function($q) use($restricted_content_ids,$user_age_restriction_ids,$kids_age_ids){ 
					$q->where('status', '!=', 2)
					->whereNotIn('id',$restricted_content_ids)
					->whereNotIn('age_rating_id',$user_age_restriction_ids)
					->whereIn('age_rating_id',$kids_age_ids)
					->orderBy('id','desc');
					}])
				->whereIn('genre_id',$genre_ids)
				->where('content_id','!=',$content_id)
				->groupBy('content_id')
				->orderBy('content_id','desc')
				->limit(9)->get()->toArray();

			}else{
				$more_like_this_data = ContentGenres::with(['content' => function($q) use($restricted_content_ids,$user_age_restriction_ids){ 
					$q->where('content.status', '!=', 2)
						->whereNotIn('content.id',$restricted_content_ids)
						->whereNotIn('age_rating_id',$user_age_restriction_ids)
						->orderBy('content.id','desc');
					}])
				->whereIn('genre_id',$genre_ids)
				->where('content_id','!=',$content_id)
				->groupBy('content_id')
				->orderBy('content_id','desc')->limit(9)->get()->toArray();
			}

			$result = array();
			if(!empty($more_like_this_data) && count($more_like_this_data)>0){
				$i=0;
				foreach ($more_like_this_data as $key => $value) {
					if(!empty($value['content']) && count($value['content'])>0){
						$result[$i]['id'] = $value['content']['id'];
						$result[$i]['poster'] = $value['content']['poster'];
						$result[$i]['is_free'] = $value['content']['is_free'];	
						$result[$i]['slug'] = $value['content']['slug'];
						$i++;
					}
				}
			}
			
			return (new ContentResource($content))
                ->additional([
                'status' => true,
	            'message' => '',
	            'more_like_this' => $result,
	            'app_string' => __('message.content_detail')
            ]);

		}else{
			return response([
                'status' => false,
                'message' => __('message.no_data_found')
            ]);
		}
		
	}

	public function kids(Request $request){

        $restricted_content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
        $user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

        $child_adv_content = CommonHelper::ConfigGet('child_adv_content_id');

			$main = Content::select('id','poster','is_free','age_rating_id')->with(['ageratings'])
	        ->whereHas('ageratings', function($q) use($request) {
	       		$q->where('children',1);
			})
			->whereNotIn('id',$restricted_content_ids)
			->whereNotIn('age_rating_id',$user_age_restriction_ids)
	        ->where('status','!=',2)
	        ->orderBy('content.id','desc');

	     if(!in_array($child_adv_content,$restricted_content_ids)){
	    	if(isset($child_adv_content) && !empty($child_adv_content)){
				$content_data = Content::find($child_adv_content);
				$main = $main->where('id',$content_data->id);
	        } 	
	     }

        $content['main'] = $main->first();
        
        
		$genre_data = Genre::with(['content.ageratings','content' => function($q) use($restricted_content_ids,$user_age_restriction_ids){
					    $q->whereNotIn('content.id',$restricted_content_ids)->whereNotIn('age_rating_id',$user_age_restriction_ids)->orderBy('content.id','desc');
					}])->whereHas('content', function($q){
					       		$q->where('status','!=',2);
							})
					->with(['user_genres' => function($q) use($request){
							    $q->where('user_genres.user_id', $request->user()->id)->orderBy('user_genres.id','asc');
							}])
					->where('status', 1)->get();

		if(count($genre_data)>0){
			
			$content['category'] = array();

			$preferred_languages_array = $general_array = $temp_preferred_languages_array = $temp_general_languages_array = array();

			foreach ($genre_data as $key => $value) {

					$content['category'][$key]['id'] = $value['id'];
					$content['category'][$key]['name'] = $value['name'];
					
					if(!empty($value->user_genres) && $value->user_genres->genre_id == $value['id']){

						$temp_preferred_languages_array['id'] = $value['id'];
						$temp_preferred_languages_array['name'] = $value['name'];
						$temp_preferred_languages_array['content'] = array();
						$temp_preferred_languages_array['view_more'] = FALSE;

						if(!empty($value['content']) && count($value['content'])>0){

							$temp_content_array = $content_array = array();

							foreach ($value['content'] as $ckey => $cvalue) {

								if($cvalue->ageratings->age <= 14){
									if(count($content_array) <= 4) {
										
										$temp_content_array['id'] = $cvalue['id'];
										$temp_content_array['poster'] = $cvalue['poster'];
										$temp_content_array['is_free'] = $cvalue['is_free'];

										array_push($content_array,$temp_content_array);
									}else{
										$temp_preferred_languages_array['view_more'] = TRUE;
									}
								}

							}
							$temp_preferred_languages_array['content'] = $content_array;
							
							if(empty($temp_preferred_languages_array['content'])){
								unset($temp_preferred_languages_array);
							}else{
								array_push($preferred_languages_array,$temp_preferred_languages_array);
							}
							
						}else{
							unset($temp_preferred_languages_array);
						}

					}else{
					
						$temp_general_languages_array['id'] = $value['id'];
						$temp_general_languages_array['name'] = $value['name'];
						$temp_general_languages_array['content'] = array();
						$temp_general_languages_array['view_more'] = FALSE;

						if(!empty($value['content']) && count($value['content'])>0){

							$temp_content_array = $content_array = array();
							foreach ($value['content'] as $ckey => $cvalue) {

								if($cvalue->ageratings->age <= 14){
									
									if(count($content_array) <= 4) {
										
										$temp_content_array['id'] = $cvalue['id'];
										$temp_content_array['poster'] = $cvalue['poster'];
										$temp_content_array['is_free'] = $cvalue['is_free'];

										array_push($content_array,$temp_content_array);
									}else{
										$temp_general_languages_array['view_more'] = TRUE;
									}
								}

							}

							
							$temp_general_languages_array['content'] = $content_array;
							
							if(empty($temp_general_languages_array['content'])){
								unset($temp_general_languages_array);
							}else{
								array_push($general_array,$temp_general_languages_array);	
							}

						}else{
							unset($temp_general_languages_array);
						}
						
						
					}	
			}

		}	

		$content['data'] = array_merge($preferred_languages_array,$general_array);

		if($content){
			return response([
                'status' => true,
                'message' => '',
                'data' => $content,
                'app_string' => __('message.home')
            ]);
        }else{
            return response([
                'status' => false,
                'message' => __('message.no_data_found'),
                'app_string' => __('message.home')
            ]);
        }

	}

	public function kids_view_all(Request $request){
		
		$validator = Validator::make(request()->all(), [
            'genres_id' => 'required',
        ]);

		if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }
        
        $restricted_content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
        $user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

        $content = Content::with(['genres','ageratings'])
        ->select('id','poster','is_free','age_rating_id')
        ->whereHas('genres', function($q) use($request) {
       		$q->where('genre.id', $request->genres_id);
       		$q->where('genre.status', 1);
		})
		->whereHas('ageratings', function($q) use($request) {
	       		$q->where('children',1);
		})
		->whereNotIn('id',$restricted_content_ids)
		->whereNotIn('age_rating_id',$user_age_restriction_ids)
        ->where('status','!=',2)
        ->orderBy('content.id','desc')
        ->paginate(16);

		if($content){
			return response([
                'status' => true,
                'message' => '',
                'data' => $content
            ]);
        }else{
            return response([
                'status' => false,
                'message' => __('message.no_data_found')
            ]);
        }

	}

	public function search(Request $request)
    {	
    	
        $validator = Validator::make(request()->all(), [
            'filter_string' => 'required|min:3'
        ]);

         if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }

        $request->filter_string =trim(str_replace("%"," ",$request->filter_string));

        $string_len = strlen($request->filter_string);
        if($string_len < 2){
        	return response([
                'status' => false,
                'message' => __('message.no_data_found')
            ]);
        }
        
        $user = auth()->user();

		$kids_age_ids = AgeRatings::where('children',1)->get()->pluck('id')->toArray();

        $content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
        
        $user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

        $content = Content::with(['genres','ageratings'])->where('name','LIKE','%'.$request->filter_string.'%')->whereNotIn('id',$content_ids)->whereNotIn('age_rating_id',$user_age_restriction_ids)->where('status','!=',2);
        
        if($user->is_child == 1){
			$content = $content->whereHas('ageratings', function($q) use($request) { $q->where('children', 1); });
		}

        if(!empty($request->genres_id)){
			$content = $content->whereHas('genres', function($g) use($request) {
       			$g->where('genre_id', $request->genres_id);
			});
		}

		$content = $content->orderBy('id','desc')->get();

        $content_search_res = array();
    	if(!empty($content)){
			foreach ($content as $key => $value) {
				$content_search_res[$key]['id'] = $value['id'];
				$content_search_res[$key]['name'] = $value['name']; 
				$content_search_res[$key]['slug'] = $value['slug']; 
			}
    	}

    	$content_search_ids = array_column($content_search_res, 'id');

        $category_data = Content::with(['genres','ageratings'])
        ->whereHas('genres', function($q) use($request) {
       		$q->where('genre.name', 'LIKE','%'.$request->filter_string.'%');
       		$q->where('genre.status', 1);
		})
		->whereNotIn('id',$content_ids)
		->whereNotIn('id',$content_search_ids)
		->whereNotIn('age_rating_id',$user_age_restriction_ids)
        ->where('status','!=',2);
        if($user->is_child == 1){
			$category_data = $category_data->whereHas('ageratings', function($q) use($request) { $q->where('children', 1); });
		}

		if(!empty($request->genres_id)){
			$category_data = $category_data->whereHas('genres', function($q) use($request) {
       			$q->where('genre_id', $request->genres_id);
			});
		}

        $category_data = $category_data->orderBy('content.id','desc')->get();

        $cat_search_res = array();
    	if(!empty($category_data)){
			foreach ($category_data as $key => $value) {
				$cat_search_res[$key]['id'] = $value->id;
				$cat_search_res[$key]['name'] = $value->name; 
				$cat_search_res[$key]['slug'] = $value->slug; 
			}
    	}

    	$cat_search_ids = array_column($cat_search_res, 'id');

        $artist_data = Content::with(['genres','content_cast.artist','ageratings'])
        ->whereHas('content_cast.artist', function($q) use($request) {
       		$q->where('name', 'LIKE','%'.$request->filter_string.'%');
		})
		->whereNotIn('id',$content_ids)
		->whereNotIn('id',$content_search_ids)
		->whereNotIn('id',$cat_search_ids)
		->whereNotIn('age_rating_id',$user_age_restriction_ids)
        ->where('status','!=',2);
        if($user->is_child == 1){
			$artist_data = $artist_data->whereHas('ageratings', function($q) use($request) { $q->where('children', 1); });
		}

        $artist_data = $artist_data->orderBy('content.id','desc')->get();
        
        $artist_search_res = array();
    	if(!empty($artist_data)){
			foreach ($artist_data as $key => $value) {
				$artist_search_res[$key]['id'] = $value->id;
				$artist_search_res[$key]['name'] = $value->name; 
				$artist_search_res[$key]['slug'] = $value->slug; 
			}
    	}
    	
    	$response = array_merge($content_search_res, $cat_search_res, $artist_search_res); 
    	
		return response([
            'status' => true,
            'message' => '',
            'data' => $response
        ]);
    	
    }

    public function category_search(Request $request)
    {	
    	
        $validator = Validator::make(request()->all(), [
            'filter_string' => 'required|min:3',
            'genre_id' => 'required'
        ]);

         if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }

        $request->filter_string =trim(str_replace("%"," ",$request->filter_string));

        $string_len = strlen($request->filter_string);
        if($string_len < 3){
        	return response([
                'status' => false,
                'message' => __('message.no_data_found')
            ]);
        }
        
        $user = auth()->user();

		$kids_age_ids = AgeRatings::where('children',1)->get()->pluck('id')->toArray();

        $content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
        
        $user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

        $content = Content::with(['genres','ageratings'])->where('name','LIKE','%'.$request->filter_string.'%')->whereNotIn('id',$content_ids)
        	->whereNotIn('age_rating_id',$user_age_restriction_ids)->where('status','!=',2)
        	->whereHas('genres', function($g) use($request) {
       			$g->where('genre_id', $request->genre_id);
			});;
        
        if($user->is_child == 1){
			$content = $content->whereHas('ageratings', function($q) use($request) { $q->where('children', 1); });
		}

		$content = $content->orderBy('id','desc')->get();

        $content_search_res = array();
    	if(!empty($content)){
			foreach ($content as $key => $value) {
				$content_search_res[$key]['id'] = $value['id'];
				$content_search_res[$key]['name'] = $value['name']; 
			}
    	}

		return response([
            'status' => true,
            'message' => '',
            'data' => $content_search_res
        ]);
    	
    }

    public function watching(Request $request){
    	
    	$validator = Validator::make(request()->all(), [
            'user_id' => 'required',
            'parent_id' => 'required',
            'season_id' => 'required',
            'content_id' => 'required',
            'video_id' => 'required',
            'seconds' => 'required',
            'udid' => 'required',
            'device_name' => 'required',
            'plan_id' => 'required'
        ]);

         if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }

    	$user = $request->user();

    	if($request->plan_id > 0){
    		$attribute = PlanAttribute::wherePlanId($request->plan_id)->where('attribute_id', 5)->first();
			$allowed_screens = (int)$attribute->value;
			$allowed_screens = $allowed_screens - 1;
    	}else{
    		$allowed_screens = 0;
    	}

    	$user_id = $request->user_id;
    	$parent_id = $request->parent_id;

    	if($request->parent_id == 0){
    		$users = User::where(function($q) use ($user_id) {
					          $q->where('id', $user_id)
					            ->orWhere('parent_id', $user_id);
					      })->where('status',1)->get()->pluck('id')->toArray();
    	}else{
    		$users = User::where(function($q) use ($user_id, $parent_id) {
					          $q->where('id', $parent_id)
					            ->orWhere('parent_id', $parent_id);
					      })->where('status',1)->get()->pluck('id')->toArray();
    	}
	


		$today = Carbon::now();
		$created_date_time = $today->subSeconds(10)->format('Y-m-d H:i:s');

		$streaming = Streaming::where(function($q) use ($created_date_time) {
					          $q->where('updated_at', '>=', $created_date_time)
					            ->orWhere('created_at', '>=', $created_date_time);
					      })->whereIn('user_id', $users)->with(['content'])
							->groupBy('udid')->where('udid', '!=', $request->udid)->get();

		if(count($streaming) > $allowed_screens){
			$contents = $streaming->pluck('content');
			$stream_data = [];
			foreach ($streaming as $key => $value) {
				$temp['device_name'] = $value->device_name;
				$temp['name'] = $value->content['name'];
				array_push($stream_data, $temp);
			}

			$response = array(
				'user_id'=> (int)$user_id,
	            'content_id'=> (int)$request->content_id,
	            'season_id' => (int)$request->season_id,
	            'video_id' => (int)$request->video_id,
	            'udid'=> $request->udid,
	            'message' => "stop_watching",
	            'watching_data' => $stream_data
			);

			return response([
	            'status' => FALSE,
	            'message' => 'stop_watching',
	            'data' => $response
	        ]);
		}else{
			$stream_data = Streaming::where('user_id', $user_id)->where('video_id', $request->video_id)->first();
			if($stream_data){

				if($stream_data->seconds != $request->seconds){
					if($request->seconds == 0){
						$seconds = 1;
					}else{
						$seconds = $request->seconds;
					}

					$update = array(
						'seconds' => $seconds,
						'udid' => $request->udid,
						'device_name' => $request->device_name
					);
					$updated_data = Streaming::where('user_id', $user_id)->where('video_id', $request->video_id)->update($update);

					return response([
			            'status' => TRUE,
			            'message' => 'seconds updated'
			        ]);
				}else{
					return response([
			            'status' => FALSE,
			            'message' => 'Same seconds'
			        ]);
				}
			}else{

				if($request->seconds == 0){
					$seconds = 1;
				}else{
					$seconds = $request->seconds;
				}

				$today = Carbon::now();
				$today_ymdhis = $today->format('Y-m-d H:i:s');

				$MediaCovertLogs = Streaming::create([
                    'user_id' => $user_id,
                    'content_id' => $request->content_id,
                    'season_id' => $request->season_id,
                    'video_id' => $request->video_id,
                    'seconds' => $seconds,
                    'udid' => $request->udid,
                    'device_name' => $request->device_name,
                    'created_at' => $today_ymdhis
                ]);

                $ActivityLog = ActivityLog::create([
                    'user_id' => $user_id,
                    'content_id' => $request->content_id,
                    'video_id' => $request->video_id
                ]);

                return response([
			            'status' => TRUE,
			            'message' => 'streaming data added'
			        ]);
			}
		}
    }

}