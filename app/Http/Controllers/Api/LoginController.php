<?php



namespace App\Http\Controllers\Api;



use App\Http\Controllers\Controller;

use App\Http\Requests\Api\LoginRequest;

use App\Http\Requests\Api\SocialLoginRequest;

use App\Http\Resources\Api\AppleDetailResource;

use App\Http\Resources\UserResource;

use App\Models\AppleDetail;

use App\Models\User;

use CommonHelper;

use App\Models\Devices;
use App\Models\UsersGroup;
use App\Traits\ApiResponser;
use App\Traits\StripeTraits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;

use App\Mail\BulkMail;

use Mail;
use Illuminate\Support\Facades\Hash;

use NotificationHelper;
use Carbon\Carbon;

use App\Models\Plan;
use App\Models\MultiLanguageKey;
use App\Models\MultiLanguageValue;

class LoginController extends Controller

{
    use ApiResponser, StripeTraits;

    /**
     *  logs in user with email or password
     */

    public function login(LoginRequest $request)
    {
        if (!Auth::attempt(['email' => $request->username, 'password' => $request->password]) && !Auth::attempt(['username' => $request->username, 'password' => $request->password])) {

            return response()->json([

                'status' => false,

                'message' => 'Invalid login details'

            ], 200);

        }

        // $user = User::where('email', $request->email)->firstOrFail();

        // $user->expireAllTokens();

        // $user->device_token = $request->device_token;

        // $user->update();

        // $token = $user->createToken('authToken')->accessToken;

        $user = User::find($user_id);

        $token = $user->createToken('authToken')->accessToken;




        return (new UserResource($user))->additional([

            'status' => 'true',

            'bearer_token' => $token,

            'message' => 'Logged In successfully'

        ]);

    }



    /**

     *  logs in social login user

     */

    // public function socialLogin(SocialLoginRequest $request)
    
    public function socialLogin(Request $request)
    {       

        $validator = Validator::make($request->all(),[
            'social_id' => 'required',
            'social_type' => 'required'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ], 200);
        }

            $user = User::where('social_id',request()->input('social_id'))->where('social_type',request()->input('social_type'))->first();        
            
            if(!empty($user)){
                if($user->status == 1){
                    $user->token = $user->createToken('auth_token')->accessToken;
            
                    return (new UserResource($user))->additional([
                        'status' => true,
                        'message' =>  __('message.api.login_success'),
                    ]);
                }elseif ($user->status == 2 && !is_null($user->deactivated_time)) {
                    return response([
                        'status' => false,
                        'message' =>  __('message.api.deleted_account')
                    ], 200);
                }else{
                    return response([
                        'status' => false,
                        'message' =>  __('message.api.account_deactivated')
                    ], 200);
                }
                

            }else{
                return response()->json([                
                        'status' => false,                    
                        'message' => __('message.api.register_account_website')
                    ]);
            }
    }

    public function websiteSocialLogin(Request $request)
    {       

        $validator = Validator::make($request->all(),[
            'first_name' => 'required|max:50',
            'last_name' => 'max:50',
            'email' => 'required|email',
            'device_type' => 'required',
            'device_token' => 'required',
            'social_id' => 'required',
            'social_type' => 'required',
            //'phone' => 'required',
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ], 200);
        }


        $social_id = request()->input('social_id');        
        $social_type = request()->input('social_type');

        $user = User::where('social_id', $social_id)->where('social_type', $social_type)->first();        
        //if the user already exists
        
        if ($user) {

            if($user->status == '0'){
                return response()->json([                
                    'status' => false,                    
                    'message' =>  __('message.api.account_deactivated')                
                ]);
            }else if($user->status == '2'){
                return response()->json([                
                    'status' => false,                    
                    'message' => __('message.api.account_deactivated')                    
                ]);
            }else{                
                /*$devices = Devices::where('user_id', $user->id)->update([
                    'name' => '',
                    'token' => $deviceToken,
                    'type' => $deviceType
                ]);*/
                $accessToken = $user->createToken('authToken')->accessToken;
                $user->token = $accessToken;
                return (new UserResource($user))->additional([
                    'status' => true,
                    'message' => __('message.api.login_success')
                ]);

            }

        }else{

            $user = User::where('email', $request->email)->first();
            if($user){
                $accessToken = $user->createToken('authToken')->accessToken;
                $user->token = $accessToken;
                return (new UserResource($user))->additional([
                    'status' => true,
                    'message' => __('message.api.login_success')
                ]);
            }
            
            /*($duplicate_email_count > 0){
                return response([
                            'status' => false,
                            'message' => "Email already associated with another account"
                        ]);
            }*/

            $user_data = [];
            $user_data['password'] = Hash::make($request->email);
            $user_data['first_name'] = $request->first_name;
            $user_data['last_name'] = "";
            $user_data['email'] = $request->email;
            $user_data['email_verified_at'] = Carbon::now();
            $user_data['country_code'] = '+1';
            $user_data['currency_code'] = 'USD';
            $user_data['phone'] = '';
            $user_data['phone_verified'] = 0;
            $user_data['status'] = 1;
            $user_data['social_type'] = $request->social_type;
            $user_data['social_id'] = $request->social_id;
            $user_data['reference_code'] = "AWE".CommonHelper::random_number();
            $user_data['referral_expiration_date'] = Carbon::now()->addDays(CommonHelper::ConfigGet('referral_code_expiration_days'))->format('Y-m-d');
            $user_data['used_referral_code'] = '';

            $user_id = User::insertGetId($user_data); 

            UsersGroup::create([
                'user_id' => $user_id,
                'group_id' => '4',
            ]);

            $user = User::find($user_id);
            $result = $this->createCustomer($user);

            if($result['status'] == TRUE){
                $user->stripe_id = $result['data']->id;
                $user->save();

                $user = User::find($user_id);
                $accessToken = $user->createToken('authToken')->accessToken;
                $user->token = $accessToken;

                Mail::to($request->email)->send(new BulkMail(config('app.name'), $request->first_name, $request->email, config('app.address1'), config('app.address2')));
            
                /*if(isset($request->profile_picture) && $request->profile_picture != ''){
                    $dir = "images/users/";
                    $upload = CommonHelper::imageUploadByLink($request->profile_picture, $dir);
                    if($upload['status'] == TRUE){
                        $profile_picture = $upload['image_name'];
                        User::where('id', $user->id)->update(['profile_picture' => $profile_picture]);
                        $user->profile_picture = $profile_picture;
                    }
                    
                }*/

                if ($social_type == "apple") {
                    //if it is apple the registering the apple user
                    $this->registerAppleUser($request->first_name, null, $request->email, $social_id);
                    
                }

                return (new UserResource($user))->additional([
                    'status' => true,
                    'message' => __('message.api.register_success')
                ]);

            }else{

                $user->delete();
                UsersGroup::where('user_id', $user_id)->delete();
                return response([
                    'status' => false,
                    'message' => $result['message']
                ]);
            }

        }

    }
    
    /**

     *  registers apple user or updates it

     */

    public function registerAppleUser($firstName = null, $lastName = null, $email, $appleId)
    {

        $appleUser = AppleDetail::where('apple_id', $appleId)->first();
        if ($appleUser) {
            //if user exists then return this
            return $appleUser;
        }
        return AppleDetail::create([
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'apple_id' => $appleId,
        ]);
        //else return the new apple user
    }

    /**

     *  returns the apple account details

     */
    public function appleDetails(Request $request, $appleId)
    {
        $accountDetails =  AppleDetail::where('apple_id', $appleId)->first();
        if (!$accountDetails) {
            return response()->json([
                'status' =>  false,
                'message' => __('There was an error connecting to the Apple ID server. Go to Setting -> Password & Security->App using your Apple ID->'.env("APP_NAME").'->Stop using apple ID')

            ]);

        }

        return (new AppleDetailResource($accountDetails))->additional([
            'status' =>  true,
            'message' => 'successfully'

        ]);

    }

    public function main_screens_strings()
    {
        $data = MultiLanguageKey::with(['multi_language_value'])->get();

        $result = array();
        if(!empty($data) && count($data)>0) {
            
            foreach ($data as $key => $value) {
                $result[$value['group']][$value['language_key']] = !empty($value['multi_language_value']['language_value'])?$value['multi_language_value']['language_value']:'';
            }
            
        }

        return response([
            'status' => true,
            'data' => $result,
        ]);
    }



    /**

     *  logs user out

     */

    public function logOut()

    {

        $user = request()->user();

        $user->device_token = null;

        $user->update();

        //remove the device details



        $user->currentAccessToken()->delete();

        //revoke current token



        return response()->json([

            'status' =>  true,

            'message' => __('messages.api.logged_out')

        ]);

    }

}