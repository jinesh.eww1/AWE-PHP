<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\MyList;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use NotificationHelper;


class NotificationController extends Controller {

    use ApiResponser;


    public function index() {

        $user = auth()->user();

        $notification = Notification::where('user_id',$user->id)->orderBy('id','desc')->get();

        if($notification){
            return response([
                'status' => true,
                'message' => '',
                'data' => $notification
            ]);
        }else{
            return response([
                'status' => false,
                'message' => __('message.no_data_found')
            ]);
        }

    }

}
