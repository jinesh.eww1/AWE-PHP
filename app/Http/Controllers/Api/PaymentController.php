<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\PaymentHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Traits\ApiResponser;
use App\Traits\StripeTraits;
use App\Models\Plan;
use Carbon\Carbon;

use Srmklive\PayPal\Services\PayPal as PayPalClient;

class PaymentController extends Controller {

    use ApiResponser, StripeTraits;

    public function stripe_plan_purchase(Request $request) {

        $validator = Validator::make(request()->all(), [
            'name' => 'required',
            'number' => 'required',
            'exp_month_year' => 'required',
            'cvv' => 'required',
            'plan_id' => 'required',
            'plan_type' => 'required'

        ]);

        if (!$validator->fails()){

            $user = $request->user();

            if(!is_null($user->stripe_id) && !is_null($user->currency_code)){

                $payment['name'] = $request->name;
                $payment['number'] = $request->number;
                $payment['exp_month_year'] = $request->exp_month_year;
                $payment['cvv'] = $request->cvv;
                $payment['email'] = $user->email;
                $payment['postal_code'] = $user->zipcode;
                $payment['name'] = $user->name;
                $payment['user_id'] = $user->id;

                $result = $this->createPaymentMethod($payment, $user);

                if($result['status'] == TRUE){
                    $pm_id = $result['response']->id;

                    $plan = Plan::find($request->plan_id);
                    $country = Country::where('dialing_code',$user->country_code)->with(['currency'])->first();
                    $price = Price::where('country_id', $country->id)->where('plan_id', $request->plan_id)->first();
                    
                    $stripe_price_id = ($request->plan_type == '1')?$price->stripe_monthly_id:$price->stripe_yearly_id;
                    $amount = ($request->plan_type == '1')?$price->monthly_price:$price->yearly_price;

                    $user = User::find($user->id);

                    $result = $this->setSubscription($user, $pm_id, $stripe_price_id);

                    if($result['status'] == TRUE){

                        $end_date_obj = ($request->plan_type == '1')?Carbon::now()->addDays(30):Carbon::now()->addDays(365);
                        $end_date = $end_date_obj->format('Y-m-d');

                        $user->expiration_date = $end_date;
                        $user->plan_id = $request->plan_id;
                        $user->plan_type = $request->plan_type;
                        $user->save();

                        $update_other_data['expiration_date'] = $end_date;
                        $update_other_data['plan_id'] = $request->plan_id;
                        $update_other_data['plan_type'] = $request->plan_type;
                        User::where('parent_id', $user->id)->update($update_other_data);

                        $insert_data = array(
                            'user_id' => $user->id,
                            'mode' => 'stripe',
                            'plan_id' => $request->plan_id,
                            'type' => $request->plan_type,
                            'transaction_id' => $result['response']['id'],
                            'amount' => $amount,
                            'response' => json_encode($result)
                        );

                        PaymentHistory::create($insert_data);

                        $expiration_date = Carbon::parse($end_date)->format('d F Y');
                        $response = array(
                            'plan_id' => $request->plan_id,
                            'plan_type' => $request->plan_type,'expiration_date_formatted' => $expiration_date,
                            
                        );

                        return $this->successResponse($response, __('message.payment.payment_successful'));
                    }else{

                        $insert_data = array(
                            'user_id' => $user->id,
                            'mode' => 'stripe',
                            'plan_id' => $request->plan_id,
                            'type' => $request->plan_type,
                            'transaction_id' => '0',
                            'amount' => 0,
                            'response' => json_encode($result)
                        );

                        PaymentHistory::create($insert_data);
                        // /$result['message'] = "Invalid card";
                        dd($result);
                        return $this->errorResponse($result['message']);
                    }

                }else{
                    return $this->errorResponse($result['message']);
                }

            }else{
                return $this->errorResponse(__('message.payment.account_not_found'));
            }
        }
        return $this->errorResponse($validator->messages(), true);

    }

    public function cancel_plan() {

        $user = Auth::user();

        $payment_history = PaymentHistory::where('user_id',$user->id)->orderBy('id','desc')->first();
        if($payment_history){
            $mode = $payment_history->mode;
            if($mode == 'stripe'){
                $result = $this->cancelCurrentSubscription($payment_history->transaction_id);
                if($result['status'] == TRUE){

                    $user->plan_id = NULL;
                    $user->plan_type = NULL;
                    $user->expiration_date = NULL;
                    $user->save();
                    return $this->successResponse([], __('message.payment.plan_cancelled'));
                }else{
                    return $this->errorResponse($result['message']);
                }
            }else if($mode == 'paypal'){

                //$plan_data = json_decode($payment_history->response,true);

                $subscription_id = $payment_history->transaction_id;
                $provider = new PayPalClient;
                $provider->getAccessToken();   
                $result = $provider->cancelSubscription($subscription_id, 'Cancel plan via API');

                if(isset($result['type']) && $result['type'] == 'error'){

                    return response([
                        'status' => FALSE,
                        'message' => 'You can not cancel the plan.'
                    ]);
                }else{
                    
                    $user->plan_id = NULL;
                    $user->plan_type = NULL;
                    $user->expiration_date = NULL;
                    $user->save();

                    return $this->successResponse([], __('message.payment.plan_cancelled'));
                }

            }else{
                return ([
                    'status' => FALSE,
                    'message' => __('message.something_went_wrong')
                ]);
            }
        }else{
            return ([
                    'status' => FALSE,
                    'message' => __('No any current plan found')
                ]);
        }

    }

    public function stripe_plan_change(Request $request) {

        $validator = Validator::make(request()->all(), [
            'plan_id' => 'required',
            'plan_type' => 'required'

        ]);

        if (!$validator->fails()){

            $user = $request->user();

            if(!is_null($user->stripe_id)){

                $plan = Plan::find($request->plan_id);
                $country = Country::where('dialing_code',$user->country_code)->with(['currency'])->first();
                $price = Price::where('country_id', $country->id)->where('plan_id', $request->plan_id)->first();
                
                $stripe_price_id = ($request->plan_type == '1')?$price->stripe_monthly_id:$price->stripe_yearly_id;
                
                $result = $this->changeSubscription($user, $stripe_price_id);

                if($result['status'] == TRUE){

                    $end_date_obj = ($request->plan_type == '1')?Carbon::now()->addDays(30):Carbon::now()->addDays(365);
                    $amount = ($request->plan_type == '1')?$price->monthly_price:$price->yearly_price;
                    $end_date = $end_date_obj->format('Y-m-d');

                    $user->plan_id = $request->plan_id;
                    $user->plan_type = $request->plan_type;
                    $user->expiration_date = $end_date;
                    $user->save();

                    $expiration_date = Carbon::parse($end_date)->format('d F Y');

                    $insert_data = array(   
                            'user_id' => $user->id,  
                            'mode' => 'stripe', 
                            'plan_id' => $request->plan_id, 
                            'type' => $request->plan_type,  
                            'transaction_id' => $result['response']['stripe_id'],
                            'amount' => $amount,
                            'response' => json_encode($result)  
                        );  
                    PaymentHistory::create($insert_data);
                    
                    $response = array(
                        'plan_id' => $request->plan_id,
                        'plan_type' => $request->plan_type,
                        'expiration_date_formatted' => $expiration_date,
                    );

                    return $this->successResponse($response, __('message.payment.plan_puchased'));
                }else{
                    return $this->errorResponse($result['message']);
                }

            }else{
                return $this->errorResponse(__('message.payment.account_not_found'));
            }
        }
        return $this->errorResponse($validator->messages(), true);

    }

    public function plan_change_new_card(Request $request) {

        $validator = Validator::make(request()->all(), [
            'name' => 'required',
            'number' => 'required',
            'exp_month_year' => 'required',
            'cvv' => 'required',
            'plan_id' => 'required',
            'plan_type' => 'required'
        ]);

        if (!$validator->fails()){

            $user = $request->user();

            if(!is_null($user->stripe_id)){

                $payment['name'] = $request->name;
                $payment['number'] = $request->number;
                $payment['exp_month_year'] = $request->exp_month_year;
                $payment['cvv'] = $request->cvv;
                $payment['email'] = $user->email;
                $payment['postal_code'] = $user->zipcode;
                $payment['name'] = $user->name;
                $payment['user_id'] = $user->id;

                $result = $this->createPaymentMethod($payment);
                if($result['status'] == TRUE){
                    $pm_id = $result['response']->id;


                    $plan = Plan::find($request->plan_id);
                    $country = Country::where('dialing_code',$user->country_code)->with(['currency'])->first();
                    $price = Price::where('country_id', $country->id)->where('plan_id', $request->plan_id)->first();
                    
                    $stripe_price_id = ($request->plan_type == '1')?$price->stripe_monthly_id:$price->stripe_yearly_id;
                    $result = $this->changeSubscription($user, $stripe_price_id);

                    if($result['status'] == TRUE){

                        $end_date_obj =  Carbon::now()->addDays(30);
                        $end_date = $end_date_obj->format('Y-m-d');

                        $amount = ($request->plan_type == '1')?$price->monthly_price:$price->yearly_price;
                        $user->expiration_date = $end_date;
                        $user->plan_id = $request->plan_id;
                        $user->plan_type = $request->plan_type;
                        $user->save();
                        $insert_data = array(
                            'user_id' => $user->id,
                            'mode' => 'stripe',
                            'plan_id' => $request->plan_id,
                            'type' => $request->plan_type,
                            'transaction_id' => $result['response']['stripe_id'],
                            'amount' => $amount,
                            'response' => json_encode($result)
                        );

                        PaymentHistory::create($insert_data);

                        $expiration_date = Carbon::parse($end_date)->format('d F Y');
                        $response = array(
                            'plan_id' => $request->plan_id,
                            'plan_type' => $request->plan_type,
                            'expiration_date_formatted' => $expiration_date,
                        );

                        return $this->successResponse($response, __('message.payment.payment_successful'));
                    }else{

                        $insert_data = array(
                            'user_id' => $user->id,
                            'mode' => 'stripe',
                            'plan_id' => $request->plan_id,
                            'type' => $request->plan_type,
                            'transaction_id' => $result['response']['stripe_id'],
                            'amount' => 0,
                            'response' => json_encode($result)
                        );

                        PaymentHistory::create($insert_data);

                        return $this->errorResponse($result['message']);
                    }

                }else{
                    return $this->errorResponse($result['message']);
                }

            }else{
                return $this->errorResponse(__('message.payment.account_not_found'));
            }
        }
        return $this->errorResponse($validator->messages(), true);

    }

    public function get_prev_payment_card() {

        $user = Auth::user();
        if($user->stripe_id != ''){
            $payment_method = $this->getPaymentMethod($user);

            if($payment_method['status'] == TRUE && ($payment_method['response'])){

                $card = $payment_method['response']->card;

                $data = array(
                    'brand' => $card->brand,
                    'exp_month_year' => $card->exp_month.'/'.$card->exp_year,
                    'account_number_last4' => $card->last4,
                );

                return response([
                    'status' => TRUE,
                    'data' => $data,
                    'message' => ''
                ]);
            }else{
                return response([
                    'status' => FALSE,
                    'message' => 'No any payment card found'
                ]);
            }
            
        }else{
            return response([
                'status' => FALSE,
                'message' => 'No any payment card found'
            ]);
        }
        

    }

}

