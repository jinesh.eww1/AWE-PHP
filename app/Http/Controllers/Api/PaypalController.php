<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Traits\ApiResponser;
use App\Models\Plan;
use App\Models\Devices;
use App\Models\PaymentHistory;
use Srmklive\PayPal\Services\PayPal as PayPalClient;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DateTime;
use CommonHelper;

class PaypalController extends Controller {

    use ApiResponser;

    public function plan_purchase(Request $request) {

        $validator = Validator::make(request()->all(), [
            'plan_id' => 'required',
            'plan_type' => 'required|integer|between:1,2'
        ]);

        if (!$validator->fails()){
            $plan_detail = Plan::findOrfail($request->plan_id);

            $provider = new PayPalClient;
            $provider->getAccessToken();
            if($request->plan_type == 1){
                $plan = $provider->showPlanDetails($plan_detail->paypal_monthly_subscription_id);
            }elseif($request->plan_type == 2) {
                $plan = $provider->showPlanDetails($plan_detail->paypal_yearly_subscription_id);
            }
            
            $user = auth()->user();

            if((isset($plan['type']) && $plan['type'] == 'error') || (isset($plan['error'])))
            {
                return response([
                    'status' => false,
                    'message' => 'Plan not found'
                ]);
            }


            $days_count = $plan['billing_cycles'][0]['frequency']['interval_count'];

            $data = array(
                'plan_id' => $plan['id'],
                'application_context' => array(
                    'return_url' => url('/').'/api/paypal/plan/success/'.$user->id.'/'.$request->plan_id.'/'.$request->plan_type.'/'.Carbon::now()->addDays($days_count)->format("d-F-Y"),
                    'cancel_url' => url('/').'/api/paypal/plan/return/cancel'
                )
            );

            $subscription = $provider->createSubscription($data);

            $response['link'] = $subscription['links'][0]['href'];
            
            return response([
                'status' => true,
                'message' => '',
                'data' => $response
            ]);

        }
        return $this->errorResponse($validator->messages(), true);

    }


    public function return_success($id,$plan_id,$plan_type,$expiration_date,Request $request){
        $provider = new PayPalClient;
        $provider->getAccessToken();   
        $token = $request->token;
        $ba_token = $request->ba_token;
        $subscription_id = $request->subscription_id;
        $user = User::find($id);
        //$result = $provider->activateSubscription($request->subscription_id, 'Reactivating the subscription');
        $result = $provider->showSubscriptionDetails($request->subscription_id);

        if(isset($result['type']) && $result['type'] == 'error'){
            return response([
                'status' => false,
                'message' => '',
                'data'=>$result
            ]);
        }else{
          if($result['status'] == 'APPROVAL_PENDING'){

            $params['message'] = __('message.paypal.subscription_not_activated');
            return view('transaction_fail',$params);

          }elseif ($result['status'] == 'ACTIVE') {

            $user->plan_id = $plan_id;
            $user->plan_type = $plan_type;
            $user->expiration_date = Carbon::parse($expiration_date)->format('Y-m-d');
            $user->save();

            $previous_plan_cancel = PaymentHistory::where('user_id',$id)->where('mode','paypal')->orderBy('id','desc')->first();

            if(!empty($previous_plan_cancel)){
                
                $plan_data = json_decode($previous_plan_cancel->response,true);
                $subscription_id = $previous_plan_cancel->transaction_id;
                $can_result = $provider->cancelSubscription($subscription_id, 'Not satisfied with the service');

            }

            $plan = Plan::find($plan_id);
            $amount = ($plan_type == '1')?$plan->monthly_price:$plan->yearly_price;

            $payment_history = PaymentHistory::create([
                'user_id' => $id,
                'mode' => "paypal",
                'plan_id' => $plan_id,
                'type' => $plan_type,
                'transaction_id' => $request->subscription_id,
                'amount' => $amount,
                'response' => json_encode($result)
            ]);


            $params['message'] = __('message.payment.payment_successful');
            return view('transaction_success',$params);
          }
        }

    }

    public function return_cancel(){
        $params['message'] = __('message.paypal.transaction_cancelled');
        return view('transaction_fail',$params);
    }

}