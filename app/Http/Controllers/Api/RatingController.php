<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\Rating;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RatingController extends Controller {

    use ApiResponser;


    public function add(Request $request) {
        $validator = Validator::make(request()->all(), [
            'content_id' => 'required|integer',
            'rating' => 'required|integer|between:1,5'
        ]);
        if (!$validator->fails()) {

                if (Rating::whereContentId($request->content_id)->whereUserId($request->user()->id)->first()) {

                        return response([
                            'status' => false,
                            'message' => __('message.rating_exits')
                        ]);

                } else {

                    $rating = new Rating;
                    $rating->user_id = $request->user()->id;
                    $rating->content_id = $request->content_id;
                    $rating->rating = $request->rating;
                    $rating->save();

                    return response([
                        'status' => true,
                        'message' => __('message.rating_success')
                    ]);
                }
            
        }

        return $this->errorResponse($validator->messages(), true);
    }

}
