<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Plan;
use App\Models\UsersGroup;
use App\Models\Devices;
use App\Models\Chat;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\UserResource;
use App\Traits\StripeTraits;
use CommonHelper;
use Carbon\Carbon;
use Mail;
use App\Mail\AccountDeleteMail;
use NotificationHelper;

use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;

class UserController extends Controller {

    use ApiResponser, StripeTraits;


    public function register_device_list(Request $request) {
       
       $user = User::whereId($request->user()->id)->where('status',1)->first();
       if($user->parent_id == '0'){
            $user_id = $request->user()->id;
       }else{
            $user_id = $user->parent_id;
       }

        $device_list = Devices::where('user_id',$user_id)->groupBy('udid')->orderBy('id','desc')->paginate(10);

        return response([
            'status' => true,
            'message' =>'',
            'data' =>$device_list
        ]);

    }


    public function deregister_user(Request $request) {
       
        $validator = Validator::make(request()->all(), [
            'devices_id' => 'required|integer'
        ]);

        if (!$validator->fails()) {
            $device = Devices::find($request->devices_id);

            if($device->api_token_id != ''){
                //$id = (new Parser(new JoseEncoder()))->parse($device->api_token_id)->claims()->all()['jti'];
                $id = $device->api_token_id;

                $request->user()->tokens->each(function($token, $key) use ($id) {
                    if($token->id == $id){
                        $token->delete();
                    }
                });
            }

            if ($device->token != "") {
                $push_title = 'Access revoked';
                //$push_data = array();
                $push_data = 'Auto logged out.';
                $notification_arr['type'] = 'device_deregister';
                $push_type = 'device_deregister';
                $notification = NotificationHelper::send($device->token, $push_title, $push_data, $push_type, $notification_arr);
            }
            
            $device->delete();

            return response([
                'status' => true,
                'message' => __('message.deregister_device_sucess'),
            ]);
        }

       return $this->errorResponse($validator->messages(), true);

    }

    public function add(Request $request) {
       
        $validator = Validator::make(request()->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'is_child' => 'required|integer|between:0,1'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::find($request->user()->id);

            if($user->parent_id == 0){
                $user_count = User::where('parent_id', $user->id)->count();
                $parent_id = $user->id;
            }else{
                $user_count = User::where('parent_id', $user->parent_id)->count();
                $parent_id = $user->parent_id;
            }

            if($user_count >= 4){
                return response([
                    'status' => false,
                    'message' => __('message.user.account_limit')
                ]);
            }

            $new_user = new User(); 
            $new_user->parent_id = $parent_id;
            $new_user->email = $user->email;
            $new_user->phone = $user->phone;
            $new_user->country_code = $user->country_code;
            $new_user->currency_code = $user->currency_code;
            $new_user->phone_verified = $user->phone_verified;
            //$new_user->email_verified = $user->email_verified;
            $new_user->password = '';
            $new_user->status = 1;
            $new_user->is_child = $request->is_child;
            $new_user->first_name = $request->first_name;
            $new_user->last_name = $request->last_name;
            $new_user->save();

            UsersGroup::create([
                'user_id' => $new_user->id,
                'group_id' => '4',
            ]);

            if(isset($request->profile_picture) && !empty($request->profile_picture)){
                if ($request->hasFile('profile_picture')) {
                    $dir = env('AWS_S3_MODE')."/users";
                    $profile_picture = CommonHelper::s3Upload($request->file('profile_picture'), $dir);
             
                    $update_data['profile_picture'] = $profile_picture;

                    User::where('id',$new_user->id)->update($update_data);
                }
            }

            return ([
                'status' => true,
                'data' => User::find($new_user->id),
                'message' => __('message.account_added')
            ]);
        }

    }

    public function remove(Request $request) {
       
        $validator = Validator::make(request()->all(), [
            'user_id' => 'required'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::find($request->user_id);

            if(!isset($user->parent_id)){
                return response([
                    'status' => FALSE,
                    'message' => __('message.api.user_not_found')
                ]);
            }

            if($user->parent_id == 0){
                
                $support_email = CommonHelper::ConfigGet('from_email');
                $reactivation_date = Carbon::now()->addDays(CommonHelper::ConfigGet('account_recoverable_days'))->format('d F Y');
                
                $title = "Account Deleted";
                $description = "<p>Your account will be permanently removed from the system on ".$reactivation_date.". Contact the administrator on <a href = 'mailto: ".$support_email."'>".$support_email."</a> in case you want to re-activate your account.</p>";
        
                User::where('id', $user->id)->update(['status'=> 2, 'deactivated_time' => Carbon::now()]);

                User::where('parent_id', $user->id)->update(['status'=> 2 ]);

                $user->revokeAllTokens();
                $users = User::where('parent_id', $user->id)->get();
                if(isset($users) && count($users) > 0){
                    foreach ($users as $key => $value) {
                        $value->revokeAllTokens();
                    }
                }

                Mail::to($user->email)->send(new AccountDeleteMail($title,$description, config('app.address1'), config('app.address2')));

            }else{
                $user->revokeAllTokens();
                User::where('id', $user->id)->delete();
            }

            return response([
                'status' => TRUE,
                'message' => __('message.account_deleted')
            ]);
        }

    }

    public function generate_passport_token(Request $request){

        $validator = Validator::make(request()->all(), [
            'user_id' => 'required',
            'device_name' => 'required',
            'device_token' => 'required',
            'device_type' => 'required',
            'ip' => 'required',
            'udid' => 'required',
            'language_id' => '',
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::find($request->user_id);

            if($user){

                if(isset($request->language_id) && !empty($request->language_id)){
                    $user->language_id = $request->language_id;
                    $user->save();
                }

                $location = \Location::get($request->ip);
                $city = $location->cityName;
                $state = $location->regionName;
                $country = $location->countryName;
                $latitude = $location->latitude;
                $longitude = $location->longitude;

                $user->token = $api_token_id = $user->createToken('authToken')->accessToken;

                $device = Devices::where('user_id', $user->id)->where('udid', $request->udid)->where('name', $request->device_name)->where('type', $request->device_type)->first();

                if(!isset($device)){
                    $devices = Devices::insert([
                        'user_id' => $user->id,
                        'parent_id' => $user->parent_id,
                        'name' => $request->device_name,
                        'token' => $request->device_token,
                        'api_token_id' => $api_token_id,
                        'type' => $request->device_type,
                        'udid' => $request->udid,
                        'city' => $city,
                        'state' => $state,
                        'country' => $country,
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                    ]);
                }else{
                    $device->api_token_id = $api_token_id;
                    $device->city = $city;
                    $device->state = $state;
                    $device->country = $country;
                    $device->token = $request->device_token;
                    $device->udid = $request->udid;
                    $device->latitude = $latitude;
                    $device->longitude = $longitude;
                    $device->save();
                }

                
                
                return (new UserResource($user))->additional([
                        'status' => true,
                        'message' => ''
                    ]);
            }else{
                return response([
                    'status' => false,
                    'message' => __('message.api.user_not_found')
                ]);
            }
            
        }

    }

    public function add_lock(Request $request){

        $validator = Validator::make(request()->all(), [
            'user_id' => 'required|numeric',
            'pin' => 'required|numeric|digits:4'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::find($request->user_id);
            $user->lock_password = $request->pin;
            $user->save();

            return ([
                'status' => true,
                'message' => __('message.lock_added')
            ]);
        
        }

    }

    public function verify_lock(Request $request){

        $validator = Validator::make(request()->all(), [
            'user_id' => 'required',
            'pin' => 'required|numeric|digits:4'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::whereId($request->user_id)->where('lock_password', $request->pin)->first();

            if($user){

                return ([
                    'status' => true,
                    'message' => __('message.verified')
                ]);
            }else{
                return response([
                    'status' => false,
                    'message' => __('message.incorrect_pin')
                ]);
            }
            
        }

    }


    public function disable_pin(Request $request){

        $validator = Validator::make(request()->all(), [
            'user_id' => 'required'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::whereId($request->user_id)->where('status',1)->first();

            if($user){
                
                User::whereId($request->user_id)->update(['lock_password'=>null]);

                return ([
                    'status' => true,
                    'message' => __('message.disable_pin')
                ]);
            }else{
                return response([
                    'status' => false,
                    'message' => __('message.api.user_not_found'),
                ]);
            }
        }
    }

    public function change_pin(Request $request){

        $validator = Validator::make(request()->all(), [
            'user_id' => 'required',
            'pin' => 'required|numeric|digits:4'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::whereId($request->user_id)->where('status',1)->first();

            if($user){
                
                User::whereId($request->user_id)->update(['lock_password'=>$request->pin]);

                return ([
                    'status' => true,
                    'message' => __('message.change_pin')
                ]);
            }else{
                return response([
                    'status' => false,
                    'message' => __('message.api.user_not_found'),
                ]);
            }
            
        }

    }


    public function save_language(Request $request){

        $validator = Validator::make(request()->all(), [
            'language_id' => 'required'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user_id = auth()->user()->id;

            $update_data['language_id'] = $request->language_id;

            User::whereId($user_id)->update($update_data);

            return response([
                'status' => true,
                'message' => 'Saved',
            ]);
        }

    }

    public function chat_history(){

        $user_id = auth()->user()->id;

        $chat = Chat::where('sender_id',$user_id)->orWhere('receiver_id',$user_id)->orderBy('id', 'desc')->paginate(25);

        if(!empty($chat) && count($chat)>0){
            return response([
                'status' => true,
                'message' => '',
                'data'=> $chat
            ]);    
        }else{
            return response([
                'status' => false,
                'message' => __('message.chat.not_found')
            ]);
        }

    }

}