<?php



namespace App\Http\Controllers;



use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\ContactUs;

use App\Models\Devices;
use App\Models\User;
use App\Models\Channel;
use App\Models\SystemConfig;
use App\Models\ActivityLog;

use CommonHelper;
use PaymentHelper;

use App\Mail\BulkMail;
use App\Mail\ForgotMail;
use App\Mail\RegistrationMail;
use Carbon\Carbon;

use Mail;

use Validator;

use NotificationHelper;
use StreamHelper;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Session;

class CommonController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function account_activation(Request $request, $token)
    {        
        // request()->validate([
        //     'email' => 'required'
        // ]);
        // $requestData = request()->only(['email']);
        $user = User::where('remember_token', $token)->first();
        $data['address1'] = config('app.address1');
        $data['address2'] = config('app.address2');
        if($user)
        {            
            User::where('id', $user->id)->update([
                'email_verified_at' => Carbon::now(),
                'status' => 1,
                'remember_token' => NULL,
            ]);            
            $data['message'] = 'Account Activated successfully';
            return view('template.account_activated',$data);            
        } else{
            $data['message'] = 'Account Already Activated';
            return view('template.account_activated',$data);
        }
    }

    public function auth_account_activation(Request $request, $token)
    {        
        $user = User::where('remember_token', $token)->first();
        $data['address1'] = config('app.address1');
        $data['address2'] = config('app.address2');
        if($user)
        {
            $data['user'] = $user;
            $data['token'] = $token;
            return view('template/create-password',$data);
        } else{
            $data['message'] = 'Link has been expired';
            return view('template.account_activated',$data);
        }
    }

    public function craete_password(Request $request, $id, $token)
    {
        $user = User::where('id', $id)->where('remember_token', $token)->first();
        $data['address1'] = config('app.address1');
        $data['address2'] = config('app.address2');
        if($user)
        {
            User::where('id', $id)->update([
                'password' => bcrypt($request->password),
                'email_verified_at' => Carbon::now(),
                'status' => 1,
                'remember_token' => NULL,
            ]);

            $data['message'] = 'Your password reset is successful.';
            return view('template.account_activated',$data);
        } else
        {
            $data['message'] = 'Link has been expired';
            return view('template.account_activated',$data);
        }
    }

    public function forgot_password(Request $request) {

        $validator = Validator::make(request()->all(), [
            'email' => 'required',
        ]);

        if (!$validator->fails()) {
            $user = User::whereEmail($request->email)->first();
            if($user){
                $status = Password::sendResetLink(
                    $request->only('email')
                );

                if ($status == Password::RESET_LINK_SENT) {
                    if ($request->type == 'store') {
                        return redirect()->route('store.forgot')->with('success','Reset Password link sent to your register email address.');                    
                    } else
                    {
                        return redirect()->route('board.forgot')->with('success','Reset Password link sent to your register email address.');
                    }                    
                }else{
                    if ($request->type == 'store') {
                        return redirect()->route('store.forgot')->with('error','Email address is not registered');                    
                    } else
                    {
                        return redirect()->route('board.forgot')->with('error','Email address is not registered');
                    }
                    
                }
            }else{
                if ($request->type == 'store') {
                    return redirect()->route('store.forgot')->with('error','Account does not exist');                    
                } else
                {
                    return redirect()->route('board.forgot')->with('error','Account does not exist');
                }
            }
        }
        return redirect()->route('board.forgot')->with('error',$validator->messages());
        return $this->errorResponse($validator->messages(), true);
    }

    public function store_forgot_password(Request $request) {
        return view('auth.storeforgot');
    }

    public function board_forgot_password(Request $request) {
        return view('auth.boardforgot');
    }

    public function add_channel_id(Request $request){
        if(isset($request->channel_id) && $request->channel_id != ''){
            $user_id = Auth::user()->id;;            
            if(isset($user_id) && !empty($user_id))
            {
                $data = Channel::whereUserId($user_id)->whereChannelId($request->channel_id)->first();
                if(isset($data) && !empty($data))
                {
                    if($data->channel_id != $request->channel_id)
                    {
                        Channel::create([
                            'user_id' => $user_id,
                            'channel_id' => $request->channel_id,
                        ]);
                    }
                } else
                {
                    Channel::create([
                        'user_id' => $user_id,
                        'channel_id' => $request->channel_id,
                    ]);
                }
            }
            echo json_encode(array("is_success" => true, "post" => $request, 'restore' => TRUE));
        }else{
            echo json_encode(array("is_success" => false, "post" => $request));
        }
    }

    public function make_payment(Request $request){
        $response = PaymentHelper::makeRequest($request->token);
        if ($response->ResponseCode == "00000"){
            $GatewayTransID = $response->responseMessage->GatewayTransID;
            $TransactionID = $response->TransactionID;
            return redirect('transaction_success/'.$TransactionID.'/'.$GatewayTransID);
            // return redirect('transaction_success/'.$request->order_id);
        }else{
            return redirect('transaction_fail/'.$request->order_id);
        } 
    }

    public function transaction_fail($id){
        $params['main_text'] = "FAILED";
        $params['sub_text'] = "Payment Transaction Failed.";
        return view('transaction_fail',$params);
    }

    public function transaction_success($TransactionID, $GatewayTransID){
    // public function transaction_success($TransactionID){
        $params['main_text'] = "SUCCESS";
        $params['sub_text'] = "Payment Transaction Completed Successfully.";
        return view('transaction_fail',$params);
    }

    public function generate_token($id){
        $params['order_id'] = $id;
        return view('payment',$params);
        // 4005 5500 0000 0019
    }

    public function refund($TransactionID, $GatewayTransID){
        $response = PaymentHelper::makeRefund($TransactionID, $GatewayTransID);
        if ($response->ResponseCode == "00000"){
            $GatewayTransID = $response->responseMessage->GatewayTransID;
            $TransactionID = $response->TransactionID;
            return redirect('transaction_success/'.$TransactionID.'/'.$GatewayTransID);
        }else if ($response->ResponseCode == "10042"){
            // Already refunded
            $GatewayTransID = $response->responseMessage->GatewayTransID;
            $TransactionID = $response->TransactionID;
            return redirect('transaction_fail/'.$TransactionID);
        }else{
            return redirect('transaction_fail/'.$TransactionID);
        } 
    }


    public static function terms_conditions()
    {
        $param['terms_conditions'] = SystemConfig::where('path','terms_conditions')->value('value');
        return view('admin.pages.terms_conditions', $param);
    }

    public static function terms_conditions_testing()
    {
        StreamHelper::convert();
    }
    
    public static function privacy_policy()
    {
     
        $param['privacy_policy'] = SystemConfig::where('path','privacy_policy')->value('value');
        return view('admin.pages.privacy_policy', $param);
 
    }

     public static function activity_log($user_id)
    {
    
        $param['activity_log'] = User::select('id','first_name','last_name')->with(['activity_log.content','activity_log.video.season','activity_log'=>function($q) {
                                    $q->orderBy('id','desc');
                                }])
                                ->where('id', $user_id)
                                ->orWhere('parent_id', $user_id)
                                ->orderBy('id','asc')
                                ->get();
                                
        return view('activity_log', $param);

    }


    public static function loyalty_reward_terms_condition()
    {
     
        $param['loyalty_reward_terms_condition'] = SystemConfig::where('path','loyalty_reward_terms_condition')->value('value');
        return view('admin.pages.loyalty_reward_terms_condition', $param);
    }

    public static function streaming()
    {
        StreamHelper::test1();
    }

}
