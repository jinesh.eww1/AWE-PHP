<?php
namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Services\AppleToken;
use App\Traits\ApiWebsite;
use App\Providers\RouteServiceProvider;
use Session;

use App\Models\User;
use CommonHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Laravel\Socialite\Two\User as OAuthTwoUser;
use Symfony\Component\HttpFoundation\Response;


class AppleSocialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    use ApiWebsite;

    public function __construct()
    {
    }

    public function redirectToApple(Request $request)
    {
        return Socialite::driver('apple')->redirect();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function handleCallback(Request $request)
    {

      

      //config()->set('services.apple.client_secret', $appleToken->generate());
      //$socialUser = Socialite::driver('apple')->stateless()->user();

      try {
            //$user = Socialite::driver('apple')->user();
        
            if($request['device_name'] == 'Safari' || empty($request['device_token'])){
                $request['device_token'] = Str::random(30);
            }

            $finduser = User::where('social_id', $request['id'])->where('parent_id',0)->first();
            
            $variables['email'] = $request['email'];
            $variables['device_name'] = $request['device_name'];
            $variables['device_token'] = $request['device_token'];
            $variables['device_type'] = 'web';
            $variables['social_id'] = $request['id'];
            $variables['social_type'] = 'apple';
            $variables['first_name'] = 'Apple';
            $variables['last_name'] = 'User';

            $api = 'social-signin';
            $method = 2;

            if($finduser){
                
                $response = $this->api_call($api, $method, $variables);

                if($response['status'] == true){
                    session()->put('token', $response['data']['token']);
                    session()->put('user_data', $response['data']);
                   
                    $response['redirect_url'] = route('whos.watching');
                    return $response;

                }else{
                   
                   $response['redirect_url'] = '';
                   return $response;

                    //return redirect()->back()->with('error',$response['message']);

                }
       
            }else{

                $variables['reference_code'] = "AWE".CommonHelper::random_number();
                $variables['currency_code'] = 'USD';
                $variables['status'] = 1;

                $response = $this->api_call($api, $method, $variables);

                if($response['status'] == true){


                    session()->put('token', $response['data']['token']);
                    //session()->put('user_data', $response['data']);
                    
                    $gtapi = 'generate/token';
                    $gtmethod = 2;
                    
                        $gtvariables['user_id'] = $response['data']['id'];
                        $gtvariables['language_id'] = $response['data']['language_id'];
                        $gtvariables['device_name'] = $request['device_name'];
                        $gtvariables['device_token'] = $request['device_token'];
                        $gtvariables['device_type'] = 'web';
                        //$gtvariables['latitude'] = $request['latitude'];
                        //$gtvariables['longitude'] = $request['longitude'];
                        $gtvariables['ip'] = $request['ip'];
                        $gtvariables['udid'] = $request['device_token'];

                    $gtresponse = $this->api_call($gtapi, $gtmethod, $gtvariables);

                    if($gtresponse['status'] == true){
                        session()->put('token', $gtresponse['data']['token']);
                        session()->put('user_data', $gtresponse['data']);
                        session()->put('device_token', $request['device_token']);
                        
                        $response['redirect_url'] = route('preferred.category');
                    }else{
                        //return redirect()->route('whos.watching');
                        $response['redirect_url'] = route('whos.watching');
                    }

                    //return redirect()->route('preferred.category')->with('success',$response['message']);
                    
                    return $response;

                }else{

                  $response['redirect_url'] = '';
                  return $response;

                    //return redirect()->back()->with('error',$response['message']);
                }
    
            }

          } catch (Exception $e) {
            dd($e->getMessage());
          }
	}
}
?>
