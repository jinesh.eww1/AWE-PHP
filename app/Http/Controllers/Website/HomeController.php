<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Traits\ApiWebsite;
use App\Models\User;
use App\Models\UserRestrictedContent;
use App\Models\UserAgeRestriction;
use App\Models\AgeRatings;
use App\Models\Content;
use App\Models\MyList;
use App\Models\Language;
use App\Models\Notification;
use CommonHelper;
use Session;
use \Crypt;
use Exception;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Providers\RouteServiceProvider;
use Socialite;
use Illuminate\Http\Response;
use Cookie;
use DB;
use App;
use PragmaRX\Countries\Package\Countries;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Redirect;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;




class HomeController extends Controller
{
    use ApiWebsite;

    public function index(Request $request)
    {   

        if($request->isMethod('post')){

            if($request['device_name'] == 'Safari' || empty($request['device_token'])){
                $request['device_token'] = Str::random(30);
            }

            if(!empty($request['country_code']) && $request['phone']){
                $request['country_code'] = '+'.str_replace($request['phone'], '', $request['country_code']);    
            }else{
                $request['country_code'] = '';
            }
            
            $api = 'login';
            $method = 2;
            
            $variables['email'] = !empty($request['email'])?$request['email']:$request['phone'];
            $variables['password'] = trim($request['password']);
            $variables['device_name'] = $request['device_name'];
            $variables['device_token'] = $request['device_token'];
            $variables['device_type'] = 'web';
            //$variables['latitude'] = $request['latitude'];
            //$variables['longitude'] = $request['longitude'];
            $variables['ip'] = $request['ip'];
            $variables['country_code'] = $request['country_code'];
            

            $response = $this->api_call($api, $method, $variables);

            if($response['status'] == true){

                session()->put('token', $response['data']['token']);
                session()->put('user_data', $response['data']);
                session()->put('device_token', $request['device_token']);
                
                $language = Language::where('api_key',!empty(session()->get('locale'))?session()->get('locale'):'en')->first();

                $api = 'language/add';
                $method = 2;
                $lan_variables['language_id'] = $language->id;

                $lan_response = $this->api_call($api, $method,$lan_variables);

                if($lan_response['status'] == true){

                    if (isset($language->api_key) && in_array($language->api_key, Session::get('language_array'))) {
                        app()->setLocale($language->api_key);
                    }

                    session()->put('locale', $language->api_key);

                    $api = 'appstring/main';
                    $method = 1;
                
                    $appstring = $this->api_call($api, $method);

                    if($appstring['status'] == true){
                        session()->put('app_string', $appstring['data']);
                    }

                }

                return redirect()->route('whos.watching')->with('success',$response['message']);
            }else{
                return redirect()->back()->withInput($request->all())->with('error',$response['message']);
            }
            
        }


        $api = 'appstring/main';
        $method = 1;
    
        $appstring = $this->api_call($api, $method);

        if($appstring['status'] == true){
            session()->put('app_string', $appstring['data']);
        }
        
        if(Session::has('user_data.id') && Session::has('token')){
            return redirect()->route('home','movie');
        }else{

            $api = 'preferred/languages';
            $method = 2;
            $response = $this->api_call($api, $method);            

            if($response['status'] == true){
                $language_array = array_column($response['data'], 'api_key','type');
                session::put('language_array',$language_array);
                $data['language'] = $response['data'];
            }else{
                $data['language'] = Language::all();
            }

            return view('website.pages.login',$data);
        }

    }

    public function signup(Request $request)
    {

        if($request->isMethod('post')){

            $request->validate([
               'email_otp' => 'required|same:check_email_otp',
               'mobile_otp' => 'required|same:check_mobile_otp',
            ],[
                'email_otp.same'=>'Email OTP is incorrect.',
                'mobile_otp.same'=>'Mobile OTP is incorrect.',
            ]);

            if($request['device_name'] == 'Safari' || empty($request['device_token'])){
                $request['device_token'] = Str::random(30);
            }

            if(!empty($request['country_code'])){
                $request['country_code'] = "+".$request['country_code'];
            }else{
                $request['country_code'] = '';
            }
            
            
            $countries = new Countries();

            $country_detail = $countries->where('cca2', strtoupper($request->country_iso2))->first()->toArray();

            if(!empty($country_detail['currencies'][0]) && isset($country_detail['currencies'][0])){
                $currency_code = $country_detail['currencies'][0];
            }else{
                $currency_code = 'USD';
            }

            $api = 'register';
            $method = 2;
                
            $variables['first_name'] = $request['first_name'];
            $variables['last_name'] = $request['last_name'];
            $variables['email'] = $request['email'];
            $variables['password'] = trim($request['password']);
            $variables['password_confirmation'] = trim($request['password_confirmation']);
            $variables['phone'] = $request['phone'];
            $variables['referral_code'] = !empty($request['referral_code'])?$request['referral_code']:'';
            $variables['country_code'] = $request['country_code']; //$request->country_code;
            $variables['currency_code'] = $currency_code; //$request->currency_code;
            $variables['device_name'] = $request['device_name'];
            $variables['device_token'] = $request['device_token'];
            $variables['device_type'] = 'web';
            
            $response = $this->api_call($api, $method, $variables);

           // dd($response,$response['data']['token'],Session::get('token'));
            //$response['status'] = true;

            if($response['status'] == true){

                session()->put('token', $response['data']['token']);

                $api = 'generate/token';
                $method = 2;
                
                
                $log_variables['user_id'] = $response['data']['id'];
                $log_variables['language_id'] = $response['data']['language_id'];
                $log_variables['device_name'] = $request['device_name'];
                $log_variables['device_token'] = $request['device_token'];
                $log_variables['device_type'] = 'web';
                //$log_variables['latitude'] = $request['latitude'];
                //$log_variables['longitude'] = $request['longitude'];
                $log_variables['ip'] = $request['ip'];
                $log_variables['udid'] = $request['device_token'];

            
                $log_response = $this->api_call($api, $method, $log_variables);
                
                if($log_response['status'] == true){
                    
                    session()->put('token', $log_response['data']['token']);
                    session()->put('user_data', $log_response['data']);
                    session()->put('device_token', $request['device_token']);
                    session()->put('device_token', $request['device_token']);
                    

                    return redirect()->route('preferred.category')->with('success',$response['message']);
                }else{
                    return redirect()->back()->with('error',$log_response['message']);
                }
                                
                return redirect()->route('index')->with('success',$response['message']);
            }else{
                return redirect()->back()->with('error',$response['message']);
            }
            

        }
         if(Session::has('user_data')){
            return redirect()->route('home','movie');
        }else{
            return view('website.pages.signup');
        }
    }

    public function forgot_password(Request $request)
    {  
        if($request->isMethod('post')){

            if(!empty($request['country_code']) && $request['phone']){
                $request['country_code'] = "+".$request['country_code'];    
            }else{
                $request['country_code'] = '';
            }

            $api = 'forgot_password';
            $method = 2;
            
            $variables['email'] = !empty($request->email)?$request->email:$request->phone;
            $variables['country_code'] = $request->country_code;
            
            $response = $this->api_call($api, $method,$variables);
            
            if($response['status'] == false){
                return redirect()->back()->with('error',$response['message']);        
            }else{
                if($response['status'] == true && isset($response['data']) && !empty($response['data'])){
                    session()->put('mobile_otp', $response['data']['otp']);
                    session()->put('user_id', $response['data']['user_id']);
                    $post_data = $request->all(); 
                    $post_data['type'] = 'forgot-password'; 

                    return redirect()->route('verify.otp',$post_data)->with('success',$response['message']);

                    //return view('website.pages.verify_otp',$post_data);

                }
                return redirect()->route('index')->with('success',$response['message']);        
            }

        } 
        return view('website.pages.forgot_password');
    }



    public function resend_mobile_otp_forgot(Request $request)
    {  
        $api = 'forgot_password';
        $method = 2;

        $variables['email'] = $request->mobile;
        $variables['country_code'] = "+".$request->code;
        $response = $this->api_call($api, $method, $variables);
       
        if($response['status'] == false){
            return json_encode($response);
        }else{
            session()->put('mobile_otp', $response['data']['otp']);
            return json_encode($response);
        }
    }

    public function resend_mobile_otp(Request $request)
    {  
        $api = 'send_otp';
        $method = 2;

        $variables['phone'] = $request->mobile;
        $variables['country_code'] = "+".$request->code;
        $response = $this->api_call($api, $method, $variables);
       
        if($response['status'] == false){
            return json_encode($response);
        }else{
            session()->put('mobile_otp', $response['data']['otp']);
            return json_encode($response);
        }
    }

    public function verify_otp(Request $request)
    {   

        if($request->isMethod('post')){
            
            $verify_otp = $request['verify_otp_1'].$request['verify_otp_2'].$request['verify_otp_3'].$request['verify_otp_4'];
            if(Session::get("mobile_otp") == $verify_otp){

                //dd(Session::get("user_data"));
                if(!empty(Session::get("user_id"))){
                     $user_id = Session::get('user_id');
                }else{
                    if(Session::get('user_data.parent_id') > 0){
                        $user_id = Session::get('user_data.parent_id');
                    }else{
                        $user_id = Session::get('user_data.id');
                    }   
                }

                $user = User::where('id',$user_id)->first();

                if (empty($user)) {
                    return redirect()->back()->withErrors(['error' => trans('User does not exist')]);
                }


                if($request['type'] == 'account-profile'){
                  
                    $countries = new Countries();

                    $country_detail = $countries->where('cca2', strtoupper($request['country_iso2']))->first()->toArray();

                    if(!empty($country_detail['currencies'][0]) && isset($country_detail['currencies'][0])){
                        $currency_code = $country_detail['currencies'][0];
                    }else{
                        $currency_code = 'USD';
                    }

                    if(Session::get('user_data.parent_id') > 0){
                        $user_id = Session::get('user_data.parent_id');
                    }else{
                        $user_id = Session::get('user_data.id');
                    }
                    
                    $variables['first_name'] = $user->first_name;
                    $variables['last_name'] = !empty($user->last_name)?$user->last_name:'User';
                    $variables['user_id'] = $user_id;
                    $variables['phone'] = $request['phone'];
                    $variables['country_code'] = '+'.$request['country_code']; //$request->country_code;
                    $variables['currency_code'] = $currency_code; //$request->currency_code;
                    
                    $api = 'profile/view/edit';
                    $method = 2;
                    $response = $this->api_call($api, $method, $variables);
                    if($response['status'] == true){
                        session()->put('user_data.phone', $request['phone']);
                        session()->put('user_data.country_code', '+'.$request['country_code']);
                        session()->put('user_data.phone_verified', 1);

                        return redirect()->route('profile')->with('success',$response['message']);
                    }else{
                        return redirect()->back()->with('error',$response['message']);
                    }   
                }else{
                    
                    $create_token = Password::createToken($user);
               
                    //Get the token just created above
                    $tokenData = DB::table('password_resets')->where('email', $user->email)->first();

                    return Redirect::to('/reset-password/'.$create_token."?email=".$user->email);    
                }   
                

            }

        }
        
        return view('website.pages.verify_otp');
    }

    public function home($type)
    {   

        if($type =='movie'){
            $variables['content_type'] = 1;
        }elseif ($type =='tv-shows') {
            $variables['content_type'] = 2;
        }else{
            $variables['content_type'] = 1;
        }

        $api = 'content';
        $method = 2;
        
        $response['home'] = $this->api_call($api, $method,$variables);
        $response['type'] = \Request::segment(2);

        if($response['home']['status'] == true){

            $category = $response['home']['data']['category'];      

            session()->put('category', $category);
            
            return view('website.pages.home',$response);
        }else{
            return redirect()->back()->with('error',$response['home']['message']);
        }
        
    }


    public function content_detail($slug,Request $request)
    {   
        $api = 'content/details/'.$slug;
        $method = 1;

        if(isset($request->search) && $request->search == 1){
            $variables['search'] = 1;
            $response = $this->api_call($api, $method,$variables);
        }else{
            $response = $this->api_call($api, $method);
        }

        
        if($response['status'] == true){
            return view('website.pages.content_detail',$response);
        }else{
            return redirect()->back()->with('error',$response['message']);
        }
       
    }


    public function my_list()
    {   
        if(!empty(request()->get('page'))){
            $variables['page'] = (int)request()->get('page');
        }else{
            $variables['page'] = 1;
        }
        $variables['type'] = "web";
        $api = 'mylist/list';
        $method = 1;
        $response['my_list'] = $this->api_call($api,$method,$variables);

        return view('website.pages.my_list',$response);
    }

    public function view_more($type,$category_type,Request $request)
    {   

        
        if($type =='movie'){
            $variables['content_type'] = 1;
        }elseif ($type =='tv-shows') {
            $variables['content_type'] = 2;
        }else{
            $variables['content_type'] = 1;
        }
        $variables['type'] = 'web';
        $genres_id = base64_decode($category_type);


        if(!empty(request()->get('page'))){
            $variables['page'] = (int)request()->get('page');
        }else{
            $variables['page'] = 1;
        }

        $api = 'content/view/all';
        $method = 2;
        $variables['genres_id'] = base64_decode($category_type);
    
        $response['view_more'] = $this->api_call($api, $method,$variables);

        $response['type'] = $type;
        $response['category_type'] = $category_type;
        
        if(!empty($response['view_more']['data']['data']) && count($response['view_more']['data']['data'])>0){
            return view('website.pages.view_more',$response);
        }else{
            if(Session::get('app_string')){ $msg = Session::get('app_string.app_titles_and_messages.no_data_found'); }else{ $msg = CommonHelper::multi_language('app_titles_and_messages','no_data_found')->multi_language_value->language_value; }

            return redirect()->back()->with('error',$msg);    
        }
                
    }

    public function view_plan()
    {   
        $api = 'plan';
        $method = 1;
        $response['plan'] = $this->api_call($api, $method);
        $response['user'] = Session::get('user_data');
        
        if(!empty($response['user']['plan_id'])){
            return view('website.pages.view_plan',$response);
        }else{
            if(Session::get('app_string')){ $msg = Session::get('app_string.app_titles_and_messages.plan_not_active'); }else{ $msg = CommonHelper::multi_language('app_titles_and_messages','plan_not_active')->multi_language_value->language_value; }
           return redirect()->route('all.plans')->with('error',$msg); 
        }
    }

    public function cancel_plan($plan_id,$plan_type)
    {  
        //dd(Session::get('user_data'));
        if(!empty($plan_id) && !empty($plan_type)){

            $api = 'stripe/plan/cancel';
            $method = 1;
            $response = $this->api_call($api, $method); 
            
            if($response['status'] == true){

                $api = 'profile/view/edit';
                $method = 2;
                $variables['user_id'] = Session::get('user_data.id');

                $data['user'] = $this->api_call($api, $method,$variables);
                Session::forget('user_data');
                Session::put('user_data',$data['user']['data']);

                return view('website.pages.cancel_plan',$response);
            }else{
                return redirect()->back()->with('error',$response['message']);
            }

            
        }else{
            if(Session::get('app_string')){ $msg = Session::get('app_string.app_titles_and_messages.something_went_wrong'); }else{ $msg = CommonHelper::multi_language('app_titles_and_messages','something_went_wrong')->multi_language_value->language_value; }
            
            return redirect()->back()->with('error',$msg);
        }

    }

    public function confirm_cancel_plan()
    {   
        
        $api = 'stripe/plan/cancel';
        $method = 1;
        $response = $this->api_call($api, $method); 

        if($response['status'] == true){
            return redirect()->route('all.plans')->with('success',$response['message']);
        }else{
            return redirect()->back()->with('error',$response['message']);
        }

    }

    public function logout()
    {      
        $api = 'logout';
        $method = 2;
        $variables['user_id'] = Session::get('user_data.id');
        $variables['udid'] = Session::get('device_token');

        $response = $this->api_call($api, $method,$variables);
        
        if($response['status'] == true){
            $language = Session::get('locale');
            Session::flush();
            session()->put('locale',$language);
            return redirect()->route('index')->with('success',__('message.api.logout_success'));
        }else{
            return redirect()->back()->with('error',$response['message']);
        }
    }

    public function language_change(Request $request)
    {   
        if (isset($request['location']) && in_array($request['location'], Session::get('language_array'))) {
            app()->setLocale($request['location']);
        }
        //dd(app()->getLocale());
        session()->put('locale', $request['location']);

        $api = 'appstring/main';
        $method = 1;
    
        $appstring = $this->api_call($api, $method);

        if($appstring['status'] == true){
            session()->put('app_string', $appstring['data']);

            if(Session::get('app_string')){ $msg = Session::get('app_string.app_language.language_updated'); }else{ $msg = CommonHelper::multi_language('app_language','language_updated')->multi_language_value->language_value; }
            $res['status'] = true;
            $res['message'] = $msg;

            return $res;

        }else{
            return false;
        }

        /*$api = 'category/list';
        $method = 2;
        $variables['user_id'] = Session::get('user_data.id');
        $category = $this->api_call($api, $method,$variables);
        
        if($category == true){            
            session()->put('category', $category['data']);
        }*/

        //dd(session()->get('locale'));
        
    }

    public function preferred_category(Request $request)
    {   
        if($request->isMethod('post')){

            $api = 'category/add';
            $method = 2;
            $variables['user_id'] = Session::get("user_data.id");
            $variables['genres_id'] = implode(",",$request->preferred_category);

            $response = $this->api_call($api, $method,$variables);

            if($response['status'] == true){
                return redirect()->route('all.plans')->with('sucess',$response['message']);
            }else{
                return redirect()->route('preferred.category')->with('error',$response['message']);
            }
        }
    
        $api = 'category/list';
        $method = 2;
        $variables['user_id'] = Session::get("user_data.id");

        $response = $this->api_call($api, $method,$variables);

        if($response['status'] == true){
            return view('website.pages.preferred_category',$response);
        }else{
            return redirect()->route('home','movie');
        }
        
    }

    public function send_email_otp($email)
    {   
        $api = 'send_otp_email';
        $method = 2;
        $variables['email'] = $email;

        $response = $this->api_call($api, $method, $variables);

        return json_encode($response);

    }

    public function send_otp_mobile(Request $request)
    {   
        $api = 'send_otp';
        $method = 2;
        
        $variables['phone'] = $request->mobile;
        $variables['country_code'] = "+".$request->code;
        $response = $this->api_call($api, $method, $variables);
       
        if($response['status']==true){
            session()->put('mobile_otp', $response['data']['otp']);
        }
        return json_encode($response);
    }


    public function preferred_language(Request $request)
    {   
        if($request->isMethod('post')){
            
            $api = 'language/add';
            $method = 2;
            $variables['language_id'] = !empty($request['language_id'])?$request['language_id']:1;

            $response = $this->api_call($api, $method,$variables);

            if($response['status'] == true){

                $language = Language::find($variables['language_id']);

                if (isset($language->api_key) && in_array($language->api_key, Session::get('language_array'))) {
                    app()->setLocale($language->api_key);
                }

                //dd(app()->getLocale());
                session()->put('locale', $language->api_key);

                $api = 'appstring/main';
                $method = 1;
            
                $appstring = $this->api_call($api, $method);

                if($appstring['status'] == true){
                    session()->put('app_string', $appstring['data']);
                }

                return redirect()->route('home','movie');
            }else{
                return redirect()->back();
            }
        }


        $api = 'preferred/languages';
        $method = 2;
        $variables['user_id'] = Session::get("user_data.id");
        $response = $this->api_call($api, $method,$variables);

        return view('website.pages.preferred_language',$response);
    }

    public function whos_watching(Request $request)
    {   
       
        $api = 'accounts';
        $method = 1;
        $response = $this->api_call($api, $method);
        
        return view('website.pages.whos_watching',$response);
    }
    public function generate_token(Request $request)
    {   

        if($request['device_name'] == 'Safari'){
            $request['device_token'] = Str::random(30);
        }
        
        $api = 'generate/token';
        $method = 2;
        $user = User::find($request['id']);
         
            $variables['user_id'] = $user->id;
            $variables['language_id'] = $user->language_id;
            $variables['device_name'] = $request['device_name'];
            $variables['device_token'] = $request['device_token'];
            $variables['device_type'] = 'web';
            //$variables['latitude'] = $request->latitude;
            //$variables['longitude'] = $request->longitude;
            $variables['ip'] = $request['ip'];
            $variables['udid'] = $request['device_token'];

        $response = $this->api_call($api, $method,$variables);

        if($response['status'] == true){
            session()->put('token', $response['data']['token']);
            session()->put('user_data', $response['data']);
            session()->put('device_token', $variables['device_token']);
            

            /*$language = Language::find($response['data']['language_id']);

            if (isset($language->api_key) && in_array($language->api_key, Session::get('language_array'))) {
                app()->setLocale($language->api_key);
            }
            //dd(app()->getLocale());
            session()->put('locale', $language->api_key);

            $api = 'appstring/main';
            $method = 1;
        
            $appstring = $this->api_call($api, $method);

            if($appstring['status'] == true){
                session()->put('app_string', $appstring['data']);
            }*/

        }

        return $response;
    }

    public function account_add(Request $request)
    {   
        if($request->isMethod('post')){

            $api = 'account/add';
            $method = 2;
            
            if($request['is_child'] == 'on'){
                $request['is_child'] = 1;
            }else{
                $request['is_child'] = 0;
            }

            $variables['first_name'] = $request['first_name'];
            $variables['last_name'] = $request['last_name'];
            $variables['is_child'] = $request['is_child'];
            $variables['profile_picture'] = $request['profile_picture'];

            $response = $this->api_call($api, $method,$variables);
            if($response['status'] == true){
                
                if ($request->hasFile('profile_picture')) {
                    $dir = env('AWS_S3_MODE')."/users";
                    $profile_picture = CommonHelper::s3Upload($request->file('profile_picture'), $dir);
                    $update_data['profile_picture'] = $profile_picture;

                    User::whereId($response['data']['id'])->update($update_data);
                }

                if($request['type']=='profile'){
                    return redirect()->route('profile')->with('success',$response['message']);
                }else{
                    return redirect()->route('whos.watching')->with('success',$response['message']);
                }
            }else{
                return redirect()->back();
            }

        }
        return view('website.pages.add-profile');
    }

    public function set_cookie(Request $request)
    {
        $minutes = 24 * 7;
        $response = new Response('AgeRestriction');
        //$response->withCookie(cookie('age-restriction', 'YES', $minutes));
        Cookie::queue('age-restriction', "Yes", $minutes);

        $data['status'] = true;
        $data['message'] = 'successfully';
        // $data['response'] = $response;
        return json_encode($data);
    }

    public function get_cookie(Request $request)
    {
        $value = Cookie::get('age-restriction');
        dd($value);
    }

/*public function change_password()
    {
        return View('website.pages.change_password');
    }

    public function change_password_post()
    {
        $request = $_POST;
        $api = 'change_password';
        $method = 2;

        $variables['current_password'] = $request['current_password'];
        $variables['password'] = $request['password'];
        $variables['password_confirmation'] = $request['password_confirmation'];

        $response = $this->api_call($api, $method, $variables);
        if (isset($response) && isset($response['status']) && $response['status'] == true) {
            return redirect()->route('home','movie')->with('success', $response['message']);
        }
        return redirect()->route('change_password')->with('error', $response['message']);
    }*/
    
    public function popular_search(Request $request)
    {   
        $api = 'search/popular';
        $method = 1;
        $response = $this->api_call($api, $method);
        
        if($response['status'] == true){
            return view('website.pages.popular_search',$response);
        }else{
            return redirect()->back()->with('error',$response['message']);
        }
        
    }

    public function search(Request $request)
    {   
        $api = 'content/search';
        $method = 2;
        $variables['filter_string'] = $request['search'];
        $response = $this->api_call($api, $method,$variables);
        if($response['status'] == true && count($response['data'])>0){
            return $response;
        }else{
            
            if(Session::get('app_string')){ $msg = Session::get('app_string.search.search_data_not_found').' '.$request['search'].'. </br> '.Session::get('app_string.search.search_again'); }else{ $msg =  CommonHelper::multi_language('search','search_data_not_found')->multi_language_value->language_value.' '.$request['search'].'. </br> '.CommonHelper::multi_language('search','search_again')->multi_language_value->language_value;} 

            return $msg;
        }

    }

    public function content_liked(Request $request)
    {   
        $api = 'favorite/like';
        $method = 2;
        $variables['content_id'] = $request['id'];
        $variables['status'] = $request['status'];
        $response = $this->api_call($api, $method,$variables);
        
        if($response['status'] == true){
            session::put('user_data.liked',true);
            return $response;    
        }else{
            return $response;
        }
        
    }

    public function content_disliked(Request $request)
    {   
        $api = 'favorite/dislike';
        $method = 2;
        $variables['content_id'] = $request['id'];
        $variables['status'] = $request['status'];
        $response = $this->api_call($api, $method,$variables);
        
        if($response['status'] == true){
            session::put('user_data.disliked',false);
            return $response;    
        }else{
            return $response;
        }
        
    }

    
    public function content_my_list(Request $request)
    {   
        $api = 'mylist/add';
        $method = 2;
        $variables['content_id'] = $request['id'];
        $variables['status'] = $request['status'];
        $response = $this->api_call($api, $method,$variables);
        
        if($response['status'] == true){
            if($request['status'] == 1){
                session::put('user_data.my_list',false);
            }else{
                session::put('user_data.my_list',true);
            }
            
            return $response;    
        }else{
            return $response;
        }
        
    }

    
    public function notifications()
    {   
        $notification_data = Notification::where('user_id',Session::get('user_data.id'))->orderBy('id','desc')->get();
        
        if($notification_data){
            return $notification_data;
        }else{
            return false;
        }   
    }

    
}