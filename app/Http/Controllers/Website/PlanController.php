<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Traits\ApiWebsite;
use App\Models\User;
use App\Models\Plan;
use CommonHelper;
use Session;
use \Crypt;
use Exception;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Response;
use Cookie;
use App\Models\PaymentHistory;
use Srmklive\PayPal\Services\PayPal as PayPalClient;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DateTime;

class PlanController extends Controller
{
    use ApiWebsite;

    

    public function choose_plan()
    {   
        $api = 'plan';
        $method = 1;

        $response['data'] = $this->api_call($api, $method);
        $response['user'] = Session::get('user_data');
        
        return view('website.pages.all_plans',$response);

    }


    public function choose_payment_method(Request $request)
    {   
        if($request->isMethod('get')){
            return redirect()->route('all.plans');
        }
        //dd($request->all());
        $api = 'stripe/payment/method/default';
        $method = 1;
        $response['api_data'] = $this->api_call($api, $method);
        $response['post_data'] = $request->all();
        //$response['plan_detail'] = Plan::where('id',$request->plan_id)->first();

        return view('website.pages.choose_payment_method',$response);

    }



    public function subscription_plan_overview(Request $request)
    {   
        if($request->isMethod('get')){
            return redirect()->route('all.plans');
        }
        $request['exp_month_year'] = !empty($request['exp_month_year'])?date('m/Y',strtotime($request['exp_month_year'])):'';
        
        if($request['selected_card'] == 1){
            $api = 'stripe/payment/method/default';
            $method = 1;
            $response['api_data'] = $this->api_call($api, $method);
        }else{
            $request['number'] = $request['number'];
            $request['last_four_number'] = str_pad(substr($request['number'], -4), strlen($request['number']), ' ', STR_PAD_LEFT);
        }
        
        $api = 'plan';
        $method = 1;
        $response['api_plan_detail'] = $this->api_call($api, $method);
        $response['plan_detail'] = Plan::where('id',$request->plan_id)->first();
        $response['post_data'] = $request->all();
        
        //dd($response);

        return view('website.pages.subscription_plan_overview',$response);

    }



    public function stripe_return_response(Request $request)
    {   
        
        if($request['selected_card'] == 0){
             
            $api = 'stripe/plan/puchase';
            $method = 2;

            $variables['plan_type'] =$request['plan_type'];
            $variables['plan_id'] =$request['plan_id'];
            $variables['name'] =$request['name'];
            $variables['number'] =$request['number'];
            $variables['exp_month_year'] =$request['exp_month_year'];
            $variables['cvv'] =$request['cvv'];
        
        }else{

            $api = 'stripe/plan/update';
            $method = 2;

            $variables['plan_type'] =$request['plan_type'];
            $variables['plan_id'] =$request['plan_id'];

        }
        
        $response = $this->api_call($api, $method,$variables);
            
        if($response['status'] == true){

            Session::put('user_data.plan_id',$request['plan_id']);
            Session::put('user_data.plan_type',$request['plan_type']);
            
            return redirect()->route('home','movie')->with('success',$response['message']);
        }else{
            return redirect()->route('all.plans')->with('error',$response['message']);
        }
    }

    public function paypal_return_response(Request $request)
    {   
        //dd(Session::get('user_id'));
        if(empty($request->subscription_id) || empty($request->plan_id) || empty($request->plan_type)){
            return response([
                'status' => false,
                'message' => ''
            ]);
        }  

        $provider = new PayPalClient;
        $provider->getAccessToken();   
        
        $subscription_id = $request->subscription_id;
        $plan_id = $request->plan_id;
        $plan_type = $request->plan_type;
        $user_id = Session::get('user_data.id');
        $user = User::find($user_id);
        //dd($user);
        $result = $provider->showSubscriptionDetails($request->subscription_id);
        //dd(Carbon::parse($result['billing_info']['next_billing_time'])->format('Y-m-d'));

        if(isset($result['type']) && $result['type'] == 'error'){
            return response([
                'status' => false,
                'message' => ''
            ]);
        }else{
          if($result['status'] == 'APPROVAL_PENDING'){

            return response([
                'status' => false,
                'message' => __('message.paypal.subscription_not_activated')
            ]);

          }elseif ($result['status'] == 'ACTIVE') {

            $user->plan_id = $plan_id;
            $user->plan_type = $plan_type;
            $user->expiration_date = Carbon::parse($result['billing_info']['next_billing_time'])->format('Y-m-d');
            $user->save();

            User::where('parent_id',$user_id)->update(['plan_id'=>$plan_id,'plan_type'=>$plan_type]);

            Session::put('user_data.plan_id',$plan_id);
            Session::put('user_data.plan_type',$plan_type);

            $previous_plan_cancel = PaymentHistory::where('user_id',$user_id)->where('mode','paypal')->orderBy('id','desc')->first();

            if(!empty($previous_plan_cancel)){
                $plan_data = json_decode($previous_plan_cancel->response,true);
                $subscription_id = $previous_plan_cancel->transaction_id;
                $can_result = $provider->cancelSubscription($subscription_id, 'Not satisfied with the service');
            }

            $payment_history = PaymentHistory::create([
                'user_id' => $user_id,
                'mode' => "paypal",
                'plan_id' => $plan_id,
                'type' => $plan_type,
                'transaction_id' => $request->subscription_id,
                'response' => json_encode($result)
            ]);

            return response([
                'status' => true,
                'message' => __('message.payment.payment_successful')
            ]);
            
          }
        }
    }

}