<?php
namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Traits\ApiWebsite;
use App\Models\User;
use App\Models\Language;
use App\Models\AskToUs;
use App\Models\PaymentHistory;
use CommonHelper;
use Session;
use \Crypt;
use Exception;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Providers\RouteServiceProvider;
use Socialite;
use Illuminate\Http\Response;
use Cookie;
use DB;
use App;
use PragmaRX\Countries\Package\Countries;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Redirect;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;
use File;


class SettingController extends Controller
{
    use ApiWebsite;

    public function profile(Request $request)
    { 
        $api = 'accounts';
        $method = 1;
        $response = $this->api_call($api, $method);
        
        //dd($response);

        return view('website.pages.profile',$response);
    }


    public function allow_notification(Request $request)
    { 

        $api = 'notifications/update';
        $method = 2;
        $variables['status'] = $request['status'];

        $response = $this->api_call($api, $method,$variables);
        
        return $response;

    }

    public function account_profile(Request $request)
    { 

        if($request->isMethod('post')){

            if(!empty($request['location'])){

                $language = Language::where('api_key',$request['location'])->first();

                $api = 'language/add';
                $method = 2;
                $variables['language_id'] = $language['id'];

                $response = $this->api_call($api, $method,$variables);

                app()->setLocale($request['location']);
                session()->put('locale', $request['location']);

                $api = 'appstring/main';
                $method = 1;
            
                $appstring = $this->api_call($api, $method);

                if($appstring['status'] == true){
                    session()->put('app_string', $appstring['data']);
                }


            }

            if(!empty($request['phone']) && !empty($request['country_code'])){

                $api = 'send_otp';
                $method = 2;
                
                $variables['phone'] = $request['phone'];
                $variables['country_code'] = "+".$request['country_code'];
                $response = $this->api_call($api, $method, $variables);
                
                if($response['status'] == true){
                    session()->put('mobile_otp', $response['data']['otp']);
                    
                    $post_data = $request->all(); 
                    return redirect()->route('verify.otp',$post_data)->with('success',$response['message']);
                }else{
                    return redirect()->back()->with('error',$response['message']);
                }
                

            }

            if(Session::get('app_string')){ $msg = Session::get('app_string.app_titles_and_messages.profile_update'); }else{ $msg = CommonHelper::multi_language('app_titles_and_messages','profile_update')->multi_language_value->language_value; }
            
            return redirect()->route('profile')->with('success',$msg);

        }
        
        if(Session::has('user_data.id') && Session::has('token')){
            $api = 'preferred/languages';
            $method = 2;
            $response = $this->api_call($api, $method);

            if($response['status'] == true){
                $data['language'] = $response['data'];
            }else{
                $data['language'] = Language::all();
            }
                
            $data['user'] = Session::get('user_data');
            return view('website.pages.account-profile',$data);
        }else{
            return redirect()->route('index');
        }

        

    }

    public function change_password(Request $request)
    { 

        if($request->isMethod('post')){

            $api = 'change_password';
            $method = 2;
            
            $variables['current_password'] = trim($request['current_password']);
            $variables['password'] = trim($request['password']);
            $variables['password_confirmation'] = trim($request['password_confirmation']);

            $response = $this->api_call($api, $method, $variables);
            
            if($response['status'] == true){
                return redirect()->route('profile')->with('success',$response['message']);
            }else{
                return redirect()->back()->with('error',$response['message']);
            }
            
        }

        return view('website.pages.change-password');

    }

    public function register_device(Request $request)
    { 

        $api = 'user/devices';
        $method = 1;
        if(!empty(request()->get('page'))){
            $variables['page'] = (int)request()->get('page');
        }else{
            $variables['page'] = 1;
        }
        $variables['type'] = "web";
        $response = $this->api_call($api, $method, $variables);
        
        if($response['status'] == true){

            return view('website.pages.register-device',$response);
        }else{
            return redirect()->back()->with('error',$response['message']);
        }

    }

    public function remove_device(Request $request)
    { 
        $api = 'user/device/delete';
        $method = 2;
        $variables['devices_id'] =$request->devices_id;
        
        $response = $this->api_call($api, $method, $variables);
        
        if($response['status'] == true){
            return redirect()->route('register-device')->with('success',$response['message']);
        }else{
            return redirect()->back()->with('error',$response['message']);
        }
        
    }

    public function share_activity()
    { 
        $user = Session::get('user_data');
        if(!empty($user) && $user['parent_id'] == 0){
            return view('website.pages.share_activity',$user);
        }else{

            if(Session::get('app_string')){ $msg = Session::get('app_string.app_titles_and_messages.no_data_found'); }else{ $msg = CommonHelper::multi_language('app_titles_and_messages','no_data_found')->multi_language_value->language_value; }

            return redirect()->back()->with('error',$msg);   
        }
        
    }


    public function manage_profile()
    { 
        $api = 'accounts';
        $method = 1;
        $response = $this->api_call($api, $method);        
        return view('website.pages.manage-profile',$response);
    }


    public function edit_profile($id,Request $request)
    {   
        if($request->isMethod('post')){

            $api = 'profile/view/edit';
            $method = 2;
            
            if($request['is_child'] == 'on'){
                $request['is_child'] = 1;
            }else{
                $request['is_child'] = 0;
            }

            $variables['first_name'] = $request['first_name'];
            $variables['last_name'] = !empty($request['last_name'])?$request['last_name']:'User';
            $variables['is_child'] = $request['is_child'];
            $variables['user_id'] = $request['user_id'];

            if(isset($request['phone']) && !empty($request['phone'])){
                $variables['phone'] = $request['phone'];
                $variables['currency_code'] = $request['currency_code'];
                $variables['country_code'] = $request['country_code'];
            }

            $response = $this->api_call($api, $method,$variables);
            
            if($response['status'] == true){
                if ($request->hasFile('profile_picture')) {
                    $dir = env('AWS_S3_MODE')."/users";
                    $profile_picture = CommonHelper::s3Upload($request->file('profile_picture'), $dir);
                    $update_data['profile_picture'] = $profile_picture;

                    User::whereId($request['user_id'])->update($update_data);
                }

                if($request['user_id'] == session::get('user_data.id')){
                    $api = 'profile/view/edit';
                    $method = 2;
                    $variables['user_id'] = $request['user_id'];

                    $data['user'] = $this->api_call($api, $method,$variables);
                    if($data['user']['status'] == true){
                        Session::put('user_data.profile_picture_full_url',$data['user']['data']['profile_picture_full_url']);
                    }

                }

                return redirect()->route('manage-profile')->with('success',$response['message']);
            }else{
                return redirect()->back()->with('error',$response['message']);
            }

        }
        $id = base64_decode($id);

        $api = 'profile/view/edit';
        $method = 2;
        $variables['user_id'] = $id;

        $data['user'] = $this->api_call($api, $method,$variables);
        
        if($data['user']['status'] == true){
            return view('website.pages.edit-profile',$data);
        }else{
            return redirect()->back()->with('error',$data['user']['message']);
        }
        
    }


    public function lock_profile($id,Request $request)
    {   
        
        $data['id'] = base64_decode($id);
        $data['user'] = User::find(base64_decode($id));

        if(isset($request['type']) && !empty($request['type'])){
            $data['type'] = $request['type'];
        }else{
            $data['type'] = '';
        }

        if($request->isMethod('post')){

            $api = 'verify/pin';
            $method = 2;

            $variables['user_id'] = $request['user_id'];
            $variables['pin'] = $request['password'];
            
            $response = $this->api_call($api, $method,$variables);  

            if($response['status'] == true){

                if(isset($request['type']) && !empty($request['type'])){
                    $api = 'generate/token';
                    $method = 2;
                    $user = User::find($request['user_id']);
                        
                        if($request['device_name'] == 'Safari' || empty($request['device_token'])){
                            $device_token = Str::random(30);
                        }else{
                            $device_token = Session::get('device_token');
                        }

                        $variables['user_id'] = $user->id;
                        $variables['language_id'] = $user->language_id;
                        $variables['device_name'] = $request['device_name'];
                        $variables['device_token'] = $device_token;
                        $variables['device_type'] = 'web';
                        //$variables['latitude'] = $request['latitude'];
                        //$variables['longitude'] = $request['longitude'];
                        $variables['ip'] = $request['ip'];
                        $variables['udid'] = Session::get('device_token');

                    $gt_response = $this->api_call($api, $method,$variables);

                    if($gt_response['status'] == true){
                        session()->put('token', $gt_response['data']['token']);
                        session()->put('user_data', $gt_response['data']);
                        
                        $language = Language::find($gt_response['data']['language_id']);

                        if (isset($language->api_key) && in_array($language->api_key, config('app.available_locales'))) {
                            app()->setLocale($language->api_key);
                        }

                        session()->put('locale', $language->api_key);

                       return redirect()->route('home','movie');    
                    }

                }else{
                    return redirect()->route('edit-profile',base64_encode($request['user_id']))->with('success',$response['message']);    
                }

            }else{
                return redirect()->back()->with('error',$response['message']);
            }

        }
        
        return view('website.pages.lock-profile',$data);
    }


    public function profile_lock($id,Request $request)
    {   
        if($request->isMethod('post')){
            
            if($request['disable_pin'] == 'on'){
                $api = 'disable/pin';
                $method = 2;

                $variables['user_id'] = $request['user_id'];
                
                $response = $this->api_call($api, $method,$variables);  
            }else{
                $api = 'add/pin';
                $method = 2;

                $variables['user_id'] = $request['user_id'];
                $variables['pin'] = $request['pin_1'].$request['pin_2'].$request['pin_3'].$request['pin_4'];
                
                $response = $this->api_call($api, $method,$variables);
            }

            if($response['status'] == true){
                return redirect()->route('edit-profile',base64_encode($request['user_id']))->with('success',$response['message']);
            }else{
                return redirect()->back()->with('error',$response['message']);
            }

        }

        $data['id'] = base64_decode($id);
        $api = 'profile/view/edit';
        $method = 2;
        $variables['user_id'] = $data['id'];

        $data['user'] = $this->api_call($api, $method,$variables);
        
        if($data['user']['status'] == false){
            return redirect()->back()->with('error',$data['user']['message']);
        }
        
        return view('website.pages.profile-lock',$data);
    }

    public function playback_setting($id,Request $request)
    {   

        $data['id'] = base64_decode($id);
        
        $api = 'profile/view/edit';
        $method = 2;
        $variables['user_id'] = $data['id'];

        $data['user'] = $this->api_call($api, $method,$variables);
        
        if($data['user']['status'] == false){
            return redirect()->back()->with('error',$data['user']['message']);
        }

        if($request->isMethod('post')){
        
        if(isset($request['autoplay_previews']) && $request['autoplay_previews'] == 'on'){
            $request['autoplay_previews'] = 1;
        }else{
            $request['autoplay_previews'] = 0;
        }

        if(isset($request['autoplay']) && $request['autoplay'] == 'on'){
            $request['autoplay'] = 1;
        }else{
            $request['autoplay'] = 0;
        }
                  
                $api = 'playback/change';
                $method = 2;

                $variables['user_id'] = $request['user_id'];
                $variables['autoplay'] = $request['autoplay'];
                $variables['autoplay_previews'] = $request['autoplay_previews'];
                
                $response = $this->api_call($api, $method,$variables);
        
            if($response['status'] == true){

                session::put('user_data.autoplay',$request['autoplay']);
                session::put('user_data.autoplay_previews',$request['autoplay_previews']);

                return redirect()->route('edit-profile',base64_encode($request['user_id']))->with('success',$response['message']);
            }else{
                return redirect()->back()->with('error',$response['message']);
            }

        }

        return view('website.pages.playback-setting',$data);
    }


    public function preferred_categories($id,Request $request)
    {  

        if($request->isMethod('post')){
                
            $api = 'category/add';
            $method = 2;
            $variables['user_id'] = $request['user_id'];
            $variables['genres_id'] = implode(",",$request->preferred_category);

            $response = $this->api_call($api, $method,$variables);

            if($response['status'] == true){
                return redirect()->route('edit-profile',base64_encode($request['user_id']))->with('success',$response['message']);
            }else{
                return redirect()->back()->with('error',$response['message']);
            }

        }


        $api = 'category/list';
        $method = 2;
        $variables['user_id'] = base64_decode($id);

        $response['category'] = $this->api_call($api, $method,$variables);
        $response['id'] = base64_decode($id);

        if($response['category']['status'] == true){
            return view('website.pages.selected-preferred-categories',$response);
        }else{
            return redirect()->back()->with('error',$response['category']['message']);
        }

        
    }


    public function help()
    {          
        $api = 'faq';
        $method = 1;
        
        $response = $this->api_call($api, $method);
        
        if($response['status'] == true){
            return view('website.pages.help',$response);
        }else{
            return redirect()->back()->with('error',$response['message']);
        }

    }

    public function feedback(Request $request)
    {    
        
        $api = 'feedback/add';
        $method = 2;
        $variables['faq_id'] = $request['id'];
        $variables['user_id'] = $request['user_id'];
        $variables['description'] = $request['feedback_text'];

        $response = $this->api_call($api, $method,$variables);
        
        return $response;

    }

    public function ask_to_us_add(Request $request)
    {    
        if($request->isMethod('post')){

            if(!empty($request['question'])){
                $insert = AskToUs::create([
                    'user_id'=>!empty(Session::get('user_data.id'))?Session::get('user_data.id'):0,
                    'question'=>$request['question']
                ]);

                if($insert){
                    return 1;
                }else{
                    return 0;
                }
            }else{
                return 0;
            }
        }

    }


    public function viewing_restrictions($id, Request $request)
    {    
        if($request->isMethod('post')){

            $api = 'useragerestriction/add';
            $method = 2;
            $variables['user_id'] = $request['user_id'];
            $variables['age_rating_id'] = !empty($request['age_rating_id'])?implode(",",$request['age_rating_id']):'';
            $variables['content_id'] = $request['content_id'];
            

            $response = $this->api_call($api, $method,$variables);

            if($response['status'] == true){
                return redirect()->route('edit-profile',base64_encode($request['user_id']))->with('success',$response['message']);
            }else{
                return redirect()->back()->with('error',$response['message']);
            }

        }


        $api = 'useragerestriction/list';
        $method = 2;
        $variables['user_id'] = base64_decode($id);

        $response['user_restriction'] = $this->api_call($api, $method,$variables);
        $response['id'] = base64_decode($id);

        if($response['user_restriction']['status'] == true){

            $api = 'profile/view/edit';
            $method = 2;
            $variables['user_id'] = base64_decode($id);
            $response['user'] = $this->api_call($api, $method,$variables);

            return view('website.pages.viewing-restrictions',$response);

        }else{
           return redirect()->back()->with('error',$response['user_restriction']['message']);
        }

    }

    public function viewing_restrictions_search(Request $request)
    {   
        $api = 'useragerestriction/search';
        $method = 2;
        $variables['filter_string'] = $request['search'];
        $response = $this->api_call($api, $method,$variables);
        if($response['status'] == true && count($response['data'])>0){
            return $response;
        }else{
            
            if(Session::get('app_string')){ $msg = Session::get('app_string.search.search_data_not_found').' '.$request['search'].'. </br> '.Session::get('app_string.search.search_again'); }else{ $msg =  CommonHelper::multi_language('search','search_data_not_found')->multi_language_value->language_value.' '.$request['search'].'. </br> '.CommonHelper::multi_language('search','search_again')->multi_language_value->language_value;} 

            return $msg;
        }
        

    }


    public function payment_history()
    {   

        if(Session::has('user_data.id') && Session::has('token')){
            
            $data['payment_history'] = PaymentHistory::with(['users','plan'])->where('user_id',Session::get('user_data.id'))->orderBy('id','desc')->get();
            return view('website.pages.payment-history',$data);

        }else{

            if(Session::get('app_string')){ $msg = Session::get('app_string.app_titles_and_messages.no_data_found'); }else{ $msg = CommonHelper::multi_language('app_titles_and_messages','no_data_found')->multi_language_value->language_value; }
            
            return redirect()->back()->with('error',$msg);    
        }
    }


    public function account_remove($id)
    {   
        $api = 'account/remove';
        $method = 2;
        $variables['user_id'] = base64_decode($id);
        $response = $this->api_call($api, $method,$variables);
        
        if($response['status'] == true){
            
            if(Session::get('user_data.id') == $variables['user_id']){
                Session::flush();
                return redirect()->route('index')->with('success',$response['message']);    
            }
            
            return redirect()->route('manage-profile')->with('success',$response['message']);

        }else{
           return redirect()->back()->with('error',$response['message']);
        }

        
    }


     public function forgot_pin($id,Request $request)
    {   
        
        $data['id'] = base64_decode($id);
        $data['user'] = User::find(base64_decode($id));

        if($request->isMethod('post')){

            if($data['user']->parent_id > 0){
                $data['user'] = User::find($data['user']->parent_id);
            }

            if(Hash::check($request->password, $data['user']->password)) {
                
                session()->put('user_data', $data['user']);

                return redirect()->route('profile-lock',$id.'?type=forgot-pin');

            } else {
                if(Session::get('app_string')){ $msg = Session::get('app_string.forgot_password.correct-password'); }else{ $msg = CommonHelper::multi_language('forgot_password','correct-password')->multi_language_value->language_value; }
                return redirect()->back()->with('error',$msg);
            }
        }

        return view('website.pages.forgot-pin',$data);
    }


    public function chat_support(Request $request)
    {   

        $api = 'chat/history';
        $method = 1;

        if(!empty(request()->get('page'))){
            $variables['page'] = (int)request()->get('page');
        }else{
            $variables['page'] = 1;
        }

        $response = $this->api_call($api, $method,$variables);
        
        if($response['status'] == true){

            if($request->ajax()){
                return $response;
            }else{
                return view('website.pages.chat-support',$response);     
            }

        }else{
            return view('website.pages.chat-support',$response);     
        }
        
    }


}