<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Traits\ApiWebsite;
use App\Models\User;
use CommonHelper;
use Session;
use \Crypt;
use Exception;
use App\Providers\RouteServiceProvider;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Response;
use Cookie;
use Illuminate\Support\Str;

class SocialLoginController extends Controller
{
    use ApiWebsite;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle(Request $request)
    {   
        if($request['device_name'] == 'Safari' || empty($request['device_token'])){
            $request['device_token'] = Str::random(30);
        }

        //session()->put('latitude', $request['latitude']);
        //session()->put('longitude', $request['longitude']);
        session()->put('ip', $request['ip']);
        session()->put('device_name', $request['device_name']);
        session()->put('device_token', $request['device_token']);

        return Socialite::driver('google')->redirect();
    }
        
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback(Request $request)
    {
        try {
        
            $user = Socialite::driver('google')->user();

            $finduser = User::where('email', $user['email'])->where('parent_id',0)->first();
            
            $variables['email'] = $user['email'];
            $variables['device_name'] = Session::get('device_name');
            $variables['device_token'] = Session::get('device_token');
            $variables['device_type'] = 'web';
            $variables['social_id'] = $user['id'];
            $variables['social_type'] = 'google';
            $variables['first_name'] = $user->user['given_name'];
            $variables['last_name'] = $user->user['family_name'];

            $api = 'social-signin';
            $method = 2;

            if($finduser){
                
                $response = $this->api_call($api, $method, $variables);

                if($response['status'] == true){
                    session()->put('token', $response['data']['token']);
                    session()->put('user_data', $response['data']);
                   
                    return redirect()->route('whos.watching')->with('success',$response['message']);
                }else{
                    return redirect()->back()->with('error',$response['message']);
                }
       
            }else{

                $variables['reference_code'] = "AWE".CommonHelper::random_number();
                $variables['currency_code'] = 'USD';
                $variables['status'] = 1;
            
                $response = $this->api_call($api, $method, $variables);
                $response['status'] = true;
                if($response['status'] == true){
                    
                    session()->put('token', $response['data']['token']);

                    $gtapi = 'generate/token';
                    $gtmethod = 2;
                    
                        $gtvariables['user_id'] = $response['data']['id'];
                        $gtvariables['language_id'] = $response['data']['language_id'];
                        $gtvariables['device_name'] = Session::get('device_name');
                        $gtvariables['device_token'] = Session::get('device_token');
                        $gtvariables['device_type'] = 'web';
                        //$gtvariables['latitude'] = Session::get('latitude');
                        //$gtvariables['longitude'] = Session::get('longitude');
                        $gtvariables['ip'] = Session::get('ip');
                        $gtvariables['udid'] = Session::get('device_token');

                    $gtresponse = $this->api_call($gtapi, $gtmethod, $gtvariables);
                    
                    if($gtresponse['status'] == true){
                        session()->put('token', $gtresponse['data']['token']);
                        session()->put('user_data', $gtresponse['data']);

                        return redirect()->route('preferred.category')->with('success',$response['message']);
                    }else{
                        return redirect()->route('whos.watching');
                    }

                }else{
                    return redirect()->back()->with('error',$response['message']);
                }
    
            }
      
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook(Request $request)
    {   
        if($request['device_name'] == 'Safari' || empty($request['device_token'])){
            $request['device_token'] = Str::random(30);
        }

       // session()->put('latitude', $request['latitude']);
        //session()->put('longitude', $request['longitude']);
        session()->put('ip', $request['ip']);
        session()->put('device_name', $request['device_name']);
        session()->put('device_token', $request['device_token']);

        return Socialite::driver('facebook')->redirect();
    }
        
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleFacebookCallback(Request $request)
    {
        try {
            
            $user = Socialite::driver('facebook')->user();

            $finduser = User::where('email', $user['email'])->where('parent_id',0)->first();
            
            $variables['email'] = $user['email'];
            $variables['device_name'] = Session::get('device_name');
            $variables['device_token'] = Session::get('device_token');
            $variables['device_type'] = 'web';
            $variables['social_id'] = $user['id'];
            $variables['social_type'] = 'facebook';
            $variables['first_name'] = $user['name'];
            $variables['last_name'] = '';

            $api = 'social-signin';
            $method = 2;

            if($finduser){
                
                $response = $this->api_call($api, $method, $variables);

                if($response['status'] == true){
                    session()->put('token', $response['data']['token']);
                    session()->put('user_data', $response['data']);
                    
                    return redirect()->route('whos.watching')->with('success',$response['message']);
                }else{
                    return redirect()->back()->with('error',$response['message']);
                }
       
            }else{

                
                $variables['reference_code'] = "AWE".CommonHelper::random_number();
                $variables['currency_code'] = 'USD';
                $variables['status'] = 1;

                $response = $this->api_call($api, $method, $variables);

                if($response['status'] == true){

                    session()->put('token', $response['data']['token']);
                    //session()->put('user_data', $response['data']);

                    $gtapi = 'generate/token';
                    $gtmethod = 2;
                    
                        $gtvariables['user_id'] = $response['data']['id'];
                        $gtvariables['language_id'] = $response['data']['language_id'];
                        $gtvariables['device_name'] = Session::get('device_name');
                        $gtvariables['device_token'] = Session::get('device_token');
                        $gtvariables['device_type'] = 'web';
                        //$gtvariables['latitude'] = Session::get('latitude');
                        //$gtvariables['longitude'] = Session::get('longitude');
                        $gtvariables['ip'] = Session::get('ip');
                        $gtvariables['udid'] = Session::get('device_token');

                    $gtresponse = $this->api_call($gtapi, $gtmethod, $gtvariables);
                    
                    if($gtresponse['status'] == true){
                        session()->put('token', $gtresponse['data']['token']);
                        session()->put('user_data', $gtresponse['data']);
                    }else{
                        return redirect()->route('whos.watching');
                    }
                    return redirect()->route('preferred.category')->with('success',$response['message']);
                }else{
                    return redirect()->back()->with('error',$response['message']);
                }
    
            }

        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

}