<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class Localization {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        // Check header request and determine localizaton


        if (Session::has('locale')) {
           $local = Session::get('locale');
        }else{
            $local = ($request->hasHeader('X-localization')) ? $request->header('X-localization'):'en';    
        }
        
        //App::setLocale($local);
        app()->setLocale($local);
        /*// set laravel localization
        // 
        
        // continue request*/
        return $next($request);
    }
}
