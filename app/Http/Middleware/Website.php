<?php
namespace App\Http\Middleware;

use Session;
use Closure;
use Illuminate\Http\Request;

class Website {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        if (!Session::get('user_data')) {
            return redirect('login');
        } else {
            return $next($request);
        }
        return $next($request);
    }
}
