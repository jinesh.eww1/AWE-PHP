<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\PlanAttribute;

class AccountListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {   

        $is_cast_tv = 0;
        if($this->plan_id > 0){
            $attr = PlanAttribute::where('plan_id', $this->plan_id)->where('attribute_id', 3)->first();
            $is_cast_tv = $attr->value;
        }

        return [
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'full_name' => $this->first_name.' '.$this->last_name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'country_code' => !empty($this->country_code)?$this->country_code:'',
            'phone' => $this->phone,
            'stripe_id' => $this->stripe_id,
            'profile_picture_full_url' => $this->profile_picture_full_url,
            'is_child' => $this->is_child,
            'social_id' => isset($this->social_id) ? $this->social_id : '',
            'social_type' => isset($this->social_type) ? $this->social_type : '',
            'autoplay' => $this->autoplay,
            'autoplay_previews' => $this->autoplay_previews,
            'notification' => $this->notification,
            'plan_id' => $this->plan_id,
            'plan_type' => $this->plan_type,
            'language_id' => (int)$this->language_id,
            'expiration_date' => $this->expiration_date,
            'expiration_date_formatted' => $this->expiration_date_formatted,
            'lock_password' => !empty($this->lock_password)?(string)$this->lock_password:'',
            'phone_verified' => $this->phone_verified,
            'email_verified' => !empty($this->email_verified_at)?$this->email_verified_at:'',
            'reference_code' => !empty($this->reference_code)?$this->reference_code:'',
            'is_cast_tv' => (int)$is_cast_tv,
        ];
    }
}
