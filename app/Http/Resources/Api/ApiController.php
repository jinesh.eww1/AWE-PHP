<?php defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class ApiController extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        header("Access-Control-Allow-Origin: *");
        $this
            ->load
            ->helper('common_helper');

             // Load paypal library & subscripemodel
        $this->load->library('paypal_lib');
        $this->load->model('subscripe');
    }

    /* --------------------------Event List Start-------------------------- */
    public function event_list_get($id = "", $search = '')
    {
        $response = array();

        if ($id != "")
        {
            $where = "`events`.`trash` = '0'";

            $month_array = array(
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July ',
                'August',
                'September',
                'October',
                'November',
                'December',
            );

            $search_month = ucfirst($search);

            if (in_array($search_month, $month_array))
            {
                $date = date('n', strtotime($search_month));

                if ($date != '')
                {
                    $where = $where . "AND (MONTH(events.event_date_time) = " . $date . " )";
                }
            }
            else if (isset($search))
            {
                $where = $where . "AND (events.events_name LIKE '%" . $search . "%' or events.city LIKE '%" . $search . "%')";
            }
            $current_date = date('Y-m-d');
            $where = "`events`.`trash` = '0' And events.event_date_time >='" . $current_date . "'";

            $all_events = $this
                ->db
                ->query('SELECT events.id, events.events_name, events.event_description,events.state_id,events.price,events.city,state.name as state_name,events.event_date_time as created_at,(SELECT image FROM user_event_pics  WHERE user_event_pics.type = "event" LIMIT 1) as image FROM events LEFT JOIN state ON events.state_id = state.id WHERE ' . $where . ' ORDER BY id DESC')->result_array();
            //echo $this->db->last_query();
            //exit();
            //_pre($all_events);
            $user_event_ids_data = $this
                ->Common_Model
                ->get_table_data_with_select_where("event_attendies", array(
                "event_id"
            ) , array(
                "user_id" => $id
            ));

            $user_event_ids_array = array();

            if (!empty($user_event_ids_data))
            {
                $user_event_ids_array = array_column($user_event_ids_data, "event_id");
            }
            $response["status"] = true;

            $event_list = array();
            $event_attending = array();

            if (!empty($all_events))
            {
                $response["message"] = "Event list get successfully!";

                foreach ($all_events as $ae)
                {
                    $event_id = $ae['id'];
                    $event_image = $this
                        ->db
                        ->query('SELECT image FROM user_event_pics  WHERE user_event_pics.type = "event" AND user_event_id=' . $event_id . ' LIMIT 1')->result_array();
                    if (!empty($event_image))
                    {
                        $ae["image"] = image_exist_or_not($event_image[0]['image']);
                    }
                    else
                    {
                        $ae["image"] = image_exist_or_not(" ");
                    }

                    if (in_array($ae["id"], $user_event_ids_array))
                    {
                        array_push($event_attending, $ae);
                    }
                    else
                    {
                        array_push($event_list, $ae);
                    }
                }
            }
            else
            {
                $response["message"] = "No events available!";
            }

            $is_user_subscription_expired = $this
                ->Common_Model
                ->IsSubscriptionCheck($id);
            //_pre($event_list);
            foreach ($event_list as $key => $value)
            {
                $event_list[$key]['date'] = convert_date_month_name($value['created_at']);
                $event_list[$key]['time'] = __time($value['created_at']);
                $event_list[$key]['isTicketbuy'] = 0;
                $event_list[$key]['isSubscription'] = $is_user_subscription_expired;
            }
            foreach ($event_attending as $key => $value)
            {
                $event_list[$key]['isTicketbuy'] = 0;
                $event_list[$key]['isSubscription'] = $is_user_subscription_expired;
            }

            $response["event_list"] = $event_list;
            $response["event_attending"] = $event_attending;

            // $response["isTicketbuy"] = 0;
            // $response["isSubscription"] = 1;
            
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }

        $this->response($response);
    }
    /* --------------------------Event List End-------------------------- */

    /* -------------------------- POST Event List Start-------------------------- */
    public function event_list_post()
    {
        $response = array();
        $post_fields["id"] = isset($_POST["id"]) ? $_POST["id"] : "";

        $error_post = validate_post_fields($post_fields);

        if (empty($error_post))
        {
            $response = array();
            $current_date = date('Y-m-d');
            $where = "`events`.`trash` = '0' And events.event_date_time >='" . $current_date . "'";

            if (isset($_POST["search_type"]) && $_POST["search_type"] == 'name' && isset($_POST["search_text"]))
            {
                $where = $where . "AND events.events_name LIKE '%" . $_POST["search_text"] . "%'";
            }
            else if (isset($_POST["search_type"]) && $_POST["search_type"] == 'city' && isset($_POST["search_text"]))
            {
                $where = $where . "AND events.city LIKE '%" . $_POST["search_text"] . "%'";
            }
            else if (isset($_POST["search_type"]) && isset($_POST["search_text"]) && $_POST["search_type"] == 'month')
            {
                $search_month = ucfirst($_POST["search_text"]);

                $date = date('n', strtotime($search_month));

                if ($date != '')
                {
                    $where = $where . "AND (MONTH(events.event_date_time) = " . $date . " )";
                }
            }

            $all_events = $this
                ->db
                ->query('SELECT events.id, events.events_name, events.event_description, events.price,events.city,state.name as state_name,events.event_date_time as created_at,events.url_for_ticket,(SELECT image FROM user_event_pics  WHERE user_event_pics.type = "event" LIMIT 1) as image FROM events  LEFT JOIN state ON events.state_id = state.id WHERE ' . $where . ' ORDER BY event_date_time ASC')->result_array();

                // echo $this->db->last_query();die;

            $user_event_ids_data = $this
                ->Common_Model
                ->get_table_data_with_select_where("event_attendies", array(
                "event_id"
            ) , array(
                "user_id" => $post_fields["id"]
            ));
            // print_r($all_events);exit;
            $user_event_ids_array = array();

            if (!empty($user_event_ids_data))
            {
                $user_event_ids_array = array_column($user_event_ids_data, "event_id");
            }
            //print_r($user_event_ids_array);exit;
            $response["status"] = true;

            $event_list = array();
            $event_attending = array();

            if (!empty($all_events))
            {
                $response["message"] = "Event list get successfully!";

                //print_r($all_events);exit;
                foreach ($all_events as $ae)
                {
                    //echo $ae['id'].'<br>';
                    $event_id = $ae['id'];

                    $event_image = $this
                        ->db
                        ->query('SELECT image FROM user_event_pics  WHERE user_event_pics.type = "event" AND user_event_id=' . $event_id . ' LIMIT 1')->result_array();

                    if (!empty($event_image))
                    {
                        $ae["image"] = image_exist_or_not($event_image[0]['image']);
                    }
                    else
                    {
                        //$ae["image"] =image_exist_or_not(" ");
                        $ae["image"] = BASE_URL() . 'assets/images/no-image.png';
                    }

                    if (in_array($ae["id"], $user_event_ids_array))
                    {
                        array_push($event_attending, $ae);
                    }
                    else
                    {
                        array_push($event_list, $ae);
                    }
                }

            }
            else
            {
                $response["message"] = "No events available!";
            }
            //print_r($event_list);exit;
            $is_user_subscription_expired = $this
                ->Common_Model
                ->IsSubscriptionCheck($_POST["id"]);

            foreach ($event_list as $key => $value)
            {
                $event_list[$key]['date'] = convert_date_month_name($value['created_at']);
                $event_list[$key]['time'] = __time($value['created_at']);
                $event_list[$key]['isTicketbuy'] = $this
                    ->Common_Model
                    ->isTicketbuy($_POST["id"], $value['id']);
                //$event_list[$key]['isSubscription'] = 1;
                $event_list[$key]['isSubscription'] = $is_user_subscription_expired;
            }
            foreach ($event_attending as $key => $value)
            {
                $event_attending[$key]['date'] = convert_date_month_name($value['created_at']);
                $event_attending[$key]['time'] = __time($value['created_at']);
                $event_attending[$key]['isTicketbuy'] = $this
                    ->Common_Model
                    ->isTicketbuy($_POST["id"], $value['id']);
                //$event_list[$key]['isSubscription'] = 1;
                $event_attending[$key]['isSubscription'] = $is_user_subscription_expired;
            }

            $response["event_list"] = $event_list;
            $response["event_attending"] = $event_attending;
        }
        else
        {
            $response["status"] = false;
            $response["message"] = $error_post;
        }

        $this->response($response);
    }
    /* --------------------------POST Event List End-------------------------- */

    /* --------------------------Event Details Start-------------------------- */
    public function event_details_get($id = "", $user_id = "")
    {
        $response = array();
        if ($id != "")
        {
            //$event_details = $this->Common_Model->get_one_table_data("events",array("id" => $id));
            $this
                ->db
                ->select('events.*,state.name as state_name');
            $this
                ->db
                ->join('state', 'events.state_id =state.id', 'left');
            $this
                ->db
                ->where('events.id', $id);
            $this
                ->db
                ->where('events.trash', "0");

            $event_details = $this
                ->db
                ->get('events')
                ->row_array();

            if (!empty($event_details))
            {
                $response["status"] = true;
                $response["message"] = "Event details get successfully!";
                $response["data"] = $event_details;
                $response["data"]["event_date_time"] = __datetime($response["data"]["event_date_time"]);
                $response["data"]["event_pics"] = array();
                $response["data"]["isTicketbuy"] = $this
                    ->Common_Model
                    ->isTicketbuy($user_id, $id);

                if ($user_id != "")
                {
                    $is_user_subscription_expired = $this
                        ->Common_Model
                        ->IsSubscriptionCheck($user_id);

                    $response["data"]["isSubscription"] = $is_user_subscription_expired;
                }
                else
                {
                    $response["data"]["isSubscription"] = 1;
                }

                $event_pics_details = $this
                    ->Common_Model
                    ->get_table_data_with_select_where("user_event_pics", array(
                    "id",
                    "image"
                ) , array(
                    "user_event_id" => $id,
                    "type" => "event"
                ));
                if (!empty($event_pics_details))
                {
                    foreach ($event_pics_details as $epd)
                    {
                        $image_exist = file_exist($epd["image"]);
                        if ($image_exist)
                        {
                            array_push($response["data"]["event_pics"], image_exist_or_not($epd["image"]));
                        }
                        else
                        {
                            $this
                                ->Common_Model
                                ->delete_table_data("user_event_pics", array(
                                "id" => $epd["id"]
                            ));
                        }
                    }
                }
                else
                {
                    array_push($response["data"]["event_pics"], BASE_URL() . 'assets/images/no-image.png');
                }
                $field_data = array(
                    "users.*"
                );
                $join_condition = "event_attendies.user_id = users.id";
                $where_data = array(
                    "event_attendies.event_id" => $id,
                    "event_attendies.user_id!=" => $user_id
                );
                $attending_users_list = $this
                    ->Common_Model
                    ->get_two_table_join_data("event_attendies", "users", $field_data, $join_condition, $where_data);
                $response["data"]["attending_users_list"] = $attending_users_list;
            }
            else
            {
                $response["status"] = false;
                $response["message"] = "Event not found!";
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        $this->response($response);
    }
    /* --------------------------Event Details End-------------------------- */

    /* --------------------------Send Message Start-------------------------- */
    public function send_message_post()
    {
        $response = array();
        $post_fields["sender_id"] = isset($_POST["sender_id"]) ? $_POST["sender_id"] : "";
        $post_fields["receiver_id"] = isset($_POST["receiver_id"]) ? $_POST["receiver_id"] : "";
        $post_fields["message"] = isset($_POST["message"]) ? $_POST["message"] : "";

        $error_post = validate_post_fields($post_fields);
        if (empty($error_post))
        {
            $post_fields["message"] = escape_str($post_fields["message"]);
            $post_fields["created_at"] = date("Y-m-d H:i:s");
            $post_fields["read_status"] = "0";
            $result = $this
                ->Common_Model
                ->insert_data("chat", $post_fields);
            if ($result != 0)
            {
                $response["status"] = true;
                $response["message"] = "Message send successfully!";
            }
            else
            {
                $response["status"] = false;
                $response["message"] = "Problem when send message. Please try again!";
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = $error_post;
        }
        $this->response($response);
    }
    /* --------------------------Send Message End-------------------------- */

    /* --------------------------Read Message End-------------------------- */
    public function read_message_get($id = "")
    {
        $response = array();
        if ($id != "")
        {
            $update_data = array();
            $update_data["read_status"] = "1";
            $result = $this
                ->Common_Model
                ->update_table_data("chat", $update_data, array(
                "id" => $id
            ));
            if ($result)
            {
                $response["status"] = true;
                $response["message"] = "Message readed successfully!";
            }
            else
            {
                $response["status"] = false;
                $response["message"] = "Problem when update read message. Please try again!";
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = $error_post;
        }
        $this->response($response);
    }
    /* --------------------------Read Message End-------------------------- */

    /* --------------------------Chat Message History Start-------------------------- */
    public function chat_history_post()
    {
        $response = array();
        $post_fields["sender_id"] = isset($_POST["sender_id"]) ? $_POST["sender_id"] : "";
        $post_fields["receiver_id"] = isset($_POST["receiver_id"]) ? $_POST["receiver_id"] : "";
        $post_fields["page"] = isset($_POST["page"]) ? $_POST["page"] : "";

        $error_post = validate_post_fields($post_fields);
        if (empty($error_post))
        {
            $where = "(`user_id` = '" . $post_fields["sender_id"] . "' AND `starred_id` = '" . $post_fields["receiver_id"] . "') OR (`user_id` = '" . $post_fields["receiver_id"] . "' AND `starred_id` = '" . $post_fields["sender_id"] . "')";
            $query_data = array(
                "table" => "`user_starred`",
                "fields" => array(
                    "`id`"
                ) ,
                "wherestring" => !empty($where) ? $where : "",
                "totalrow" => "1",
            );
            $count_Starred = $this
                ->Common_Model
                ->get_query_data($query_data);
            if ($count_Starred)
            {
                $length = 10;
                $start = $post_fields["page"] * 10 - 10;
                $where = "(`sender_id` = '" . $post_fields["sender_id"] . "' AND `receiver_id` = '" . $post_fields["receiver_id"] . "') OR (`sender_id` = '" . $post_fields["receiver_id"] . "' AND `receiver_id` = '" . $post_fields["sender_id"] . "')";
                $query_data = array(
                    "table" => "`chat`",
                    "fields" => array(
                        "`id`,`message`,`read_status`"
                    ) ,
                    "wherestring" => !empty($where) ? $where : "",
                    "orderby" => "created_at",
                    "sort" => "DESC",
                    "num" => $length,
                    "offset" => $start,
                );
                $chat_history = $this
                    ->Common_Model
                    ->get_query_data($query_data);
                /* $chat_history = array_reverse($chat_history); */
                if (!empty($chat_history))
                {
                    $response["status"] = true;
                    $response["message"] = "Chat history get successfully!";
                    $response["data"] = $chat_history;
                }
                else
                {
                    $response["status"] = false;
                    $response["message"] = "No further chat available!";
                }
            }
            else
            {
                $response["status"] = false;
                $response["message"] = "This user not starred with other!";
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = $error_post;
        }
        $this->response($response);
    }
    /* --------------------------Chat Message History End-------------------------- */

    /* --------------------------Matches list All New & Starred -------------------------- */
    public function matches_get($id = "", $matches_type = "", $page = 1)
    {
        $response = array();
        $color_code = $this
            ->config
            ->item('no_starred');
        if (isset($id) && isset($matches_type) && $id != '' && $matches_type != '')
        {
            $where_data = array(
                "id" => $id,
                "trash" => "0"
            );
            $user_info = $this
                ->db
                ->get_where('users', $where_data)->result_array();
            
            $gender = "Women";
            if (!empty($user_info)) { 
                $interested_in = $user_info[0]['interested_in'];
                if ($user_info[0]['gender'] == "Male")
                {
                    $gender = "Men";
                }
            }    

            if ($page == 0)
            {
                $page = 1;
            }

            if ($page == 1)
            {
                $start = 0;
            }
            else
            {
                $start = ($page - 1) * 10;
            }

            $limit = 10;

            $this
                ->db
                ->select('user_starred.*');
            $this
                ->db
                ->join('users', 'user_starred.starred_id =users.id', 'left');
            $this
                ->db
                ->where('user_starred.user_id', $id);
            $this
                ->db
                ->where('users.trash', "0");
            $this
                ->db
                ->where('users.status', "1");

            $total_starred = $this
                ->db
                ->get('user_starred')
                ->num_rows();
            //echo $this->db->last_query();exit;
            if (isset($total_starred) && $total_starred != '')
            {
                $starred_count = $total_starred;
            }
            else
            {
                $starred_count = 0;
            }

            if ($matches_type == "new")
            {
                $cur_date = date('Y-m-d H:i:s');

                // user record of last 5 days
                $this
                    ->db
                    ->select('*');
                $this
                    ->db
                    ->where('created_at BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW()');
                $this
                    ->db
                    ->where('trash', "0");
                $this
                    ->db
                    ->where('status', "1"); //0 = false, 1 = true , 0 = lock, 1 = unlock
                if ($interested_in != 'Both')
                {
                    if ($interested_in == 'Men')
                    {
                        $this
                            ->db
                            ->where('gender', 'Male');
                    }
                    else
                    {
                        $this
                            ->db
                            ->where('gender', 'Female');
                    }

                }
                $this
                    ->db
                    ->group_start();
                $this
                    ->db
                    ->where('interested_in', $gender);
                $this
                    ->db
                    ->or_where('interested_in', 'Both');
                $this
                    ->db
                    ->group_end();
                $this
                    ->db
                    ->where_not_in('id', $id);
                $this
                    ->db
                    ->limit($limit, $start);
                $this
                    ->db
                    ->order_by('id', 'DESC');

                $new_users = $this
                    ->db
                    ->get('users')
                    ->result_array();

                //echo $this->db->last_query();
                //exit;
                if (!empty($new_users))
                {
                    $response["status"] = true;

                    $response["message"] = "New matches get successfully!";
                    $current_day = date('Y-m-d H:i:s');
                    foreach ($new_users as $key => $new_user_value)
                    {

                        if ($new_user_value['profile_pic'] != '')
                        {
                            $new_users[$key]['profile_pic'] = BASE_URL() . $new_user_value['profile_pic'];
                        }
                        else
                        {
                            $new_users[$key]['profile_pic'] = BASE_URL() . 'assets/images/person.png';
                        }

                        if ($new_user_value['full_name'] == '')
                        {
                            $new_users[$key]['full_name'] = $new_user_value['first_name'] . ' ' . $new_user_value['last_name'];
                        }
                        else if ($new_user_value['first_name'] == '' && $new_user_value['last_name'] == '')
                        {
                            $new_users[$key]['first_name'] = $new_user_value['user_name'];
                            $new_users[$key]['last_name'] = $new_user_value['user_name'];
                        }

                        $horoscope_details = [];

                        $horoscope_details[] = ["name" => $this
                            ->Common_Model
                            ->get_sunsign($new_users[$key]['id']) , "image" => BASE_URL() . 'assets/images/horoscope/gemini-rashi.png'];

                        $horoscope_details[] = ["name" => $this
                            ->Common_Model
                            ->get_moonsign($new_users[$key]['id']) , "image" => BASE_URL() . 'assets/images/horoscope/aries-rashi.png'];
                        $new_users[$key]["horoscope"] = $horoscope_details;

                    }
                    $response["data"] = $new_users;

                    if (!empty($response["data"]) && $response["data"] != '')
                    {

                        foreach ($response["data"] as $key => $value)
                        {
                            $star_color = $this
                                ->config
                                ->item('no_starred');
                            $response["data"][$key]["match_score"] = 93;

                            $response["data"][$key]["is_online"] = 1; // 0 = false, 1 = true
                            $this
                                ->db
                                ->select('*');
                            $this
                                ->db
                                ->where('user_id', $id);
                            $this
                                ->db
                                ->where('starred_id', $value['id']);

                            $is_starred = $this
                                ->db
                                ->get('user_starred')
                                ->row_array();

                            if (is_null($is_starred))
                            {
                                $is_starred_user = 0; //no
                                
                            }
                            else
                            {
                                $this
                                    ->db
                                    ->select('*');
                                $this
                                    ->db
                                    ->where('user_id', $value['id']);
                                $this
                                    ->db
                                    ->where('starred_id', $id);
                                $is_starred_reversed = $this
                                    ->db
                                    ->get('user_starred')
                                    ->row_array();
                                if (is_null($is_starred_reversed))
                                {
                                    $is_starred_user = 1; //yes
                                    $star_color = $this
                                        ->config
                                        ->item('single_starred');
                                }
                                else
                                {
                                    $is_starred_user = 2;
                                    $star_color = $this
                                        ->config
                                        ->item('both_starred');
                                }
                            }

                            $response["data"][$key]["star_color"] = $star_color; // 1 = yes starred user, 0 = no starred usre
                            $response["data"][$key]["is_starred"] = $is_starred_user; // 1 = yes starred user, 0 = no starred usre
                            $response["data"][$key]["is_online"] = 0; // 1 = yes starred user, 0 = no starred usre
                            $response["starred_count"] = $starred_count;
                        }
                    }
                }
                else
                {
                    $response["status"] = true;
                    $response["message"] = "No new matches at this time. Please check again soon!";
                    $response["data"] = [];
                    $response["starred_count"] = $starred_count;

                }

            }
            else if ($matches_type == "all")
            {
                $this
                    ->db
                    ->select('*');
                $this
                    ->db
                    ->where('trash', "0");
                $this
                    ->db
                    ->where('status', "1");
                if ($interested_in != 'Both')
                {
                    if ($interested_in == 'Men')
                    {
                        $this
                            ->db
                            ->where('gender', 'Male');
                    }
                    else
                    {
                        $this
                            ->db
                            ->where('gender', 'Female');
                    }
                    $this
                        ->db
                        ->group_start();
                    $this
                        ->db
                        ->where('interested_in', $gender);
                    $this
                        ->db
                        ->or_where('interested_in', 'both');
                    $this
                        ->db
                        ->group_end();
                }
                $this
                    ->db
                    ->where_not_in('id', $id);
                $this
                    ->db
                    ->limit($limit, $start);
                $this
                    ->db
                    ->order_by('id', 'DESC');

                $new_users = $this
                    ->db
                    ->get('users')
                    ->result_array();

                // echo $this->db->last_query();
                // exit;
                if (!empty($new_users))
                {
                    $response["status"] = true;

                    $response["message"] = "All matches get successfully!";

                    foreach ($new_users as $key => $new_user_value)
                    {
                        $star_color = $this
                            ->config
                            ->item('no_starred');
                        if ($new_user_value['profile_pic'] != '')
                        {
                            $new_users[$key]['profile_pic'] = BASE_URL() . $new_user_value['profile_pic'];
                        }
                        else
                        {
                            $new_users[$key]['profile_pic'] = BASE_URL() . 'assets/images/person.png';
                        }

                        if ($new_user_value['full_name'] == '')
                        {
                            $new_users[$key]['full_name'] = $new_user_value['first_name'] . ' ' . $new_user_value['last_name'];
                        }
                        else if ($new_user_value['first_name'] == '' && $new_user_value['last_name'] == '')
                        {
                            $new_users[$key]['first_name'] = $new_user_value['user_name'];
                            $new_users[$key]['last_name'] = $new_user_value['user_name'];
                        }
                        $horoscope_details = [];

                        $horoscope_details[] = ["name" => $this
                            ->Common_Model
                            ->get_sunsign($new_users[$key]['id']) , "image" => BASE_URL() . 'assets/images/horoscope/gemini-rashi.png'];

                        $horoscope_details[] = ["name" => $this
                            ->Common_Model
                            ->get_moonsign($new_users[$key]['id']) , "image" => BASE_URL() . 'assets/images/horoscope/aries-rashi.png'];
                        $new_users[$key]["horoscope"] = $horoscope_details;
                    }

                    $response["data"] = $new_users;

                    foreach ($response["data"] as $key => $value)
                    {
                        $response["data"][$key]["match_score"] = 93;

                        $response["data"][$key]["is_online"] = 1; // 0 = true, 1 = false
                        

                        $this
                            ->db
                            ->select('*');
                        $this
                            ->db
                            ->where('user_id', $id);
                        $this
                            ->db
                            ->where('starred_id', $value['id']);

                        $is_starred = $this
                            ->db
                            ->get('user_starred')
                            ->row_array();

                        if (is_null($is_starred))
                        {
                            $is_starred_user = 0; //no
                            
                        }
                        else
                        {
                            $this
                                ->db
                                ->select('*');
                            $this
                                ->db
                                ->where('user_id', $value['id']);
                            $this
                                ->db
                                ->where('starred_id', $id);
                            $reversed_is_starred = $this
                                ->db
                                ->get('user_starred')
                                ->row_array();
                            if (is_null($reversed_is_starred))
                            {
                                $is_starred_user = 1; //yes
                                $star_color = $this
                                    ->config
                                    ->item('single_starred');
                            }
                            else
                            {
                                $is_starred_user = 2; //yes
                                $star_color = $this
                                    ->config
                                    ->item('both_starred');
                            }

                        }

                        $response["data"][$key]["star_color"] = $star_color; // 1 = yes starred user, 0 = no starred usre
                        $response["data"][$key]["is_starred"] = $is_starred_user; // 1 = yes starred user, 0 = no starred usre
                        $response["data"][$key]["is_online"] = 0;
                        $response["starred_count"] = $starred_count;
                    }

                }
                else
                {
                    $response["status"] = true;
                    $response["message"] = "Not anymore matches found!";
                    $response["data"] = [];
                    $response["starred_count"] = $starred_count;
                }
            }
            else if ($matches_type == "starred")
            {
                $this
                    ->db
                    ->select('*');
                $this
                    ->db
                    ->where('user_id', $id);

                $starred_users_data = $this
                    ->db
                    ->get('user_starred')
                    ->result_array();
                if (!empty($starred_users_data))
                {
                    foreach ($starred_users_data as $value)
                    {
                        $starred_users_list[] = $value['starred_id'];
                    }

                    $this
                        ->db
                        ->select('*');
                    $this
                        ->db
                        ->where('trash', "0");
                    $this
                        ->db
                        ->where('status', "1");
                    if ($interested_in != 'Both')
                    {
                        if ($interested_in == 'Men')
                        {
                            $this
                                ->db
                                ->where('gender', 'Male');
                        }
                        else
                        {
                            $this
                                ->db
                                ->where('gender', 'Female');
                        }
                        $this
                            ->db
                            ->group_start();
                        $this
                            ->db
                            ->where('interested_in', $gender);
                        $this
                            ->db
                            ->or_where('interested_in', 'both');
                        $this
                            ->db
                            ->group_end();
                    }
                    $this
                        ->db
                        ->where_in("id", $starred_users_list);
                    $this
                        ->db
                        ->limit($limit, $start);
                    $this
                        ->db
                        ->order_by('id', 'DESC');

                    $starred_users = $this
                        ->db
                        ->get('users')
                        ->result_array();
                    //echo $this->db->last_query();
                    // exit;
                    if (!empty($starred_users))
                    {
                        $response["status"] = true;

                        $response["message"] = "Starred matches get successfully!";

                        foreach ($starred_users as $key => $starred_users_value)
                        {
                            $star_color = $this
                                ->config
                                ->item('no_starred');
                            if ($starred_users_value['profile_pic'] != '')
                            {
                                $starred_users[$key]['profile_pic'] = BASE_URL() . $starred_users_value['profile_pic'];
                            }
                            else
                            {
                                $starred_users[$key]['profile_pic'] = BASE_URL() . 'assets/images/person.png';
                            }

                            if ($starred_users_value['full_name'] == '')
                            {
                                $starred_users[$key]['full_name'] = $starred_users_value['first_name'] . ' ' . $starred_users_value['last_name'];
                            }
                            else if ($starred_users_value['first_name'] == '' && $starred_users_value['last_name'] == '')
                            {
                                $starred_users[$key]['first_name'] = $starred_users_value['user_name'];
                                $starred_users[$key]['last_name'] = $starred_users_value['user_name'];
                            }
                            $horoscope_details = [];

                            $horoscope_details[] = ["name" => $this
                                ->Common_Model
                                ->get_sunsign($starred_users[$key]['id']) , "image" => BASE_URL() . 'assets/images/horoscope/gemini-rashi.png'];

                            $horoscope_details[] = ["name" => $this
                                ->Common_Model
                                ->get_moonsign($starred_users[$key]['id']) , "image" => BASE_URL() . 'assets/images/horoscope/aries-rashi.png'];
                            $starred_users[$key]["horoscope"] = $horoscope_details;
                        }

                        $response["data"] = $starred_users;

                        foreach ($response["data"] as $key => $value)
                        {
                            $response["data"][$key]["match_score"] = 93;

                            $response["data"][$key]["is_online"] = 1; // 0 = true, 1 = false
                            // $response["data"][$key]["horoscope"] = $horoscope_details;
                            $this
                                ->db
                                ->select('*');
                            $this
                                ->db
                                ->where('user_id', $id);
                            $this
                                ->db
                                ->where('starred_id', $value['id']);

                            $is_starred = $this
                                ->db
                                ->get('user_starred')
                                ->row_array();

                            if (is_null($is_starred))
                            {
                                $is_starred_user = 0; //no
                                
                            }
                            else
                            {
                                $this
                                    ->db
                                    ->select('*');
                                $this
                                    ->db
                                    ->where('user_id', $value['id']);
                                $this
                                    ->db
                                    ->where('starred_id', $id);
                                $reversed_is_starred = $this
                                    ->db
                                    ->get('user_starred')
                                    ->row_array();
                                if (is_null($reversed_is_starred))
                                {
                                    $is_starred_user = 1; //yes
                                    $star_color = $this
                                        ->config
                                        ->item('single_starred');
                                }
                                else
                                {
                                    $is_starred_user = 2; //yes
                                    $star_color = $this
                                        ->config
                                        ->item('both_starred');
                                }
                            }
                            // $is_starred_user = 1;
                            $response["data"][$key]["is_starred"] = $is_starred_user; // 0 = yes starred user, 1 = no starred usre
                            $response["data"][$key]["is_online"] = 0;
                            $response["starred_count"] = $starred_count;
                            $response["data"][$key]["star_color"] = $star_color;
                        }
                    }
                    else
                    {
                        $response["status"] = true;
                        $response["message"] = "Throw that special soul a star to let them know you think they are Stellar!

						 Click on the star icon on any user profile, and their profile will show up here.

						 You never know, they may even throw a star back at ya!";

                        $response["data"] = [];
                        $response["starred_count"] = $starred_count;
                    }
                }
                else
                {
                    $response["status"] = true;
                    $response["message"] = "Throw that special soul a star to let them know you think they are Stellar!

						 Click on the star icon on any user profile, and their profile will show up here.

						 You never know, they may even throw a star back at ya!";
                    $response["data"] = [];
                    $response["starred_count"] = $starred_count;
                }
            }

        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        if (isset($response['data']) && !empty($response['data']))
        {
            for ($i = 0;$i < count($response['data']);$i++)
            {
                $time = strtotime($response['data'][$i]['date_of_birth']);
                if (date('m-d') == date('m-d', $time))
                {
                    $response['data'][$i]['is_birthday'] = 1;
                }
                else
                {
                    $response['data'][$i]['is_birthday'] = 0;
                }
            }
        }

        $this->response($response);
    }
    /* --------------------------Matches List All New & Starred End-------------------------- */

    /* --------------------------Matches User Details-------------------------- */

    public function match_user_details_get($id = "", $loginid = "")
    {
        $response = array();
        $star_color = $this
            ->config
            ->item('no_starred');
        if ($id != "")
        {
            $user_details = $this
                ->Common_Model
                ->get_one_table_data("users", array(
                "id" => $id,
                'trash' => "0",
                'status' => "1"
            ));

            if (!empty($user_details))
            {
                $response["status"] = true;
                $response["message"] = "Users details get successfully!";
                $user_details['profile_pic'] = file_exist_or_not($user_details['profile_pic']);

                $response["data"] = $user_details;

                $horoscope_details = [];

                $horoscope_details[] = ["name" => $this
                    ->Common_Model
                    ->get_sunsign($id) , "image" => BASE_URL() . 'assets/images/horoscope/gemini-rashi.png'];

                $horoscope_details[] = ["name" => $this
                    ->Common_Model
                    ->get_moonsign($id) , "image" => BASE_URL() . 'assets/images/horoscope/aries-rashi.png'];

                $is_user_subscription_expired = $this
                    ->Common_Model
                    ->IsSubscriptionCheck($loginid);
                if ($response['data']['height_in_feet'] == NULL)
                {
                    $response['data']['height_in_feet'] = '';
                }
                if ($response['data']['height_in_inches'] == NULL)
                {
                    $response['data']['height_in_inches'] = '';
                }
                $response["data"]["match_score"] = $this->calculate_compatibility($loginid, $id);
                $response["data"]["is_online"] = 1; // 0 = true, 1 = false
                $response["data"]["mutual_kindness_friendship"] = 40;
                $response["data"]["communication"] = 55;
                $response["data"]["romance_attraction"] = 70;
                $response["data"]["overall_compatability"] = 85;
                $time = strtotime($response['data']['date_of_birth']);
                if (date('m-d') == date('m-d', $time))
                {
                    $response['data']['is_birthday'] = 1;
                }
                else
                {
                    $response['data']['is_birthday'] = 0;
                }
                //$response["data"]["isSubscription"] = 1;
                $response["data"]["isSubscription"] = $is_user_subscription_expired;
                $response["data"]["horoscope"] = $horoscope_details;

                $commons_groups = $this
                    ->db
                    ->query("SELECT group_user.group_id FROM `group_user` WHERE `user_id` IN (" . $id . "," . $loginid . ")")->result_array();

                $group_in_common = [];

                if (!empty($commons_groups))
                {
                    $arr = [];

                    foreach ($commons_groups as $key => $value)
                    {
                        $arr[] = $value['group_id'];
                    }

                    $commons_groups_ids = array_diff_assoc($arr, array_unique($arr));
                    $grp_var_ids = implode(',', $commons_groups_ids);

                    if (!empty($grp_var_ids))
                    {
                        $grp = $this
                            ->db
                            ->query("SELECT group_user.group_id,GROUP_CONCAT( DISTINCT users.full_name) as users,GROUP_CONCAT( DISTINCT users.id) as user_id,groups.total_members,groups.group_name,groups.group_image FROM group_user LEFT JOIN users ON group_user.user_id = users.id LEFT JOIN groups ON groups.id = group_user.group_id INNER JOIN  user_subscription ON user_subscription.user_id =  users.id WHERE user_subscription.status = '0' AND user_subscription.end_date >= '" . date('Y-m-d') . "' AND groups.status ='1' AND groups.trash = '0' AND find_in_set(group_user.group_id, ( SELECT GROUP_CONCAT(group_user.group_id) FROM group_user WHERE group_user.user_id = " . $loginid . ")) AND group_user.group_id IN(" . $grp_var_ids . ") GROUP BY group_user.group_id")->result_array();
                        // echo $this->db->last_query();exit;
                        // print_r($grp);exit;
                        for ($i = 0;$i < count($grp);$i++)
                        {
                            // echo "here";
                            $grp_data = [];
                            $grp_data['group_id'] = $grp[$i]['group_id'];
                            $grp_data['group_image'] = BASE_URL() . $grp[$i]['group_image'];
                            $grp_data['group_name'] = $grp[$i]['group_name'];
                            $users = explode(',', $grp[$i]['users']);
                            $user_id = explode(',', $grp[$i]['user_id']);
                            // print_r($users);exit;
                            $grp_user = [];

                            if (count($user_id) < 3)
                            {
                                foreach ($user_id as $value)
                                {
                                    //$grp_user[]['name'] = $value;
                                    $users_list = $this
                                        ->db
                                        ->select('first_name,last_name')
                                        ->where('id', $value)->get('users')
                                        ->row_array();
                                    if (!empty($users_list))
                                    {
                                        $grp_user[]['name'] = $users_list['first_name'] . ' ' . $users_list['last_name'];
                                    }
                                }

                                $count = 0;
                            }
                            else
                            {
                                for ($j = 0;$j < 2;$j++)
                                {
                                    //$grp_user[]['name'] = $users[$j];
                                    $users_list = $this
                                        ->db
                                        ->select('first_name,last_name')
                                        ->where('id', $user_id[$j])->get('users')
                                        ->row_array();
                                    if (!empty($users_list))
                                    {
                                        $grp_user[]['name'] = $users_list['first_name'] . ' ' . $users_list['last_name'];
                                    }
                                }

                                $count = count($user_id) - 2;
                            }
                            $grp_data['users'] = $grp_user;
                            $grp_data['count'] = $count;
                            $group_in_common[] = $grp_data;
                            $grp_data = [];
                        }
                        // var_dump(count($group_in_common));
                        if (!empty($group_in_common) && count($group_in_common) > 2)
                        {
                            $response["data"]["group_in_common"] = array_slice($group_in_common, 0, 2);
                            $response["data"]["view_all_group"] = 1; // 0 false, 1 true
                            
                        }
                        else
                        {
                            $response["data"]["group_in_common"] = $group_in_common;
                            $response["data"]["view_all_group"] = 0; // 0 false, 1 true
                            
                        }
                    }
                    else
                    {
                        $response["data"]["group_in_common"] = $group_in_common;
                        $response["data"]["view_all_group"] = 0; // 0 false, 1 true
                        
                    }
                }
                else
                {
                    $response["data"]["group_in_common"] = $group_in_common;
                    $response["data"]["view_all_group"] = 0; // 0 false, 1 true
                    
                }
                $current_date = date('Y-m-d');
                $where = "`events`.`trash` = '0' And events.event_date_time >='" . $current_date . "'";
                $this
                    ->db
                    ->select('*');
                $this
                    ->db
                    ->join('events', 'events.id = event_attendies.event_id', 'join');
                $this
                    ->db
                    ->join('user_event_pics', 'user_event_pics.user_event_id = event_attendies.event_id', 'join');
                $this
                    ->db
                    ->where('event_attendies.user_id', $id);
                $this
                    ->db
                    ->where('events.trash', "0");
                $this
                    ->db
                    ->where('events.event_date_time >= "' . $current_date . '"');
                $this
                    ->db
                    ->order_by('event_attendies.id', 'DESC');

                $attending_events_lists = $this
                    ->db
                    ->get('event_attendies')
                    ->result_array();

                $attending_event_list = [];
                foreach ($attending_events_lists as $key => $value)
                {
                    $value['image'] = BASE_URL() . $value['image'];
                    array_push($attending_event_list, $value);
                }

                $response["data"]["user_photos"] = array();

                $userpics = [];
                $user_pics = $this
                    ->db
                    ->query("SELECT path FROM users_pics WHERE user_id = " . $id . " AND deleted_at IS NULL ORDER BY id desc")->result_array();
                foreach ($user_pics as $key => $value)
                {

                    array_push($userpics, BASE_URL() . $value['path']);
                }

                $response["data"]["user_photos"] = $userpics;

                if (!empty($attending_event_list))
                {
                    $response["data"]["attending_events_lists"] = array_slice($attending_event_list, 0, 2);
                }
                else
                {
                    $response["data"]["attending_events_lists"] = [];
                }
                $view_all_event = 0;
                if (count($attending_event_list) > 2)
                {
                    $view_all_event = 1;
                }
                $response["data"]["view_all_event"] = $view_all_event;

                $is_starred = $this
                    ->db
                    ->select('*')
                    ->where('user_id', $loginid)->where('starred_id', $user_details['id'])->from('user_starred')
                    ->get()
                    ->num_rows();

                if ($is_starred > 0)
                {
                    // $is_starred_user = 1; //yes
                    $this
                        ->db
                        ->select('*');
                    $this
                        ->db
                        ->where('user_id', $user_details['id']);
                    $this
                        ->db
                        ->where('starred_id', $loginid);
                    $reversed_is_starred = $this
                        ->db
                        ->get('user_starred')
                        ->row_array();
                    if (is_null($reversed_is_starred))
                    {
                        $is_starred_user = 1; //yes
                        $star_color = $this
                            ->config
                            ->item('single_starred');
                    }
                    else
                    {
                        $is_starred_user = 2; //yes
                        $star_color = $this
                            ->config
                            ->item('both_starred');
                    }

                }
                else
                {
                    $is_starred_user = 0; //no
                    
                }

                $view_insert['user_id'] = $loginid;
                $view_insert['partner_id'] = $id;
                $view_insert['created_at'] = date('Y-m-d H:i:s');
                $this
                    ->db
                    ->insert('viewed_profile', $view_insert);

                $response["data"]["is_starred"] = $is_starred_user; // 0 = yes starred user, 1 = no starred usre
                $response["data"]["star_color"] = $star_color; // 0 = yes starred user, 1 = no starred usre
                
            }
            else
            {
                $response["status"] = false;
                $response["message"] = "User not found!";
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        $this->response($response);
    }

    /* --------------------------Matches User Details End-------------------------- */

    /* --------------------------View All Group-------------------------- */

    public function view_all_groups_get($id = "", $match_user_id = "")
    {
        $response = array();

        if ($id != "")
        {
            $user_details = $this
                ->Common_Model
                ->get_one_table_data("users", array(
                "id" => $id,
                'trash' => "0",
                'status' => "1"
            ));
            // print_r($user_details);exit;
            if (!empty($user_details))
            {

                if (isset($match_user_id) && $match_user_id != '')
                {
                    $commons_groups = $this
                        ->db
                        ->query("SELECT group_user.user_id,group_user.group_id FROM `group_user` WHERE `user_id` IN (" . $id . "," . $match_user_id . ")")->result_array();

                    if (!empty($commons_groups))
                    {

                        $arr = [];

                        foreach ($commons_groups as $key => $value)
                        {
                            $this
                                ->db
                                ->select('user_subscription.*');
                            $this
                                ->db
                                ->where('user_subscription.user_id', $value['user_id']);
                            $this
                                ->db
                                ->where('user_subscription.status', "0");
                            $this
                                ->db
                                ->where('user_subscription.end_date >', date('Y-m-d'));
                            $user_subscription_data = $this
                                ->db
                                ->get('user_subscription')
                                ->num_rows();

                            if (isset($user_subscription_data) && $user_subscription_data > 0)
                            {
                                $arr[] = $value['group_id'];
                            }
                            //$arr[] = $value['group_id'];
                            
                        }

                        $commons_groups_ids = array_diff_assoc($arr, array_unique($arr));
                        $grp_var_ids = implode(',', $commons_groups_ids);
                    }
                }

                if (isset($grp_var_ids) && !empty($grp_var_ids))
                {
                    $user_data = $this
                        ->db
                        ->query("SELECT group_user.group_id,group_user.user_id,GROUP_CONCAT( DISTINCT users.full_name) as users,GROUP_CONCAT( DISTINCT users.id) as user_id,groups.group_name,groups.group_image FROM group_user LEFT JOIN users ON group_user.user_id = users.id LEFT JOIN groups ON groups.id = group_user.group_id INNER JOIN  user_subscription ON user_subscription.user_id =  users.id WHERE user_subscription.status = '0' AND user_subscription.end_date >= " . date('Y-m-d') . " AND find_in_set(group_user.group_id, ( SELECT GROUP_CONCAT(group_user.group_id) FROM group_user WHERE group_user.user_id = " . $id . ")) AND group_user.group_id IN(" . $grp_var_ids . ") GROUP BY group_user.group_id")->result_array();
                }
                else
                {
                    $user_data = $this
                        ->db
                        ->query("SELECT group_user.group_id,group_user.user_id,GROUP_CONCAT( DISTINCT users.full_name) as users,GROUP_CONCAT( DISTINCT users.id) as user_id,groups.group_name,groups.group_image FROM group_user LEFT JOIN users ON group_user.user_id = users.id LEFT JOIN groups ON groups.id = group_user.group_id INNER JOIN  user_subscription ON user_subscription.user_id =  users.id WHERE user_subscription.status = '0' AND user_subscription.end_date >= " . date('Y-m-d') . " AND find_in_set(group_user.group_id, ( SELECT GROUP_CONCAT(group_user.group_id) FROM group_user WHERE group_user.user_id = " . $id . ")) GROUP BY group_user.group_id")->result_array();
                }
                //print_r($user_data);exit;
                //echo $this->db->last_query();exit;
                if (!empty($user_data))
                {
                    foreach ($user_data as $key => $user_data_value)
                    {

                        $user_ids = $user_data_value['users'];
                        $userids = $user_data_value['user_id'];
                        $users = explode(',', $user_ids);
                        $userid = explode(',', $userids);
                        //print_r($userid);exit;
                        $user_names = [];

                        $grp_user = [];
                        if (count($userid) < 3)
                        {
                            foreach ($userid as $user_value)
                            {
                                //$grp_user[]['name'] = $user_value;
                                $users_list = $this
                                    ->db
                                    ->select('first_name,last_name')
                                    ->where('id', $user_value)->get('users')
                                    ->row_array();
                                if (!empty($users_list))
                                {
                                    $grp_user[]['name'] = $users_list['first_name'] . ' ' . $users_list['last_name'];
                                }
                            }
                            $count = 0;
                        }
                        else
                        {
                            for ($j = 0;$j < 2;$j++)
                            {
                                //$grp_user[]['name'] = $users[$j];
                                $users_list = $this
                                    ->db
                                    ->select('first_name,last_name')
                                    ->where('id', $userid[$j])->get('users')
                                    ->row_array();
                                if (!empty($users_list))
                                {
                                    $grp_user[]['name'] = $users_list['first_name'] . ' ' . $users_list['last_name'];
                                }
                            }
                            $count = count($userid) - 2;
                        }

                        if (isset($user_data_value['group_image']) && $user_data_value['group_image'] != '')
                        {
                            $image_file = $user_data_value['group_image'];
                        }
                        else
                        {
                            $image_file = 'assets/images/person.png';
                        }
                        // print_r($user_data_value);
                        $user_data1[$key]['group_id'] = $user_data_value['group_id'];
                        $user_data1[$key]['group_name'] = $user_data_value['group_name'];
                        $user_data1[$key]['group_image'] = BASE_URL() . $image_file;
                        $user_data1[$key]['users'] = $grp_user;
                        $user_data1[$key]['count'] = $count;

                    }
                    // exit();
                    $response["status"] = true;
                    $response["data"] = $user_data1;
                }
                else
                {
                    $response["status"] = true;
                    $response["data"] = "There is no common users in same group";
                }
            }
            else
            {
                $response["status"] = false;
                $response["message"] = "User group not found!";
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        $this->response($response);
    }

    /* --------------------------View All Group End-------------------------- */
    /* --------------------------View All Group-------------------------- */

    public function view_all_events_get($id = "")
    {
        $response = array();

        if ($id != "")
        {
            $user_details = $this
                ->Common_Model
                ->get_one_table_data("users", array(
                "id" => $id,
                'trash' => "0",
                'status' => "1"
            ));
            // print_r($user_details);exit;
            if (!empty($user_details))
            {

                $current_date = date('Y-m-d');
                $where = "`events`.`trash` = '0' And events.event_date_time >='" . $current_date . "'";
                $this
                    ->db
                    ->select('*');
                $this
                    ->db
                    ->join('events', 'events.id = event_attendies.event_id', 'join');
                $this
                    ->db
                    ->join('user_event_pics', 'user_event_pics.user_event_id = event_attendies.event_id', 'left');
                $this
                    ->db
                    ->where('event_attendies.user_id', $id);
                $this
                    ->db
                    ->where('events.event_date_time >= "' . $current_date . '"');
                $this
                    ->db
                    ->order_by('event_attendies.id', 'DESC');

                $attending_events_lists = $this
                    ->db
                    ->get('event_attendies')
                    ->result_array();
                // var_dump($attending_events_lists);
                $attending_event_list = [];
                foreach ($attending_events_lists as $key => $value)
                {
                    $value['image'] = BASE_URL() . $value['image'];
                    array_push($attending_event_list, $value);
                }
                if (!empty($attending_event_list))
                {
                    $response['status'] = true;
                    $response['message'] = "Attending Event List get successfully.";
                    $response['data'] = $attending_event_list;
                }
                else
                {
                    $response["status"] = false;
                    $response["message"] = "No attending event found.";
                }

            }
            else
            {
                $response["status"] = false;
                $response["message"] = "User group not found!";
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        $this->response($response);
    }

    /* --------------------------View All Group End-------------------------- */

    /* --------------------------View All User-------------------------- */

    public function view_all_users_get($id = "")
    {
        $response = array();

        if ($id != "")
        {
            $group_details = $this
                ->Common_Model
                ->get_one_table_data("groups", array(
                "id" => $id,
                'trash' => "0",
                'status' => "1"
            ));
            // print_r($group_details);exit;
            if (!empty($group_details))
            {
                if ($group_details['type'] == "all")
                {
                    $this
                        ->db
                        ->select('users.*');
                    $this
                        ->db
                        ->where('users.status', "1");
                    $this
                        ->db
                        ->where('users.trash', "0");
                    $this
                        ->db
                        ->group_by('users.id');

                    $user_data = $this
                        ->db
                        ->get('users')
                        ->result_array();
                    //echo count($user_data);exit;
                    
                }
                else
                {
                    $this
                        ->db
                        ->select('users.*', 'group_user.*');
                    $this
                        ->db
                        ->join('group_user', 'users.id = group_user.user_id');
                    $this
                        ->db
                        ->where('group_user.group_id', $id);
                    $this
                        ->db
                        ->where('users.status', "1");
                    $this
                        ->db
                        ->where('users.trash', "0");
                    $this
                        ->db
                        ->group_by('users.id');

                    $user_data = $this
                        ->db
                        ->get('users')
                        ->result_array();
                }

                if (!empty($user_data))
                {
                    //print_r($user_data);exit;
                    $data = array();
                    foreach ($user_data as $value)
                    {
                        if ($value['id'] != '')
                        {
                            $idd[] = $value['id'];
                            $this
                                ->db
                                ->select('user_subscription.*');
                            $this
                                ->db
                                ->where('user_subscription.user_id', $value['id']);
                            $this
                                ->db
                                ->where('user_subscription.status', "0");
                            $this
                                ->db
                                ->where('user_subscription.end_date >', date('Y-m-d'));
                            $user_subscription_data = $this
                                ->db
                                ->get('user_subscription')
                                ->num_rows();
                            if (isset($user_subscription_data) && $user_subscription_data > 0)
                            {
                                $value['profile_pic'] = image_exist_or_not($value['profile_pic']);
                                $data[] = $value;
                            }

                        }

                    }
                    //echo count($idd);
                    //exit;
                    $response["status"] = true;
                    $response["data"] = $data;
                    //$response["count"] = count($data);
                    
                }
                else
                {
                    $response["status"] = true;
                    $response["data"] = "No users in this group";
                }
            }
            else
            {
                $response["status"] = false;
                $response["message"] = "Group not found!";
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        $this->response($response);
    }

    /* --------------------------View All User-------------------------- */

    /* --------------------------Add User To Starred List-------------------------- */

    public function add_remove_starred_post()
    {
        $response = array();
        $post_fields["user_id"] = isset($_POST["user_id"]) ? $_POST["user_id"] : "";
        $post_fields["starred_id"] = isset($_POST["starred_id"]) ? $_POST["starred_id"] : "";
        $star_color = $this
            ->config
            ->item('no_starred');
        $error_post = validate_post_fields($post_fields);

        if (empty($error_post))
        {
            $user_details = $this
                ->db
                ->select('user_starred.*')
                ->from('user_starred')
                ->where('user_id', $post_fields["user_id"])->where('starred_id', $post_fields["starred_id"])->get()
                ->num_rows();

            $user_data = $this
                ->db
                ->select('users.user_name')
                ->from('users')
                ->where('id', $post_fields["user_id"])->get()
                ->row_array();

            if ($user_details > 0)
            {
                $where = array(
                    'user_id' => $post_fields["user_id"],
                    'starred_id' => $post_fields["starred_id"]
                );

                $this
                    ->Common_Model
                    ->delete_table_data("user_starred", $where);

                /*if (!empty($user_data))
                {
                send_push($post_fields["starred_id"],'users','marked as unfavorite', $user_data['user_name']." Removed you from starred.", "SendBulkPush");
                }*/

                $response['status'] = true;
                $response['message'] = "Successfully removed from starred users";
                $response['is_starred'] = 0; //removed
                

                
            }
            else
            {
                $insert_starred_data['user_id'] = $post_fields["user_id"];
                $insert_starred_data['starred_id'] = $post_fields["starred_id"];
                $insert_starred_data['created_at'] = date("Y-m-d");

                $last_astro_insert_id = $this
                    ->Common_Model
                    ->insert_data("user_starred", $insert_starred_data);

                $response['status'] = true;
                $response['message'] = "Successfully added to starred users";
                $response['is_starred'] = 1; //added
                
            }
            $this
                ->db
                ->select('user_starred.*');
            $this
                ->db
                ->join('users', 'user_starred.starred_id =users.id', 'left');
            $this
                ->db
                ->where('user_starred.user_id', $post_fields["user_id"]);
            $this
                ->db
                ->where('users.trash', "0");
            $this
                ->db
                ->where('users.status', "1");
            $user_starred_count = $this
                ->db
                ->get('user_starred')
                ->num_rows();
            $response['starred_count'] = isset($user_starred_count) ? $user_starred_count : 0;
            $this
                ->db
                ->select('*');
            $this
                ->db
                ->where('user_id', $post_fields["user_id"]);
            $this
                ->db
                ->where('starred_id', $post_fields["starred_id"]);
            $is_starred = $this
                ->db
                ->get('user_starred')
                ->row_array();

            if (is_null($is_starred))
            {

                $is_starred_user = 0; //no
                
            }
            else
            {
                $this
                    ->db
                    ->select('*');
                $this
                    ->db
                    ->where('user_id', $post_fields["starred_id"]);
                $this
                    ->db
                    ->where('starred_id', $post_fields["user_id"]);
                $is_starred_reversed = $this
                    ->db
                    ->get('user_starred')
                    ->row_array();
                $extraparam = array(
                    'user_id' => $post_fields['user_id'],
                    'starred_id' => $post_fields['starred_id']
                );
                $suser_data = $this
                    ->db
                    ->select('*')
                    ->from('users')
                    ->where('id', $post_fields["starred_id"])->where('star_notification', '1')
                    ->where('push_notification', '1')
                    ->get()
                    ->row_array();
                if (is_null($is_starred_reversed))
                {
                    $is_starred_user = 1; //yes
                    $star_color = $this
                        ->config
                        ->item('single_starred');

                    if (!empty($user_data) && !empty($suser_data))
                    {
                        send_push($post_fields["starred_id"], 'users', 'Star Received', $user_data['user_name'] . " thinks you are Stellar!", "4", $extraparam);
                    }

                }
                else
                {
                    $is_starred_user = 2;
                    $star_color = $this
                        ->config
                        ->item('both_starred');
                    if (!empty($user_data) && !empty($suser_data))
                    {
                        send_push($post_fields["starred_id"], 'users', 'Star Received', $user_data['user_name'] . " thinks you are Stellar too!", "4", $extraparam);
                    }
                }

            }
            $response['star_color'] = $star_color; //removed
            
        }
        else
        {
            $response["status"] = false;
            $response["message"] = $error_post;
        }
        $this->response($response);
    }

    /* --------------------------Add User To Starred List End-------------------------- */

    /* --------------------------Starred List-------------------------- */

    public function starred_list_get($id = "", $page = 1)
    {
        $response = array();

        if ($id != "")
        {
            if ($page == 0)
            {
                $page = 1;
            }

            if ($page == 1)
            {
                $start = 0;
            }
            else
            {
                $start = ($page - 1) * 5;
            }

            $limit = 5;

            $this
                ->db
                ->select('*');
            $this
                ->db
                ->join('users', 'user_starred.starred_id = users.id', 'join');
            $this
                ->db
                ->where('user_starred.user_id', $id);
            $this
                ->db
                ->where('users.trash', "0");
            $this
                ->db
                ->where('users.status', "0");
            $this
                ->db
                ->limit($limit, $start);
            $this
                ->db
                ->order_by('user_starred.created_at', 'DESC');

            $starred_lists = $this
                ->db
                ->get('user_starred')
                ->result_array();

            if (!empty($starred_lists))
            {
                $response["status"] = true;
                $response["message"] = "Starred users get successfully!";
                $response["data"] = $starred_lists;
            }
            else
            {
                $response["status"] = true;
                $response["message"] = "Starred users not found!";
                $response["data"] = [];
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        $this->response($response);
    }

    /* --------------------------Add User To Starred List End-------------------------- */

    /* -------------------------- Subscription List Start-------------------------- */
    public function subscription_list_get($type = "", $user_id = "")
    {
        $response = array();
        //$u_plan_id = $type;
        $type = 4;

        if ($type != "" && $user_id != "")
        {
            $price_list = [];
            $user_plan = $this
                ->db
                ->select('*')
                ->from('user_subscription')
                ->where('user_id', $user_id)->limit('1')
                ->order_by('id', 'DESC')
                ->get()
                ->result_array();
            $diffDays = 0;
            $plan_purchase = 0;
            //print_r($user_plan);exit;
            if (!empty($user_plan))
            {
                $now = new DateTime($user_plan[0]['end_date']);
                $otherDate = new DateTime($user_plan[0]['start_date']);
                $diffrence = $now->diff($otherDate);
                $diffDays = (integer)$diffrence->format("%a");

            }
            //echo $diffDays;exit;
            $this
                ->db
                ->select('id, CASE WHEN plan_name = 1 THEN "View Pics" WHEN plan_name = 2 THEN "Attend Event" WHEN plan_name = 3 THEN "Group Chat" WHEN plan_name = 4 THEN "General Subscription" END AS plan_name, month_price as price, description, status, created_at, trash', false);
            $this
                ->db
                ->from('subscription');
            $this
                ->db
                ->where('plan_name', $type);
            $this
                ->db
                ->where(array(
                'trash' => 0,
                'status' => 1
            ));
            $month_price_list = $this
                ->db
                ->get()
                ->result_array();

            foreach ($month_price_list as $key => $value)
            {
                $month_price_list[$key]['id'] = 1;
                $month_price_list[$key]['duration'] = "/Month";

                if ($diffDays == 30)
                {
                    $plan_purchase = 1;
                }
                $month_price_list[$key]['plan_purchase'] = $plan_purchase;
                $price_list[] = $month_price_list[$key];
            }
            $plan_purchase = 0;
            $this
                ->db
                ->select('id, CASE WHEN plan_name = 1 THEN "View Pics" WHEN plan_name = 2 THEN "Attend Event" WHEN plan_name = 3 THEN "Group Chat" WHEN plan_name = 4 THEN "General Subscription" END AS plan_name, three_month_price as price, description, status, created_at, trash', false);
            $this
                ->db
                ->from('subscription');
            $this
                ->db
                ->where('plan_name', $type);
            $this
                ->db
                ->where(array(
                'trash' => 0,
                'status' => 1
            ));
            $three_month_price_list = $this
                ->db
                ->get()
                ->result_array();
            foreach ($three_month_price_list as $key => $value)
            {
                $three_month_price_list[$key]['id'] = 2;
                $three_month_price_list[$key]['duration'] = "/3 Months";
                if ($diffDays == 90)
                {
                    $plan_purchase = 1;
                }
                $three_month_price_list[$key]['plan_purchase'] = $plan_purchase;
                $price_list[] = $three_month_price_list[$key];
            }
            $plan_purchase = 0;
            $this
                ->db
                ->select('id, CASE WHEN plan_name = 1 THEN "View Pics" WHEN plan_name = 2 THEN "Attend Event" WHEN plan_name = 3 THEN "Group Chat" WHEN  plan_name = 4 THEN "General Subscription" END AS plan_name, year_price as price, description, status, created_at, trash', false);
            $this
                ->db
                ->from('subscription');
            $this
                ->db
                ->where('plan_name', $type);
            $this
                ->db
                ->where(array(
                'trash' => 0,
                'status' => 1
            ));
            $year_price_list = $this
                ->db
                ->get()
                ->result_array();
            foreach ($year_price_list as $key => $value)
            {
                $year_price_list[$key]['id'] = 3;
                $year_price_list[$key]['duration'] = "/Year";
                if ($diffDays == 365)
                {
                    $plan_purchase = 1;
                }
                $year_price_list[$key]['plan_purchase'] = $plan_purchase;
                $price_list[] = $year_price_list[$key];
            }

            if (!empty($month_price_list) && !empty($three_month_price_list) && !empty($year_price_list))
            {
                $response["status"] = true;
                $response["message"] = "Subscriptions get successfully!";
                $response['data'] = $price_list;
            }
            else
            {
                $response["status"] = true;
                $response["message"] = "Subscriptions not found!";
                $response["data"] = [];
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        $this->response($response);
    }
    /* -------------------------- Subscription List End-------------------------- */

    /* -------------------------- Prmocode List Start-------------------------- */
    public function promocode_list_get()
    {
        $response = array();
        $current_date = date('Y-m-d');
        $this
            ->db
            ->select('id, promocode_image as image, title as promocode title, description, promo_code as code, status,expiry_date');
        $this
            ->db
            ->from('promo_code');
        $this
            ->db
            ->where('status', "1");
        $this
            ->db
            ->where('trash', "0");
        $this
            ->db
            ->where('expiry_date >', $current_date);
        $this
            ->db
            ->order_by('id', "desc");
        $promocode_list = $this
            ->db
            ->get()
            ->result_array();
        foreach ($promocode_list as $key => $value)
        {
            $promocode_list[$key]['image'] = BASE_URL() . $value['image'];
        }

        if (!empty($promocode_list))
        {
            $response["status"] = true;
            $response["message"] = "Promocode get successfully!";
            $response["data"] = $promocode_list;
        }
        else
        {
            $response["status"] = true;
            $response["message"] = "Promocode not found!";
            $response["data"] = [];
        }

        $this->response($response);
    }
    /* -------------------------- Prmocode List End-------------------------- */

    /* --------------------------Contact Us-------------------------- */

    public function contact_us_post()
    {

        $response = array();
        $post_fields["user_id"] = isset($_POST["user_id"]) ? $_POST["user_id"] : "";
        $post_fields["email"] = isset($_POST["email"]) ? $_POST["email"] : "";
        $post_fields["message"] = isset($_POST["message"]) ? $_POST["message"] : "";
        $post_fields["help"] = isset($_POST["help"]) ? $_POST["help"] : "";

        $error_post = validate_post_fields($post_fields);

        if (empty($error_post))
        {

            $user_details = $this
                ->db
                ->select('users.*')
                ->from('users')
            // ->where('id',$post_fields["user_id"])
            
                ->where('email', $post_fields["email"])->where(array(
                'trash' => '0',
                'status' => '1'
            ))
                ->get()
                ->row_array();

            $admin = $this
                ->db
                ->get_where('admin', array(
                'type' => 'admin'
            ))
                ->row_array();

            if (!empty($user_details))
            {
                $post_fields["created_at"] = date('Y-m-d H:i:s');
                $post_fields["trash"] = "0";

                $this
                    ->Common_Model
                    ->insert_data("contact_us", $post_fields);

                $insert_id = $this
                    ->db
                    ->insert_id();

                if ($insert_id != '')
                {
                    $subject = $this
                        ->config
                        ->item("admin_site_name") . " - Contact Us";

                    $template_path = BASE_URL() . "email_template/contact_us.html";

                    $template = file_get_contents($template_path);

                    $template = admin_create_email_template($template);

                    $template = str_replace("##FULLNAME##", $admin["full_name"], $template);

                    $template = str_replace("##USERNAME##", $user_details["full_name"], $template);

                    $template = str_replace("##MESSAGE##", $post_fields['message'], $template);

                    $template = str_replace("##HELP##", $post_fields['help'], $template);

                    //send_mail('phpdeveloper7036@gmail.com',$subject,$template);
                    send_mail($admin["email"], $subject, $template);
                }

                $response['status'] = true;
                $response['message'] = "Inquiry sent successfully.";

            }
            else
            {
                $response['status'] = false;
                $response['message'] = "User not found!";
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = $error_post;
        }
        $this->response($response);
    }

    /* --------------------------Add User To Starred List End-------------------------- */

    /* -------------------------Match Filter List Start --------------------------------*/
    public function match_filter_new_post()
    { // print_r($_POST);
        $response = array();
        $user_count = 0;
        $post_fields["id"] = isset($_POST["userid"]) ? $_POST["userid"] : "";
        $post_fields["type"] = isset($_POST["type"]) ? $_POST["type"] : "";
        $post_fields["page"] = isset($_POST["page"]) ? $_POST["page"] : "";
        $where_data = array(
            "id" => $post_fields["id"],
            "trash" => "0"
        );
        $user_info = $this
            ->db
            ->get_where('users', $where_data)->result_array();
        $gender = "Women";
        $interested_in = $user_info[0]['interested_in'];
        if ($user_info[0]['gender'] == "Male")
        {
            $gender = "Men";
        }
        $error_post = validate_post_fields($post_fields);

        $post_fields["user_name"] = isset($_POST["user_name"]) ? $_POST["user_name"] : "";
        //$post_fields["age"] = isset($_POST["age"]) ? $_POST["age"] : "";
        $post_fields["location"] = isset($_POST["location"]) ? $_POST["location"] : "";
        $post_fields["sunsign"] = isset($_POST["sunsign"]) ? $_POST["sunsign"] : "";
        $post_fields["moonsign"] = isset($_POST["moonsign"]) ? $_POST["moonsign"] : "";
        $post_fields["latitude"] = isset($_POST["latitude"]) ? $_POST["latitude"] : "";
        $post_fields["longitude"] = isset($_POST["longitude"]) ? $_POST["longitude"] : "";
        $post_fields["miles"] = isset($_POST["miles"]) ? $_POST["miles"] : "";
        $post_fields["height_in_feet"] = isset($_POST["height_in_feet"]) ? $_POST["height_in_feet"] : "";
        $post_fields["height_in_inches"] = isset($_POST["height_in_inches"]) ? $_POST["height_in_inches"] : "";
        $post_fields["interested_in"] = isset($_POST["interested_in"]) ? $_POST["interested_in"] : "";
        $post_fields["max_height"] = isset($_POST["max_height"]) ? $_POST["max_height"] : "";
        $post_fields["min_height"] = isset($_POST["min_height"]) ? $_POST["min_height"] : "";
        $post_fields["max_age"] = isset($_POST["max_age"]) ? $_POST["max_age"] : "";
        $post_fields["min_age"] = isset($_POST["min_age"]) ? $_POST["min_age"] : "";
        $post_fields["within_days"] = isset($_POST["within_days"]) ? $_POST["within_days"] : "";
        $id = $post_fields["id"];
        if (empty($error_post))
        {
            $page = $post_fields['page'];
            if ($page == 0)
            {
                $page = 1;
            }
            if ($page == 1)
            {
                $start = 0;
            }
            else
            {
                $start = ($page - 1) * 10;
            }

            $users_interested = $this
                ->db
                ->select('interested_in')
                ->where('id', $id)->get('users')
                ->row_array();
            $visited_users = $this
                ->db
                ->select('partner_id')
                ->where('user_id', $id)->get('visited_profile')
                ->result_array();
            $visited_id = array();
            if (!empty($visited_users))
            {
                foreach ($visited_users as $vusers)
                {

                }
            }
            $this
                ->db
                ->select('*');
            $this
                ->db
                ->where('user_id', $id);
            $starred_users_data = $this
                ->db
                ->get('user_starred')
                ->result_array();
            $starred_users_list = [];
            if (!empty($starred_users_data))
            {
                foreach ($starred_users_data as $value)
                {
                    $starred_users_list[] = $value['starred_id'];
                }
            }

            $name = $post_fields['user_name'];
            $age = '';
            $loc = $post_fields['location'];
            $sunsign = $post_fields['sunsign'];
            $moonsign = $post_fields['moonsign'];
            $height_in_feet = $post_fields['height_in_feet'];
            $height_in_inches = $post_fields['height_in_inches'];
            // $interested_in = $post_fields['interested_in'];
            $id = $post_fields['id'];
            $miles = $post_fields['miles'];
            $matches_type = $post_fields['type'];
            $max_height = $post_fields["max_height"];
            $min_height = $post_fields["min_height"];
            $max_age = $post_fields["max_age"];
            $min_age = $post_fields["min_age"];
            $within_days = $post_fields["within_days"];
            $limit = 10;
            //echo $max_age;
            //echo $min_age;
            $this
                ->db
                ->select('users.*,astro_details.moon_sign,astro_details.sun_sign');
            $this
                ->db
                ->where('users.trash', "0");
            $this
                ->db
                ->where('users.status', "1");
            $this
                ->db
                ->where_not_in('users.id', $id);
            if ($interested_in != 'Both')
            {
                if ($interested_in == 'Men')
                {
                    $this
                        ->db
                        ->where('gender', 'Male');
                }
                else
                {
                    $this
                        ->db
                        ->where('gender', 'Female');
                }

            }
            $this
                ->db
                ->group_start();
            $this
                ->db
                ->where('interested_in', $gender);
            $this
                ->db
                ->or_where('interested_in', 'Both');
            $this
                ->db
                ->group_end();
            $this
                ->db
                ->join('astro_details', 'astro_details.user_id = users.id', 'left');

            if ($matches_type == "new")
            {
                $this
                    ->db
                    ->where('users.created_at BETWEEN DATE_SUB(NOW(), INTERVAL 5 DAY) AND NOW()');
            }
            if ($matches_type == "starred")
            {
                if (!empty($starred_users_list))
                {
                    $this
                        ->db
                        ->where_in("users.id", $starred_users_list);
                }
                else
                {
                    $response["status"] = true;
                    $response["message"] = "No matches found!";
                    $response["data"] = [];
                    $this->response($response);
                    exit;
                }
            }
            $where = 0;
            if ($name != "" || $max_age != "" || $min_age != "" || $loc != "" || $sunsign != '' || $moonsign != "" || ($min_height != "" && $max_height != "" || $within_days != ''))
            {
                $this
                    ->db
                    ->group_start();

                if ($name != "")
                {
                    $this
                        ->db
                        ->or_like('users.first_name', $name, 'both');
                    $this
                        ->db
                        ->or_like('users.user_name', $name, 'both');
                    $where = $where . " + CASE WHEN users.first_name LIKE '%" . $name . "%' OR users.user_name LIKE '%" . $name . "%' THEN 10 ELSE 0 END";
                }
                /*if($age != "")
                {
                $this->db->or_where('users.age',$age);
                $where = $where ." + CASE WHEN users.age = '".$age."' THEN 9 ELSE 0 END";
                }*/
                if ($max_age != "" && $min_age != '')
                {

                    $this
                        ->db
                        ->or_where('users.age >=', $min_age);
                    $this
                        ->db
                        ->where('users.age <=', $max_age);
                    $where = $where . " + CASE WHEN users.age >= '" . $min_age . "' and users.age <='" . $max_age . "' THEN 9 ELSE 0 END";
                }

                if ($loc != "")
                {
                    $this
                        ->db
                        ->or_like('users.city', $loc, 'both');
                    $this
                        ->db
                        ->or_like('users.state', $loc, 'both');
                    $where = $where . " + CASE WHEN users.city LIKE '%" . $loc . "%' THEN 8 ELSE 0 END";
                    $where = $where . " + CASE WHEN users.state LIKE '%" . $loc . "%' THEN 8 ELSE 0 END";
                }
                if ($sunsign != "")
                {
                    $this
                        ->db
                        ->or_where('astro_details.sun_sign', $sunsign);
                    $where = $where . " + CASE WHEN astro_details.sun_sign = '" . $sunsign . "' THEN 7 ELSE 0 END";
                }
                if ($moonsign != "")
                {
                    $this
                        ->db
                        ->or_where('astro_details.moon_sign', $moonsign);
                    $where = $where . " + CASE WHEN astro_details.moon_sign = '" . $moonsign . "' THEN 6 ELSE 0 END";
                }
                if ($min_height != "" && $max_height != "")
                {
                    $this
                        ->db
                        ->or_where('users.height_in_feet >=', $min_height);
                    $this
                        ->db
                        ->where('users.height_in_feet <=', $max_height);
                    $where = $where . " + CASE WHEN users.height_in_feet >= '" . $min_height . "' and users.height_in_feet <= '" . $max_height . "' THEN 11 ELSE 0 END";
                }
                if ($within_days != "")
                {

                    $this
                        ->db
                        ->where("DATEDIFF(NOW(), users.created_at) <= " . $within_days);

                }
                /*if($max_height != "")
                {
                $this->db->or_where('users.height_in_feet <=', $max_height);
                $where = $where ." + CASE WHEN users.height_in_feet <= '".$max_height."' THEN 12 ELSE 0 END";
                }*/
                $this
                    ->db
                    ->group_end();
                $this
                    ->db
                    ->order_by($where, 'DESC');
                $this
                    ->db
                    ->group_by('id');
            }
            else
            {
                $this
                    ->db
                    ->where('users.trash', "0");
                $this
                    ->db
                    ->order_by('users.id', 'DESC');
            }

            $new_users_filter_data = $this
                ->db
                ->from("users")
                ->get()
                ->result_array();
            $user_count = count($new_users_filter_data);
            $new_users_filter_data = array_slice($new_users_filter_data, $start, $limit);
            //echo $this->db->last_query();exit;
            // $data = array();
            // $km['setting_value'] = (array) $this->db->select('setting_value')->get('general_settings')->row();
            // $km = implode(',', $km['setting_value']);
            //echo $this->db->last_query();
            //exit;
            $km = $miles;
            $i = 0;
            $data = [];

            foreach ($new_users_filter_data as $row)
            {

                if ($post_fields["latitude"] != "" && $post_fields["longitude"] != "" && $post_fields["miles"] != '')
                {
                    if (is_numeric($row['latitude']) && is_numeric($row['longitude']) && is_numeric($post_fields["latitude"]) && is_numeric($post_fields["longitude"]))
                    {
                        $distance = distance($row['latitude'], $row['longitude'], $post_fields["latitude"], $post_fields["longitude"], "K");
                        // echo "distance: ".$distance."\n";
                        // echo "km: ".$km."\n";
                        $distance = 0.621371 * $distance;
                        if ($distance <= $km)
                        {
                            // print_r($row);
                            $data[$i] = $row;
                            $data[$i]['distance'] = $distance;
                            $i++;
                        }
                    }
                }
                else
                {
                    // print_r($row);
                    $data[$i] = $row;
                    $i++;
                }
            }

            // print_r($new_users_filter_data);exit;
            if (!empty($data))
            {
                $response["status"] = true;
                $response["message"] = "New matches get successfully!";
                $response['count'] = $user_count;
                foreach ($data as $key => $new_user_value)
                {
                    if ($new_user_value['profile_pic'] != '')
                    {
                        $data[$key]['profile_pic'] = BASE_URL() . $new_user_value['profile_pic'];
                    }
                    else
                    {
                        $data[$key]['profile_pic'] = BASE_URL() . 'assets/images/person.png';
                    }
                    $horoscope_details = [];

                    $horoscope_details[] = ["name" => $this
                        ->Common_Model
                        ->get_sunsign($data[$key]['id']) , "image" => BASE_URL() . 'assets/images/horoscope/gemini-rashi.png'];

                    $horoscope_details[] = ["name" => $this
                        ->Common_Model
                        ->get_moonsign($data[$key]['id']) , "image" => BASE_URL() . 'assets/images/horoscope/aries-rashi.png'];
                    $data[$key]["horoscope"] = $horoscope_details;
                }

                $response["data"] = $data;
                // print_r($new_users_filter_data);exit();
                foreach ($response["data"] as $key => $value)
                {
                    $star_color = $this
                        ->config
                        ->item('no_starred');
                    $response["data"][$key]["moon_sign"] = isset($value["moon_sign"]) ? $value["moon_sign"] : $this
                        ->Common_Model
                        ->get_moonsign($value['id']);
                    $response["data"][$key]["sun_sign"] = isset($value["sun_sign"]) ? $value["sun_sign"] : $this
                        ->Common_Model
                        ->get_sunsign($value['id']);
                    $response["data"][$key]["match_score"] = $this->calculate_compatibility($_POST['userid'], $value['id']);
                    $response["data"][$key]["is_online"] = 1; // 0 = false, 1 = true
                    $this
                        ->db
                        ->select('*');
                    $this
                        ->db
                        ->where('user_id', $id);
                    $this
                        ->db
                        ->where('starred_id', $value['id']);
                    $is_starred = $this
                        ->db
                        ->get('user_starred')
                        ->row_array();

                    if (is_null($is_starred))
                    {

                        $is_starred_user = 0; //no
                        
                    }
                    else
                    {
                        $this
                            ->db
                            ->select('*');
                        $this
                            ->db
                            ->where('user_id', $value['id']);
                        $this
                            ->db
                            ->where('starred_id', $id);
                        $is_starred_reversed = $this
                            ->db
                            ->get('user_starred')
                            ->row_array();
                        if (is_null($is_starred_reversed))
                        {
                            $is_starred_user = 1; //yes
                            $star_color = $this
                                ->config
                                ->item('single_starred');
                        }
                        else
                        {
                            $is_starred_user = 2;
                            $star_color = $this
                                ->config
                                ->item('both_starred');
                        }

                    }

                    $response["data"][$key]["is_starred"] = $is_starred_user; // 1 = yes starred user, 0 = no starred usre
                    $response["data"][$key]["is_online"] = 0; // 1 = yes starred user, 0 = no starred usre
                    $response["data"][$key]["star_color"] = $star_color; // 1 = yes starred user, 0 = no starred usre
                    
                }
            }
            else
            {
                $response["status"] = true;
                $response["message"] = "No matches found!";
                $response["data"] = [];
                $response['count'] = $user_count;

            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        if (isset($response['data']) && !empty($response['data']))
        {
            for ($i = 0;$i < count($response['data']);$i++)
            {
                $time = strtotime($response['data'][$i]['date_of_birth']);
                if (date('m-d') == date('m-d', $time))
                {
                    $response['data'][$i]['is_birthday'] = 1;
                }
                else
                {
                    $response['data'][$i]['is_birthday'] = 0;
                }
            }
        }
        $this->response($response);
    }
    /* -------------------------Match Filter List End --------------------------------*/

    /* -------------------------Match Filter List Start --------------------------------*/

    public function match_filter_post()
    { // print_r($_POST);
        $response = array();
        $user_count = 0;
        $post_fields["id"] = isset($_POST["userid"]) ? $_POST["userid"] : "";
        $post_fields["type"] = isset($_POST["type"]) ? $_POST["type"] : "";
        $post_fields["page"] = isset($_POST["page"]) ? $_POST["page"] : "";
        $post_fields["latitude"] = isset($_POST["latitude"]) ? $_POST["latitude"] : "";
        $post_fields["longitude"] = isset($_POST["longitude"]) ? $_POST["longitude"] : "";
        $post_fields["miles"] = isset($_POST["miles"]) ? $_POST["miles"] : "";
        
        $where_data = array(
            "id" => $post_fields["id"],
            "trash" => "0"
        );
        $user_info = $this
            ->db
            ->get_where('users', $where_data)->result_array();
        $gender = "Women";
        $interested_in = $user_info[0]['interested_in'];
        if ($user_info[0]['gender'] == "Male")
        {
            $gender = "Men";
        }
        $error_post = validate_post_fields($post_fields);

        $post_fields["user_name"] = isset($_POST["user_name"]) ? $_POST["user_name"] : "";
        //$post_fields["age"] = isset($_POST["age"]) ? $_POST["age"] : "";
        $post_fields["location"] = isset($_POST["location"]) ? $_POST["location"] : "";
        $post_fields["sunsign"] = isset($_POST["sunsign"]) ? $_POST["sunsign"] : "";
        $post_fields["moonsign"] = isset($_POST["moonsign"]) ? $_POST["moonsign"] : "";
 
        $post_fields["height_in_feet"] = isset($_POST["height_in_feet"]) ? $_POST["height_in_feet"] : "";
        $post_fields["height_in_inches"] = isset($_POST["height_in_inches"]) ? $_POST["height_in_inches"] : "";
        $post_fields["interested_in"] = isset($_POST["interested_in"]) ? $_POST["interested_in"] : "";
        $post_fields["max_height"] = isset($_POST["max_height"]) ? $_POST["max_height"] : "";
        $post_fields["min_height"] = isset($_POST["min_height"]) ? $_POST["min_height"] : "";
        $post_fields["max_age"] = isset($_POST["max_age"]) ? $_POST["max_age"] : "";
        $post_fields["min_age"] = isset($_POST["min_age"]) ? $_POST["min_age"] : "";
        $post_fields["within_days"] = isset($_POST["within_days"]) ? $_POST["within_days"] : "";
        $id = $post_fields["id"];

        if ($post_fields["type"] == "new")
        {

            $viewed_profile = $this
                ->db
                ->get_where('viewed_profile', array(
                'user_id' => $id
            ))->result_array();

            // echo $this->db->last_query();die;
            if (!empty($viewed_profile))
            {
                $view_partner_id = array_column($viewed_profile, 'partner_id');
            }
            else
            {
                $view_partner_id = '0';
            }

            $this
                ->db
                ->select('users.*');
            $this
                ->db
                ->from('users');
            $this
                ->db
                ->where(array(
                'status' => '1',
                'trash' => '0'
            ));
            $this
                ->db
                ->where_not_in('id', $view_partner_id);
            $this
                ->db
                ->where_not_in('id', $id);
            $user_data = $this
                ->db
                ->get()
                ->result_array();

                // echo $this->db->last_query();die;

            if (!empty($user_data) && isset($user_data))
            {
                $view_user_id = array_column($user_data, 'id');
            }
            else
            {
                $view_user_id = '0';
            }

            //_pre($view_user_id);
            
        }

        $this
            ->db
            ->select('*');
        $this
            ->db
            ->from('block_users');
        $this
            ->db
            ->where_in('status', ['0', '1']);
        $this
            ->db
            ->group_start(); // Open bracket
        $this
            ->db
            ->where('user_id', $id);
        $this
            ->db
            ->or_where('partner_id', $id);
        $this
            ->db
            ->group_end(); // Close bracket
        $hide_user = $this
            ->db
            ->get()
            ->result_array();
        if (!empty($hide_user))
        {
            $hide_user_id = array_column($hide_user, "partner_id");
            $hide_partner_id = array_column($hide_user, "user_id");
            $hide_user_id = array_unique(array_merge($hide_user_id, $hide_partner_id));
        }
        else
        {
            $hide_user_id = '0';
        }

        if (empty($error_post))
        {
            $page = $post_fields['page'];
            if ($page == 0)
            {
                $page = 1;
            }
            if ($page == 1)
            {
                $start = 0;
            }
            else
            {
                $start = ($page - 1) * 10;
            }

            $users_interested = $this
                ->db
                ->select('interested_in')
                ->where('id', $id)->get('users')
                ->row_array();
            $visited_users = $this
                ->db
                ->select('partner_id')
                ->where('user_id', $id)->get('visited_profile')
                ->result_array();
            $visited_id = array();
            if (!empty($visited_users))
            {
                foreach ($visited_users as $vusers)
                {

                }
            }
            $this
                ->db
                ->select('*');
            $this
                ->db
                ->where('user_id', $id);
            $starred_users_data = $this
                ->db
                ->get('user_starred')
                ->result_array();
            $starred_users_list = [];
            if (!empty($starred_users_data))
            {
                foreach ($starred_users_data as $value)
                {
                    $starred_users_list[] = $value['starred_id'];
                }
            }

            $name = $post_fields['user_name'];
            $age = '';
            $loc = $post_fields['location'];
            $sunsign = $post_fields['sunsign'];
            $moonsign = $post_fields['moonsign'];
            $height_in_feet = $post_fields['height_in_feet'];
            $height_in_inches = $post_fields['height_in_inches'];
            // $interested_in = $post_fields['interested_in'];
            $id = $post_fields['id'];
            $miles = $post_fields['miles'];
            $matches_type = $post_fields['type'];
            $max_height = $post_fields["max_height"];
            $min_height = $post_fields["min_height"];
            $max_age = $post_fields["max_age"];
            $min_age = $post_fields["min_age"];
            $within_days = $post_fields["within_days"];
            $limit = 10;
            //echo $max_age;
            //echo $min_age;
            

            $this
                ->db
                ->select('users.*,astro_details.moon_sign,astro_details.sun_sign');
            $this
                ->db
                ->where('users.trash', "0");
            $this
                ->db
                ->where('users.status', "1");
            $this
                ->db
                ->where_not_in('users.id', $id);
            if ($interested_in != 'Both')
            {
                if ($interested_in == 'Men')
                {
                    $this
                        ->db
                        ->where('gender', 'Male');
                }
                else
                {
                    $this
                        ->db
                        ->where('gender', 'Female');
                }

            }
            $this
                ->db
                ->group_start();
            $this
                ->db
                ->where('interested_in', $gender);
            $this
                ->db
                ->or_where('interested_in', 'Both');
            $this
                ->db
                ->group_end();
            $this
                ->db
                ->join('astro_details', 'astro_details.user_id = users.id', 'left');

            if ($matches_type == "new")
            {
                $this
                    ->db
                    ->group_start();
                $this
                    ->db
                    ->where_in('users.id', $view_user_id);
                $this
                    ->db
                    ->where('DATEDIFF(NOW(), users.created_at) <= 7');
                $this
                    ->db
                    ->group_end();

            }

            if (!empty($hide_user))
            {
                $this
                    ->db
                    ->where_not_in('users.id', $hide_user_id);
            }

            if ($matches_type == "starred")
            {
                if (!empty($starred_users_list))
                {
                    $this
                        ->db
                        ->where_in("users.id", $starred_users_list);
                }
                else
                {
                    $response["status"] = true;
                    $response["message"] = "No matches found!";
                    $response["data"] = [];
                    $this->response($response);
                    exit;
                }
            }
            $where = 0;

            if ($name != "" || $max_age != "" || $min_age != "" || $loc != "" || $sunsign != '' || $moonsign != "" || ($min_height != "" && $max_height != "" || $within_days != ''))
            {
                $this
                    ->db
                    ->group_start();

                if ($name != "")
                {
                    $this
                        ->db
                        ->or_like('users.first_name', $name, 'both');
                    $this
                        ->db
                        ->or_like('users.user_name', $name, 'both');
                    $where = $where . " + CASE WHEN users.first_name LIKE '%" . $name . "%' OR users.user_name LIKE '%" . $name . "%' THEN 10 ELSE 0 END";
                }
                /*if($age != "")
                {
                $this->db->or_where('users.age',$age);
                $where = $where ." + CASE WHEN users.age = '".$age."' THEN 9 ELSE 0 END";
                }*/
                if ($max_age != "" && $min_age != '')
                {

                    $this
                        ->db
                        ->or_where('users.age >=', $min_age);
                    $this
                        ->db
                        ->where('users.age <=', $max_age);
                    $where = $where . " + CASE WHEN users.age >= '" . $min_age . "' and users.age <='" . $max_age . "' THEN 9 ELSE 0 END";
                }

                if ($loc != "")
                {
                    $this
                        ->db
                        ->or_like('users.city', $loc, 'both');
                    $this
                        ->db
                        ->or_like('users.state', $loc, 'both');
                    $where = $where . " + CASE WHEN users.city LIKE '%" . $loc . "%' THEN 8 ELSE 0 END";
                    $where = $where . " + CASE WHEN users.state LIKE '%" . $loc . "%' THEN 8 ELSE 0 END";
                }
                if ($sunsign != "")
                {
                    $this
                        ->db
                        ->or_where('astro_details.sun_sign', $sunsign);
                    $where = $where . " + CASE WHEN astro_details.sun_sign = '" . $sunsign . "' THEN 7 ELSE 0 END";
                }
                if ($moonsign != "")
                {
                    $this
                        ->db
                        ->or_where('astro_details.moon_sign', $moonsign);
                    $where = $where . " + CASE WHEN astro_details.moon_sign = '" . $moonsign . "' THEN 6 ELSE 0 END";
                }
                if ($min_height != "" && $max_height != "")
                {
                    $this
                        ->db
                        ->or_where('users.height_in_feet >=', $min_height);
                    $this
                        ->db
                        ->where('users.height_in_feet <=', $max_height);
                    $where = $where . " + CASE WHEN users.height_in_feet >= '" . $min_height . "' and users.height_in_feet <= '" . $max_height . "' THEN 11 ELSE 0 END";
                }
                if ($within_days != "")
                {

                    $this
                        ->db
                        ->where("DATEDIFF(NOW(), users.created_at) <= " . $within_days);

                }
                /*if($max_height != "")
                {
                $this->db->or_where('users.height_in_feet <=', $max_height);
                $where = $where ." + CASE WHEN users.height_in_feet <= '".$max_height."' THEN 12 ELSE 0 END";
                }*/
                $this
                    ->db
                    ->group_end();
                $this
                    ->db
                    ->order_by($where, 'DESC');
                $this
                    ->db
                    ->group_by('id');
            }
            else
            {
                $this
                    ->db
                    ->where('users.trash', "0");
                $this
                    ->db
                    ->order_by('users.id', 'DESC');
            }

            $new_users_filter_data = $this
                ->db
                ->from("users")
                ->get()
                ->result_array();
            $user_count = count($new_users_filter_data);
            $new_users_filter_data = array_slice($new_users_filter_data, $start, $limit);
            //echo $this->db->last_query();exit;
            // $data = array();
            // $km['setting_value'] = (array) $this->db->select('setting_value')->get('general_settings')->row();
            // $km = implode(',', $km['setting_value']);
            //echo $this->db->last_query();
            //exit;
            $km = $miles;
            $i = 0;
            $data = [];

            foreach ($new_users_filter_data as $row)
            {

                if ($post_fields["latitude"] != "" && $post_fields["longitude"] != "" && $post_fields["miles"] != '' && $post_fields["type"] != "starred")
                {
                    if (is_numeric($row['latitude']) && is_numeric($row['longitude']) && is_numeric($post_fields["latitude"]) && is_numeric($post_fields["longitude"]))
                    {
                        $distance = distance($row['latitude'], $row['longitude'], $post_fields["latitude"], $post_fields["longitude"], "K");
                        // echo "distance: ".$distance."\n";
                        // echo "km: ".$km."\n";
                        $distance = 0.621371 * $distance;
                        if ($distance <= $km)
                        {
                            // print_r($row);
                            $data[$i] = $row;
                            $data[$i]['distance'] = $distance;
                            $i++;
                        }
                    }
                }
                else
                {
                    // print_r($row);
                    $data[$i] = $row;
                    $i++;
                }
            }

            // print_r($new_users_filter_data);exit;
            if (!empty($data))
            {

                $response["status"] = true;
                $response["message"] = "New matches get successfully!";
                $response['count'] = $user_count;
                foreach ($data as $key => $new_user_value)
                {
                    if ($new_user_value['profile_pic'] != '')
                    {
                        $data[$key]['profile_pic'] = BASE_URL() . $new_user_value['profile_pic'];
                    }
                    else
                    {
                        $data[$key]['profile_pic'] = BASE_URL() . 'assets/images/person.png';
                    }
                    $horoscope_details = [];

                    $horoscope_details[] = ["name" => $this
                        ->Common_Model
                        ->get_sunsign($data[$key]['id']) , "image" => BASE_URL() . 'assets/images/horoscope/gemini-rashi.png'];

                    $horoscope_details[] = ["name" => $this
                        ->Common_Model
                        ->get_moonsign($data[$key]['id']) , "image" => BASE_URL() . 'assets/images/horoscope/aries-rashi.png'];
                    $data[$key]["horoscope"] = $horoscope_details;

                }

                $response["data"] = $data;

                // print_r($new_users_filter_data);exit();
                foreach ($response["data"] as $key => $value)
                {
                    $star_color = $this
                        ->config
                        ->item('no_starred');
                    $response["data"][$key]["moon_sign"] = isset($value["moon_sign"]) ? $value["moon_sign"] : $this
                        ->Common_Model
                        ->get_moonsign($value['id']);
                    $response["data"][$key]["sun_sign"] = isset($value["sun_sign"]) ? $value["sun_sign"] : $this
                        ->Common_Model
                        ->get_sunsign($value['id']);
                    $response["data"][$key]["match_score"] = $this->calculate_compatibility($_POST['userid'], $value['id']);
                    $response["data"][$key]["is_online"] = 1; // 0 = false, 1 = true
                    $this
                        ->db
                        ->select('*');
                    $this
                        ->db
                        ->where('user_id', $id);
                    $this
                        ->db
                        ->where('starred_id', $value['id']);
                    $is_starred = $this
                        ->db
                        ->get('user_starred')
                        ->row_array();

                    if (is_null($is_starred))
                    {

                        $is_starred_user = 0; //no
                        
                    }
                    else
                    {
                        $this
                            ->db
                            ->select('*');
                        $this
                            ->db
                            ->where('user_id', $value['id']);
                        $this
                            ->db
                            ->where('starred_id', $id);
                        $is_starred_reversed = $this
                            ->db
                            ->get('user_starred')
                            ->row_array();
                        if (is_null($is_starred_reversed))
                        {
                            $is_starred_user = 1; //yes
                            $star_color = $this
                                ->config
                                ->item('single_starred');
                        }
                        else
                        {
                            $is_starred_user = 2;
                            $star_color = $this
                                ->config
                                ->item('both_starred');
                        }

                    }

                    $response["data"][$key]["is_starred"] = $is_starred_user; // 1 = yes starred user, 0 = no starred usre
                    $response["data"][$key]["is_online"] = 0; // 1 = yes starred user, 0 = no starred usre
                    $response["data"][$key]["star_color"] = $star_color; // 1 = yes starred user, 0 = no starred usre
                    
                }
            }
            else
            {
                $response["status"] = true;
                $response["message"] = "No matches found!";
                $response["data"] = [];
                $response['count'] = $user_count;

            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        if (isset($response['data']) && !empty($response['data']))
        {
            for ($i = 0;$i < count($response['data']);$i++)
            {
                $time = strtotime($response['data'][$i]['date_of_birth']);
                if (date('m-d') == date('m-d', $time))
                {
                    $response['data'][$i]['is_birthday'] = 1;
                }
                else
                {
                    $response['data'][$i]['is_birthday'] = 0;
                }
            }
        }
        $this->response($response);
    }
    /* -------------------------Match Filter List End --------------------------------*/

    /* -------------------------Notification Count --------------------------------*/
    public function notification_count_get($user_id = '')
    {
        $response = array();

        if ($user_id != "")
        {
            $notificationCount = $this
                ->db
                ->get_where('notification', array(
                'user_id' => $user_id,
                'read_status' => 0
            ))->num_rows();

            $response['status'] = true;
            $response['notification_count'] = $notificationCount;
        }
        else
        {
            $response['status'] = false;
            $response['message'] = 'user_id paramater is missing';
        }

        $this->response($response);
    }
    /* -------------------------Notification Count End --------------------------------*/

    /* -------------------------Notification List --------------------------------*/

    public function notification_get($user_id = '', $page = '')
    {
        $response = array();

        if ($user_id != '')
        {
            $update_data['read_status'] = 1;

            $this
                ->db
                ->update('notification', $update_data, array(
                'user_id' => $user_id
            ));

            $this
                ->db
                ->select('*');
            $this
                ->db
                ->from('notification');
            $this
                ->db
                ->where('user_id', $user_id);
            $this
                ->db
                ->order_by('id', 'desc');

            $limit = 10;

            if (isset($page) && $page != "" && $page != "1")
            {
                $start = $limit * ($page - 1);

                $this
                    ->db
                    ->limit($limit, $start);
            }
            else
            {
                $this
                    ->db
                    ->limit($limit);
            }

            $notification = $this
                ->db
                ->get()
                ->result_array();
            $this
                ->db
                ->update('notification', $update_data, array(
                'user_id' => $user_id
            ));

            $this
                ->db
                ->select('*');
            $this
                ->db
                ->from('notification');
            $this
                ->db
                ->where('user_id', $user_id);
            $this
                ->db
                ->order_by('id', 'desc');
            $notification_count = $this
                ->db
                ->get()
                ->num_rows();

            if (!empty($notification))
            {
                $user_details = $this
                    ->db
                    ->get_where('users', array(
                    'id' => $user_id
                ))->row_array();
                //print_r($user_details);exit;
                if (isset($user_details['profile_pic']) && $user_details['profile_pic'] != '')
                {
                    if (file_exists($user_details['profile_pic']))
                    {
                        $notification_icon = BASE_URL() . $user_details['profile_pic'];
                    }
                    else
                    {
                        $notification_icon = $this
                            ->config
                            ->item("notification-icon");
                    }
                }
                else
                {
                    $notification_icon = $this
                        ->config
                        ->item("notification-icon");
                }

                foreach ($notification as $key => $value)
                {

                    $notification[$key]['icon'] = $notification_icon;
                    $notification[$key]['notification_data_decode'] = json_decode($notification[$key]['notification_data']);
                    $now = new DateTime;
                    $otherDate = new DateTime($value['created_at']);
                    $diffrence = $otherDate->diff($now);
                    $diffDays = (integer)$diffrence->format("%R%a");
                    if ($diffDays === 0)
                    {
                        $diffmin = $diffrence->format("%i");
                        $now = strtotime(date('Y-m-d H:i:s'));
                        $otherDate = strtotime($value['created_at']);
                        $diffmin = round(abs($otherDate - $now) / 60);
                        if ($diffmin > 60)
                        {
                            $diffmin = round($diffmin / 60);
                            $time = $diffmin . " hours ago";

                        }
                        else
                        {
                            $time = $diffmin . " min ago";

                        }

                    }
                    elseif ($diffDays === - 1)
                    {
                        $time = date('d-m-Y', strtotime($value['created_at']));

                    }
                    else
                    {
                        $time = date('d-m-Y', strtotime($value['created_at']));
                    }
                    $notification[$key]['time'] = $time;
                }

                $response['status'] = true;
                $response['notification'] = $notification;
                $response['notification_count'] = $notification_count;
            }
            else
            {
                $response['status'] = true;
                $response['notification_count'] = $notification_count;
                $response['notification'] = array();
            }
        }
        else
        {
            $response['status'] = false;
            $response['message'] = "user_id is missing!";

        }
        $this->response($response);
    }

    /* -------------------------Notification List end --------------------------------*/
    /* -------------------------Notification Clear --------------------------------*/
    public function notificationclear_get($user_id = '')
    {
        $response = array();

        if ($user_id != '')
        {
            $this
                ->db
                ->where('user_id', $user_id);
            $this
                ->db
                ->delete('notification');

            $response['status'] = true;
            $response['message'] = "Notification Cleared Successfully.";

        }
        else
        {
            $response['status'] = false;
            $response['message'] = "user_id is missing!";

        }
        $this->response($response);
    }

    /* -------------------------Notification Clear --------------------------------*/
    /* -------------------------Accept Payment --------------------------------*/
    public function purchase_subscription_post()
    {
        require_once ('application/libraries/stripe-php/init.php');

        $response = array();

        //$post_fields["plan_id"] = isset($_POST["plan_id"]) ? $_POST["plan_id"] : "";
        // $post_fields["user_id"] = isset($_POST["user_id"]) ? $_POST["user_id"] : "";
        // $post_fields["promocode_id"] = isset($_POST["promocode_id"]) ? $_POST["promocode_id"] : "";
        $post_fields["amount"] = isset($_POST["amount"]) ? $_POST["amount"] : "";
        $post_fields["name"] = isset($_POST["name"]) ? $_POST["name"] : "";
        $post_fields["email"] = isset($_POST["email"]) ? $_POST["email"] : "";
        $post_fields["number"] = isset($_POST["number"]) ? $_POST["number"] : "";
        $post_fields["exp_month"] = isset($_POST["exp_month"]) ? $_POST["exp_month"] : "";
        $post_fields["exp_year"] = isset($_POST["exp_year"]) ? $_POST["exp_year"] : "";
        $post_fields["cvc"] = isset($_POST["cvc"]) ? $_POST["cvc"] : "";

        $error_post = validate_post_fields($post_fields);

        if (empty($error_post))
        {
            $where = array(
                'email' => $post_fields["email"],
                'trash' => '0'
            );
            $user = (array)$this
                ->db
                ->get_where('users', $where)->row();

            if ((isset($_POST['plan_id']) && $_POST['plan_id'] != '') || (isset($_POST['ticket_id']) && $_POST['ticket_id'] != ''))
            {
                if (!empty($user))
                {

                    $total_amount = $post_fields["amount"];

                    $pay_amount = max($total_amount, 0);

                    if ($pay_amount > 0)
                    {
                        try
                        {

                            $description = isset($_POST["description"]) ? $_POST["description"] : "";

                            \Stripe\Stripe::setApiKey($this
                                ->config
                                ->item('stripe_secret'));

                            $token = \Stripe\Token::create(['card' => ['number' => $post_fields["number"], 'name' => $post_fields["name"], 'exp_month' => $post_fields["exp_month"], 'exp_year' => $post_fields["exp_year"], 'cvc' => $post_fields["cvc"], ], ]);

                            $customer = \Stripe\Customer::create(array(
                                'email' => $post_fields["email"],
                                'source' => $token
                            ));

                            $charge = \Stripe\Charge::create(["customer" => $customer->id, "amount" => $pay_amount, "currency" => "usd", "description" => $description, "receipt_email" => $customer->email, ]);

                            $chargeJson = $charge->jsonSerialize();
                            // Use Stripe's library to make requests...
                            
                        }
                        catch(\Stripe\Exception\CardException $e)
                        {
                            $return_array = ["response_status" => $e->getHttpStatus() , "status" => false, "type" => $e->getError()->type, "code" => $e->getError()->code, "param" => $e->getError()->param, "message" => $e->getError()->message, ];

                            $return_str = json_encode($return_array);
                            http_response_code($e->getHttpStatus());
                            $this->response($return_array);
                            exit;
                        }
                        catch(\Stripe\Exception\RateLimitException $e)
                        {
                            // Too many requests made to the API too quickly
                            
                        }
                        catch(\Stripe\Exception\InvalidRequestException $e)
                        {
                            // Invalid parameters were supplied to Stripe's API
                            
                        }
                        catch(\Stripe\Exception\AuthenticationException $e)
                        {
                            // Authentication with Stripe's API failed
                            // (maybe you changed API keys recently)
                            
                        }
                        catch(\Stripe\Exception\ApiConnectionException $e)
                        {
                            // Network communication with Stripe failed
                            
                        }
                        catch(\Stripe\Exception\ApiErrorException $e)
                        {
                            // Display a very generic error to the user, and maybe send
                            // yourself an email
                            
                        }
                        catch(Exception $e)
                        {
                            // Something else happened, completely unrelated to Stripe
                            
                        }

                        if (isset($chargeJson['amount_refunded']) && $chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1)
                        {
                            $amount = $chargeJson['amount'];
                            $balance_transaction = $chargeJson['balance_transaction'];
                            $currency = $chargeJson['currency'];
                            $status = $chargeJson['status'];
                            $date = date("Y-m-d H:i:s");

                            $where = array(
                                'email' => $post_fields["email"],
                                'trash' => '0'
                            );
                            $user = (array)$this
                                ->db
                                ->get_where('users', $where)->row();

                            if (!empty($user))
                            {
                                $data_db['user_id'] = $user['id'];
                                $user_id = $user['id'];
                            }
                            else
                            {
                                $data_db['user_id'] = "0";
                                $user_id = '0';
                            }

                            $data_db['response'] = $charge;
                            $data_db['result_desc'] = $chargeJson["description"];
                            $data_db['card_num'] = string_encrypt($post_fields["number"]);
                            $data_db['card_cvc'] = string_encrypt($post_fields["cvc"]);
                            $data_db['card_exp_month'] = string_encrypt($post_fields["exp_month"]);
                            $data_db['card_exp_year'] = string_encrypt($post_fields["exp_year"]);
                            $data_db['receipt_email'] = $chargeJson["receipt_email"];
                            $data_db['customer'] = $chargeJson["customer"];
                            $data_db['amount'] = $amount;
                            $data_db['currency'] = $currency;
                            $data_db['balance_transaction'] = $balance_transaction;
                            $data_db['status'] = $status;
                            $data_db['created_at'] = $date;
                            $data_db['transaction_date'] = $date;

                            // print_r($data_db);
                            // exit;
                            if ($this
                                ->db
                                ->insert('transaction', $data_db))
                            {
                                if ($this
                                    ->db
                                    ->insert_id() && $status == 'succeeded')
                                {
                                    $last_insert_id = $this
                                        ->db
                                        ->insert_id();

                                    if (isset($_POST['plan_id']) && $_POST['plan_id'] != '')
                                    {
                                        // echo "string";
                                        // exit;
                                        $where = array(
                                            'id' => $_POST["plan_id"]
                                        );

                                        $subscription_data = (array)$this
                                            ->db
                                            ->get_where('subscription_duration', $where)->row();

                                        if (!empty($subscription_data))
                                        {
                                            $days = $subscription_data['duration'];
                                            $end_date = date('Y-m-d', strtotime('+' . $days . ' days'));
                                        }
                                        else
                                        {
                                            $end_date = date("Y-m-d H:i:s");
                                        }

                                        $user_membership['user_id'] = $user_id;
                                        $user_membership['plan_id'] = '4';
                                        $user_membership['start_date'] = date("Y-m-d H:i:s");
                                        $user_membership['end_date'] = $end_date;
                                        $user_membership['status'] = "0";
                                        $user_membership['transaction_id'] = $last_insert_id;
                                        // $user_membership['promo_code'] = $post_fields["promocode_id"];
                                        $user_membership['remember_token'] = md5(generateRandomString(8));
                                        $user_membership['created_at'] = date('created_at');

                                        $this
                                            ->db
                                            ->insert('user_subscription', $user_membership);
                                    }
                                    else if (isset($_POST['ticket_id']) && $_POST['ticket_id'] != '')
                                    {
                                        $user_membership['user_id'] = $user_id;
                                        $user_membership['ticket_id'] = $_POST['ticket_id'];
                                        $user_membership['purchase_date'] = date("Y-m-d H:i:s");
                                        $user_membership['status'] = "0";
                                        $user_membership['transaction_id'] = $last_insert_id;
                                        // $user_membership['promo_code'] = $post_fields["promocode_id"];
                                        $user_membership['created_at'] = date('created_at');

                                        $this
                                            ->db
                                            ->insert('user_tickets', $user_membership);
                                    }

                                    $response["status"] = true;
                                    $response["message"] = "Transaction successful";
                                    $response["data"] = $charge;
                                }
                                else
                                {
                                    $response["status"] = false;
                                    $response["message"] = "Transaction has been failed1";
                                }
                            }
                            else
                            {
                                $response["status"] = false;
                                $response["message"] = "Transaction has been failed1";
                            }
                        }
                        else
                        {
                            $response["status"] = false;
                            $response["message"] = "Transaction has been failed1";
                        }
                    }
                    else
                    {
                        if (isset($_POST['promocode_id']) && $_POST['promocode_id'] != '' && $_POST['promocode_id'] != 0)
                        {
                            if (isset($_POST['plan_id']) && $_POST['plan_id'] != '')
                            {

                                $where = array(
                                    'id' => $_POST["plan_id"]
                                );

                                $subscription_data = (array)$this
                                    ->db
                                    ->get_where('subscription_duration', $where)->row();

                                if (!empty($subscription_data))
                                {
                                    $days = $subscription_data['duration'];
                                    $end_date = date('Y-m-d', strtotime('+' . $days . ' days'));
                                }
                                else
                                {
                                    $end_date = date("Y-m-d H:i:s");
                                }

                                $where = array(
                                    'email' => $post_fields["email"],
                                    'trash' => '0'
                                );
                                $user = (array)$this
                                    ->db
                                    ->get_where('users', $where)->row();

                                if (!empty($user))
                                {
                                    $data_db['user_id'] = $user['id'];
                                    $user_id = $user['id'];
                                }
                                else
                                {
                                    $data_db['user_id'] = "0";
                                    $user_id = '0';
                                }

                                $user_membership['user_id'] = $user_id;
                                $user_membership['plan_id'] = $_POST['plan_id'];
                                $user_membership['start_date'] = date("Y-m-d H:i:s");
                                $user_membership['end_date'] = $end_date;
                                $user_membership['status'] = "0";
                                $user_membership['promo_code'] = $_POST['promocode_id'];
                                $user_membership['remember_token'] = md5(generateRandomString(8));
                                $user_membership['created_at'] = date('created_at');

                                $this
                                    ->db
                                    ->insert('user_subscription', $user_membership);
                            }
                            else if (isset($_POST['ticket_id']) && $_POST['ticket_id'] != '')
                            {
                                $user_membership['user_id'] = $user_id;
                                $user_membership['ticket_id'] = $_POST['ticket_id'];
                                $user_membership['purchase_date'] = date("Y-m-d H:i:s");
                                $user_membership['status'] = "0";
                                // $user_membership['promo_code'] = $post_fields["promocode_id"];
                                $user_membership['created_at'] = date('created_at');

                                $this
                                    ->db
                                    ->insert('user_tickets', $user_membership);
                            }

                            $response["status"] = true;
                            $response["message"] = "Transaction successful";
                        }
                        else
                        {
                            $response["status"] = false;
                            $response["message"] = "Transaction has been failed";
                        }

                    }
                }
                else
                {
                    $response["status"] = false;
                    $response["message"] = "This email is not linked with users account";
                }
            }
            else
            {
                $response['status'] = false;
                $response['message'] = 'Parameter is missing';
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = $error_post;
        }
        $this->response($response);
    }
    /* -------------------------Accept Payment end --------------------------------*/

    public function push_notification_post()
    {
        // $booking_info['user_id'] = 197;
        $booking_info['user_id'] = $_POST['user_id'];
        $booking_info['table_name'] = 'users';
        $booking_info['push_title'] = 'testing push';
        $booking_info['push_message'] = "test push request has been sent";
        $booking_info['message_type'] = "send_push";
        //print_r($booking_info);exit;
        send_push($booking_info['user_id'], $booking_info['table_name'], $booking_info['push_title'], $booking_info['push_message'], $booking_info['message_type']);

    }

    // for testing
    public function subscription_post()
    {

        require_once ('application/libraries/stripe-php/init.php');

        $response = array();

        $post_fields["promocode_id"] = isset($_POST["promocode_id"]) ? $_POST["promocode_id"] : "";
        $post_fields["amount"] = isset($_POST["amount"]) ? $_POST["amount"] : "";
        $post_fields["name"] = isset($_POST["name"]) ? $_POST["name"] : "";
        $post_fields["email"] = isset($_POST["email"]) ? $_POST["email"] : "";
        $post_fields["number"] = isset($_POST["number"]) ? $_POST["number"] : "";
        $post_fields["exp_month"] = isset($_POST["exp_month"]) ? $_POST["exp_month"] : "";
        $post_fields["exp_year"] = isset($_POST["exp_year"]) ? $_POST["exp_year"] : "";
        $post_fields["cvc"] = isset($_POST["cvc"]) ? $_POST["cvc"] : "";

        $error_post = validate_post_fields($post_fields);

        if (empty($error_post))
        {
            $where = array(
                'email' => $post_fields["email"],
                'trash' => '0'
            );
            $user = (array)$this
                ->db
                ->get_where('users', $where)->row();

            if ((isset($_POST['plan_id']) && $_POST['plan_id'] != '') || (isset($_POST['ticket_id']) && $_POST['ticket_id'] != ''))
            {
                if (!empty($user))
                {
                    $total_amount = $post_fields["amount"];

                    $pay_amount = max($total_amount, 0);

                    $stripe = new \Stripe\StripeClient($this
                        ->config
                        ->item('stripe_secret'));

                    $subscription_data = $this
                        ->db
                        ->get_where('subscription', array(
                        'id' => $_POST['plan_id']
                    ))->row_array();

                    if (!empty($subscription_data))
                    {
                        if (isset($_POST['plan_id']) && $_POST['plan_id'] == 1)
                        {
                            if ($total_amount == $subscription_data['month_price'])
                            {
                                $price_id = 'price_1HkRmlJwzqM0ySpi419pSU3E';
                            }
                            else if ($total_amount == $subscription_data['three_month_price'])
                            {
                                $price_id = 'price_1HkS4RJwzqM0ySpiKUJP9pfd';
                            }
                            else if ($total_amount == $subscription_data['year_price'])
                            {
                                $price_id = 'price_1HkS6lJwzqM0ySpiK6VZGt1m';
                            }
                        }
                        else if (isset($_POST['plan_id']) && $_POST['plan_id'] == 2)
                        {
                            if ($total_amount == $subscription_data['month_price'])
                            {
                                $price_id = 'price_1HkS91JwzqM0ySpioNI4L3mz';
                            }
                            else if ($total_amount == $subscription_data['three_month_price'])
                            {
                                $price_id = 'price_1HkS8IJwzqM0ySpi8PLXvnKv';
                            }
                            else if ($total_amount == $subscription_data['year_price'])
                            {
                                $price_id = 'price_1HkRteJwzqM0ySpihMLSWkAk';
                            }
                        }
                        else if (isset($_POST['plan_id']) && $_POST['plan_id'] == 3)
                        {
                            if ($total_amount == $subscription_data['month_price'])
                            {
                                $price_id = 'price_1HkSAOJwzqM0ySpiR12A6QEK';
                            }
                            else if ($total_amount == $subscription_data['three_month_price'])
                            {
                                $price_id = 'price_1HkSBgJwzqM0ySpisk2mTdEg';
                            }
                            else if ($total_amount == $subscription_data['year_price'])
                            {
                                $price_id = 'price_1HkSCUJwzqM0ySpipgOyFCGg';
                            }
                        }
                        else if (isset($_POST['plan_id']) && $_POST['plan_id'] == 4)
                        {
                            if ($total_amount == $subscription_data['month_price'])
                            {
                                $price_id = 'price_1HkSF3JwzqM0ySpi2qeh0fdH';
                            }
                            else if ($total_amount == $subscription_data['three_month_price'])
                            {
                                $price_id = 'price_1HkSELJwzqM0ySpidDLmrGmi';
                            }
                            else if ($total_amount == $subscription_data['year_price'])
                            {
                                $price_id = 'price_1HkSDqJwzqM0ySpiXGgVzL9r';
                            }
                        }
                    }

                    if ($pay_amount > 0 && isset($price_id) && $price_id != '')
                    {
                        try
                        {

                            $description = isset($_POST["description"]) ? $_POST["description"] : "";

                            \Stripe\Stripe::setApiKey($this
                                ->config
                                ->item('stripe_secret'));

                            $token = \Stripe\Token::create(['card' => ['number' => $post_fields["number"], 'name' => $post_fields["name"], 'exp_month' => $post_fields["exp_month"], 'exp_year' => $post_fields["exp_year"], 'cvc' => $post_fields["cvc"], ], ]);

                            $customer = \Stripe\Customer::create(array(
                                'email' => $post_fields["email"],
                                'source' => $token
                            ));

                            $user_subscriptions = $stripe
                                ->subscriptions
                                ->create(['customer' => $customer['id'], 'items' => [['price' => $price_id], ], ]);

                            // $subscriptions = $stripe->subscriptions->all();
                            $sub_id = $user_subscriptions['id'];
                            $subscriptions = $stripe
                                ->subscriptions
                                ->retrieve($sub_id, []);

                            print_r($subscriptions);
                            exit;
                        }
                        catch(\Stripe\Exception\CardException $e)
                        {
                            $return_array = ["response_status" => $e->getHttpStatus() , "status" => false, "type" => $e->getError()->type, "code" => $e->getError()->code, "param" => $e->getError()->param, "message" => $e->getError()->message, ];

                            $return_str = json_encode($return_array);
                            http_response_code($e->getHttpStatus());
                            $this->response($return_array);
                            exit;
                        }
                        catch(\Stripe\Exception\RateLimitException $e)
                        {
                            // Too many requests made to the API too quickly
                            
                        }
                        catch(\Stripe\Exception\InvalidRequestException $e)
                        {
                            // Invalid parameters were supplied to Stripe's API
                            
                        }
                        catch(\Stripe\Exception\AuthenticationException $e)
                        {
                            // Authentication with Stripe's API failed
                            // (maybe you changed API keys recently)
                            
                        }
                        catch(\Stripe\Exception\ApiConnectionException $e)
                        {
                            // Network communication with Stripe failed
                            
                        }
                        catch(\Stripe\Exception\ApiErrorException $e)
                        {
                            // Display a very generic error to the user, and maybe send
                            // yourself an email
                            
                        }
                        catch(Exception $e)
                        {
                            // Something else happened, completely unrelated to Stripe
                            
                        }
                    }
                    else
                    {
                        $response["status"] = false;
                        $response["message"] = "Transaction has been failed";
                    }
                }
                else
                {
                    $response["status"] = false;
                    $response["message"] = "This email is not linked with users account";
                }
            }
            else
            {
                $response['status'] = false;
                $response['message'] = 'Parameter is missing';
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = $error_post;
        }
        $this->response($response);
    }
    // end for testing
    public function subscription_expiry_notification_post()
    {
        $this
            ->db
            ->select('users.*,user_subscription.*');
        $this
            ->db
            ->join('user_subscription', 'user_subscription.user_id = users.id', 'join');
        $this
            ->db
            ->where('user_subscription.status', '0');
        $this
            ->db
            ->where('users.status', '1');
        $this
            ->db
            ->where('users.trash', '0');
        $this
            ->db
            ->from('users');

        $users_list = $this
            ->db
            ->get()
            ->result_array();

        foreach ($users_list as $row)
        {
            $user_id = $row['user_id'];

            if ($user_id > 0)
            {
                $certificates = [];

                $expire = strtotime($row['end_date']);

                $today = strtotime("today");

                if ($today == $expire)
                {
                    $plan_name = '';

                    if ($row['plan_id'] == 1)
                    {
                        $plan_name = "view pics";
                    }
                    elseif ($row['plan_id'] == 2)
                    {
                        $plan_name = "attend event";
                    }
                    elseif ($row['plan_id'] == 3)
                    {
                        $plan_name = "group chat";
                    }
                    send_push_expire($user_id, 'users', 'Subscription Expired', 'Your ' . $plan_name . ' subscription has been expired.', 'SubscriptionExpire', $row['device_type'], $row['device_token']);
                }
            }
        }
        echo "Cron executed successfully";
    }

    public function test_match_filter_post()
    {
        // print_r($_POST);
        $response = array();

        $post_fields["id"] = isset($_POST["userid"]) ? $_POST["userid"] : "";
        $post_fields["type"] = isset($_POST["type"]) ? $_POST["type"] : "";
        $post_fields["page"] = isset($_POST["page"]) ? $_POST["page"] : "";

        $error_post = validate_post_fields($post_fields);
        $post_fields["user_name"] = isset($_POST["user_name"]) ? $_POST["user_name"] : "";
        $post_fields["age"] = isset($_POST["age"]) ? $_POST["age"] : "";
        $post_fields["location"] = isset($_POST["location"]) ? $_POST["location"] : "";
        $post_fields["sunsign"] = isset($_POST["sunsign"]) ? $_POST["sunsign"] : "";
        $post_fields["moonsign"] = isset($_POST["moonsign"]) ? $_POST["moonsign"] : "";
        $id = $post_fields["id"];
        if (empty($error_post))
        {
            $page = $post_fields['page'];
            if ($page == 0)
            {
                $page = 1;
            }
            if ($page == 1)
            {
                $start = 0;
            }
            else
            {
                $start = ($page - 1) * 10;
            }

            $this
                ->db
                ->select('*');
            $this
                ->db
                ->where('user_id', $id);
            $starred_users_data = $this
                ->db
                ->get('user_starred')
                ->result_array();
            $starred_users_list = [];
            if (!empty($starred_users_data))
            {
                foreach ($starred_users_data as $value)
                {
                    $starred_users_list[] = $value['starred_id'];
                }
            }

            $name = $post_fields['user_name'];
            $age = $post_fields['age'];
            $loc = $post_fields['location'];
            $sunsign = $post_fields['sunsign'];
            $moonsign = $post_fields['moonsign'];
            $id = $post_fields['id'];
            $matches_type = $post_fields['type'];
            $limit = 10;
            $this
                ->db
                ->select('users.*,astro_details.moon_sign,astro_details.sun_sign');
            $this
                ->db
                ->where('users.trash', "0");
            $this
                ->db
                ->where('users.status', "1");
            $this
                ->db
                ->where_not_in('users.id', $id);
            $this
                ->db
                ->join('astro_details', 'astro_details.user_id = users.id', 'left');
            $this
                ->db
                ->limit($limit, $start);
            if ($matches_type == "new")
            {
                $this
                    ->db
                    ->where('users.created_at BETWEEN DATE_SUB(NOW(), INTERVAL 5 DAY) AND NOW()');
            }
            if ($matches_type == "starred")
            {
                if (!empty($starred_users_list))
                {
                    $this
                        ->db
                        ->where_in("users.id", $starred_users_list);
                }
                else
                {
                    $response["status"] = true;
                    $response["message"] = "No matches found!";
                    $response["data"] = [];
                    $this->response($response);
                    exit;
                }
            }
            $where = 0;
            if ($name != "" || $age != "" || $loc != "" || $sunsign != '' || $moonsign != "")
            {
                $this
                    ->db
                    ->group_start();

                if ($name != "")
                {
                    $this
                        ->db
                        ->or_like('users.full_name', $name, 'both');
                    $where = $where . " + CASE WHEN users.full_name LIKE '%" . $name . "%' THEN 10 ELSE 0 END";
                }
                if ($age != "")
                {
                    $this
                        ->db
                        ->or_where('users.age', $age);
                    $where = $where . " + CASE WHEN users.age = '" . $age . "' THEN 9 ELSE 0 END";
                }
                if ($loc != "")
                {
                    $this
                        ->db
                        ->or_like('users.place_of_birth', $loc, 'both');
                    $where = $where . " + CASE WHEN users.place_of_birth LIKE '%" . $loc . "%' THEN 8 ELSE 0 END";
                }
                if ($sunsign != "")
                {
                    $this
                        ->db
                        ->or_where('astro_details.sun_sign', $sunsign);
                    $where = $where . " + CASE WHEN astro_details.sun_sign = '" . $sunsign . "' THEN 7 ELSE 0 END";
                }
                if ($moonsign != "")
                {
                    $this
                        ->db
                        ->or_where('astro_details.moon_sign', $moonsign);
                    $where = $where . " + CASE WHEN astro_details.moon_sign = '" . $moonsign . "' THEN 6 ELSE 0 END";
                }
                $this
                    ->db
                    ->group_end();
                $this
                    ->db
                    ->order_by($where, 'DESC');
            }
            else
            {
                $this
                    ->db
                    ->where('users.trash', "0");
                $this
                    ->db
                    ->order_by('users.id', 'DESC');
            }

            $new_users_filter_data = $this
                ->db
                ->from("users")
                ->get()
                ->result_array();
            // echo count($new_users_filter_data);exit;
            // $data = array();
            $km['setting_value'] = (array)$this
                ->db
                ->select('setting_value')
                ->get('general_settings')
                ->row();
            $km = implode(',', $km['setting_value']);
            $i = 0;
            $data = [];

            foreach ($new_users_filter_data as $row)
            {

                // echo "lat: ".$row['latitude']."\n";
                // echo "lng: ".$row['longitude']."\n";
                $distance = distance($row['latitude'], $row['longitude'], $_POST['latitude'], $_POST['longitude'], "K");
                // echo "distance: ".$distance."\n";
                // echo "km: ".$km."\n";
                if ($distance <= $km)
                {
                    // print_r($row);
                    $data[$i] = $row;
                    $data[$i]['distance'] = $distance;
                    $i++;
                }

            }

            // print_r($new_users_filter_data);exit;
            if (!empty($data))
            {
                $response["status"] = true;
                $response["message"] = "New matches get successfully!";
                foreach ($data as $key => $new_user_value)
                {
                    if ($new_user_value['profile_pic'] != '')
                    {
                        $data[$key]['profile_pic'] = BASE_URL() . $new_user_value['profile_pic'];
                    }
                    else
                    {
                        $data[$key]['profile_pic'] = BASE_URL() . 'assets/images/person.png';
                    }
                }

                $response["data"] = $data;
                // print_r($new_users_filter_data);exit();
                foreach ($response["data"] as $key => $value)
                {
                    $response["data"][$key]["moon_sign"] = isset($value["moon_sign"]) ? $value["moon_sign"] : $this
                        ->config
                        ->item('moon-sign');
                    $response["data"][$key]["sun_sign"] = isset($value["sun_sign"]) ? $value["sun_sign"] : $this
                        ->config
                        ->item('sun-sign');
                    $response["data"][$key]["match_score"] = 93;
                    $response["data"][$key]["is_online"] = 1; // 0 = false, 1 = true
                    $horoscope_details = [];
                    $horoscope_details[] = ["name" => isset($value["sun_sign"]) ? $value["sun_sign"] : $this
                        ->config
                        ->item('sun-sign') , "image" => BASE_URL() . 'assets/images/horoscope/gemini-rashi.png'];
                    $horoscope_details[] = ["name" => isset($value["moon_sign"]) ? $value["moon_sign"] : $this
                        ->config
                        ->item('moon-sign') , "image" => BASE_URL() . 'assets/images/horoscope/aries-rashi.png'];
                    $response["data"][$key]["horoscope"] = $horoscope_details;
                    $this
                        ->db
                        ->select('*');
                    $this
                        ->db
                        ->where('user_id', $id);
                    $this
                        ->db
                        ->where('starred_id', $value['id']);
                    $is_starred = $this
                        ->db
                        ->get('user_starred')
                        ->row_array();
                    if (is_null($is_starred))
                    {
                        $is_starred_user = 0; //no
                        
                    }
                    else
                    {
                        $is_starred_user = 1; //yes
                        
                    }

                    $response["data"][$key]["is_starred"] = $is_starred_user; // 1 = yes starred user, 0 = no starred usre
                    $response["data"][$key]["is_online"] = 0; // 1 = yes starred user, 0 = no starred usre
                    
                }
            }
            else
            {
                $response["status"] = true;
                $response["message"] = "No matches found!";
                $response["data"] = [];
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        if (isset($response['data']) && !empty($response['data']))
        {
            for ($i = 0;$i < count($response['data']);$i++)
            {
                $time = strtotime($response['data'][$i]['date_of_birth']);
                if (date('m-d') == date('m-d', $time))
                {
                    $response['data'][$i]['is_birthday'] = 1;
                }
                else
                {
                    $response['data'][$i]['is_birthday'] = 0;
                }
            }
        }
        $this->response($response);
    }

    public function notification_status_get($userid = '')
    {

        $login_user_data = (array)$this
            ->db
            ->get_where("users", array(
            "id" => $userid
        ))->row();
        if (!empty($login_user_data))
        {
            $update_data = array(
                'read_status' => 1
            );
            $updated_result = $this
                ->db
                ->update('notification', $update_data, array(
                'user_id' => $userid
            ));
            if ($updated_result)
            {
                $response['status'] = true;
                $response['message'] = 'Message read!';
            }
            else
            {
                $response['status'] = true;
                $response['message'] = 'Notification status can not updated!';
            }
        }
        else
        {
            $response['status'] = false;
            $response['message'] = 'Sorry, User not available!';

        }
        $this->response($response);
    }

    public function statelist_get()
    {
        $state_data = $this
            ->db
            ->query('SELECT * FROM states')
            ->result_array();;
        if (!empty($state_data))
        {
            $response['status'] = true;
            $response['message'] = 'State Data Get Successfully';
            $response['data'] = $state_data;

        }
        else
        {
            $response['status'] = false;
            $response['message'] = 'No any state available!';

        }
        $this->response($response);
    }
    function calculate_compatibility($id1 = 1, $id2 = 3)
    {
        $user1_data = $this->calculate_compatibility_fun($id1, $id2);
        $user2_data = $this->calculate_compatibility_fun($id2, $id1);
        // var_dump($user1_data);
        // var_dump($user2_data);
        // exit;
        $stellar_gained_points = $user1_data['stellar_score'] + $user2_data['stellar_score'];
        if ($user1_data['capacity'] <= 0)
        {
            $user1_data['capacity'] = 1;
        }
        if ($stellar_gained_points > 0)
        {
            $total = $user1_data['totalscore'] + $stellar_gained_points;
            $compatibility = $total / $user1_data['capacity'];
        }
        else
        {

            $compatibility = $user1_data['totalscore'] / $user1_data['capacity'];
        }
        if ($compatibility > 100)
        {
            $compatibility = 100;
        }
        if ($compatibility < 0)
        {
            $compatibility = 0;
        }
        return round($compatibility * 100);

    }
    function calculate_compatibility_fun($id1, $id2)
    {
        $object_array = $this
            ->db
            ->query('SELECT * FROM user_object')
            ->result_array();
        $user_f = $this
            ->db
            ->query('SELECT * FROM planet_data WHERE user_id=' . $id1 . ' AND object != "Chiron" AND object != "North Node" AND object != "South Node" AND object != "Midheaven"')->result_array();
        $user_s = $this
            ->db
            ->query('SELECT * FROM planet_data WHERE user_id=' . $id2 . ' AND object != "Chiron" AND object != "North Node" AND object != "South Node" AND object != "Midheaven"')->result_array();
        $datapoint = $this
            ->db
            ->query('SELECT * FROM user_datapoints')
            ->result_array();
        $datapoint_array = array();
        $object_multi = array();
        foreach ($object_array as $key => $value)
        {
            $object_multi[$value['name']] = $value['multiplier'];
        }
        foreach ($datapoint as $key => $value)
        {
            $datapoint_array[$value['diff']] = $value;
        }

        $user_f_data = array();
        foreach ($user_f as $key => $value)
        {
            $user_f_data[$value['object']] = $value;
        }

        $user_s_data = array();
        foreach ($user_s as $key => $value)
        {
            $user_s_data[$value['object']] = $value;
        }

        $difference = array();
        $negative = 0;
        $totalscore = 0;
        $stellarscore = 0;
        $diff = 0;
        if (!empty($user_s_data) && !empty($user_f_data))
        {
            foreach ($object_array as $key => $value)
            {

                foreach ($user_s_data as $key1 => $value1)
                {
                   // var_dump($value['name']);
                   // var_dump($key1);
                   // var_dump($user_f_data[$value['name']]['full_position']);
                   // var_dump($user_s_data[$key1]['full_position']);
                 
                    $diff = round($user_f_data[$value['name']]['full_position'] - $user_s_data[$key1]['full_position']);
                   
                    if ($diff > 0)
                    {
                        $difference[$value['name']][$key1]['diff'] = $diff;
                         if($difference[$value['name']][$key1]['diff'] > 188){
                            // echo "here";
                            $difference[$value['name']][$key1]['diff'] = 360 - $difference[$value['name']][$key1]['diff'];
                        }
                        $diff = $difference[$value['name']][$key1]['diff'];
                    }
                    else
                    {
                        
                        $difference[$value['name']][$key1]['diff'] =  360 + $diff;
                        // var_dump($difference[$value['name']][$key1]['diff']);
                        if($difference[$value['name']][$key1]['diff'] > 188){
                            // echo "here";
                            $difference[$value['name']][$key1]['diff'] = 360 - $difference[$value['name']][$key1]['diff'];
                        }
                        $diff = $difference[$value['name']][$key1]['diff'];

                    }
                    // var_dump($diff);
                    if (isset($datapoint_array[$diff]))
                    {
                          // var_dump($difference[$value['name']][$key1]['diff']);

                        $difference[$value['name']][$key1]['score'] = $datapoint_array[$diff]['score'];
                        $difference[$value['name']][$key1]['stellar_score'] = $datapoint_array[$diff]['score'] * $object_multi[$key1];
                        $difference[$value['name']][$key1]['multiplier'] = $object_multi[$key1];
                        if($value['name'] == "Sun" || $value['name'] == "Moon" || $value['name'] == "Ascendant"){
                                if(($value['name'] == "Moon" && $key1 == "Moon") || ($value['name'] == "Sun" && $key1 == "Jupiter")){
                                    $object_multi[$key1] = 1;
                                }
                                // var_dump($value['name']);
                                // var_dump($key1);
                                $stellarscore = $stellarscore + ($datapoint_array[$diff]['score'] * $object_multi[$key1]);
                                // var_dump($difference[$value['name']][$key1]['stellar_score']);
                        }
                         $totalscore = $totalscore + $datapoint_array[$diff]['score'];
                        if ($datapoint_array[$diff]['score'] < 0)
                        {
                            $negative = $negative + $datapoint_array[$diff]['score'];
                        }
                    }
                    else
                    {
                        $difference[$value['name']][$key1]['stellar_score'] = 0;
                        $difference[$value['name']][$key1]['score'] = 0;
                    }
                     // var_dump($difference[$value['name']][$key1]['score']);
                     

                }

            }
        }
        $capacity = $totalscore - $negative;

        $response['stellar_score'] = round($stellarscore);
        $response['totalscore'] = $totalscore;
        $response['negative'] = $negative;
        $response['capacity'] = $capacity;
        // var_dump($response); exit;
        return $response;

    }

    public function event_attending_post()
    {
        $response = array();
        $post_fields['user_id'] = $_POST['user_id'];
        $post_fields['event_id'] = $_POST['event_id'];
        $error_post = validate_post_fields($post_fields);
        if (empty($error_post))
        {
            $event_users = (array)$this
                ->db
                ->get_where("event_attendies", array(
                "user_id" => $post_fields['user_id'],
                'event_id' => $post_fields['event_id']
            ))->row();
            if (empty($event_users))
            {
                $even_user_data['user_id'] = $post_fields['user_id'];
                $even_user_data['event_id'] = $post_fields['event_id'];
                $result = $this
                    ->Common_Model
                    ->insert_data("event_attendies", $post_fields);
                $response["status"] = true;
                $response["message"] = "Event is marked as attending";
            }
            else
            {
                $this
                    ->Common_Model
                    ->delete_table_data("event_attendies", array(
                    "id" => $event_users["id"]
                ));
                $response["status"] = false;
                $response["message"] = "Event has successfully been removed from your attending list and profile";
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        $this->response($response);
    }

    public function hide_profile_post()
    {
        $post_fields['user_id'] = $_POST['user_id'];
        $post_fields['partner_id'] = $_POST['partner_id'];
        $post_fields['status'] = $_POST['status'];
        $error_post = validate_post_fields($post_fields);
        $post_fields['reason'] = isset($_POST['reason']) ? $_POST['reason'] : NULL;
        if (empty($error_post))
        {
            $block_user = (array)$this
                ->db
                ->get_where("block_users", array(
                "user_id" => $post_fields['user_id'],
                'partner_id' => $post_fields['partner_id'],
                "status" => $post_fields['status']
            ))->row();
            if (empty($block_user))
            {
                $result = $this
                    ->Common_Model
                    ->insert_data("block_users", $post_fields);
                $response["status"] = true;
                if ($post_fields['status'] == '0')
                {
                    $response["message"] = "User Removed successfully";
                }
                else if ($post_fields['status'] == '1')
                {
                    $where = array(
                        'user_id' => $post_fields['user_id'],
                        'starred_id' => $post_fields['partner_id']
                    );
                    $this
                        ->Common_Model
                        ->delete_table_data("user_starred", $where);
                    $where1 = array(
                        'starred_id' => $post_fields['user_id'],
                        'user_id' => $post_fields['partner_id']
                    );
                    $this
                        ->Common_Model
                        ->delete_table_data("user_starred", $where1);
                    $response["message"] = "User blocked successfully";
                }
                else
                {
                    // echo "herer";exit;
                    $subject = $this
                        ->config
                        ->item("admin_site_name") . " - User is reported";

                        $support_data['ticket_number'] = rand(0,9999);
                        $support_data['user_id'] = $_POST["user_id"];
                        $support_data['partner_id'] = $_POST["partner_id"];
                        $support_data['message'] = $post_fields['reason'];
                        $support_data['status'] ='open';
                        $this->Common_Model->insert_data("support_ticket", $support_data);
                        $user_data = (array)$this->db->get_where('users', array('id' => $_POST["user_id"]))->row();
                        $template_path = BASE_URL() . "email_template/report_user.html";
                        $template = file_get_contents($template_path);
                        $template = admin_create_email_template($template);
                        $template = str_replace("##USERNAME##", $user_data["user_name"], $template);
                        $template = str_replace("##REASON##", $post_fields['reason'], $template);
                        $template = str_replace("##TICKET##",  $support_data['ticket_number'], $template);
                        send_mail($this->config->item('admin_site_email'), $subject, $template);
                        // send_mail('phpdeveloper7036@gmail.com', $subject, $template);
                    $response["message"] = "User reported successfully";
                }
            }
            else
            {
                if ($block_user['status'] == '1')
                {
                    $where = array(
                        'id' => $block_user['id'],
                        'status' => '1'
                    );
                    $this
                        ->Common_Model
                        ->delete_table_data("block_users", $where);
                    $response["status"] = true;
                    $response["message"] = "User unblocked successfully";
                }
                else if ($block_user['status'] == '2')
                {
                    $result = $this
                        ->Common_Model
                        ->insert_data("block_users", $post_fields);

                    $response["status"] = true;
                    $response["message"] = "User reported successfully.";
                    // echo "herer";exit;
                    $subject = $this
                        ->config
                        ->item("admin_site_name") . " - User is reported";

                        $support_data['ticket_number'] = rand(0,9999);
                        $support_data['user_id'] = $_POST["user_id"];
                        $support_data['partner_id'] = $_POST["partner_id"];
                        $support_data['message'] = $post_fields['reason'];
                        $support_data['status'] ='open';

                        $this->Common_Model->insert_data("support_ticket", $support_data);

                        $user_data = (array)$this->db->get_where('users', array('id' => $_POST["user_id"]))->row();
                        $template_path = BASE_URL() . "email_template/report_user.html";
                        $template = file_get_contents($template_path);
                        $template = admin_create_email_template($template);
                        $template = str_replace("##USERNAME##", $user_data["user_name"], $template);
                        $template = str_replace("##REASON##", $post_fields['reason'], $template);
                        $template = str_replace("##TICKET##",  $support_data['ticket_number'], $template);
                        send_mail($this->config->item('admin_site_email'), $subject, $template);

                       
                        // send_mail('phpdeveloper7036@gmail.com', $subject, $template);
                }
                else
                {
                    $response["status"] = false;
                    $response["message"] = "User already removed";
                }

            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        $this->response($response);
    }

    public function check_status_post()
    {
        $post_fields['user_id'] = $_POST['user_id'];
        $post_fields['partner_id'] = $_POST['partner_id'];

        $error_post = validate_post_fields($post_fields);
        if (empty($error_post))
        {
            $this
                ->db
                ->select('*');
            $this
                ->db
                ->from('block_users');
            $this
                ->db
                ->where_in('status', ['0', '1', '2']);
            $this
                ->db
                ->group_start(); // Open bracket
            $this
                ->db
                ->group_start();
            $this
                ->db
                ->where('user_id', $_POST["user_id"]);
            $this
                ->db
                ->where('partner_id', $_POST["partner_id"]);
            $this
                ->db
                ->group_end();
            $this
                ->db
                ->or_group_start();
            $this
                ->db
                ->where('partner_id', $_POST["user_id"]);
            $this
                ->db
                ->where('user_id', $_POST["partner_id"]);
            $this
                ->db
                ->group_end();
            $this
                ->db
                ->group_end(); // Close bracket
            $hide_user = $this
                ->db
                ->order_by('id', 'DESC')
                ->limit('1')
                ->get()
                ->result_array();
            if (empty($hide_user) || $hide_user[0]['status'] == '2')
            {
                $response["status"] = true;
                $response["user_status"] = '2';
                $response["status_user_id"] = '0';
                $response["status_partner_id"] = '0';
                $response["message"] = 'Not any status found';
            }
            else
            {
                $response["status"] = false;
                $response["user_status"] = $hide_user[0]['status'];
                $response["status_user_id"] = $hide_user[0]['user_id'];
                $response["status_partner_id"] = $hide_user[0]['partner_id'];
                if ($hide_user[0]['user_id'] == $_POST['user_id'])
                {
                    if ($hide_user[0]['status'] == '0')
                    {
                        $response['message'] = "User is in your remove List.";
                    }
                    else if ($hide_user[0]['status'] == '1')
                    {
                        $response['message'] = "User is in your block List.";
                    }
                    else
                    {
                        $response['message'] = "You have reported for this user.";
                    }
                }
                else
                {
                    if ($hide_user[0]['status'] == '0')
                    {
                        $response['message'] = "User removed you.";
                    }
                    else if ($hide_user[0]['status'] == '1')
                    {
                        $response['message'] = "User blocked you.";
                    }
                    else
                    {
                        $ticket_id = rand(0,9999);
                        $data_support['user_id'] =  $post_fields['user_id'];
                        $data_support['user_id'] =  $post_fields['user_id'];
                        $response['message'] = "User repored for you profile.";
                    }
                }
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
            $response["user_status"] = '2';
            $response["status_user_id"] = '0';
            $response["status_partner_id"] = '0';
        }
        $this->response($response);
    }

    public function block_user_list_post()
    {
        $post_fields['user_id'] = $_POST['user_id'];
        $post_fields['type'] = '1';
        $error_post = validate_post_fields($post_fields);
        if (empty($error_post))
        {
            $this
                ->db
                ->select('*');
            $this
                ->db
                ->from('block_users');
            $this
                ->db
                ->where('status', '1');
            // $this->db->group_start(); // Open bracket
            $this
                ->db
                ->where('user_id', $_POST["user_id"]);
            // $this->db->or_where('partner_id', $_POST["user_id"]);
            // $this->db->group_end(); // Close bracket
            $hide_user = $this
                ->db
                ->get()
                ->result_array();
            $hide_users = array();
            foreach ($hide_user as $huser)
            {
                $user_id = $huser['user_id'];
                if ($_POST['user_id'] == $huser['user_id'])
                {
                    $user_id = $huser['partner_id'];
                }
                $user_data = (array)$this
                    ->db
                    ->get_where('users', array(
                    'id' => $user_id
                ))->row();
                $user_data['profile_pic'] = BASE_URL() . $user_data['profile_pic'];
                array_push($hide_users, $user_data);
            }
            if (!empty($hide_user))
            {
                $response["status"] = true;
                $response["message"] = "Blocked User Get Successfully";
                $response["data"] = $hide_users;
            }
            else
            {
                $response["status"] = false;
                $response["message"] = "No any block user found.";
            }
        }
        else
        {
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        }
        $this->response($response);
    } 

    public function content_get()
    {

      
        $privacy_policy_url = BASE_URL().'privacy-policy';
        $term_condition_url  = BASE_URL().'term-condition';
        if ($privacy_policy_url !='' || $term_condition_url !='') 
        {
            $response['status'] = true;
            $response['privacy_policy'] = $privacy_policy_url;
            $response['terms_condition'] = $term_condition_url;
            
        }
        else
        {
            $response["status"] = false;
           
        }
        
        $this->response($response);
    } 

/*     public function buy_post(){
 

        $post_fields['user_id'] = isset($_POST['user_id']) ? $_POST['user_id'] : "" ;
        $post_fields['subscription_id'] = isset($_POST['subscription_id']) ? $_POST['subscription_id'] : "" ;
        $post_fields['amount'] = isset($_POST['amount']) ? $_POST['amount'] : "" ; 

        $error_post = validate_post_fields($post_fields);

        if (empty($error_post))
        { 
            $data = $_POST;

            $userID = $data['user_id'];
            $subscription_id = $data['subscription_id'];
            $amount = $data['amount'];  
                    
            $returnURL = base_url().'paypal/success';
            $cancelURL = base_url().'paypal/cancel';
            $notifyURL = base_url().'paypal/ipn'; 
            // Add fields to paypal form
            $this->paypal_lib->add_field('return', $returnURL);
            $this->paypal_lib->add_field('cancel_return', $cancelURL);
            $this->paypal_lib->add_field('notify_url', $notifyURL); 
            $this->paypal_lib->add_field('user_id', $userID);
            $this->paypal_lib->add_field('subscription_id',  $subscription_id);
            $this->paypal_lib->add_field('amount',  $amount);
            
            // Render paypal form
            $result = $this->paypal_lib->paypal_auto_form();
        }else{ 
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        
            $this->response($response);
        }

 
    }
 */
    public function current_lat_lng_post()
    { 
        $post_fields['user_id'] = isset($_POST['user_id']) ? $_POST['user_id'] : "" ;
        $post_fields['current_lat'] = isset($_POST['current_lat']) ? $_POST['current_lat'] : "" ;
        $post_fields['current_lng'] = isset($_POST['current_lng']) ? $_POST['current_lng'] : "" ; 
        $post_fields['current_city'] = isset($_POST['current_city']) ? $_POST['current_city'] : "" ;
        $post_fields['current_state'] = isset($_POST['current_state']) ? $_POST['current_state'] : "" ; 


        $error_post = validate_post_fields($post_fields);

        if (empty($error_post))
        {  
            $update_data = array(
                'current_lat' => $_POST['current_lat'],
                'current_lng' => $_POST['current_lng'],
                'current_city' => $_POST['current_city'],
                'current_state' => $_POST['current_state']

            );
            $updated_result = $this
                ->db
                ->update('users', $update_data, array(
                'id' => $_POST['user_id']
            ));

            $response["status"] = true;
            $response["message"] = "Current latitude longtitude updated successfully.";
 
        }else{ 
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        
            
        }
        $this->response($response);
 
    }



    
/*  
    public function buy_post(){
 

        $post_fields['user_id'] = isset($_POST['user_id']) ? $_POST['user_id'] : "" ;
        // $post_fields['subscription_id'] = isset($_POST['subscription_id']) ? $_POST['subscription_id'] : "" ;
        $post_fields['amount'] = isset($_POST['amount']) ? $_POST['amount'] : "" ; 

        $error_post = validate_post_fields($post_fields);

        if (empty($error_post))
        {  
            $data = $_POST;
            $this->paypal_call($data);

            $url = "http://3.22.182.248/ApiController/paypal_call";

            $response["status"] = true;
            $response["message"] = "Get URL successfully.";
            $response["data"] = array('URL' =>  $url );
            $this->response($response);


        }else{ 
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        
            $this->response($response);
        }

 
    }

    public function paypal_call($data){

        // $data = $_POST;

        $userID = $data['user_id'];
        $subscription_id = $data['subscription_id'];
        $amount = $data['amount'];  
                

        $returnURL = base_url().'paypal/success';
        $cancelURL = base_url().'paypal/cancel';
        $notifyURL = base_url().'paypal/ipn'; 
        // Add fields to paypal form
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL); 
        $this->paypal_lib->add_field('user_id', $userID);
        $this->paypal_lib->add_field('subscription_id',  $subscription_id);
        $this->paypal_lib->add_field('amount',  $amount);
        
        // Render paypal form
        $result = $this->paypal_lib->paypal_auto_form();

    }   */


/*     public function buy_post(){
 

        $post_fields['user_id'] = isset($_GET['user_id']) ? $_GET['user_id'] : "" ;
        // $post_fields['subscription_id'] = isset($_GET['subscription_id']) ? $_GET['subscription_id'] : "" ;
        $post_fields['amount'] = isset($_GET['amount']) ? $_GET['amount'] : "" ; 

        $error_post = validate_post_fields($post_fields);

        if (empty($error_post))
        {  
            $data = $_GET;
            $this->paypal_call($data);

            $url = "http://3.22.182.248/ApiController/paypal_call";

            $response["status"] = true;
            $response["message"] = "Get URL successfully.";
            $response["data"] = array('URL' =>  $url );
            $this->response($response);


        }else{ 
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        
            $this->response($response);
        }

 
    }

    public function paypal_call($data){

        // $data = $_GET;
 
        $userID = $data['user_id'];
        $subscription_id = $data['subscription_id'];
        $amount = $data['amount'];  
                

        $returnURL = base_url().'paypal/success';
        $cancelURL = base_url().'paypal/cancel';
        $notifyURL = base_url().'paypal/ipn'; 
        // Add fields to paypal form
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL); 
        $this->paypal_lib->add_field('user_id', $userID);
        $this->paypal_lib->add_field('subscription_id',  $subscription_id);
        $this->paypal_lib->add_field('amount',  $amount);
        
        // Render paypal form
        $result = $this->paypal_lib->paypal_auto_form();

    }   */


    
    public function buy_post(){
 

        $post_fields['user_id'] = isset($_POST['user_id']) ? $_POST['user_id'] : "" ; 
        $post_fields['amount'] = isset($_POST['amount']) ? $_POST['amount'] : "" ; 
        $post_fields['type'] = isset($_POST['type']) ? trim($_POST['type']) : "" ; 
        $post_fields['id'] = isset($_POST['id']) ? $_POST['id'] : "" ; 
     
        $error_post = validate_post_fields($post_fields);
        if (empty($error_post))
        {  
           
            $data = $_POST;  

            if(trim($post_fields['type']) == "subscription"){
                $id = $_POST['id'];
            }else if(trim($post_fields['type']) == "purchase_soul"){
                $id = $_POST['id'];
            }else if(trim($post_fields['type']) == "purchase_report"){ 
                if (isset($_POST['report']) && $_POST['report'] == 1)
                {
                    $report_id = 1;
                }
                else
                {
                    $report_id = 2;
                }

                $id = $_POST['id']."_".$report_id;
            }


            $url = "http://3.22.182.248/Paypalsubscription/buy/".$id."/".$post_fields['user_id']."/".$post_fields['amount']."/".$post_fields['type'];

            $returnURL = base_url().'paypal/success';
            $success_url = base_url().'paypal/success_msg';
            $cancelURL = base_url().'paypal/cancel';

            $response["status"] = true;
            $response["message"] = "Get URL successfully.";
            $response["data"] = array('URL' =>  $url , 'success_url' =>  $success_url, 'cancel_url' =>  $cancelURL );
            $this->response($response); 

        }else{ 
            $response["status"] = false;
            $response["message"] = "Parameter is missing!";
        
            $this->response($response);
        }

 
    }

    /* public function paypal_call($id,$userID,$amount){

      
 
        // $userID = $data['user_id'];
        // $subscription_id = $data['subscription_id'];
        // $amount = $data['amount'];  
                

        $returnURL = base_url().'paypal/success';
        $cancelURL = base_url().'paypal/cancel';
        $notifyURL = base_url().'paypal/ipn'; 
        // Add fields to paypal form
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        // $this->paypal_lib->add_field('notify_url', $notifyURL); 
        $this->paypal_lib->add_field('user_id', $userID);
        $this->paypal_lib->add_field('subscription_id',  $subscription_id);
        $this->paypal_lib->add_field('amount',  $amount);
        
        // Render paypal form
        $result = $this->paypal_lib->paypal_auto_form();

    }   */
}

 