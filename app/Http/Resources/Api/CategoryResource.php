<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\UserGenres;


class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {        
        $user_id = $request->user_id;
        $genre_ids = UserGenres::select('genre_id')->where('user_id',$user_id)->get()->pluck('genre_id')->toArray();

        if(in_array($this->id, $genre_ids)){
            $selected = true;
        }else{
            $selected = false;
        }

       return [
            'id' => $this->id,
            'name' => $this->name,
            'status' => $this->status,
            'selected' => $selected,
        ];
       

    }
}
