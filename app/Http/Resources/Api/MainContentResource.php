<?php
namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\MyList;
use App\Models\FavoriteVideo;

class MainContentResource extends JsonResource

{
    /**

     * Transform the resource into an array.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable

     */

    public function toArray($request)
    {
        $mylist = MyList::where('content_id',$this->id)->where('user_id',request()->user()->id)->count();
        $user_like = FavoriteVideo::where('content_id',$this->id)->where('user_id',request()->user()->id)->first();
        if(isset($user_like)) {
            if($user_like->status == 1){
                $is_likes = TRUE;
                $is_disliked = FALSE;
            }else{
                $is_likes = FALSE;
                $is_disliked = TRUE;
            }
        }else{
            $is_likes = FALSE;
            $is_disliked = FALSE;
        }

        if($this->content_type != 1){
            $content_seasons = ContentSeasonResource::collection($this->season); 
        }else{
            $content_seasons = array();
        }

        return [
            'id' => $this->id,
            'poster' => $this->poster,
            'detail_poster' => $this->detail_poster,
            'is_free' => $this->is_free,
            'slug' => $this->slug,
            'name' => $this->name,
            'synopsis' => $this->synopsis,
            'trailer' => $this->trailer,
            'content_type' => $this->content_type,
            'my_list' => (isset($mylist) && $mylist > 0)?TRUE:FALSE,
            'liked' => $is_likes,
            'disliked' => $is_disliked,
            'content_seasons' => $content_seasons,
            'video' => VideoResource::collection($this->episode),
        ];

    }

}