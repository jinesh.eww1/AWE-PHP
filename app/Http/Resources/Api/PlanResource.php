<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
use App\Models\Country;
use App\Models\Price;
use AmrShawky\LaravelCurrency\Facade\Currency;
use Symfony\Component\Intl\Currencies;


class PlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {        
        $user_data = auth()->user();

        if($user_data->parent_id == 0){
            $plan_id = $user_data->plan_id;
            $plan_type = $user_data->plan_type;
            $expiration_date = $user_data->expiration_date_formatted;
        }else{
            $user = User::whereId($user_data->parent_id)->where('status',1)->first();
            $plan_id = $user->plan_id;
            $plan_type = $user->plan_type;
            $expiration_date = $user->expiration_date_formatted;
        }

        if($plan_id == $this->id){
            $selected = true;
        }else{
            $selected = false;
            $plan_type = NULL;
            $expiration_date = '';
        }

        $country = Country::where('dialing_code',$user_data->country_code)->with(['currency'])->first();
        $symbol = $country->currency->symbol;
        $currency_code = $country->currency->code;


        $price = Price::where('country_id', $country->id)->where('plan_id', $this->id)->first();
        //$currency_code = !empty($user_data->currency_code)?$user_data->currency_code:'USD';

        /*if($currency_code == 'USD'){
            $monthly_price = $this->monthly_price;
            $yearly_price = $this->yearly_price;
        }else{
            $monthly_price = strval(round(Currency::convert()->from('USD')->to($currency_code)->amount($this->monthly_price)->get()));
            $yearly_price = strval(round(Currency::convert()->from('USD')->to($currency_code)->amount($this->yearly_price)->get())); 
        }*/

        //$symbol = Currencies::getSymbol($currency_code);

       return [
            'id' => $this->id,
            'name' => $this->name,
            'stripe_monthly_price_id' => $this->stripe_monthly_price_id,
            'stripe_yearly_price_id' => $this->stripe_yearly_price_id,
            'paypal_monthly_subscription_id' => $this->paypal_monthly_subscription_id,
            'paypal_yearly_subscription_id' => $this->paypal_yearly_subscription_id,
            'description' => $this->description,
            'monthly_price' => strval(round($price->monthly_price)),
            'yearly_price' => strval(round($price->yearly_price)),
            'currency' => $currency_code,
            'currency_sign' => $symbol,
            'selected' => $selected,
            'plan_type' => $plan_type,
            'expiration_date_formatted' => $expiration_date,
            'attribute' => PlanAttributeResource::collection($this->attributes),
        ];
    }
}
