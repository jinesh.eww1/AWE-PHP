<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Streaming;

class VideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    
    public function toArray($request)
    {   
        $watched_duration_data = Streaming::where('video_id',$this->id)->where('user_id',request()->user()->id)->first();
        $watched_perc = 0; 
        $remaining_duration = '';
        $watched_duration_secs = 0;
        if(!empty($watched_duration_data)){
            $watched_duration_secs = $watched_duration_data->seconds;

            $watched_pending_secs = $this->duration_seconds - $watched_duration_secs;

            if($watched_pending_secs > 0){
                $watched_perc = ( $watched_duration_secs * 100 ) / $this->duration_seconds;
                $watched_perc = (int) round(($watched_perc), 0);
            }

            if($watched_pending_secs > 0){
                $watched_duration_h = gmdate("H", $watched_pending_secs);
                $watched_duration_m = gmdate("i", $watched_pending_secs);
                if($watched_duration_h > 0){
                    $remaining_duration .= $watched_duration_h.'h ';
                }
                if($watched_duration_m > 0){
                    $remaining_duration .= $watched_duration_m.'m';
                }
                //$remaining_duration = $watched_duration_h.'h '.$watched_duration_m.'m';
                
                
                
            }
        }

        return [
            'id' => $this->id,
            'content_id' => $this->content_id,
            'season_id' => $this->season_id,
            'episode_name' => $this->episode_name,
            'episode_synopsis' => $this->episode_synopsis,
            'video' => $this->video,
            'low_quality_video' => $this->video,
            'duration_seconds' => $this->duration_seconds,
            'duration' => $this->duration,
            'watched_perc' => $watched_perc,
            'remaining_duration' => $remaining_duration,
            'watched_duration_secs' => $watched_duration_secs,
            'thumbnail_image' => !empty($this->thumbnail_image)?env('AWS_S3_URL').'video/'.$this->thumbnail_image:'',
            'video_subtitle'=> VideoSubtitleResource::collection($this->video_subtitle),
            'audio'=> AudioResource::collection($this->audio)
        ];
    }
}
