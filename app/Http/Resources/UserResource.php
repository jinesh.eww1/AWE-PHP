<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
use App\Models\PlanAttribute;

class UserResource extends JsonResource

{
    /**

     * Transform the resource into an array.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable

     */

    public function toArray($request)

    {

        if($this->parent_id != 0){
            $parent = User::find($this->parent_id);
            $plan_id = $parent->plan_id;
            $plan_type = $parent->plan_type;
            $reference_code = $parent->reference_code;
            $referral_expiration_date = $parent->referral_expiration_date;
        }else{
            $plan_id = $this->plan_id;
            $plan_type = $this->plan_type;
            $reference_code = $this->reference_code;
            $referral_expiration_date = $this->referral_expiration_date;
        }

        $is_cast_tv = $allow_download_preference = 0;
        if($plan_id > 0){
            $attr = PlanAttribute::where('plan_id', $plan_id)->where('attribute_id', 3)->first();
            $is_cast_tv = $attr->value;
            $ultra_hd_data = PlanAttribute::where('plan_id', $plan_id)->where('attribute_id', 2)->first();
            $allow_download_preference = $ultra_hd_data->value;
        }

        return [
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'full_name' => $this->first_name.' '.$this->last_name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'country_code' => !empty($this->country_code)?$this->country_code:'',
            'currency_code' => $this->currency_code,
            'phone' => $this->phone,
            'stripe_id' => $this->stripe_id,
            'profile_picture_full_url' => $this->profile_picture_full_url,
            'is_child' => $this->is_child,
            'social_id' => isset($this->social_id) ? $this->social_id : '',
            'social_type' => isset($this->social_type) ? $this->social_type : '',
            'autoplay' => $this->autoplay,
            'autoplay_previews' => $this->autoplay_previews,
            'notification' => $this->notification,
            'plan_id' => $plan_id,
            'plan_type' => $plan_type,
            'language_id' => (int)$this->language_id,
            'expiration_date' => $this->expiration_date,
            'expiration_date_formatted' => $this->expiration_date_formatted,
            'lock_password' => !empty($this->lock_password)?(string)$this->lock_password:'',
            'phone_verified' => $this->phone_verified,
            'email_verified' => !empty($this->email_verified_at)?$this->email_verified_at:'',
            'reference_code' => $reference_code,
            'referral_expiration_date' => $referral_expiration_date,
            'is_cast_tv' => (int)$is_cast_tv,
            'allow_download_preference' => $allow_download_preference,
            'token' => $this->when( $request->path() != 'api/profile/view/edit', $this->token)
        ];

    }

}