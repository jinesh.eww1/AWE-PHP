<?php

namespace App\Imports;

use App\Models\Content;
use App\Models\AgeRatings;
use App\Models\Artist;
use App\Models\Tag;
use App\Models\Genre;
use App\Models\ContentCast;
use App\Models\ContentGenres;
use App\Models\ContentTags;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;


class ContentImport implements ToCollection, WithHeadingRow, WithValidation, WithMultipleSheets, SkipsEmptyRows
{
    public function sheets(): array
    {
        return [
            0 => new ContentImport(),
            1 => new SeasonImport(),
            2 => new VideoImport(),
        ];
    }

    public function rules(): array
    {
        return [
            'name_english'=>'required',
            'name_hindi'=>'required',
            'name_french'=>'required',
            'name_mandarin'=>'required',
            'name_spanish'=>'required',
            'year'=>'required',
            'synopsis_english'=>'required',
            'synopsis_hindi'=>'required',
            'synopsis_french'=>'required',
            'synopsis_mandarin'=>'required',
            'synopsis_spanish'=>'required',
            'is_free'=>'required',
            'content_type'=>'required',
            'poster'=>'required',
            //'logo'=>'required',
            'age_rating' => ['required', 
            function ($attribute, $value, $fail) {
                if($value != ''){
                    $age_rating_name = AgeRatings::select('id','name')->where('name','LIKE','%'.trim($value).'%')->first();
                    
                    if(empty($age_rating_name)){
                        $fail("Please enter valid age rating");
                    } 

                } } ],
        ];
    }
 
    public function customValidationMessages()
    {
        return [
            'age_rating.required'    => 'Age rating must not be empty!',
        ];
    }

    /**
     * @param array $row
     *
     * @return User|null
     */
    public function collection(Collection $rows)
    {     
        $age_rating_array = AgeRatings::select('id','name')->get();
        $artist_array = Artist::select('id','name')->get();   
        $category_array = Genre::select('id','name')->get();
        $tags_array = Tag::select('id','name')->get();

        foreach ($rows as $key =>$row) 
        {   

            foreach ($age_rating_array as $agkey => $agvalue) {
                if($agvalue->name == trim($row['age_rating'])){
                    $age_rating_id = $agvalue->id;
                }        
            }    

            $content_id = Content::create([
                            'name' => [
                                'en' => $row['name_english'],
                                'hi' => $row['name_hindi'],
                                'fr' => $row['name_french'],
                                'md' => $row['name_mandarin'],
                                'sp' => $row['name_spanish']
                            ],
                            'age_rating_id' => $age_rating_id,
                            'year' => $row['year'],
                            'synopsis' => [
                                'en' => $row['synopsis_english'],
                                'hi' => $row['synopsis_hindi'],
                                'fr' => $row['synopsis_french'],
                                'md' => $row['synopsis_mandarin'],
                                'sp' => $row['synopsis_spanish']
                            ],
                            'status' => 2,
                            'is_free' => ($row['is_free'] == 'Yes')?1:0,
                            'content_type' => ($row['content_type'] == 'Movie')?1:2,
                            'poster' => $row['poster'],
                            'logo' => $row['logo'],
                            'trailer' => !empty($row['trailer'])?$row['trailer']:''
                        ]);

            
                if(!empty($row['artist'])){
                    $temp_artist_array = array_values(array_unique(explode(',', $row['artist'])));    

                    foreach ($temp_artist_array as $takey => $tavalue) {
                        
                        foreach ($artist_array as $akey => $avalue) {
                            
                            if($avalue->name == trim($tavalue)){
                                $artist_insert_data[] = [
                                    'content_id' => $content_id->id,
                                    'artist_id' =>$avalue->id 
                                ];
                            }
                        } 
                    }
                }

                if(!empty($row['category'])){
                    $temp_category_array = array_values(array_unique(explode(',', $row['category'])));    
                   
                    foreach ($temp_category_array as $tckey => $tcvalue) {
                        
                        foreach ($category_array as $cakey => $cavalue) {
                            
                            if($cavalue->name == trim($tcvalue)){
                                $category_insert_data[] = [
                                    'content_id' => $content_id->id,
                                    'genre_id' =>$cavalue->id 
                                ];
                            }
                        }
                    }
                }

                if(!empty($row['tags'])){
                    $temp_tags_array = array_values(array_unique(explode(',', $row['tags'])));    
                    
                    foreach ($temp_tags_array as $tkey => $tvalue) {
                        
                        foreach ($tags_array as $tgkey => $tgvalue) {
                            
                            if($tgvalue->name == trim($tvalue)){
                                $tags_insert_data[] = [
                                    'content_id' => $content_id->id,
                                    'tag_id' =>$tgvalue->id 
                                ];
                            }
                        }
                    }
                }
        }   

        if(!empty($artist_insert_data)){
            ContentCast::insert($artist_insert_data);    
        }
        if(!empty($category_insert_data)){
            ContentGenres::insert($category_insert_data);
        }

        if(!empty($tags_insert_data)){
            ContentTags::insert($tags_insert_data);
        }
        
    }

}
?>