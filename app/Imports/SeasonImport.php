<?php

namespace App\Imports;

use App\Models\Content;
use App\Models\Season;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;


class SeasonImport implements ToCollection, WithHeadingRow, WithValidation, SkipsEmptyRows
{

     public function rules(): array
    {
        return [
            'content_name' => ['required', 
            function ($attribute, $value, $fail) {
                if($value != ''){
                    $content_name = Content::select('id','name')->where('content_type',2)->where('name','LIKE','%'.trim($value).'%')->first();

                    if(empty($content_name)){
                        $fail("Opps, content name is not valid!, Please enter only Tv Shows's content name in season sheet");
                    } 

                } } ],
            'season_name_english' => 'required',
            'season_name_mandarin' => 'required',
            'season_name_hindi' => 'required',
            'season_name_spanish' => 'required',
            'season_name_french' => 'required',
        ];
    }
 
    public function customValidationMessages()
    {
        return [
            'content_name.required'    => 'Content name must not be empty!',
            'season_name_english.required'    => 'Season name english must not be empty!',
            'season_name_mandarin.required'    => 'Season name mandarin must not be empty!',
            'season_name_hindi.required'    => 'Season name hindi must not be empty!',
            'season_name_spanish.required'    => 'Season name spanish must not be empty!',
            'season_name_french.required'    => 'Season name french must not be empty!',
        ];
    }

   /**
     * @param array $row
     *
     * @return User|null
     */
    public function collection(Collection $rows)
    {     

        $content_array = Content::select('id','name')->get();

        foreach ($rows as $key =>$row) 
        {   
            foreach ($content_array as $ckey => $cvalue) {
                if($cvalue->name == trim($row['content_name'])){
                    $content_id = $cvalue->id;
                }        
            }    

            $season_id = Season::create([
                            'season' => [
                                'en' => $row['season_name_english'],
                                'hi' => $row['season_name_hindi'],
                                'fr' => $row['season_name_french'],
                                'md' => $row['season_name_mandarin'],
                                'sp' => $row['season_name_spanish']
                            ],
                            'content_id' => !empty($content_id)?$content_id:''
                        ]);

        }   

    }

}
?>