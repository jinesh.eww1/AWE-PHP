<?php

namespace App\Imports;

use App\Models\Content;
use App\Models\Season;
use App\Models\Video;
use App\Models\VideoSubtitle;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;


class VideoImport implements ToCollection, WithHeadingRow, WithValidation, SkipsEmptyRows
{

     public function rules(): array
    {

        return [
            'content_name' => ['required', 
            function ($attribute, $value, $fail) {                
                if($value != ''){
                    $content_name = Content::select('id','name')->where('name','LIKE','%'.trim($value).'%')->first();
                    if(empty($content_name)){
                        $fail("Please enter valid content name in video sheet");
                    } 

                } } ],
            'season_name' => ['', 
            function ($attribute, $value, $fail) {
                if($value != ''){
                    $season_name = Season::select('id','season')->where('season','LIKE','%'.trim($value).'%')->first();

                    if(empty($season_name)){
                        $fail("Please enter valid Season name in video sheet");
                    } 

                } } ],
            'video' => 'required',
            'duration' => 'required'
        ];
    }
 
    public function customValidationMessages()
    {
        return [
            'content_name.required' => 'Content name must not be empty!',
        ];
    }

   /**
     * @param array $row
     *
     * @return User|null
     */
    public function collection(Collection $rows)
    {     
        $content_array = Content::select('id','name')->get();
        $season_array = Season::select('id','season')->get();

        foreach ($rows as $key =>$row) 
        {   
            foreach ($content_array as $ckey => $cvalue) {
                if($cvalue->name == trim($row['content_name'])){
                    $content_id = $cvalue->id;
                }        
            }    

            if($row['season_name']){
                $season_id = 0;
                foreach ($season_array as $skey => $svalue) {
                    if($svalue->season == trim($row['season_name'])){
                        $season_id = $svalue->id;
                    }        
                }
            }else{
                $season_id = 0;
            } 

            $video_id = Video::create([
                            'episode_name' => [
                                'en' => $row['video_name_english'],
                                'hi' => $row['video_name_hindi'],
                                'fr' => $row['video_name_french'],
                                'md' => $row['video_name_mandarin'],
                                'sp' => $row['video_name_spanish']
                            ],
                            'episode_synopsis' => [
                                'en' => $row['video_synopsis_english'],
                                'hi' => $row['video_synopsis_hindi'],
                                'fr' => $row['video_synopsis_french'],
                                'md' => $row['video_synopsis_mandarin'],
                                'sp' => $row['video_synopsis_spanish']
                            ],
                            'content_id' => $content_id,
                            'season_id' => $season_id,
                            'thumbnail_image' => $row['thumbnail_image'],
                            'video' =>$row['video'],
                            'duration' => $row['duration']
                        ]);

                if(!empty($row['subtitle_english'])){

                    $subtitle_insert_data[] = [
                        'video_id' => $video_id->id,
                        'language_id' => 1,
                        'file'=>$row['subtitle_english'],
                    ];   

                }

                if(!empty($row['subtitle_mandarin'])){

                    $subtitle_insert_data[] = [
                        'video_id' => $video_id->id,
                        'language_id' => 2,
                        'file'=>$row['subtitle_mandarin'],
                    ];   
                    
                }

                if(!empty($row['subtitle_hindi'])){

                    $subtitle_insert_data[] = [
                        'video_id' => $video_id->id,
                        'language_id' => 3,
                        'file'=>$row['subtitle_hindi'],
                    ];   
                    
                }

                if(!empty($row['subtitle_spanish'])){

                    $subtitle_insert_data[] = [
                        'video_id' => $video_id->id,
                        'language_id' => 5,
                        'file'=>$row['subtitle_spanish'],
                    ];   
                    
                }

                if(!empty($row['subtitle_french'])){

                    $subtitle_insert_data[] = [
                        'video_id' => $video_id->id,
                        'language_id' => 4,
                        'file'=>$row['subtitle_french'],
                    ];   
                    
                }

        }

        if(!empty($subtitle_insert_data)){
            VideoSubtitle::insert($subtitle_insert_data);
        }

    }

}
?>