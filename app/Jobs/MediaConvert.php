<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\User;
use App\Models\MediaCovertLogs;
use App\Models\Video;
use Aws\MediaConvert\MediaConvertClient;
use Aws\Exception\AwsException;

use Owenoj\LaravelGetId3\GetId3;

class MediaConvert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $video_id;
    protected $base_video_path;
    protected $file_name;
    protected $subTitles;
    protected $audioSelector;

    public function __construct($video_id, $base_video_path, $filename, $subTitles = [], $audioSelector = [])
    {
        $this->video_id = $video_id;
        $this->base_video_path = $base_video_path; //Input file
        $this->file_name = $filename; //file name for genaret as output

        //ENG, FRA, ARA, SPA, HIN, ZHO
        $this->subTitles = $subTitles;
        $this->audioSelector = $audioSelector;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $mediaConvertClient = new MediaConvertClient([
            'version' => '2017-08-29',
            'region' => 'us-west-1',
            'profile' => 'default',
            'endpoint' => 'https://4nyhswgta.mediaconvert.us-west-1.amazonaws.com'
        ]);
        $basic_output_array = [
                      "ContainerSettings" => [
                        "Container" => "CMFC"
                      ],
                      "VideoDescription" => [
                        "Width" => 1920,
                        "ScalingBehavior" => "DEFAULT",
                        "Height" => 1080,
                        "TimecodeInsertion" => "DISABLED",
                        "AntiAlias" => "ENABLED",
                        "Sharpness" => 50,
                        "CodecSettings" => [
                            "Codec" => "H_264",
                            "H264Settings" => [
                                "MaxBitrate" => 5000000,
                                "RateControlMode" => "QVBR",
                                "SceneChangeDetect" => "TRANSITION_DETECTION"
                            ]
                        ],
                        "AfdSignaling" => "NONE",
                        "DropFrameTimecode" => "ENABLED",
                        "RespondToAfd" => "NONE",
                        "ColorMetadata" => "INSERT"
                      ],
                      "NameModifier" => $this->file_name."_iOS"
                  ];

        $outputs = [];
        array_push($outputs, $basic_output_array);

        if(isset($this->subTitles) && count($this->subTitles) > 0){
            foreach ($this->subTitles as $key => $value) {
                $temp = [
                          "ContainerSettings" => [
                            "Container" => "CMFC"
                          ],
                          "NameModifier" => $value['NameModifier'],
                          "CaptionDescriptions" => [
                            [
                              "CaptionSelectorName" => "Captions Selector ".$key+1,
                              "DestinationSettings" => [
                                "DestinationType" => "WEBVTT",
                                "WebvttDestinationSettings" => []
                              ],
                              "LanguageCode" => $value['LanguageCode'],
                              "LanguageDescription" => $value['LanguageDescription']
                            ]
                          ]
                      ];
                array_push($outputs, $temp);
            }
        }

        $default_audio = [
                    "ContainerSettings" => [
                      "Container" => "CMFC"
                    ],
                    "AudioDescriptions" => [
                      [
                        "AudioSourceName" => "Audio Selector 1",
                        "CodecSettings" => [
                          "Codec" => "AAC",
                          "AacSettings" => [
                            "Bitrate" => 96000,
                            "CodingMode" => "CODING_MODE_2_0",
                            "SampleRate" => 48000
                          ]
                        ],
                        "StreamName" => "English Track",
                        "LanguageCodeControl" => "USE_CONFIGURED",
                        "LanguageCode" => "ENG"
                      ]
                    ],
                    "NameModifier" => "DefaultAudio"
                ];
        array_push($outputs, $default_audio);
        
        if(isset($this->audioSelector) && count($this->audioSelector) > 0){
            foreach ($this->audioSelector as $key => $value) {
                  $temp = [
                                "ContainerSettings" => [
                                  "Container" => "CMFC"
                                ],
                                "AudioDescriptions" => [
                                  [
                                    "AudioSourceName" => "Audio Selector ".$key+2,
                                    "CodecSettings" => [
                                      "Codec" => "AAC",
                                      "AacSettings" => [
                                        "Bitrate" => 96000,
                                        "CodingMode" => "CODING_MODE_2_0",
                                        "SampleRate" => 48000
                                      ]
                                    ],
                                    "StreamName" => $value['StreamName'], // update here
                                    "LanguageCodeControl" => "USE_CONFIGURED",
                                    "LanguageCode" => $value['LanguageCode']
                                  ]
                                ],
                                "NameModifier" => $value['NameModifier']
                            ];
                  array_push($outputs, $temp);
            }
        }
        

        //dd($outputs);

        $inputs = array();
        $basic_input_array = array(
                "TimecodeSource" => "ZEROBASED",
                "FileInput" => "s3://awe-dev/".$this->base_video_path,
                "AudioSelectors" => array(
                    "Audio Selector 1" => [
                        "DefaultSelection"=> "DEFAULT",
                        "SelectorType"=> "LANGUAGE_CODE",
                        "LanguageCode"=> "ENG"
                      ]
                )
        );

        if(isset($this->audioSelector) && count($this->audioSelector) > 0){
            $audioSelector_input = [];
            foreach ($this->audioSelector as $key => $value) {
                  $temp = [
                       // "Offset" => 1,
                        //"DefaultSelection" => "NOT_DEFAULT",
                        "SelectorType" => "LANGUAGE_CODE",
                        "ExternalAudioFileInput" => "s3://awe-dev/".$value['ExternalAudioFileInput'],
                        "LanguageCode" => $value['LanguageCode']
                        //"AudioDurationCorrection" => "AUTO"
                      ];
                  $audioSelector_input['Audio Selector '.$key+1] = $temp;
            }

            $basic_input_array['AudioSelectors'] = $audioSelector_input;
        }

        if(isset($this->subTitles) && count($this->subTitles) > 0){
            $CaptionSelectors = [];
            foreach ($this->subTitles as $key => $value){
                $temp = [
                          "SourceSettings" => [
                            "SourceType" => "WEBVTT",
                            "FileSourceSettings" => [
                              "SourceFile" => "s3://awe-dev/".$value['CaptionSourceFile'],
                            ]
                          ]
                        ];
                $CaptionSelectors['Captions Selector '.$key+1] = $temp;
            }
            $basic_input_array['CaptionSelectors'] = $CaptionSelectors;
        }

        array_push($inputs, $basic_input_array);
        //dd($inputs);

        $jobSetting = [
            "OutputGroups" => [
                [
                    "CustomName" => "custom2",
                    "Name" => "CMAF",
                    "Outputs" => $outputs,
                    "OutputGroupSettings" => [
                        "Type" => "CMAF_GROUP_SETTINGS",
                        "CmafGroupSettings" => [
                            "SegmentLength" => 10,
                            "Destination" => "s3://awe-dev/videoFiles/".$this->file_name."/",
                            "FragmentLength" => 2,
                            "SegmentControl" => "SEGMENTED_FILES"
                        ]
                    ]
                ]
            ],
            "TimecodeConfig" => [
                'Source' => "ZEROBASED"
            ],
            "Inputs" => $inputs
        ];

        try {
            $result = $mediaConvertClient->createJob([
                "Queue" => "arn:aws:mediaconvert:us-west-1:114685726688:queues/Default",
                "Role" => "arn:aws:iam::114685726688:role/service-role/MediaConvert_Default_Role",
                "Settings" => $jobSetting, //JobSettings structure
                "AccelerationSettings" => array(
                    'Mode' => 'DISABLED'
                ),
                "StatusUpdateInterval"=> "SECONDS_60",
                "Priority" => 0,
                "HopDestinations" => array(),
                "UserMetadata" => []
            ]);

            /*$result = $mediaConvertClient->getJob([
                'Id' => '1658838063773-0x9fgn'
            ]);*/


            dd($result);

        } catch (AwsException $e) {
            // output error message if fails
            /*echo "ERORR";
            echo $e->getMessage();
            echo "\n";*/
        }
    }
}
