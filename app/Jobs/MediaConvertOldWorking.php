<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\User;
use App\Models\MediaCovertLogs;
use App\Models\Video;
use Aws\MediaConvert\MediaConvertClient;
use Aws\Exception\AwsException;

use Owenoj\LaravelGetId3\GetId3;

class MediaConvert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $video_id;
    protected $base_video_path;
    protected $file_name;
    protected $subTitles;
    protected $audioSelector;

    public function __construct($video_id, $base_video_path, $filename, $subTitles = [], $audioSelector = [])
    {
        $this->video_id = $video_id;
        $this->base_video_path = $base_video_path; //Input file
        $this->file_name = $filename; //file name for genaret as output

        //ENG, FRA, ARA, SPA, HIN, ZHO
        $this->subTitles = $subTitles;
        $this->audioSelector = $audioSelector;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $mediaConvertClient = new MediaConvertClient([
            'version' => '2017-08-29',
            'region' => 'us-west-1',
            'profile' => 'default',
            'endpoint' => 'https://4nyhswgta.mediaconvert.us-west-1.amazonaws.com'
        ]);
        $basic_output_array = [
                      "ContainerSettings" => [
                        "Container" => "CMFC"
                      ],
                      "VideoDescription" => [
                        "Width" => 1920,
                        "ScalingBehavior" => "DEFAULT",
                        "Height" => 1080,
                        "TimecodeInsertion" => "DISABLED",
                        "AntiAlias" => "ENABLED",
                        "Sharpness" => 50,
                        "CodecSettings" => [
                            "Codec" => "H_264",
                            "H264Settings" => [
                                "MaxBitrate" => 5000000,
                                "RateControlMode" => "QVBR",
                                "SceneChangeDetect" => "TRANSITION_DETECTION"
                            ]
                        ],
                        "AfdSignaling" => "NONE",
                        "DropFrameTimecode" => "ENABLED",
                        "RespondToAfd" => "NONE",
                        "ColorMetadata" => "INSERT"
                      ],
                      "NameModifier" => $this->file_name."_iOS"
                  ];

        $outputs = [];
        array_push($outputs, $basic_output_array);

        if(isset($this->subTitles) && count($this->subTitles) > 0){
            foreach ($this->subTitles as $key => $value) {
                $temp = [
                          "ContainerSettings" => [
                            "Container" => "CMFC"
                          ],
                          "NameModifier" => $value['NameModifier'],
                          "CaptionDescriptions" => [
                            [
                              "CaptionSelectorName" => "Captions Selector ".$key+1,
                              "DestinationSettings" => [
                                "DestinationType" => "WEBVTT",
                                "WebvttDestinationSettings" => []
                              ],
                              "LanguageCode" => $value['LanguageCode'],
                              "LanguageDescription" => $value['LanguageDescription']
                            ]
                          ]
                      ];
                array_push($outputs, $temp);
            }
        }
        
        if(isset($this->audioSelector) && count($this->audioSelector) > 0){
            foreach ($this->audioSelector as $key => $value) {
                  $temp = [
                                "ContainerSettings" => [
                                  "Container" => "CMFC"
                                ],
                                "AudioDescriptions" => [
                                  [
                                    "AudioSourceName" => "Audio Selector ".$key+1,
                                    "CodecSettings" => [
                                      "Codec" => "AAC",
                                      "AacSettings" => [
                                        "Bitrate" => 96000,
                                        "CodingMode" => "CODING_MODE_2_0",
                                        "SampleRate" => 48000
                                      ]
                                    ],
                                    "StreamName" => $value['StreamName'], // update here
                                    "LanguageCodeControl" => "FOLLOW_INPUT",
                                    "LanguageCode" => $value['LanguageCode']
                                  ]
                                ],
                                "NameModifier" => $value['NameModifier']
                            ];
                  array_push($outputs, $temp);
            }
        }
        

        //dd($outputs);

        $inputs = array();
        $basic_input_array = array(
                "TimecodeSource" => "ZEROBASED",
                "FileInput" => "s3://awe-dev/".$this->base_video_path,
                "VideoSelector" => array(),
                "AudioSelectors" => array(
                    "Audio Selector 1" => [
                        "DefaultSelection"=> "DEFAULT"
                      ]
                )
        );

        /*if(isset($this->audioSelector) && count($this->audioSelector) > 0){
            $audioSelector_input = [];
            foreach ($this->audioSelector as $key => $value) {
                  $temp = [
                        "Offset" => 1,
                        "DefaultSelection" => "NOT_DEFAULT",
                        "SelectorType" => "LANGUAGE_CODE",
                        "ExternalAudioFileInput" => "s3://awe-dev/".$value['ExternalAudioFileInput'],
                        "LanguageCode" => $value['LanguageCode'],
                        "AudioDurationCorrection" => "AUTO"
                      ];
                  $audioSelector_input['Audio Selector '.$key+1] = $temp;
            }

            $basic_input_array['AudioSelectors'] = $audioSelector_input;
        }

        if(isset($this->subTitles) && count($this->subTitles) > 0){
            $CaptionSelectors = [];
            foreach ($this->subTitles as $key => $value){
                $temp = [
                          "SourceSettings" => [
                            "SourceType" => "WEBVTT",
                            "FileSourceSettings" => [
                              "SourceFile" => "s3://awe-dev/".$value['CaptionSourceFile'],
                            ]
                          ]
                        ];
                $CaptionSelectors['Captions Selector '.$key+1] = $temp;
            }
            $basic_input_array['CaptionSelectors'] = $CaptionSelectors;
        }*/

        array_push($inputs, $basic_input_array);
        //dd($inputs);

        $outputs = array(
            [
            "Preset" => "System-Ott_Hls_Ts_Avc_Aac_16x9_1920x1080p_30Hz_8.5Mbps",
            "NameModifier" => "HLSoutputv1"
            ]
        );

        $jobSetting = [
            "OutputGroups" => [
                [
                    "Name" => "Apple HLS",
                    "Outputs" => $outputs,
                    "OutputGroupSettings" => [
                        "Type" => "HLS_GROUP_SETTINGS",
                        "HlsGroupSettings" => [
                            "ManifestDurationFormat" => "INTEGER",
                            "SegmentLength" => 3,
                            "TimedMetadataId3Period" => 10,
                            "CaptionLanguageSetting" => "OMIT",
                            "Destination" => "s3://awe-dev/videoFiles/".$this->file_name."/",
                            "TimedMetadataId3Frame" => "PRIV",
                            "CodecSpecification" => "RFC_4281",
                            "OutputSelection" => "MANIFESTS_AND_SEGMENTS",
                            "ProgramDateTimePeriod" => 600,
                            "MinSegmentLength" => 0,
                            "DirectoryStructure" => "SINGLE_DIRECTORY",
                            "ProgramDateTime" => "EXCLUDE",
                            "SegmentControl" => "SEGMENTED_FILES",
                            "ManifestCompression" => "NONE",
                            "ClientCache" => "ENABLED",
                            "StreamInfResolution" => "INCLUDE"
                        ]
                    ]
                ]
            ],
            "AdAvailOffset" => 0,
            "Inputs" => $inputs
        ];

        //dd($jobSetting);

        try {
            $result = $mediaConvertClient->createJob([
                "JobTemplate" => "arn:aws:mediaconvert:us-west-1:114685726688:jobTemplates/SingleVideoAudioResolution",
                "Queue" => "arn:aws:mediaconvert:us-west-1:114685726688:queues/Default",
                "Role" => "arn:aws:iam::114685726688:role/service-role/MediaConvert_Default_Role",
                "Settings" => $jobSetting, //JobSettings structure
                "AccelerationSettings" => array(
                    'Mode' => 'DISABLED'
                ),
                "StatusUpdateInterval"=> "SECONDS_60",
                "Priority" => 0,
                "HopDestinations" => array(),
                "UserMetadata" => []
            ]);

            /*$result = $mediaConvertClient->getJob([
                'Id' => '1658838063773-0x9fgn'
            ]);*/

            if($result['Job']['Status'] == 'SUBMITTED'){

                $MediaCovertLogs = MediaCovertLogs::create([
                    'video_id' => $this->video_id,
                    'input' => $this->base_video_path,
                    'job_id' => $result['Job']['Id'],
                    'status' => $result['Job']['Status'],
                ]);

                $track = GetId3::fromDiskAndPath('s3', '/'.$this->base_video_path);
                $seconds = (int) $track->getPlaytimeSeconds();

                $duration = '';
                if($seconds > 0){
                    $duration_h = gmdate("H", $seconds);
                    $duration_m = gmdate("i", $seconds);
                    if($duration_h > 0){
                        $duration .= $duration_h.'h ';
                    }
                    if($duration_m > 0){
                        $duration .= $duration_m.'m';
                    }

                }

                $base_video_name = str_replace('uppyfiles/', '', $this->base_video_path);
                $base_video_name = substr($base_video_name, 0, strpos($base_video_name, "."));

                $update_data = array(
                    'video' => env('AWS_S3_URL')."videoFiles/".$this->file_name."/".$base_video_name.".m3u8",
                    'duration_seconds' => $seconds,
                    'duration' => $duration
                );
                Video::whereId($this->video_id)->update($update_data);

               /*  echo "DONE";
                dd($result);*/
            }else{
               /* echo "ERORR In";
                dd($result);*/
            }

        } catch (AwsException $e) {
            // output error message if fails
            /*echo "ERORR";
            echo $e->getMessage();
            echo "\n";*/
        }
    }
}
