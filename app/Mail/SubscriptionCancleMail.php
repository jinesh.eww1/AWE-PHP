<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubscriptionCancleMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    protected $title, $email_description, $address1, $address2;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title, $email_description, $address1, $address2)
    {
        $this->title=$title;
        $this->email_description=$email_description;        
        $this->address1=$address1;
        $this->address2=$address2;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
         return $this->from(env('MAIL_FROM_ADDRESS'), config('app.name'))
         ->subject($this->title)         
                ->markdown('template.subscription_plan_cancle', [
                    'title' => $this->title,
                    'email_description' => $this->email_description,
                    'address1' => $this->address1,
                    'address2' => $this->address2,
                ]);
    }
}
