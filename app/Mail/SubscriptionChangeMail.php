<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubscriptionChangeMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    protected $title, $plan_name, $purshased_plan_type, $email_description, $address1, $address2;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title, $plan_name, $purshased_plan_type, $email_description, $address1, $address2)
    {
        $this->title=$title;
        $this->plan_name = $plan_name;
        $this->purshased_plan_type = $purshased_plan_type;
        $this->email_description=$email_description;     
        $this->address1=$address1;
        $this->address2=$address2;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
         return $this->from(env('MAIL_FROM_ADDRESS'), config('app.name'))
         ->subject($this->title)         
                ->markdown('template.subscription_plan_change', [
                    'title' => $this->title,
                    'plan_name'=>$this->plan_name,
                    'purshased_plan_type'=>$this->purshased_plan_type,
                    'email_description' => $this->email_description,
                    'address1' => $this->address1,
                    'address2' => $this->address2,
                ]);
    }
}
