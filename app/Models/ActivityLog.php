<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ActivityLog extends Model
{
    use HasFactory, SoftDeletes;
    public $timestamps = false;
    protected $table = 'activity_log';
    protected $fillable = [
        'user_id',
        'content_id'
    ];

    protected $hidden = [
        'updated_at',
        'deleted_at'
    ];

    public function content() {
        return $this->hasOne('App\Models\Content', 'id', 'content_id');
    }
    
    public function video() {
        return $this->hasOne('App\Models\Video', 'id', 'video_id');
    }
    
}
