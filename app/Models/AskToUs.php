<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class AskToUs extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $table = 'aks_to_us';
    protected $fillable = [
        'user_id',
        'question'
    ];

    protected $hidden = [
        'updated_at',
        'deleted_at'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    
}
