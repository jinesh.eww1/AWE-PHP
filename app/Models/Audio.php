<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Audio extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'video_id',
        'language_id',
    ];

    protected $hidden = [        
        'created_at',
        'updated_at'
    ];

    public function language()
    {
        return $this->belongsTo('App\Models\Language', 'language_id', 'id');
    }
}
