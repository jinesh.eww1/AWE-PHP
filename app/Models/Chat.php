<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Chat extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
         'sender_id',
         'receiver_id',
         'message'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $table = 'chat';


    protected $appends = array('created_date_time');

   
    public function getCreatedDateTimeAttribute()
    {
        if (isset($this->created_at) && $this->created_at != "") {
            $created_at = Carbon::parse($this->created_at)->format('d-m-Y h:i A'); 
            return $created_at;
        }else{
            return '';
        }
    }


    public function sender_user() {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function receiver_user() {
        return $this->belongsTo(User::class, 'receiver_id');
    }

}
