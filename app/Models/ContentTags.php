<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentTags extends Model
{

    use HasFactory;
    
    public $timestamps = false;
    protected $table = 'content_tags';
    protected $fillable = [
        'content_id',
        'tag_id'
    ];


    public function tags()
    {
        return $this->belongsTo('App\Models\Tag', 'tag_id', 'id');
    }

    public function content()
    {
        return $this->belongsTo('App\Models\Content', 'content_id', 'id');
    }

}
