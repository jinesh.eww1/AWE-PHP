<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{

    use HasFactory, SoftDeletes;
    protected $table = 'countries';
    protected $fillable = [
        'currency_id',
        'country_code',
        'country_name',
        'dialing_code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

}
