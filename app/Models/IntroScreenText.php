<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IntroScreenText extends Model
{
    use HasFactory;
    protected $table = 'intro_screen_text';
    protected $fillable = [
        'title',
        'description',
    ];
}
