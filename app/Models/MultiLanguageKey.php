<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MultiLanguageKey extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'multi_language_key';
    protected $fillable = [
        'language_key',
        'group',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    public function multi_language_value() {
        return $this->hasOne('App\Models\MultiLanguageValue','multi_language_key_id','id');
    }

}
