<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class MultiLanguageValue extends Model
{
    use HasFactory,SoftDeletes, HasTranslations;
    protected $table = 'multi_language_value';
    protected $fillable = [
        'multi_language_key_id',
        'language_value',
    ];

        public $translatable = ['language_value'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

}
