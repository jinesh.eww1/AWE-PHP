<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Plan extends Model
{

    use HasFactory,SoftDeletes, HasTranslations;
    protected $table = 'plan';
    protected $fillable = [
        'name',
        'description',
        'stripe_product_id',
        'default_monthly_price',
        'default_yearly_price',
        'monthly_price',
        'yearly_price',
        'stripe_monthly_price_id',
        'stripe_yearly_price_id'

    ];

    public $translatable = ['name','description'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class,'plan_attribute', 'plan_id', 'attribute_id')->withPivot('value');
    }
}
