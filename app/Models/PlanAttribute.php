<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanAttribute extends Model
{

    use HasFactory, SoftDeletes;
    public $timestamps = false;
    protected $table = 'plan_attribute';
    protected $fillable = [
        'plan_id',
        'attribute_id',
        'value'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function Attribute()
    {
        return $this->belongsTo('App\Models\Attribute', 'attribute_id', 'id');
    }  
    
}
