<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends Model
{

    use HasFactory, SoftDeletes;
    protected $table = 'prices';
    protected $fillable = [
        'plan_id',
        'country_id',
        'monthly_price',
        'yearly_price',
        'stripe_monthly_id',
        'stripe_yearly_id',
        'paypal_monthly_id',
        'paypal_yearly_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function plan()
    {
        return $this->belongsTo(Plan::class, 'plan_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

}
