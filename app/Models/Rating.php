<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Rating extends Model
{

    use HasFactory, SoftDeletes;
    public $timestamps = false;
    protected $table = 'rating';
    protected $fillable = [
        'user_id',
        'content_id',
        'rating'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function content() {
        return $this->hasOne('App\Models\Content', 'id', 'content_id');
    }
    
}
