<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ReferralPlans extends Model
{
    use HasFactory, SoftDeletes;
    public $timestamps = false;
    protected $table = 'referral_plans';
    protected $fillable = [
        'user_id',
        'plan_id',
        'plan_type',
        'status'
    ];

    protected $hidden = [
        'updated_at',
        'deleted_at'
    ];

    
}
