<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Streaming extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'streaming';
    protected $fillable = [
        'user_id',
        'content_id',
        'season_id',
        'video_id',
        'seconds',
        'udid',
        'device_name',
        'created_at'
    ];

    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function content(){
        return $this->belongsTo('App\Models\Content', 'content_id', 'id');
    }

    
}
