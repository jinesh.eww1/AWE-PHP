<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class UserAgeRestriction extends Model {
    
    use HasFactory;

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    protected $table = 'user_age_restriction';
    protected $fillable = [
        'user_id',
        'age_rating_id'
    ];


    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function ageratings() {
        return $this->hasOne('App\Models\AgeRatings', 'id', 'age_rating_id');
    }
    
}
