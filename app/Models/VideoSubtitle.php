<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VideoSubtitle extends Model
{

    use HasFactory;
    public $timestamps = false;
    protected $table = 'video_subtitle';
    protected $fillable = [
        'video_id',
        'language_id'
    ];
    

    public function language()
    {
        return $this->belongsTo('App\Models\Language', 'language_id', 'id');
    }
    
}
