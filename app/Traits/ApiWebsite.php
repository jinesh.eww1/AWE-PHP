<?php

namespace App\Traits;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use Session;

trait ApiWebsite {

    protected function api_call($api, $method, $variables = array()) {
        //$base_url = "https://africanworldentertainment.com/";
        //$base_url = "http://dev.awemovies.com/";
        $base_url = "https://awemovies.com/";
        //$base_url = "http://3.138.226.84/";

        $endpoint = $base_url.'api/'.$api;

        //return $variables;
        $token = Session::get('token');

        //$token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI5NWI3MzQ1Ny0xOWE4LTQ1NzgtYTUzMS04YzVlYTA3NmU0YWIiLCJqdGkiOiI2Yjk1MmU5YzM0M2E0OWE5NDI1YTgzZjg2OGRjODNjZmY5NDRjYzE0ZTE3ODI0MzZjMTIwM2JlZDBjOGRkMzViMTA5MjRlOTFhOTkxM2I5YyIsImlhdCI6MTY1NTQ2NjQ1NS44NDI3NDEsIm5iZiI6MTY1NTQ2NjQ1NS44NDI3NDUsImV4cCI6MTY4NzAwMjQ1NS44MzQzOSwic3ViIjoiMjM5Iiwic2NvcGVzIjpbXX0.lqP_nqE_aM_SHidQ5-LeqSkggPqV2J0WuBnlF9ci93j3QS92VMzEA5TxU88sInW4ZtL2PAkJ3vBzCdP5_aZTbbmXhVa5W_i-KLEcHYcPRkqYuLYvCphai08KBDOoIqNgxpEEBawG-nDgFcqbKXsG6wMdPn8fz5sK7Jn2OOv92lk2c1t43GHCPQQThzof_RvLAGwE1QZtJBjsACwZ_JT27wC7jLzS77F8O7YPCK29_DstVrBOabvc77FdHGfH4sCix5Vyzq3uIRiggmrmD4DpiZjwmNKDNZop3NUpyoJTIfsSKvgCE9lQ1PJ2cM0w1Xm6RhM1DOySc0xw_6RjwyM2ob92SVukjzNK7YYQQcuSdh78ayIINjC75OACYCi4cE3l4SDsr527aKXK0Uk86Y37IUS1YHVtFfA7a4Q433mvezfbBdpHSrjAw_PR2xP-42TV7u5gJDJY7wkJ0laJAAVa9vhCx3TEr0rT6Hkpj6Utw9ogJ9JZRpUepZc-0HJydQRW6zqPTmmab-JggrHoiLmFZBuiQ5zeNzUycMKIWbLoEnDznZJnB7PUFSIff3fcQFg8RXZH22qE4D-TR3GmSEhPiO5HOJhJHyU7nCuXUUjXsoJYW73Bl6-Vv_aqP_iGcZMVXtDdVc1KOHGhBBrzCZRV81aRKtPOElho2q6Ejhdepzc';


        if($method == 1){
            $response = Http::accept('application/json')->withToken($token)->withHeaders(['X-localization'=>Session::get('locale')])->get($endpoint, $variables);
        }else{
            $response = Http::accept('application/json')->withToken($token)->withHeaders(['X-localization'=>Session::get('locale')])->post($endpoint, $variables);
        }

        if ($response->status() == 401) {
            Auth::guard('web')->logout();
            Session::flush();
           return redirect('/')->send();
        }
        
        return $response->json();

    }


}
