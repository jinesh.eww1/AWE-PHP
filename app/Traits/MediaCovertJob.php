<?php

namespace App\Traits;

use Aws\MediaConvert\MediaConvertClient;
use Aws\Exception\AwsException;
use Aws\MediaConvert\Exception\MediaConvertException as MediaConvertError;

trait MediaCovertJob {

    protected function getJobStatus($job_id) {

        $mediaConvertClient = new MediaConvertClient([
            'version' => '2017-08-29',
            'region' => 'us-west-1',
            'profile' => 'default',
            'endpoint' => 'https://4nyhswgta.mediaconvert.us-west-1.amazonaws.com'
        ]);

        try{
            $return['result'] = $mediaConvertClient->getJob([
                    'Id' => $job_id
                ]);
            $return['status'] = TRUE;
            return $return;

        }catch(MediaConvertError $e){
            $return['status'] = FALSE;
            $return['message'] = "Job not found";
            return $return;
        }
        
    
    }

}