<?php

namespace App\Traits;
use Laravel\Cashier\Cashier;
use App\Models\User;
use Stripe;
use Carbon\Carbon;

trait StripeTraits {

    protected function createPlan($product_id, $frequency, $currency, $amount, $description = '') {

        try {

            $stripe = new \Stripe\StripeClient([
                          "api_key" => env('STRIPE_SECRET'),
                          "stripe_version" => "2022-08-01"
                        ]);

            $result = $stripe->prices->create([
                      'unit_amount' => $amount * 100,
                      'nickname' => $description,
                      'currency' => strtolower($currency),
                      'recurring' => ['interval' => $frequency],
                      'product' => $product_id,
                     /* 'currency_options' => array(
                            'eur' => array('unit_amount' => 9000),
                            'inr' => array('unit_amount' => 2550)
                          )*/
                ]);

            if(isset($result) && !empty($result)){
                $response['status'] = TRUE;
                $response['response'] = $result;
            }
        }catch (\Stripe\Exception\InvalidRequestException $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }

        return $response;
    
    }

    protected function updatePrice($price_id, $currency, $amount) {

        try {

            $stripe = new \Stripe\StripeClient([
                          "api_key" => env('STRIPE_SECRET'),
                          "stripe_version" => "2022-08-01"
                        ]);
            $currency = strtolower($currency);
            
            $result = $stripe->prices->update(
              $price_id,
                [
                'currency_options' => array(
                        $currency => array('unit_amount' => $amount * 100)
                      )
                ]

            );

            if(isset($result) && !empty($result)){
                $response['status'] = TRUE;
                $response['response'] = $result;
            }
        }catch (\Stripe\Exception\InvalidRequestException $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }

        return $response;
    
    }

    protected function createCustomer($user) {

        try{
            $stripeCustomer = $user->createOrGetStripeCustomer([
                'name' => $user->first_name." ".$user->last_name
            ]);
            $response['data']  = $stripeCustomer;
            $response['message']  = '';
            $response['status']  = TRUE;
        } catch(\Laravel\Cashier\Exceptions\CustomerAlreadyCreated $e){
            $response['message']  = $e->getMessage();
            $response['status']  = FALSE;
        }catch (Exception $e){
            $response['message']  = $e->getMessage();
            $response['status']  = FALSE;
        }
        return $response;
    
    }

    protected function setSubscription($user, $pm_id, $plan_price) {

        try{
        
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
            $result = $stripe->subscriptions->create([
                'customer' => $user->stripe_id,
                'items' => [
                            ['price' => $plan_price],
                            ],
                'default_payment_method' => $pm_id,
                'currency' => strtolower($user->currency_code)
            ]);
            
            $response['status'] = TRUE;
            $response['response'] = $result;

        } catch(\Stripe\Exception\InvalidRequestException $e){
            $response['message']  = $e->getMessage();
            $response['status']  = FALSE;
        }catch(\Laravel\Cashier\Exceptions\IncompletePayment $e){
            $response['message']  = $e->getMessage();
            $response['status']  = FALSE;
        }catch(\Stripe\Exception\CardException $e){
            $response['message']  = $e->getMessage();
            $response['status']  = FALSE;
        }catch (Exception $e){
            $response['message']  = $e->getMessage();
            $response['status']  = FALSE;
        }

        return $response;
    
    }

    protected function cancelCurrentSubscription($subscription_id) {

        try{
        
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
            $result = $stripe->subscriptions->cancel(
                $subscription_id,
                [
                    "prorate" => TRUE
                ]
            );
            
            $response['status'] = TRUE;
            $response['response'] = $result;

        } catch(\Stripe\Exception\InvalidRequestException $e){
            $response['message']  = $e->getMessage();
            $response['status']  = FALSE;
        }catch (Exception $e){
            $response['message']  = $e->getMessage();
            $response['status']  = FALSE;
        }

        return $response;
    
    }

    protected function changeSubscription($user, $plan_price) {

        try{

            if ($user->subscribed('default')){
                $result = $user->subscription('default')->noProrate()->cancelNow();
            }
            $data = $user->defaultPaymentMethod();
            $stripeCustomer = $user->newSubscription($data->id, $plan_price)->add();

            $response['status'] = TRUE;
            $response['response'] = $stripeCustomer;

        } catch(\Stripe\Exception\InvalidRequestException $e){
            $response['message']  = $e->getMessage();
            $response['status']  = FALSE;
        }catch (Exception $e){
            $response['message']  = $e->getMessage();
            $response['status']  = FALSE;
        }

        return $response;
    
    }

    protected function createPaymentMethod($payment, $user) {

        try {

            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));

            $expiryMonth = Carbon::createFromFormat('m/Y', $payment['exp_month_year'])->format('m');
            $expiryYear = Carbon::createFromFormat('m/Y', $payment['exp_month_year'])->format('Y');

            $result = $stripe->paymentMethods->create([
                'type' => 'card',
                'card' => [
                    'number' => $payment['number'],
                    'exp_month' => $expiryMonth,
                    'exp_year' => $expiryYear,
                    'cvc' => $payment['cvv']
                ]
            ]);

            if(isset($result) && !empty($result)){
                $response['status'] = TRUE;
                $response['response'] = $result;
                $pm_id = $result->id;
                $data = $user->addPaymentMethod($pm_id);
                $paymentMethod = $user->updateDefaultPaymentMethod($pm_id);
            }
        } catch (\Stripe\Exception\CardException $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }catch (\Stripe\Exception\IncompletePayment $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }

        return $response;
    
    }

    protected function getPaymentMethod($user) {

        try{

            if ($user->hasPaymentMethod()){
                $result = $user->defaultPaymentMethod();
                $response['status'] = TRUE;
                $response['response'] = $result;
            }else{
                $response['message']  = "No any payment card";
                $response['status']  = FALSE;
            }

        } catch(\Stripe\Exception\InvalidRequestException $e){
            $response['message']  = $e->getMessage();
            $response['status']  = FALSE;
        }catch (Exception $e){
            $response['message']  = $e->getMessage();
            $response['status']  = FALSE;
        }

        return $response;
    
    }


     protected function stripe_webhook_response($request) {

        \Stripe\Stripe::setApiKey('sk_test_51KNzb1FUuGEGeT7bTppNby8cagNhzutb96AkjZaGo33kG9yeJC9ij4pyeIUjLpKA4HMYZnQa3q1jzWYecUMeCjMu00Y5zhLbB2');

            $endpoint_secret = 'whsec_wg9iH5qJEPXb9EwSEDs51hNJeeRwNK2o';

            $payload = @file_get_contents('php://input');
            $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
            $event = null;

            
        try {

            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );

            $response['data']  = $event;
            $response['message']  = '';
            $response['status']  = TRUE;
            
            /*switch ($event->type) {

                case 'invoice.payment_failed':
                    $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent
                    $response['data']  = $paymentIntent;
                    $response['message']  = '';
                    $response['status']  = TRUE;
                    break;
               
                case 'invoice.payment_succeeded':
                    $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent
                    $response['data']  = $paymentIntent;
                    $response['message']  = '';
                    $response['status']  = TRUE;
                    break;
               
                default:
                    echo 'Received unknown event type ' . $event->type;
            }*/


        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        }catch(\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            echo '⚠️  Webhook error while validating signature.';
            http_response_code(400);
            exit();
          }


        return $response;
    
    }


}