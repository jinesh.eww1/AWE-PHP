
var https = require("https");
var fs = require("fs");
var unirest = require("unirest");
express = require("express"),
app = express();
const port = process.env.PORT || 3000;
var mysql = require('mysql');
var moment = require("moment");

var FCM = require("fcm-node");
var fcm = new FCM("AAAAu20IKFc:APA91bGfjxuQHsCdqI9jSSLtM3HqNUQmhUaUNxpG3y4mF5KuYdE1sqyqOPRWMbJTmThmOBkDVU6Z2tO99g1k9sO0TKWnFejp2I2iROTBktbWKuhCSL5I8p1t07PO-HilCV9A7QTnv6rL");

var ssl_option = {
    key: fs.readFileSync("/var/www/html/public/ssl/private.key"),
    cert: fs.readFileSync("/var/www/html/public/ssl/certificate.crt"),
    ca: fs.readFileSync("/var/www/html/public/ssl/bundle.crt")
};

var server = https.createServer(ssl_option, app).listen(port, function () {
    console.log("Server listening on: "+port);
});

var io = require("socket.io")(server, {pingInterval: 60000 * 24, pingTimeout: 60000 * 24, cors: { origin: '*' } });

var connect = mysql.createConnection({
    host: "database-1.ced5tkkqwbxe.us-east-2.rds.amazonaws.com",
    user: "admin",
    password: "nd9rB-By",
    database: "awe",
    charset : 'utf8mb4'
  });

/*app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});*/

connect.connect(function (error) {
    if (error) console.log(error);
    else console.log("MYSQL Connected...");
})

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});
/*app.get("/", function (req, res) {
  res.json({status:true});
});*/

io.on('connection', (socket) => {

  console.log("socket connected !!"); 

  socket.on("connect_user", function (data) {
        var user_id = data.user_id;
        console.log("-------------------------------------");
        console.log("connect_user dataa : ", data);
        console.log("-------------------------------------");
        user_id = parseInt(user_id);
        if (user_id !== false && user_id > 0 && !Number.isNaN(user_id)) {
            var socket_user_id = "user_" + user_id;
            socket.username = socket_user_id;
            socket.room = socket_user_id;
            socket.join(socket_user_id);
            io.sockets.in(socket_user_id).emit("connect_user", {message: "User Connected ..."});
            
        }
  });

  socket.on("watching", function (data) {
        
        var user_id = data.user_id;
        var parent_id = data.parent_id;
        var content_id = data.content_id;
        var season_id = data.season_id;
        var video_id = data.video_id;
        var seconds = data.seconds;
        var udid = data.udid;
        var device_name = data.device_name;
        var plan_id = data.plan_id;

        var socket_user_id = "user_" + user_id;

        if(1){
            console.log("-------------------------------------");
            console.log("watching : ", data);
            console.log("-------------------------------------"); 
        }
        

        if (user_id !== false && content_id !== false && video_id !== false && video_id != 0 && content_id != 0 && parent_id !== false && typeof udid !== "undefined" && typeof device_name !== "undefined" && typeof parent_id !== "undefined"){
            
            var created_date_time = moment();
            created_date_time.subtract(10, 'seconds');
            created_date_time = created_date_time.format('YYYY-MM-DD HH:mm:ss');
            if(parent_id == 0){
                var user_query = "Select id from users where id = '" + user_id +"' or parent_id = '" + user_id +"'";
            }else{
                var user_query = "Select id from users where parent_id = '" + parent_id +"' or id = '" + parent_id +"'";
            }

            /*if(1){
                console.log("-------------------------------------");
                console.log("user_query : ", user_query);
                console.log("-------------------------------------"); 
            }*/
            
            var childs = [];
            connect.query(user_query, function (err, results){
                if (results.length > 0){
                    childs = results.map(function(a) {return a.id;});
                }
                childs = childs.toString();

                var current_stream_query = "Select * from streaming where user_id  in (" + childs + ") and udid != '" + udid + "' and ( updated_at >= '" + created_date_time + "' or created_at >= '" + created_date_time + "' )  group by udid";

                if(1){
                        console.log("-------------------------------------");
                        console.log("current_stream_query : ", current_stream_query);
                        console.log("-------------------------------------"); 
                }

                connect.query(current_stream_query, function (err, stream_results){

                    /*if(1){
                        console.log("-------------------------------------");
                        console.log("Stop Watching : ", stream_results);
                        console.log("-------------------------------------"); 
                    }*/

                    var device_limit = 1;
                    if(typeof plan_id == "undefined"){
                        plan_id = 0;
                    }

                    var attr_query = "Select value from plan_attribute where plan_id = '" + plan_id +"' and attribute_id = 5";
                    connect.query(attr_query, function (err, attr_results){

                        console.log("-------------------------------------");
                        console.log("attr_results : ", attr_results);
                        console.log("-------------------------------------"); 
                        if (attr_results.length > 0){
                            device_limit = attr_results[0].value;
                        }

                        console.log("-------------------------------------");
                        console.log("device_limit : ", device_limit);
                        console.log("-------------------------------------"); 

                        device_limit = device_limit - 1;

                        console.log("-------------------------------------");
                        console.log("straem limit data : ", stream_results);
                        console.log("-------------------------------------");   

                        if (stream_results.length > device_limit){

                            

                            content_ids = stream_results.map(function(a) {return a.content_id;});
                            content_ids = content_ids.toString();

                            var content_query = "Select * from content where id  in (" + content_ids + ")";
                            connect.query(content_query, function (err, content_results){

                                var stream_data = [];
                                var data = {
                                    'data' : []
                                };
                                stream_results.forEach(element => {
                                    content_results.forEach(content => {
                                        var c_name = JSON.parse(content.name);
                                        if(element.content_id == content.id){
                                            var temp = {
                                                'device_name' : element.device_name,
                                                'name' : c_name.en
                                            };
                                            stream_data.push(temp);
                                        }
                                    });

                                });

                                var data = {
                                    'data' : stream_data
                                };

                                /*if(1){
                                    console.log("-------------------------------------");
                                    console.log("stream_data : ", stream_data);
                                    console.log("-------------------------------------"); 
                                }*/
                                

                                io.sockets.in("user_"  + user_id).emit("watching", {
                                    user_id: user_id,
                                    content_id: content_id,
                                    season_id: season_id,
                                    video_id: video_id,
                                    udid: udid,
                                    message : "stop_watching",
                                    watching_data : data
                                });

                            });

                        }else{
                            var data = {
                                    'data' : []
                                };
                            io.sockets.in("user_"  + user_id).emit("watching", {
                                user_id: user_id,
                                content_id: content_id,
                                season_id: season_id,
                                video_id: video_id,
                                udid: udid,
                                message : "",
                                watching_data : data
                            });

                            //User can watch

                            var query = "Select id from streaming where user_id = '" + user_id + "' and video_id = '" + video_id + "'";

                            connect.query(query, function (err, results) {

                                /*console.log("-------------------------------------");
                                console.log("results : ", results);
                                console.log("-------------------------------------"); */
                                if (results.length === 0){
                                    if(seconds == 0){
                                      seconds = 1;
                                    }

                                    var query = "insert into streaming(user_id, content_id, season_id, video_id, seconds, udid, device_name) values('" + user_id + "','" + content_id + "','" + season_id + "','" + video_id + "','" + seconds + "','" + udid + "','" + device_name + "')";

                                    var log_query = "insert into activity_log(user_id, content_id, video_id) values('" + user_id + "','" + content_id + "','" + video_id + "')";
                                    connect.query(log_query, function (log_err, log_results) {});
                                    
                                }else{
                                    if(seconds != results[0].seconds){

                                        if(seconds == 0){
                                          seconds = 1;
                                        }
                                        var query = "update streaming set seconds='" + seconds + "', udid ='" + udid + "', device_name = '" + device_name + "' where user_id='" + user_id + "' and video_id='" + video_id + "' ";
                                    }else{
                                        console.log("-------------------------------------");
                                        console.log("Same Seconds");
                                        console.log("-------------------------------------"); 
                                    }
                                    
                                }
                                connect.query(query, function (err, results) {});
                            });

                        }

                    });
                    
                });

            });

        }
        
  });

  socket.on("send_message", function (data) {
        console.log("-------------------------------------");
        console.log("send_message data : ", data);
        console.log("-------------------------------------");

        var sender_id = data.sender_id;
        var message = data.message;
        var receiver_id = data.receiver_id;
        
        if (sender_id !== false && message !== false && message != '' && sender_id != 0 && receiver_id != 0) {
            console.log("-------------------------------------");
            console.log("sender_id : ", sender_id);
            console.log("-------------------------------------");
            /*var UpdatedDate = new Date().toLocaleString("en-US", {
                timeZone: "America/Toronto"
            });*/

            var query = "Select * from users where id = '" + sender_id +"'" ;

            connect.query(query, function (user_err, user_results){

                console.log("-------------------------------------");
                console.log("sender data: ", user_results);
                console.log("-------------------------------------");

                var created_date_time = moment().format("DD-MM-YYYY hh:mm A");

                var query = "insert into chat(sender_id,receiver_id,message) values('" + sender_id + "','" + receiver_id + "','" + message + "')";
                
                connect.query(query, function (err, results) {});

                var chat_query = "Select id from chat order by id desc limit 1" ;
                var chat_id = 0;
                connect.query(chat_query, function (chat_err, chat_results){

                    console.log("-------------------------------------");
                    console.log("Inserted chat data: ", chat_results[0].id);
                    console.log("-------------------------------------");
                    chat_id = chat_results[0].id;

                    io.sockets.in("user_"  + sender_id).emit("receiver_message", {
                        sender_id: sender_id,
                        receiver_id: receiver_id,
                        message: data.message,
                        created_date_time: created_date_time,
                        receiver_user : user_results[0],
                        id :chat_id,
                        send_by:"me"
                    });

                    io.sockets.in("user_"  + receiver_id).emit("receiver_message", {
                        sender_id: sender_id,
                        receiver_id: receiver_id,
                        message: data.message,
                        created_date_time: created_date_time,
                        receiver_user : user_results[0],
                        id :chat_id,
                        send_by:"other"
                    });

                    var send_push_data = {};
                    send_push_data["sender_id"] = sender_id;
                    send_push_data["receiver_id"] = receiver_id;
                    send_push_data["message"] = data.message;
                    var push_title = "AWE Support";
                    send_push(receiver_id, push_title, data.message, "chat", send_push_data);
                

                });


                

            });

        }
    });

  socket.on('chat message', msg => {
    io.emit('chat message', msg);
    console.log(`msg:${msg}`);
  });


});

function send_push(user_id, title, message_body, type, push_data) {
    console.log("send_push data : ", user_id);

    var query = "Select * from devices where user_id = '" + user_id + "' order by id desc";

    connect.query(query, function (err, results) {
        console.log("-------------------------------------");
        console.log("chat results : ", results[0]);
        console.log("-------------------------------------"); 

        if (results !== undefined && results !== null && results.length > 0) {
            if (results[0].token !== undefined && results[0].token !== "" && results[0].token !== null) {
                console.log("-------------------------------------");
                console.log("user_id :- " +user_id + " got device token : " + results[0].token + " " + results[0].type);
                console.log("-------------------------------------");
                if(results[0].type == "android") {
                    /* console.log("android"); */
                    var message = {
                        to: results[0].token,
                        collapse_key: "AAAAu20IKFc:APA91bGfjxuQHsCdqI9jSSLtM3HqNUQmhUaUNxpG3y4mF5KuYdE1sqyqOPRWMbJTmThmOBkDVU6Z2tO99g1k9sO0TKWnFejp2I2iROTBktbWKuhCSL5I8p1t07PO-HilCV9A7QTnv6rL",
                        data: {
                            body: push_data,
                            title: title,
                            type: type,
                            message: message_body
                        }
                    };
                } else {
                    /* console.log("ios"); */
                    var message = {
                        to: results[0].token,
                        collapse_key: "AAAAu20IKFc:APA91bGfjxuQHsCdqI9jSSLtM3HqNUQmhUaUNxpG3y4mF5KuYdE1sqyqOPRWMbJTmThmOBkDVU6Z2tO99g1k9sO0TKWnFejp2I2iROTBktbWKuhCSL5I8p1t07PO-HilCV9A7QTnv6rL",
                        notification: {
                            body: message_body,
                            title: title,
                            type: type,
                            sound:"default",
                            data: push_data
                        }
                    };
                }

                fcm.send(message, function(err, response) {
                    if (err) {
                        console.log("Something has gone wrong!"+err);
                    } else {
                        /* console.log(response); */
                    }
                });
            }
        }
      
    });


  
}


