function delete_notiflix(e)
{
  var tr = $(e).parent().closest('tr');
  var id = $(e).data('id');
  var token = $(e).data('token');
  var url = $(e).data('url');
  var method = $(e).data('method');
  var alert_messgae = $(e).data('messgae');
  
   var message = '';
   if(method == 'casttype' && method !=''){
    message = alert_messgae;
   }else{
    message ="Are you sure that you want to delete this record?";
   }

  Notiflix.Confirm.Show(
    'Confirm',
    message,
    'Yes',
    'No',
    function(){
      $('#loader').show();
      $.ajax({
        url: url+'/'+id,
        type: 'post',
        dataType: "JSON",
        data: {
            "id": id,
            "_method": 'DELETE',
            "_token": token,
        },
        success: function (returnData) {
          $('#loader').hide();
            //console.log(returnData.status); return false;
          //if (url == 'category') {
            if (returnData.status == false) {
              Notiflix.Notify.Failure(returnData.message);
            } else
            {
              Notiflix.Notify.Success('Deleted');   
              tr.remove();
            }
          /*} else
          {
            Notiflix.Notify.Success('Deleted');
            tr.remove();
          }*/
        }
      }); 
  });

}

function pending_order(e)
{
  var tr = $(e).parent().closest('tr');
  var id = $(e).data('id');
  var status = $(e).data('status');
  var token = $(e).data('token');
  // console.log(status);
  // return false;

  Notiflix.Confirm.Show(
    'Confirm',
    'Are you sure that you delivered this order?',
    'Yes',
    'No',
    function(){
      $('#loader').show();
      $.ajax({
        url: '/board/pending_order',
        type: 'post',
        dataType: "JSON",
        data: {
            "id": id,
            "status": status,
            "_method": 'POST',
            "_token": token,
        },
        success: function (returnData) {
            console.log(returnData);
            $('#loader').hide();
            Notiflix.Notify.Success('Delivered');
            var replace_str = '<span class="badge badge-success">Delivered</span>';
            $(e).replaceWith(replace_str);
        }
      }); 
  });

} 
function active_deactive(e)
{
    var table = $(e).data('table');
    var id = $(e).data('id');
    var status = $(e).data('status');
	var token = $(e).data('token');
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to change status of this record?',
      'Yes',
      'No',
      function(){
        $('#loader').show();
        $.ajax({
              url: '/admin/active_deactive',
              type: "POST",
			        dataType: "JSON",
              data:{
                "table":table,
                "id":id,
				        "_token": token,
                "status":status
              },
              success: function (returnData) {
                  returnData = $.parseJSON(returnData);
                  console.log(returnData);
                  $('#loader').hide();
                  if (typeof returnData != "undefined")
                  {
                      if(returnData.is_success == false){
                          Notiflix.Notify.Failure('Something went wrong');
                      }else{
                        
                        Notiflix.Notify.Success('Updated');
                          // tr.remove();
                          if(status == 1){
                            var replace_str = '<button onclick="active_deactive(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-danger btn-xs waves-effect waves-light"  data-status="0">Inactive</button>';
                          }else{
                            var replace_str = '<button onclick="active_deactive(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-success btn-xs waves-effect waves-light" data-status="1">Active</button>';
                          }
                          $(e).replaceWith(replace_str);
                      } 
                  } 
              }
          });  
    });
}
function restore_account(e)
{
    var table = $(e).data('table');
    var id = $(e).data('id');
    var status = $(e).data('status');
  var token = $(e).data('token');
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to restore this record?',
      'Yes',
      'No',
      function(){
        $('#loader').show();
        $.ajax({
              url: '/admin/restore_account',
              type: "POST",
              data:{
                "table":table,
                "id":id,
                "_token": token,
              },
              success: function (returnData) {
                  returnData = $.parseJSON(returnData);
                  $('#loader').hide();

                  if (typeof returnData != "undefined")
                  {
                      if(returnData.status == false){
                          Notiflix.Notify.Failure(returnData.message);
                      }else{
                        
                        Notiflix.Notify.Success(returnData.message);
                          var replace_str = '<button onclick="active_deactive(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-success btn-xs waves-effect waves-light" data-status="1">Active</button>';

                          $(e).replaceWith(replace_str);
                      } 
                  } 
              }
          });  
    });
}

function active_deactive_category(e)
{
    var table = $(e).data('table');
    var id = $(e).data('id');
    var status = $(e).data('status');
	  var token = $(e).data('token');
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to change status of this record?',
      'Yes',
      'No',
      function(){
        $('#loader').show();
        $.ajax({
              url: '/board/active_deactive_category',
              type: "POST",
			        dataType: "JSON",
              data:{
                "table":table,
                "id":id,
				        "_token": token,
                "status":status
              },
              success: function (returnData) {
                  returnData = $.parseJSON(returnData);
                  console.log(returnData);
                  $('#loader').hide();
                  if (typeof returnData != "undefined")
                  {
                      if(returnData.is_success == false){
                          Notiflix.Notify.Failure('Something went wrong');
                      }else{
                        
                        Notiflix.Notify.Success('Updated');
                          // tr.remove();
                          if(status == 1){
                            var replace_str = '<button onclick="active_deactive_category(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-danger btn-xs waves-effect waves-light"  data-status="0">Inactive</button>';
                          }else{
                            var replace_str = '<button onclick="active_deactive_category(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-success btn-xs waves-effect waves-light" data-status="1">Active</button>';
                          }
                          $(e).replaceWith(replace_str);
                      } 
                  } 
              }
          });  
    });
}

function active_deactive_category(e)
{
    var table = $(e).data('table');
    var id = $(e).data('id');
    var status = $(e).data('status');
    var token = $(e).data('token');
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to change status of this record?',
      'Yes',
      'No',
      function(){
        $('#loader').show();
        $.ajax({
              url: '/admin/active_deactive_category',
              type: "POST",
              dataType: "JSON",
              data:{
                "table":table,
                "id":id,
                "_token": token,
                "status":status
              },
              success: function (returnData) {
                  returnData = $.parseJSON(returnData);
                  console.log(returnData);
                  $('#loader').hide();
                  if (typeof returnData != "undefined")
                  {
                      if(returnData.is_success == false){
                          Notiflix.Notify.Failure('Something went wrong');
                      }else{
                        
                        Notiflix.Notify.Success('Updated');
                          // tr.remove();
                          if(status == 1){
                            var replace_str = '<button onclick="active_deactive_category(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-danger btn-xs waves-effect waves-light"  data-status="0">Inactive</button>';
                          }else{
                            var replace_str = '<button onclick="active_deactive_category(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-success btn-xs waves-effect waves-light" data-status="1">Active</button>';
                          }
                          $(e).replaceWith(replace_str);
                      } 
                  } 
              }
          });  
    });
}

function active_deactive_product(e)
{
    var table = $(e).data('table');
    var id = $(e).data('id');
    var status = $(e).data('status');
    var token = $(e).data('token');
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to change status of this record?',
      'Yes',
      'No',
      function(){
        $('#loader').show();
        $.ajax({
              url: '/board/active_deactive_product',
              type: "POST",
              dataType: "JSON",
              data:{
                "table":table,
                "id":id,
                "_token": token,
                "status":status
              },
              success: function (returnData) {
                  returnData = $.parseJSON(returnData);
                  console.log(returnData);
                  $('#loader').hide();
                  if (typeof returnData != "undefined")
                  {
                      if(returnData.is_success == false){
                          Notiflix.Notify.Failure('Something went wrong');
                      }else{
                        Notiflix.Notify.Success('Updated');
                          // tr.remove();
                          if(status == 1){
                            var replace_str = '<button onclick="active_deactive_product(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-danger btn-xs waves-effect waves-light"  data-status="0">Inactive</button>';
                          }else{
                            var replace_str = '<button onclick="active_deactive_product(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-success btn-xs waves-effect waves-light" data-status="1">Active</button>';
                          }
                          $(e).replaceWith(replace_str);
                      } 
                  } 
              }
          });  
    });
}

function active_deactive_product(e)
{
    var table = $(e).data('table');
    var id = $(e).data('id');
    var status = $(e).data('status');
    var token = $(e).data('token');
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to change status of this record?',
      'Yes',
      'No',
      function(){
        $('#loader').show();
        $.ajax({
              url: '/admin/active_deactive_product',
              type: "POST",
              dataType: "JSON",
              data:{
                "table":table,
                "id":id,
                "_token": token,
                "status":status
              },
              success: function (returnData) {
                  returnData = $.parseJSON(returnData);
                  console.log(returnData);
                  $('#loader').hide();
                  if (typeof returnData != "undefined")
                  {
                      if(returnData.is_success == false){
                          Notiflix.Notify.Failure('Something went wrong');
                      }else{
                        Notiflix.Notify.Success('Updated');
                          // tr.remove();
                          if(status == 1){
                            var replace_str = '<button onclick="active_deactive_product(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-danger btn-xs waves-effect waves-light"  data-status="0">Inactive</button>';
                          }else{
                            var replace_str = '<button onclick="active_deactive_product(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-success btn-xs waves-effect waves-light" data-status="1">Active</button>';
                          }
                          $(e).replaceWith(replace_str);
                      } 
                  } 
              }
          });  
    });
}


function active_deactive_meal(e)
{
    var table = $(e).data('table');
    var id = $(e).data('id');
    var status = $(e).data('status');
    var token = $(e).data('token');
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to change status of this record?',
      'Yes',
      'No',
      function(){
        $('#loader').show();
        $.ajax({
              url: '/board/active_deactive_meal',
              type: "POST",
              dataType: "JSON",
              data:{
                "table":table,
                "id":id,
                "_token": token,
                "status":status
              },
              success: function (returnData) {
                  returnData = $.parseJSON(returnData);
                  console.log(returnData);
                  $('#loader').hide();
                  if (typeof returnData != "undefined")
                  {
                      if(returnData.is_success == false){
                          Notiflix.Notify.Failure('Something went wrong');
                      }else{
                        
                        Notiflix.Notify.Success('Updated');
                          // tr.remove();
                          if(status == 1){
                            var replace_str = '<button onclick="active_deactive_meal(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-danger btn-xs waves-effect waves-light"  data-status="0">Inactive</button>';
                          }else{
                            var replace_str = '<button onclick="active_deactive_meal(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-success btn-xs waves-effect waves-light" data-status="1">Active</button>';
                          }
                          $(e).replaceWith(replace_str);
                      } 
                  } 
              }
          });  
    });
}




function active_deactive_faq(e)
{
    var table = $(e).data('table');
    var id = $(e).data('id');
    var status = $(e).data('status');
    var token = $(e).data('token');
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to change status of this record?',
      'Yes',
      'No',
      function(){
        $('#loader').show();
        $.ajax({
              url: '/admin/active_deactive_faq',
              type: "POST",
              dataType: "JSON",
              data:{
                "table":table,
                "id":id,
                "_token": token,
                "status":status
              },
              success: function (returnData) {
                  returnData = $.parseJSON(returnData);
                  console.log(returnData);
                  $('#loader').hide();
                  if (typeof returnData != "undefined")
                  {
                      if(returnData.is_success == false){
                          Notiflix.Notify.Failure('Something went wrong');
                      }else{
                        
                        Notiflix.Notify.Success('Updated');
                          // tr.remove();
                          if(status == 1){
                            var replace_str = '<button onclick="active_deactive_faq(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-danger btn-xs waves-effect waves-light"  data-status="0">Inactive</button>';
                          }else{
                            var replace_str = '<button onclick="active_deactive_faq(this);" data-table="'+table+'" data-id="'+id+'" data-token= "'+token+'"  class="btn btn-success btn-xs waves-effect waves-light" data-status="1">Active</button>';
                          }
                          $(e).replaceWith(replace_str);
                      } 
                  } 
              }
          });  
    });
}


function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1').attr('src', e.target.result);
            $('.blah1').attr('href', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah2').attr('src', e.target.result);
            $('.blah2').attr('href', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function updateInfos() {
  //var infos = document.getElementById('infos');
  //infos.textContent = "";
  for (var i = 0; i < myVideos.length; i++) {
    //infos.textContent += myVideos[i].name + " duration: " + format(myVideos[i].duration) + '\n';
    $('#video_duration').val(format(myVideos[i].duration));
  }
}

function format(time) {   
    // Hours, minutes and seconds
    var hrs = ~~(time / 3600);
    var mins = ~~((time % 3600) / 60);
    var secs = ~~time % 60;

    // Output like "1:01" or "4:03:59" or "123:03:59"
    var ret = "";
    if (hrs > 0) {
        ret += "" + hrs + "h ";
    }
    if (secs > 30) {
        mins++;
    }
    ret += "" + mins + "m";
 
    return ret;
}