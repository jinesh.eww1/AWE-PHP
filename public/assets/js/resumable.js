
var site_url = location.origin;
var token=$('meta[name="csrf-token"]').attr('content');
var method=$('#browseFile').attr('data-method');

    let browseFile = $('#browseFile');
    let resumable = new Resumable({
        target: site_url+'/admin/'+method,
        query:{_token:token} ,// CSRF token
        fileType: ['mp4','3gp','mov','mkv'],
        
        //chunkSize: 1*1024*1024, // default is 1*1024*1024, this should be less than your maximum limit in php.ini

        headers: {
            'Accept' : 'application/json'
        },
        
        testChunks: false,
        throttleProgressCallbacks: 1,
    });

    resumable.assignBrowse(browseFile[0]);
    
    resumable.on('fileAdded', function (file) { // trigger when file picked
        showProgress();
        $('#upToggle').show();
        $('#content_submit_btn').attr('disabled',true);
        $('#video_upload_btn').attr('disabled',true);
        resumable.upload() // to actually start uploading.
    });

    resumable.on('fileProgress', function (file) { // trigger when file progress update
        updateProgress(Math.floor(file.progress() * 100));
    });

    resumable.on('fileSuccess', function (file, response) { // trigger when file upload complete
        response = JSON.parse(response)
        console.log(response);
        $('#video_link').val(response.path);
        $('#video_name').val(response.name);
        $('#content_submit_btn').attr('disabled',false);
        $('#video_upload_btn').attr('disabled',false);
        $('#browseFile').removeAttr('required');

        $('#videoPreview').attr('src', response.path);
        $('.card-footer').show();
        
    });

    resumable.on('fileError', function (file, response) { // trigger when there is any error
        alert('file uploading error.')
        $('#content_submit_btn').attr('disabled',false);
        $('#video_upload_btn').attr('disabled',false);
        $('#upToggle').hide();
    });


    let progress = $('.progress');
    function showProgress() {
        progress.find('.progress-bar').css('width', '0%');
        progress.find('.progress-bar').html('0%');
        progress.find('.progress-bar').removeClass('bg-success');
        progress.show();
    }

    function updateProgress(value) {
        progress.find('.progress-bar').css('width', `${value}%`)
        progress.find('.progress-bar').html(`${value}%`)
    }

    document.getElementById("upToggle").onclick = () => {
      if (resumable.isUploading()) { resumable.pause(); }
      else { resumable.upload(); }
    };
    
    function hideProgress() {
        progress.hide();
    }

