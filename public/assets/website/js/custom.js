$('.slider-home').slick({
    lazyLoad: 'ondemand',
    slidesToShow: 6,
    slidesToScroll: 1,
    responsive: [{
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }
    ]
});
$('.last-slider').slick({
    lazyLoad: 'ondemand',
    slidesToShow: 6,
    slidesToScroll: 1,
    responsive: [{
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }
    ]
});

$(function() {
    var current_page_URL = location.href;
    $(".navbar-nav a.nav-link").each(function() {
        if ($(this).attr("href") !== "#") {
            var target_URL = $(this).prop("href");
            if (target_URL == current_page_URL) {
                $('.navbar-nav a.nav-link').removeClass('active');
                $(this).addClass('active');
                return false;
            } else {
                $('.navbar-nav a.nav-link').removeClass('active');
                return true;
            }
        }
    });
});

$(document).ready(function() {
    $(".about-click").on('click', function(event) {
        event.preventDefault();
        $(".input-question").css('display', 'block');
    });
    $(".click-submit").on('click', function(event) {
        event.preventDefault();
        $(".input-question").css('display', 'none');;
        /* Act on the event */
    });
    $(".click-submit").on("click", function(event) {
        $(".about-question-input").val("");
    });
    $(".more_like_dis").on("click", function(){
        $(this).removeClass('fw-light');
        $(".episodes_dis").addClass("fw-light");
    });
    $(".episodes_dis").on("click", function(){
        $(this).removeClass('fw-light');
        $(".more_like_dis").addClass("fw-light");
    });
});


var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
var popoverList = popoverTriggerList.map(function(popoverTriggerEl) {
    return new bootstrap.Popover(popoverTriggerEl)
});

$(window).scroll(function(){
  if ($(this).scrollTop() > 50) {
     $('.fixed-top').addClass('box-hover');
  } else {
     $('.fixed-top').removeClass('box-hover');
  }
});
