<?php
return [
    'Name' => 'Nom',
    'api' => [
        'login_success' => 'Connecté avec succès',
        'email_phone_incorrect_format' => 'Format incorrect de l'."'".'e-mail/numéro de téléphone',
        'account_not_exist' => 'L'."'".'adresse e-mail ou le numéro de téléphone saisi ne correspond pas aux enregistrements',
        'account_deactivated' => 'Compte désactivé.',
        'otp_sent_success' => 'OTP envoyé avec succès',
        'register_success' => 'Enregistré avec succès',
        'user_not_found' => 'Utilisateur non trouvé',
        'logged_out' => 'Déconnecté avec succès',
        'notification_status_update' => 'Mise à jour de l'."'".'état des notifications',
        'under_maintenance' => 'Le serveur est en maintenance, veuillez réessayer après un certain temps',
        'new_version_available' => 'Nouvelle version de l'."'".'application disponible, veuillez mettre à jour votre application',
        'missing_app_version' => 'Version et type d'."'".'application manquants...',
        'logout_success' => 'Déconnectez-vous avec succès',
        'deleted_account' => 'Votre demande de suppression de compte a été soumise au client. Vous ne pouvez pas vous connecter.',
        'register_account_website' => 'Votre compte n'."'".'existe pas. Enregistrez votre compte à partir du site Web AWE.'
    ],
    'plan' => [
        'choose_plan' => 'Choisissez le forfait',
        'skip' => 'sauter',
        'monthly_price' => 'Prix mensuel',
         'yearly_price' => 'prix annuel',
         'subscription_overview' => "Aperçu du plan d'abonnement",
         'selected_plan' => 'Votre forfait sélectionné',
         'payment' => 'Paiement',
        'buy_now' => 'Acheter maintenant'
    ],
    'login' => [
        'welcome' => 'Bienvenue à AWE',
        'select_lang' => "Sélectionnez la langue de l'application",
        'enter_email_phone' => 'Enter your email/mobile number',
        'email_required' => 'The email field is required.',
        'email_invalid' => 'Email/Phone number incorrect format',
        'password_required' => 'The password field is required.',
        'min_8_password' => 'The password must be at least 8 characters.',
        'password' => 'Password',
        'forgot_password' => 'Forgot Password?',
        'sign_in' => 'Sign In',
        'or_continue' => 'Or Countinue with',
        'dont_have_account' => "Don't have an account?",
        'sign_up' => "Sign Up here",
    ],
    'register' => [
        'secondary_title' => 'Create an account to access the full \n version with all features.',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'Email Address',
        'send_otp' => 'Send OTP',
        'mobile' => 'Mobile Number',
        'enter_email_otp' => 'Enter Email OTP',
        'enter_mobile_otp' => 'Enter Mobile OTP',
        'password' => 'Password',
        'c_password' => 'Confirm Password',
        'referral_code' => 'Referral Code',
        'already_account' => 'Already have an account?',
        'sign_in' => 'Sign In',
        'by_clicking' => 'By Clicking',
        'agree' => ', you are agree to our Terms & Conditions',
        'first_name_required' => 'The first name field is required.',
        'first_name_min_2' => 'The first name must be at least 2 characters.',
        'last_name_required' => 'The last name field is required.',
        'email_required' => 'The email address field is required.',
        'mobile_required' => 'The mobile number field is required.',
        'email_otp_required' => 'The email OTP is required.',
        'mobile_otp_required' => 'The mobile OTP field is required.',
        'password_required' => 'The password field is required.',
        'c_password_required' => 'The confirm password field is required.',
        'min_8_password' => 'The password must be at least 8 characters.',
        'password_cpassword_same' => 'Password & confirm password should be the same',
        'valid_otp' => 'Please enter valid OTP',
    ],
    'forgot_password' => [
        'secondary_title' => 'Entrez votre adresse e-mail ou votre numéro de téléphone portable pour la récupération de votre compte.',
        'send' => 'envoyer'
    ],
    'recommendation' => 'Recommendation',
    'view_more' => 'Voir plus',
    'favorite_video_success' => 'Vidéo aimée',
    'favorite_video_remove' => 'La vidéo n'."'".'a pas aimé',
    'favorite_video_exits' => 'La vidéo existe déjà',
    'video_not_found' => 'vidéo non trouvée',
    'no_data_found'=>'Aucune donnée disponible',
    'mylist_video_success' => 'Vidéo ajoutée avec succès',
    'mylist_video_remove' => 'Vidéo supprimée avec succès',
    'feedback_success' => 'Merci pour votre avis',
    'rating_exits' => 'L'."'".'évaluation de la vidéo existe déjà',
    'rating_success' => 'Évaluation de la vidéo Ajouter avec succès',
    'faq_does_not_exist' => 'La FAQ n'."'".'existe pas',
    'user_age_restriction_success' => 'Affichage des restrictions enregistrées',
    'playback_setting_update_success' => 'Mise à jour des paramètres de lecture réussie',
    'playback_setting_not_update' => 'Paramètre de lecture non mis à jour',
    'category_data_added' => 'Données de catégorie d'."'".'utilisateurs ajoutées avec succès',
    'category_not_found' => 'Catégorie de titre sélectionnée introuvable',
    'deregister_device_sucess' => 'Appareil désenregistré avec succès',
    'device_not_found' => 'Appareil non trouvé',
    'subscription_plan_added_sucess' => 'Plan d'."'".'abonnement ajouté avec succès',
    'activity_log_added_success' => 'Journal d'."'".'activité ajouté avec succès',
    'enter_valid_user_or_content' => 'Entrez un utilisateur ou un contenu valide',
    'invalid_phone' => 'Numéro de téléphone invalide',
    'phone_email_not_registered' => 'Le numéro de téléphone n'."'".'est pas enregistré',
    'change_password_link_sent' => 'Le lien de réinitialisation du mot de passe a été envoyé à l'."'".'adresse e-mail enregistrée',
    'email_address_not_registered' => 'L'."'".'adresse e-mail n'."'".'est pas enregistrée',
    'title_restiction_not_found' => 'La restriction de titre sélectionnée est introuvable',
    'movie_tvshow_not_found' => 'Film/émission de télévision sélectionné introuvable',
    'lock_added' => 'Verrouillage du profil enregistré',
    'verified' => 'Vérifié',
    'incorrect_pin' => 'NIP incorrect. Veuillez réessayer.',
    'disable_pin' => 'Votre code PIN a bien été désactivé.',
    'change_pin' => 'Votre code PIN a été modifié avec succès.',
    'something_went_wrong' => 'Quelque chose s'."'".'est mal passé',
    'wrong_lat_long' => 'Activer pour récupérer l'."'".'emplacement en utilisant cette latitude et cette longitude',
    'account_added' => 'Profil ajouté avec succès',
    'account_deleted' => 'Profil supprimé avec succès',
    'referral_expired' => 'Le code de parrainage est expiré',
    'referral_invalid' => 'Le code de parrainage est invalide',
    'referral_used' => 'Le code de parrainage déjà utilisé',
    'social_account_not_forgot_password' => "Ce compte est déjà enregistré avec un compte social, vous ne pouvez pas oublier le mot de passe",
    'home'=>[
        'categories' =>  'Catégories',
        'whatch_now' =>  'Regarde maintenant',
        'home' =>  'Accueil',
        'kids' =>  'Enfants',
        'view_more' => 'Voir plus'
    ],
    'content_detail'=>[
        'trailer' =>  'Bande annonce',
        'my_list' =>  'Ma liste',
        'liked' =>  'Comme',
        'dislike' =>  'Ne pas aimer',
        'play' =>  'Play',
        'share' =>  'Share',
        'episodes' =>  'Épisodes',
        'more_like_this' =>  'Plus comme ça',
        'cast' => 'jeter',
        'starring' => 'Mettant en vedette',
        'this_series_is' => 'Cette série est',
        'genres' => 'Genre',
        'languages' => 'Langues',
        'audio' => 'l'."'".'audio',
        'subtitles' => 'Les sous-titres',
        'available_languages' => 'Langues disponibles',
        'available_languages_sub_text' => 'Les langues peuvent être changées pendant la lecture de la vidéo',
    ],
    'faq' => [
        'help' => 'aider'
    ],
    'payment' => [
        'account_not_found' => 'Compte client Stripe introuvable',
        'payment_successful' => 'Paiement réussi',
        'plan_cancelled' => 'Annulation du forfait',
        'plan_puchased' => 'Forfait acheté',
        'plan_cancelled' => 'plan annulé',
    ],
    'paypal' => [
        'subscription_not_activated' => 'Votre abonnement n'."'".'est pas activé',
        'transaction_cancelled' => "Votre transaction a été annulée",
        'can_not_cancel_plan' => 'Vous ne pouvez pas annuler ce plan',
        'subscription_cancelled' => 'Abonnement annulé',
        'Unable_to_cancel_plan' => 'Impossible d'."'".'annuler cet abonnement',
    ],
    'profile' => [
        'profile_updated' => 'Mise à jour du profil réussie',
        'current_password_incorrect' => "Votre mot de passe actuel est incorrect",
        'password_changed' => 'Le mot de passe a été changé avec succès',
        'preferred_language_updated' => 'Langue préférée mise à jour',
        'list_childrens' => 'Liste des enfants',
        'childrens_not_found' => 'Enfants introuvables',
        'childrens_deleted' => 'Enfants supprimés avec succès',
        'no_notifications_found' => 'Aucune notification trouvée',
        'notifications_deleted' => 'Notifications supprimées avec succès',
    ],
    'user' => [
        'account_limit' => "Vous avez atteint la limite maximale d'ajout de profils.",
        'log_generated' => 'Journal généré',
        'log_not_generated' => 'Journal non généré',
        'log_update_generated' => 'Mise à jour du journal réussie',
    ],
    'chat' => [
        'not_found' => 'Aucun chat disponible!'
    ]
];
