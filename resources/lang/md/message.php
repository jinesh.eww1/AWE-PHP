<?php
return [
    'Name' => 'Namew',
    'api' => [
        'login_success' => '登錄成功',
        'email_phone_incorrect_format' => '电子邮件/电话号码格式不正确',
        'account_not_exist' => '输入的电子邮件地址或电话号码与记录不匹配',
        'account_deactivated' => '帐户已停用。',
        'otp_sent_success' => 'otp发送成功',
        'register_success' => '注册成功.',
        'user_not_found' => '未找到用户',
        'logged_out' => '成功登出',
        'notification_status_update' => '通知状态更新',
        'under_maintenance' => '服务器正在维护中，请稍后再试',
        'new_version_available' => '应用有新版本，请升级您的应用',
        'missing_app_version' => '缺少应用程序版本和类型...',
        'logout_success' => '注销成功',
        'deleted_account' => '您的帐户删除请求已提交给客户。您无法登录。',
        'register_account_website' => '您的帐户不存在。从 AWE 网站注册您的帐户。'

    ],
    'plan' => [
        'choose_plan' => '选择计划',
        'skip' => '跳过',
        'monthly_price' => '每月价格',
         'yearly_price' => '年价',
         'subscription_overview' => '订阅计划概述',
         'selected_plan' => '您选择的计划',
         'payment' => '支付',
        'buy_now' => '立即购买'
    ],
    'login' => [
        'welcome' => '欢迎来到 AWE',
        'select_lang' => '选择应用程序语言',
        'enter_email_phone' => 'Enter your email/mobile number',
        'email_required' => 'The email field is required.',
        'email_invalid' => 'Email/Phone number incorrect format',
        'password_required' => 'The password field is required.',
        'min_8_password' => 'The password must be at least 8 characters.',
        'password' => 'Password',
        'forgot_password' => 'Forgot Password?',
        'sign_in' => 'Sign In',
        'or_continue' => 'Or Countinue with',
        'dont_have_account' => "Don't have an account?",
        'sign_up' => "Sign Up here",
    ],
    'register' => [
        'secondary_title' => '创建一个帐户以访问具有所有功能的完整版\n。',
        'first_name' => '名',
        'last_name' => '姓',
        'email' => '电子邮件地址',
        'send_otp' => '发送一次性密码',
        'mobile' => '手机号码',
        'enter_email_otp' => '输入电子邮件 OTP',
        'enter_mobile_otp' => '输入移动 OTP',
        'password' => '密码',
        'c_password' => '确认密码',
        'referral_code' => '推荐代码',
        'already_account' => '已经有一个帐户？',
        'sign_in' => '登入',
        'by_clicking' => '通过点击',
        'agree' => ', 您同意我们的条款和条件',
        'first_name_required' => '名字字段是必需的。',
        'first_name_min_2' => '名字必须至少包含 2 个字符。',
        'last_name_required' => '姓氏字段是必需的。',
        'email_required' => '电子邮件地址字段是必需的。',
        'mobile_required' => '手机号码字段为必填项。',
        'email_otp_required' => '电子邮件 OTP 是必需的。',
        'mobile_otp_required' => '移动 OTP 字段是必需的。',
        'password_required' => '密码字段是必需的。',
        'c_password_required' => '确认密码字段是必需的。',
        'min_8_password' => '密码必须至少为 8 个字符。',
        'password_cpassword_same' => '密码和确认密码应该相同',
        'valid_otp' => '请输入有效的一次性密码',
    ],
    'forgot_password' => [
        'secondary_title' => '输入您的电子邮件地址或手机号码以恢复您的帐户。',
        'send' => '发送'
    ],
    'recommendation' => '推荐',
    'view_more' => '查看更多',
    'favorite_video_success' => '喜欢的视频',
    'favorite_video_remove' => '不喜欢的视频',
    'favorite_video_exits' => '视频已退出',
    'video_not_found' => '找不到视频',
    'no_data_found'=>'无可用数据',
    'mylist_video_success' => '视频添加成功',
    'mylist_video_remove' => '视频删除成功',
    'feedback_success' => '感谢您的反馈意见',
    'rating_exits' => '视频分级已存在',
    'rating_success' => '视频评分添加成功',
    'faq_does_not_exist' => '常见问题不存在',
    'user_age_restriction_success' => '查看限制已保存',
    'playback_setting_update_success' => '播放设置更新成功',
    'playback_setting_not_update' => '播放设置未更新',
    'category_data_added' => '用户类别数据添加成功',
    'category_not_found' => '未找到所选标题类别',
    'deregister_device_sucess' => '设备注销成功',
    'device_not_found' => '没有找到设备',
    'subscription_plan_added_sucess' => '订阅计划添加成功',
    'activity_log_added_success' => '活动日志添加成功',
    'enter_valid_user_or_content' => '输入有效的用户或内容',
    'invalid_phone' => '无效的电话号码',
    'phone_email_not_registered' => '电话号码未注册',
    'change_password_link_sent' => '重置密码链接已发送至注册邮箱',
    'email_address_not_registered' => '电子邮件地址未注册',
    'title_restiction_not_found' => '未找到选定的标题限制',
    'movie_tvshow_not_found' => '未找到所选电影/电视节目',
    'lock_added' => '个人资料锁定已保存',
    'verified' => '已验证',
    'incorrect_pin' => 'PIN 码不正确。请再试一次。',
    'disable_pin' => '您的引脚已成功禁用。',
    'change_pin' => '您的密码已成功更改。',
    'something_went_wrong' => '有些不对劲',
    'wrong_lat_long' => '启用以使用此纬度和经度获取位置',
    'account_added' => '配置文件添加成功',
    'account_deleted' => '已成功删除个人资料',
    'referral_expired' => '推荐码已过期',
    'referral_invalid' => '推荐码无效',
    'referral_used' => '已使用的推荐代码',
    'social_account_not_forgot_password' => "此账号已注册社交账号，不能忘记密码",
    'home'=>[
        'categories' =>  '类别',
        'whatch_now' =>  '立即观看',
        'home' =>  '家园',
        'kids' =>  '孩子们',
        'view_more' => '查看更多'
    ],
    'content_detail'=>[
        'trailer' =>  '预告片',
        'my_list' =>  '我的清单',
        'liked' =>  '喜欢',
        'dislike' =>  '不喜欢',
        'play' =>  'Play',
        'share' =>  'Share',
        'episodes' =>  '剧集',
        'more_like_this' =>  '更像这样',
        'cast' => '投掷',
        'starring' => '主演',
        'this_series_is' => '这个系列是',
        'genres' => '流派',
        'languages' => '语言',
        'audio' => '声音的',
        'subtitles' => '字幕',
        'available_languages' => '可用语言',
        'available_languages_sub_text' => '播放视频时可以更改语言',
    ],
    'faq' => [
        'help' => '帮助'
    ],
    'payment' => [
        'account_not_found' => '未找到条纹客户帐户',
        'payment_successful' => '支付成功',
        'plan_cancelled' => '计划取消',
        'plan_puchased' => '计划购买',
        'plan_cancelled' => '计划取消',
    ],
    'paypal' => [
        'subscription_not_activated' => '您的订阅未激活',
        'transaction_cancelled' => "您的交易已被取消",
        'can_not_cancel_plan' => '您无法取消此计划',
        'subscription_cancelled' => '订阅已取消',
        'Unable_to_cancel_plan' => '无法取消此订阅',
    ],
    'profile' => [
        'profile_updated' => '配置文件更新成功',
        'current_password_incorrect' => "您当前的密码不正确",
        'password_changed' => '密码修改成功',
        'preferred_language_updated' => '首选语言已更新',
        'list_childrens' => '儿童名单',
        'childrens_not_found' => '找不到孩子',
        'childrens_deleted' => '子项删除成功',
        'no_notifications_found' => '未找到通知',
        'notifications_deleted' => '已成功删除通知',
    ],
    'user' => [
        'account_limit' => '您已达到添加配置文件的最大限制。',
        'log_generated' => '生成的日志',
        'log_not_generated' => '未生成日志',
        'log_update_generated' => '日志更新成功',
    ],
    'chat' => [
        'not_found' => '没有可用的聊天！'
    ]
];
