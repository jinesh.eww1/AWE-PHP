@extends('website.layouts.master')
@section('content')

<style type="text/css">
  .top-padding{
    padding-top: 70px !important;
    padding-bottom: 50px !important;
  }
</style>
<div class="container">
  <div class="jumbotron text-yellow top-padding position-relative">
    <div class="position-absolute arrow-btn"><a href="{{ route('profile')}}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a></div>
    <h2 class=" text-center">@php if(Session::get('app_string')){ echo Session::get('app_string.settings.viewing_activity'); }else{ echo CommonHelper::multi_language('settings','viewing_activity')->multi_language_value->language_value; } @endphp</h2>
  </div>
</div>
<?php if (count($activity_log) > 0){ ?>
<div class="container">
   <!-- Nav tabs -->
   
    <ul class="nav nav-tabs font-23 text-left px-0" role="tablist" style="width:102%;">
      @php
        if (count($activity_log) > 0){
        foreach ($activity_log as $key => $value){
          if($key == 0){
            $active = 'active text-white';
          }else{
            $active = 'text-white';
          }
      @endphp
        <li class="nav-item {{ $active }}">
          <a class="nav-link {{ $active }} bg-transparent border-0 ps-0" data-toggle="tab" href="#{{$value->id}}">{{$value->first_name}} {{$value->last_name}}</a>
        </li>
      @php
        } }
      @endphp
    </ul>
  

  <div class="tab-content">
    @php
      if (count($activity_log) > 0){
      foreach ($activity_log as $key => $value){
        if($key == 0){
          $active = 'active';
        }else{
          $active = 'fade';
        }
    @endphp

      <div id="{{$value->id}}" class="container tab-pane {{$active}}"><br>
        <div class="row">
          <table class="table table-hover text-left text-white">
          <thead>
            <tr>
              
              <th style="width: 15% !important;">@php if(Session::get('app_string')){ echo Session::get('app_string.settings.date'); }else{ echo CommonHelper::multi_language('settings','date')->multi_language_value->language_value; } @endphp</th>

              <th>@php if(Session::get('app_string')){ echo Session::get('app_string.settings.content'); }else{ echo CommonHelper::multi_language('settings','content')->multi_language_value->language_value; } @endphp</th>
            </tr>
          </thead>
          <tbody>
            @if(count($value->activity_log)>0)
              @foreach($value->activity_log as $alkey => $alvalue)
                <tr>
                  <td>{{ date('m-d-Y',strtotime($alvalue->created_at)) }}</td>
                  <td>{{ $alvalue->content->name }} {{ !emptY($alvalue->video->season->season)?" - ".$alvalue->video->season->season:'' }} {{ !emptY($alvalue->video->episode_name)?" - ".$alvalue->video->episode_name:'' }}</td>
                </tr>
              @endforeach
            @else
              <tr>
                <td colspan="2" class="text-center">
                  @php if(Session::get('app_string')){ echo Session::get('app_string.app_titles_and_messages.no_data_found'); }else{ echo CommonHelper::multi_language('app_titles_and_messages','no_data_found')->multi_language_value->language_value; } @endphp
                </td>
              </tr>
            @endif
           
          </tbody>
        </table>
        </div>
      </div>

    @php
      } }
    @endphp
  </div>

</div>

<?php }else{ ?> 
  <div class="container jumbotron text-center" style="margin-top: 5%;margin-bottom: 5%;">
    <p class="text-white">{{ CommonHelper::multi_language('app_titles_and_messages','no_data_found')->multi_language_value->language_value }}</p>
  </div>
<?php } ?>

@endsection

<script type="text/javascript">
  console.log("Sdf");
  $( document ).ready(function() {
    console.log( "ready!" );
});

const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);

app.get('/', (req, res) => {
  res.send('<h1>Hello world</h1>');
});

server.listen(3000, () => {
  console.log('listening on *:3000');
});


</script>