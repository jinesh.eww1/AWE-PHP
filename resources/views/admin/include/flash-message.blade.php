@if (session('status'))
<div class="alert alert-success alert-block" style="margin-top:14px;">
    <button type="button" class="close" data-dismiss="alert">×</button>    
    <strong>{{ session('status') }}</strong>
</div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success alert-block" style="margin-top:14px;">
    <button type="button" class="close" data-dismiss="alert">×</button>    
    <strong>{{ $message }}</strong>
</div>
@endif
  
@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>    
    <strong>{{ $message }}</strong>
</div>
@endif
   
@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>    
    <strong>{{ $message }}</strong>
</div>
@endif
   
@if ($message = Session::get('info'))
<div class="alert alert-info alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>    
    <strong>{{ $message }}</strong>
</div>
@endif


@if (count($errors) > 0)
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
          @foreach($errors->all() as $error)
          {{ $error }} <br>
          @endforeach 
</div>
@endif

<!-- 
@if ($errors->any())
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>    
    Your email or password is incorrect.
</div>
@endif -->
