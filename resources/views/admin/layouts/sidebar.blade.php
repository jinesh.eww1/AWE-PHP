<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li>
                    <a href="{{route('admin.dashboard')}}">
                        <i class="fe-airplay"></i>
                        <span> Dashboard </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('admin.support')}}">
                        <i class="fa fa-comments" aria-hidden="true"></i>
                        <span> Support Chat </span>
                    </a>
                </li>
                
                <li>
                    <a href="{{route('admin.users.index')}}">
                        <i class="fas fa-users"></i>
                        <span> User Management </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('admin.genre.index')}}">
                        <i class="fas fa-th"></i>
                        <span> Genre Management </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('admin.cast.index')}}">
                        <i class="fas fa-vote-yea"></i>
                        <span> Cast Type Management </span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.artist.index')}}">
                        <i class="fas fa-user-tie"></i>
                        <span> Artist Management </span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.tags.index')}}">
                        <i class="fas fa-tags"></i>
                        <span> Tags Management </span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.ratings.index')}}">
                        <i class="fab fa-adn"></i>
                        <span> Age Restriction </span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-dollar-sign"></i> <span>Plan</span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{route('admin.plan.index')}}" class="waves-effect"><span>Management</span></a>
                        </li>
                        <li>
                            <a href="{{route('admin.currency.index')}}" class="waves-effect"><span>Currency</span></a>
                        </li>
                        <li>
                            <a href="{{route('admin.country.index')}}" class="waves-effect"><span>Country</span></a>
                        </li>
                        <li>
                            <a href="{{route('admin.attribute.index')}}" class="waves-effect"><span>Features</span></a>
                        </li>
                        <li>
                            <a href="{{route('admin.price.index')}}" class="waves-effect"><span>Prices</span></a>
                        </li>
                    </ul>
                </li>

                 <li>
                    <a href="{{route('admin.payment_history.index')}}">
                        <i class="fas fa-dollar-sign"></i>
                        <span> Payment History </span>
                    </a>
                </li>
                
                <li>
                    <a href="{{route('admin.content.index')}}">
                        <i class="fas fa-closed-captioning"></i>
                        <span> Content Management </span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.season.index')}}">
                        <i class="fas fa-file-signature"></i>
                        <span> Season Management </span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.video.index')}}">
                        <i class="fas fa-film"></i>
                        <span> Video Management </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('admin.faq.index')}}">
                        <i class="fas fa-hands-helping"></i>
                        <span> FAQ Management </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('admin.feedback.index')}}">
                        <i class="fas fa-comment-dots"></i>
                        <span> Feedback Management </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('admin.ask-to-us.index')}}">
                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                        <span> Ask to us</span>
                    </a>
                </li>

                <li>
                    <a href="{{route('admin.multilanguage.index')}}">
                        <i class="fas fa-comment-dots"></i>
                        <span> Multi Language Management </span>
                    </a>
                </li>

                <?php /*<li>
                    <a href="{{route('admin.contactus.index')}}">
                        <i class="far fa-address-book"></i>
                        <!-- <span class="badge badge-success badge-pill float-right">4</span> -->
                        <span>Contact Us </span>
                    </a>
                </li>*/ ?>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-mail-bulk"></i> <span>{{ __('Send Mail') }}</span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{route('admin.mail.customer')}}" class="waves-effect"><span>{{__('Customer')}}</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-bell"></i> <span>{{ __('Send Notification') }}</span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{route('admin.notification.customer')}}" class="waves-effect"><span>{{__('Customer')}}</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-clipboard"></i> <span>{{ __('CMS') }}</span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{route('admin.terms_conditions')}}" class="waves-effect"><span>{{__('Terms Conditions')}}</span></a>
                        </li> 
                        <li>
                            <a href="{{route('admin.loyalty_reward_terms_condition')}}" class="waves-effect"><span>{{__('Loyalty Reward Terms Conditions')}}</span></a>
                        </li>
                        <li>
                            <a href="{{route('admin.privacy_policy')}}" class="waves-effect"><span>{{__('Privacy Policy')}}</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-settings"></i> <span>{{ __('Configuration') }}</span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{route('admin.system.config')}}" class="waves-effect"><span>{{__('System')}}</span></a>
                        </li>
                        <li>
                            <a href="{{route('admin.app.config')}}" class="waves-effect"><span>{{__('Application')}}</span></a>
                        </li>
                    </ul>
                </li>
               
                
            </ul>
        </div>
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
<!-- Left Sidebar End
