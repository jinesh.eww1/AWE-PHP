@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminviewasktous')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">User Name</th>
                                    <td colspan="5">
                                        @if(!empty($asktous->user->id))
                                        <a href="{{ route('admin.user.show', $asktous->user->id) }}" class="mr-2">{{ $asktous->user->first_name." ".$asktous->user->last_name }}</a>
                                        @endif
                                    </td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Question</th>
                                    <td colspan="5">{{ $asktous->question }}</td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>	
		</div>
	</div>
</div>
@endsection