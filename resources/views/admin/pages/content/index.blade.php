@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admincontent')}}
                </div>
                <h4 class="page-title">{{$dateTableTitle}}</h4>
            </div>
            <div class="btn-group float-right mt-2 mb-2">
                <a href="{{ url('/sample.xlsx')}}"  class="btn btn-sm btn-secondary waves-effect waves-light mr-2">
                    <span class="btn-label">
                        <i class="fa fa-download"></i>
                    </span>
                    Sample
                </a>
                <a href="{{$addUrl}}"  class="btn btn-sm btn-secondary waves-effect waves-light mr-2">
                    <span class="btn-label">
                        <i class="fa fa-plus"></i>
                    </span>
                    Add
                </a>
                <a type="button" target="_blank" href="{{ route('admin.contents.export',['type'=>'excel']) }}"
                    class="btn btn-sm btn-secondary waves-effect waves-light mr-2">XLSX Export</a>
                <a type="button" target="_blank" href="{{ route('admin.contents.export',['type'=>'csv']) }}"
                    class="btn btn-sm btn-secondary waves-effect waves-light mr-2">CSV Export</a>
                <a type="button" target="_blank" href="{{ route('admin.contents.export',['type'=>'pdf']) }}"
                    class="btn btn-sm btn-secondary waves-effect waves-light mr-2">PDF Export</a>
            </div>
		</div>
	</div>
    @include('admin.include.flash-message')
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >

          <div class="row">
              <div class="col-md-6">
                <form class="form-inline mb-2" method="post" action="{{route('admin.content.index')}}" id="filter-form">
                    <div class="form-group">
                        <select class="form-control select2" name="tag_id" id="orders">
                            <option value="">All Tags</option>
                            @if(!empty($tags) && count($tags)>0)
                            @foreach($tags as $key => $value)
                              <option value="{{$value->id}}">{{$value->name}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect waves-light ml-1" ><i class="fa fa-search"></i></button>
                </form>
              </div>

              <div class="col-md-6">
                <form class="form-inline mb-2" method="post" action="{{route('admin.import')}}" enctype="multipart/form-data">
                  @csrf
                    <div class="form-group col-12">
                        <input type="file" name="content_import" class="form-control" required data-parsley-errors-container="#content_import_error">
                        <button type="submit" class="btn btn-primary waves-effect waves-light ml-1" ><i class="fa fa-upload"></i></button>
                    </div>
                    <div id="content_import_error" class="d-block col-12">
                    </div>
                </form>


                
                  @error('content_import')
                      <div class="error">{{ $message }}</div>
                  @enderror

              </div>

              <div class="col-md-12">
                <hr>
              </div>

          </div>
          

					@include('admin.include.table')
				</div>
			</div>	
		</div>
	</div>
</div>

<script type="text/javascript">
	
function adv_content_change(e)
{
  var site_url = location.origin;
  var id = $(e).data('id');
  var token = $(e).data('token');
  //var url = $(e).data('url');
  var method = $(e).data('method');
  
  Notiflix.Confirm.Show(
    'Confirm',
    'Are you sure that you want to change this record?',
    'Yes',
    'No',
    function(){
      $('#loader').show();
      $.ajax({
        url: site_url + '/admin/'+method,
        type: 'get',
        dataType: "JSON",
        data:{ "id":id },
        success: function (returnData) {

          if (returnData.status == 'error') {
            Notiflix.Notify.Failure(returnData.message);
          }else{
            Notiflix.Notify.Success('Sucess');   
          }

          $('#loader').hide();
          $('#{{$dataTableId}}').DataTable().draw();
        }
      }); 
  },$('#{{$dataTableId}}').DataTable().draw());
}

</script>

@endsection
@section('script')
@include('admin.include.table_script')
@endsection		