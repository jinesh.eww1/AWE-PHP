@extends('admin.layouts.master')
@section('content')
<link rel="stylesheet" href="https://releases.transloadit.com/uppy/v2.13.0/uppy.min.css">
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminaddcontent')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
        </div>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                
                <form action="{{ route('admin.content.store') }}" method="POST" enctype="multipart/form-data">

                @csrf
                @method('POST')

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name_en">Name English<span class="text-danger">*</span></label>
                                <input type="text" name="name_en" parsley-trigger="change" value="{{old('name_en')}}" required placeholder="Enter Content English Name" class="form-control" id="name_en">
                                @error('name_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name_md">Name Mandarin<span class="text-danger">*</span></label>
                                <input type="text" name="name_md" parsley-trigger="change" value="{{old('name_md')}}" required placeholder="Enter Content Mandarin Name" class="form-control" id="name_md">
                                @error('name_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name_hi">Name Hindi<span class="text-danger">*</span></label>
                                <input type="text" name="name_hi" parsley-trigger="change" value="{{old('name_hi')}}" required placeholder="Enter Content Hindi Name" class="form-control" id="name_hi">
                                @error('name_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name_sp">Name Spanish<span class="text-danger">*</span></label>
                                <input type="text" name="name_sp" parsley-trigger="change" value="{{old('name_sp')}}" required placeholder="Enter Content Spanish Name" class="form-control" id="name_sp">
                                @error('name_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name_fr">Name French<span class="text-danger">*</span></label>
                                <input type="text" name="name_fr" parsley-trigger="change" value="{{old('name_fr')}}" required placeholder="Enter Content French Name" class="form-control" id="name_fr">
                                @error('name_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Age Restriction<span class="text-danger">*</span></label>
                                <select class="form-control select2" name="age_rating_id" required  data-parsley-errors-container="#age_rating_id_error">
                                    <option disabled selected>Select Age Restriction</option>
                                    @foreach ($age_rating as $value)
                                        <option value="{{ $value['id'] }}"> {{ $value['name'] }} </option>
                                    @endforeach
                                </select>
                                <div id="age_rating_id_error"></div>
                                @error('age_rating_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Year<span class="text-danger">*</span></label>
                                <input type="text" name="year" required  class="form-control" id="content_year" placeholder="Select Year" readonly />
                                @error('year')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="synopsis_en">Synopsis English<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="synopsis_en" id="content_synopsis" required placeholder="Enter Content Synopsis English" >{{old('synopsis_en')}}</textarea>

                                @error('synopsis_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="synopsis_md">Synopsis Mandarin<span class="text-danger">*</span></label>
                                
                                <textarea class="form-control" name="synopsis_md" id="content_synopsis" required placeholder="Enter Content Synopsis Mandarin" >{{old('synopsis_md')}}</textarea>

                                @error('synopsis_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="synopsis_hi">Synopsis Hindi<span class="text-danger">*</span></label>
                                
                                <textarea class="form-control" name="synopsis_hi" id="content_synopsis" required placeholder="Enter Content Synopsis Hindi" >{{old('synopsis_hi')}}</textarea>

                                @error('synopsis_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="synopsis_sp">Synopsis Spanish<span class="text-danger">*</span></label>
                                
                                <textarea class="form-control" name="synopsis_sp" id="content_synopsis" required placeholder="Enter Content Synopsis Spanish" >{{old('synopsis_sp')}}</textarea>

                                @error('synopsis_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="synopsis_fr">Synopsis French<span class="text-danger">*</span></label>
                                
                                <textarea class="form-control" name="synopsis_fr" id="content_synopsis" required placeholder="Enter Content Synopsis French" >{{old('synopsis_fr')}}</textarea>

                                @error('synopsis_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Artists<span class="text-danger">*</span></label>
                                <select class="form-control select2 artist_ids" name="artist_ids[]" required data-parsley-errors-container="#artist_ids_error" multiple>
                                    <option disabled >Select Artists</option>
                                    @foreach ($artist as $key => $value)
                                        <option value="{{ $value->id }}">{{ $value['name']." (".$value['castType']->name.")" }}</option>
                                    @endforeach
                                </select>
                                <div id="artist_ids_error"></div>
                                @error('artist_ids')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Categories<span class="text-danger">*</span></label>
                                <select class="form-control select2 genre_ids" name="genre_ids[]" required data-parsley-errors-container="#genre_ids_error" multiple>
                                    <option disabled >Select Categories</option>
                                    @foreach ($genre as $key => $value)
                                        <option value="{{ $value->id }}">{{ $value['name'] }}</option>
                                    @endforeach
                                </select>
                                <div id="genre_ids_error"></div>
                                @error('genre_ids')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Tags<span class="text-danger">*</span></label>
                                <select class="form-control select2 tag_ids" name="tag_ids[]" required data-parsley-errors-container="#tag_ids_error" multiple>
                                    <option disabled >Select Tags</option>
                                    @foreach ($tag as $key => $value)
                                        <option value="{{ $value->id }}">{{ $value['name'] }}</option>
                                    @endforeach
                                </select>
                                <div id="tag_ids_error"></div>
                                @error('genre_ids')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Content Type<span class="text-danger">*</span></label>
                                <select class="form-control select2" name="content_type" required data-parsley-errors-container="#content_type_error">
                                    <option disabled selected>Select Content Type</option>
                                    @foreach ($content_type as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                                <div id="content_type_error"></div>
                                @error('content_type')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Is free?</label>
                                <label class="switch">
                                  <input type="checkbox" name="is_free" class="form-control">
                                  <span class="slider round"></span>
                                </label>
                                @error('status')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        @php
                            $default = url(asset('images/logo.png'));
                        @endphp
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Poster<span class="text-danger">*</span> <small>(717 x 857)</small></label>
                                <input type="file" required name="poster" class="form-control" id="content_poster" data-parsley-trigger="change" data-parsley-max-file-size="5" data-parsley-filemimetypes="image/jpeg, image/png" accept="image/*" data-parsley-file-mime-types-message="Only allowed jpeg & png files" onchange="readURL1(this);">
                                @error('poster')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="col-lg-6">
                            <div class="form-group">
                                    <img class="border rounded p-0"  src="" onerror="this.src='{{$default}}'" alt="your image" style="height: 110px;width: 81px; object-fit: contain;" id="blah1"/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Detail Screen Poster<span class="text-danger">*</span> <small>(960 x 570)</small></label>
                                <input type="file" required name="detail_poster" class="form-control" data-parsley-trigger="change"  data-parsley-max-file-size="5" data-parsley-filemimetypes="image/jpeg, image/png" accept="image/*" data-parsley-file-mime-types-message="Only allowed jpeg & png files" onchange="readURL2(this);">
                                @error('detail_poster')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="col-lg-6">
                            <div class="form-group">
                                    <img class="border rounded p-0"  src="" onerror="this.src='{{$default}}'" alt="your image" style="height: 119px;width: 142px; object-fit: contain;" id="blah2"/>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="browseFile">Trailer <small>( mp4 file only )</small></label>

                                <div class="grid">
                                  <div class="column-full">
                                    <div class="UppyInput"></div>
                                    <div class="UppyInput-Progress"></div>
                                  </div>
                                </div>
                                @error('video_path')
                                    <div class="error">{{ $message }}</div>
                                @enderror

                                <div class="uploaded-files px-1">
                                </div>

                                <input type="hidden" name="video_name" id="video_name">
                                <input type="hidden" name="video_path" id="video_path">
                                <input type="hidden" name="video_full_link" id="video_full_link">
                            </div>
                        </div>

                    </div>
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" id="content_submit_btn" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.content.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                    </div>

                </form>

				</div>
			</div>	
		</div>
	</div>
</div>

<script src="https://releases.transloadit.com/uppy/v2.13.0/uppy.min.js" type="module"></script>
<script src="https://releases.transloadit.com/uppy/v2.13.0/uppy.legacy.min.js" nomodule></script>
<script src="https://releases.transloadit.com/uppy/locales/v2.1.1/en_US.min.js"></script>

<script>
window.addEventListener('DOMContentLoaded', function () {
  'use strict';
  var uppy = new Uppy.Core({
    debug: true,
    autoProceed: true,
    restrictions:{
        maxNumberOfFiles: 1,
        allowedFileTypes : ['video/mp4', 'video/*']
    },
    onBeforeFileAdded:(currentFile, files) => {
        const modifiedFile = {
        ...currentFile,
        name:  Date.now()
      }
      return modifiedFile+'.mp4'
    },
  });

  uppy.use(Uppy.FileInput, { target: '.UppyInput', pretty: false })
  .use(Uppy.StatusBar, {
    target: '.UppyInput-Progress',
    hideUploadButton: true,
    hideAfterFinish: false,
  });

  uppy.use(Uppy.AwsS3Multipart, {
      limit: 4,
      companionUrl: 'https://africanworldentertainment.com/'
    })

  //uppy.use(Uppy.Tus, { endpoint: 'https://tusd.tusdemo.net/files/' });
  uppy.on('upload-success', function (file, response) {
    console.log("response", response);
    console.log("file", file);
    var url = response.uploadURL;
    var fileName = file.name;
    console.log("get aws name key", file.s3Multipart.key);
    $('#video_name').val(fileName);
    $('#video_path').val(file.s3Multipart.key);
    $('#video_full_link').val(url);
    document.querySelector('.uploaded-files').innerHTML += '<a href="' + url + '" target="_blank"><b>' + fileName + '</b> </a>video uploaded on AWS s3 bucket!';
  });

  uppy.on('upload-error', (file, error, response) => {
      console.log('error with file:', file.id)
      console.log('error message:', error)
    })

  uppy.on('restriction-failed', (file, error) => {
        console.log('restriction error with file:', file.id)
      console.log('restriction error message:', error)
})

  window.uppy = uppy;
});
</script>

@endsection
