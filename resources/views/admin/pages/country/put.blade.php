@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admineditcountry')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6">
			<div class="card">
				<div class="card-body" >
                <form action="{{ route('admin.country.update',$country->id) }}" method="POST">
                @csrf
                @method('PUT')

                    <div class="row">

                        <div class="col-12">
                            <div class="form-group">
                                <label for="country_name">Country Name<span class="text-danger">*</span></label>
                                <input type="text" name="country_name" parsley-trigger="change" value="{{$country['country_name']}}" required placeholder="Enter Country Name" class="form-control" id="country_name">
                                @error('country_name')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="currency_id">Currency<span class="text-danger">*</span></label>
                                <select class="form-control select2" name="currency_id" required>
                                    <option disabled selected>Select Currency</option>
                                    @foreach ($currency as $value)
                                        <?php
                                        $option_val = $value['code'];
                                        if($value['stripe_supported'] == 1){
                                            $option_val .= " (stripe)";
                                        }
                                        if($value['paypal_supported'] == 1){
                                            $option_val .= " (paypal)";
                                        }
                                        ?>
                                        <option value="{{ $value['id'] }}" {{ ( $value["id"] == $country->currency_id) ? 'selected' : '' }}> {{ $option_val }} </option>
                                    @endforeach
                                </select>
                                @error('currency_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="dialing_code">Dialing Code<span class="text-danger">*</span></label>
                                <input type="text" name="dialing_code" parsley-trigger="change" value="{{$country['dialing_code']}}" required placeholder="Enter Dialing Code Ex: +1" class="form-control" id="dialing_code">
                                @error('dialing_code')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.country.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                    </div>

                </form>

				</div>
			</div>	
		</div>
	</div>
</div>
@endsection