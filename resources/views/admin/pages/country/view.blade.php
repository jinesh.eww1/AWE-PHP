@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminviewcurrency')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                            <tr>
                                <th class="text-nowrap" scope="row">Country Name</th>
                                <td>{{$country['country_name']}}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Currency</th>
                                <td>{{$country->currency->code}} - {{($country->currency->stripe_supported == 1)?'Stripe ':''}}  {{($country->currency->paypal_supported == 1)?'& Paypal ':''}} {{ "supported"}}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Dialing Code</th>
                                <td>{{$country->dialing_code}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>	
		</div>
	</div>
</div>
@endsection