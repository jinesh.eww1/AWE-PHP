@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admineditcurrency')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6">
			<div class="card">
				<div class="card-body" >
                <form action="{{ route('admin.currency.update',$currency->id) }}" method="POST">
                @csrf
                @method('PUT')

                    <div class="row">
                        
                        <div class="col-12">
                            <div class="form-group">
                                <label for="code">Code<span class="text-danger">*</span></label>
                                <input type="text" name="code" parsley-trigger="change" value="{{$currency->code}}" required placeholder="Enter code" class="form-control" id="code">
                                @error('code')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.currency.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                    </div>

                </form>

				</div>
			</div>	
		</div>
	</div>
</div>
@endsection