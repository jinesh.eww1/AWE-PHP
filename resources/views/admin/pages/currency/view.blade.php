@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminviewcurrency')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                            <tr>
                                <th class="text-nowrap" scope="row">Code</th>
                                <td>{{$currency['code']}}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Stripe Support</th>
                                <td>{{($currency['stripe_supported'] == 1)?"Yes":"No"}}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Paypal Support</th>
                                <td>{{($currency['paypal_supported'] == 1)?"Yes":"No"}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>	
		</div>
	</div>
</div>
@endsection