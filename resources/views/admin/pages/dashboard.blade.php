@extends('admin.layouts.master')
@section('css')
<link href="{{url('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

                    <!-- Start Content-->
    <div class="container-fluid">
        
        <!-- start page title -->
        @include('admin.include.flash-message')
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 
        
        <div class="row">
            
            <div class="col-md-6 col-xl-3" title="Earnings">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle  border-danger border">
                                <i class="fe-dollar-sign font-22 avatar-title text-danger"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$earning}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Earnings</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->
            <div class="col-md-6 col-xl-3" title="Customers">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle border-primary border">
                                <i class="fas fa-users font-22 avatar-title text-primary"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$users}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Customers</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3" title="Contents">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle border-success border">
                                <i class="fe-film font-22 avatar-title text-success"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$content}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Contents</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3" title="Videos">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle border-warning border">
                                <i class="fe-film font-22 avatar-title text-warning"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$video}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Videos</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3" title="Plans">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle border-danger border">
                                <i class="fe-package font-22 avatar-title text-danger"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$plan}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Plans</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3" title="Subscribers">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle  border-primary border">
                                <i class="fe-shopping-cart font-22 avatar-title text-primary"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$total_sub_count}}</span>(<span data-plugin="counterup">{{$total_sub}}</span>%)</h3>
                                <p class="text-muted mb-1 text-truncate">Subscribers</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3" title="Non-Subscribers">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle  border-success border">
                                <i class="fe-shopping-cart font-22 avatar-title text-success"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$total_non_sub_count}}</span>(<span data-plugin="counterup">{{$total_non_sub}}</span>%)</h3>
                                <p class="text-muted mb-1 text-truncate">Non-Subscribers</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

        </div>


    <div class="row">
        
        <div class="col-md-12">
            <div class="card-box text-center">
                <div class="page-title-box text-left">
                    <h4 class="page-title">10 Popular Contents</h4>
                </div>
                
                <table class="table">
                <thead>
                  <tr>
                    <th class="text-left">Name</th>
                    <th class="text-left">Views</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if(!empty($most_viewed) && count($most_viewed)>0){ 
                        foreach ($most_viewed as $key => $value) {
                            if(!empty($value['content'])) { ?>
                        <tr>
                          <td class="text-left"><a href="{{route('admin.content.show',$value['content']['id'])}}">{{ $value['content']['name'] }}</a></td>
                          <td class="text-left"><span class="badge badge-primary">{{ $value['total_content'] }}</span></td>
                        </tr>
                    <?php } } }else{ ?> 
                        <tr>
                          <td class="text-center"><strong>No data found.</strong></td>
                        </tr>
                    <?php } ?>
                    
                </tbody>
              </table>      
            </div>
        </div>
    </div>

    <div class="row">  
        <div class="col-md-12">

            <div class="card">
              <div class="card-body">
                <div id="google-pie-chart"></div>
              </div>
            </div>
     
        </div>

    </div>
    
    <div class="row">  
        <div class="col-md-12">

            <div id="google-line-chart"></div>
     
        </div>

    </div>

    



    

    </div> <!-- container -->


<script type="text/javascript" src="{{ URL::asset('assets/js/loader.js') }}"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Month Name', 'Registered Users'],

                @php 
                foreach($lineChart as $d) {
                    echo "['".$d['month_name']."', ".$d['count']."],";
                }
                @endphp
        ]);

        var options = {
            title: 'Registered Users in last 12 Months',
            height: 500,
            hAxis: {
              title: 'Months'
            },
            vAxis: {
              title: 'Users Count'
            },
            curveType: 'function',
            legend: { position: 'bottom' },
            colors: ['#D5B115'],
        };

        
        var chart = new google.visualization.LineChart(document.getElementById('google-line-chart'));

        chart.draw(data, options);


        var piedata = google.visualization.arrayToDataTable([
            ['Month Name', 'Registered User Count'],
 
                @php
                foreach($pie_chart as $key => $d) {
                    echo "['".$key."', ".$d."],";
                }
                @endphp
        ]);
 
          var pieoptions = {
            title: 'Devices',
            height: 350,
            is3D: false,
          };
 
          var piechart = new google.visualization.PieChart(document.getElementById('google-pie-chart'));
 
          piechart.draw(piedata, pieoptions);
        }
    </script>

@endsection

