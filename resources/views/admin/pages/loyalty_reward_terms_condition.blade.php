@extends('website.layouts.master')
@section('content')
<style type="text/css">
  .bg-body {
    background: linear-gradient(rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.89)) !important;
  } 
  .top-padding{
  	padding-top: 70px !important;
    padding-bottom: 50px !important;
  }
</style>
	<section class="top-padding text-white">
		<div class="container">
			<div class="row mx-2">
				<div class="col-md-12">
					<?php 
						echo !empty($loyalty_reward_terms_condition)?$loyalty_reward_terms_condition:'';
					?>
				</div>
			</div>
		</div>
	</section>
@endsection
