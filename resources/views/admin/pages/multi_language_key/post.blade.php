@extends('admin.layouts.master')
@section('content')

<style type="text/css">
    .autocomplete {
      position: relative;
    }
    .autocomplete-items {
      position: absolute;
      border: 1px solid #d4d4d4;
      border-bottom: none;
      border-top: none;
      z-index: 99;
      /*position the autocomplete items to be the same width as the container:*/
      top: 100%;
      left: 0;
      right: 0;
    }

    .autocomplete-items div {
      padding: 10px;
      cursor: pointer;
      background-color: #fff; 
      border-bottom: 1px solid #d4d4d4; 
    }

    /*when hovering an item:*/
    .autocomplete-items div:hover {
      background-color: #e9e9e9; 
    }

    /*when navigating through the items using the arrow keys:*/
    .autocomplete-active {
      background-color: DodgerBlue !important; 
      color: #ffffff; 
    }

</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('addmultilanguage')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body" >
                <form action="{{ route('admin.multilanguage.store') }}" method="POST">
                @csrf
                @method('POST')

                    <div class="row">
                          
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="language_key">Language Key<span class="text-danger">*</span></label>
                                <input type="text" name="language_key" parsley-trigger="change" value="{{old('language_key')}}" required placeholder="Enter Language Key" class="form-control" id="language_key">
                                @error('language_key')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group autocomplete">
                                <label for="group">Group</label>
                                <input type="text" name="group" parsley-trigger="change" value="{{old('group')}}" placeholder="Enter Group" class="form-control" id="group">
                                <input type="hidden" name="group_array" id="group_array" value="{{ implode(',',$group) }}">
                                @error('group')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="language_value_en">Language Value English<span class="text-danger">*</span></label>
                                <input type="text" name="language_value_en" required parsley-trigger="change" value="{{old('language_value_en')}}" placeholder="Enter Language English Value" class="form-control" id="language_value_en">
                                @error('language_value_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="language_value_md">Language Value Mandarin<span class="text-danger">*</span></label>
                                <input type="text" name="language_value_md" required parsley-trigger="change" value="{{old('language_value_md')}}" placeholder="Enter Language Mandarin Value" class="form-control" id="language_value_md">
                                @error('language_value_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="language_value_hi">Language Value Hindi<span class="text-danger">*</span></label>
                                <input type="text" name="language_value_hi" required parsley-trigger="change" value="{{old('language_value_hi')}}" placeholder="Enter Language Hindi Value" class="form-control" id="language_value_hi">
                                @error('language_value_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="language_value_sp">Language Value Spanish<span class="text-danger">*</span></label>
                                <input type="text" name="language_value_sp" required parsley-trigger="change" value="{{old('language_value_sp')}}" placeholder="Enter Language Spanish Value" class="form-control" id="language_value_sp">
                                @error('language_value_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="language_value_fr">Language Value French<span class="text-danger">*</span></label>
                                <input type="text" name="language_value_fr" required parsley-trigger="change" value="{{old('language_value_fr')}}" placeholder="Enter Language French Value" class="form-control" id="language_value_fr">
                                @error('language_value_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.multilanguage.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>

                    </div>

                </form>

                </div>
            </div>  
        </div>
    </div>
</div>


       
@endsection