@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('viewmultilanguage')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>

    @php
    $name = $multilanguage->multi_language_value->getTranslations('language_value');
    @endphp

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th class="text-nowrap" scope="row">Language Key</th>
                                    <td colspan="5">{{$multilanguage->language_key}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Group</th>
                                    <td colspan="5">{{$multilanguage->group}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Language Value English</th>
                                    <td colspan="5">{{!empty($name['en'])?$name['en']:''}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Language Value Mandarin</th>
                                    <td colspan="5">{{!empty($name['md'])?$name['md']:''}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Language Value Hindi</th>
                                    <td colspan="5">{{!empty($name['hi'])?$name['hi']:''}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Language Value Spanish</th>
                                    <td colspan="5">{{!empty($name['sp'])?$name['sp']:''}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Language Value French</th>
                                    <td colspan="5">{{!empty($name['fr'])?$name['fr']:''}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>	
		</div>
	</div>
</div>
@endsection