@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminviewpayment_history')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th class="text-nowrap" scope="row">User</th>
                                    <td colspan="5">
                                        @if($payment_history['users'])
                                            <a href="{{ route('admin.user.show', $payment_history['users']['id']) }}" class="mr-2">
                                                {{ $payment_history['users']['first_name']." ".$payment_history['users']['last_name'] }}
                                            </a>
                                        @else
                                            {{ "-" }}
                                        @endif
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Mode</th>
                                    <td colspan="5">{{ ucfirst($payment_history['mode']) }}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Plan Name</th>
                                    <td colspan="5"><a href="{{ route('admin.plan.show',$payment_history['plan']['id']) }}" class="mr-2"> {{ ucfirst($payment_history['plan']['name']) }} </a></td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Type</th>
                                    <td colspan="5"><?php if($payment_history['type'] == 1){
                                        echo "Monthly";
                                    }elseif($payment_history['type'] == 2){
                                        echo "Yearly";
                                    }else{
                                        echo '';
                                    } ?> </td>  
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Transaction Id</th>
                                    <td colspan="5">{{ !empty($payment_history['transaction_id'])?$payment_history['transaction_id']:'' }} </a></td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Price</th>
                                    <td colspan="5">$@php 
                                    if($payment_history['type'] == 1){
                                        echo $payment_history['plan']['monthly_price'];
                                    }elseif($payment_history['type'] == 2){
                                        echo $payment_history['plan']['yearly_price'];
                                    }else{
                                        echo '';
                                    } @endphp </td>  
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Transaction DateTime</th>
                                    <td colspan="5">{{ date('d F Y h:i A',strtotime($payment_history['created_at'])) }}</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
			</div>	
		</div>
	</div>
</div>
@endsection