@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admineditplan')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                <form action="{{ route('admin.plan.update',$plan->id) }}" method="POST"  enctype="multipart/form-data">
                @csrf
                @method('PUT')
                @php
                $name = $plan->getTranslations('name');
                $description = $plan->getTranslations('description');
                @endphp

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name_en">Name English</label>
                                <input type="text" name="name_en" readonly value="{{$name['en']}}" class="form-control" placeholder="Enter Plan English Name" id="name_en">
                                @error('name_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                         <div class="col-6">
                            <div class="form-group">
                                <label for="description_en">Description English<span class="text-danger">*</span></label>
                                <textarea name="description_en" parsley-trigger="change" required placeholder="Enter Plan English Description" class="form-control" id="description_en">{{!empty($description['en'])?$description['en']:''}}</textarea>
                                @error('description_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-6">
                            <div class="form-group">
                                <label for="name_md">Name Mandarin</label>
                                <input type="text" name="name_md" readonly value="{{$name['md']}}"  placeholder="Enter Plan Mandarin Name" class="form-control" id="name_md">
                                @error('name_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="description_md">Description Mandarin<span class="text-danger">*</span></label>
                                <textarea name="description_md" parsley-trigger="change" required placeholder="Enter Plan Mandarin Description" class="form-control" id="description_md">{{!empty($description['md'])?$description['md']:''}}</textarea>
                                @error('description_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="name_hi">Name Hindi</label>
                                <input type="text" name="name_hi" readonly value="{{$name['hi']}}"  placeholder="Enter Plan Hindi Name" class="form-control" id="name_hi">
                                @error('name_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-6">
                            <div class="form-group">
                                <label for="description_hi">Description Hindi<span class="text-danger">*</span></label>
                                <textarea name="description_hi" parsley-trigger="change" required placeholder="Enter Plan Hindi Name" class="form-control" id="description_hi">{{!empty($description['hi'])?$description['hi']:''}}</textarea>
                                @error('description_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-6">
                            <div class="form-group">
                                <label for="name_sp">Name Spanish</label>
                                <input type="text" name="name_sp" readonly value="{{$name['sp']}}"  placeholder="Enter Plan Spanish Name" class="form-control" id="name_sp">
                                @error('name_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="description_sp">Description Spanish<span class="text-danger">*</span></label>
                                <textarea name="description_sp" parsley-trigger="change" required placeholder="Enter Plan Spanish Description" class="form-control" id="description_sp">{{!empty($description['sp'])?$description['sp']:''}}</textarea>
                                @error('description_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="name_fr">Name French</label>
                                <input type="text" name="name_fr" readonly value="{{$name['fr']}}"  placeholder="Enter Plan French Name" class="form-control" id="name_fr">
                                @error('name_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="col-6">
                            <div class="form-group">
                                <label for="description_fr">Description French<span class="text-danger">*</span></label>
                                <textarea name="description_fr" parsley-trigger="change" required placeholder="Enter Plan French Description" class="form-control" id="description_fr">{{!empty($description['fr'])?$description['fr']:''}}</textarea>
                                @error('description_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="price">Monthly Price</label>
                                <input type="text" readonly name="monthly_price" value="{{$plan->monthly_price}}" class="form-control"> 
                                @error('price')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="price">Yearly Price</label>
                                <input type="text" readonly name="yearly_price" value="{{$plan->yearly_price}}" class="form-control"> 
                                @error('price')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.plan.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                    </div>

                </form>

				</div>
			</div>	
		</div>
	</div>
</div>
@endsection