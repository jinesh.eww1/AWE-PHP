@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminaddprice')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
        </div>
	</div>
	<div class="row">
		<div class="col-xl-6">
			<div class="card">
				<div class="card-body" >
                <form action="{{ route('admin.price.store') }}" method="POST">
                @csrf
                @method('POST')


                    <div class="row">

                        <div class="col-12">
                            <div class="form-group">
                                <label for="plan_id">Plan<span class="text-danger">*</span></label>
                                <select class="form-control select2" name="plan_id" required data-parsley-errors-container="#plan_id_error">
                                    <option disabled selected>Select Plan</option>
                                    @foreach ($plan as $value)
                                    @php
                                    $name = $value->getTranslations('name');
                                    @endphp
                                        <option value="{{ $value->id }}" > {{ $name['en'] }} </option>
                                    @endforeach
                                </select>
                                <div id="plan_id_error"></div>
                                @error('plan_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="country_id">Country<span class="text-danger">*</span></label>
                                <select class="form-control select2" name="country_id" id="country_id" required data-parsley-errors-container="#country_id_error">
                                    <option disabled selected>Select Country</option>
                                    @foreach ($country as $value)
                                        <option value="{{ $value->id }}" data-currency="{{$value->currency->symbol}}"> {{ $value->country_name." | ".$value->currency->code." | " }} {{ ($value->currency->stripe_supported == 1)?"Stripe":""}} {{ ($value->currency->paypal_supported == 1)?" | PayPal":""}}</option>
                                    @endforeach
                                </select>
                                <div id="country_id_error"></div>
                                @error('country_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-12">
                            <div class="form-group">
                                <label for="monthly_price">Monthly Price<span class="text-danger">*</span> <small>( 30 Days )</small></label>
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                  </div>
                                  <input type="number" class="form-control" name="monthly_price" aria-label="Amount (to the nearest dollar)" required step="0.01" data-parsley-type="number" min="1" data-parsley-min="1" id="monthly_price" data-parsley-errors-container="#monthly_price_error" placeholder="Ex. 2.99">
                                </div>
                                <div id="monthly_price_error"></div>
                                @error('monthly_price')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="yearly_price">Yearly Price<span class="text-danger">*</span> <small>( 365 Days )</small></label>
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                  </div>
                                  <input type="number" class="form-control" name="yearly_price" aria-label="Amount (to the nearest dollar)" required step="0.01" data-parsley-type="number" min="1" data-parsley-min="1" id="yearly_price" data-parsley-gt="#monthly_price" data-parsley-errors-container="#yearly_price_error" placeholder="Ex. 20.99">
                                </div>
                                <div id="yearly_price_error"></div>
                                @error('yearly_price')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.price.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                    </div>

                </form>

				</div>
			</div>	
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        $("#country_id").change(function() {
            var currency = $('option:selected', this).attr('data-currency');
            if(currency == ''){
                currency = "$";
            }
            $('.input-group-text').html(currency);
        });
    });
</script>
@endsection 