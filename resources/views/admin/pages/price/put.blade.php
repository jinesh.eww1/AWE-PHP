@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admineditprice')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6">
			<div class="card">
				<div class="card-body" >
                <form action="{{ route('admin.price.update',$price->id) }}" method="POST">
                @csrf
                @method('PUT')

                    <div class="row">

                        <div class="col-12">
                            <div class="form-group">
                                <label for="plan_id">Plan</label>
                                <select class="form-control select2" name="plan_id" disabled>
                                    <option disabled selected>{{ $price->plan->name }}</option>
                                </select>
                            </div>
                        </div>


                        <div class="col-12">
                            <div class="form-group">
                                <label for="country_id">Country</label>
                                <select class="form-control select2" name="country_id" disabled>
                                    <option disabled selected>{{ $price->country->country_name }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="monthly_price">Monthly Price<span class="text-danger">*</span> <small>( 30 Days )</small></label>
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">{{ $price->country->currency->symbol }}</span>
                                  </div>
                                  <input type="number" class="form-control" name="monthly_price" aria-label="Amount (to the nearest dollar)" required step="0.01" data-parsley-type="number" min="1" data-parsley-min="1" id="monthly_price" data-parsley-errors-container="#monthly_price_error" placeholder="Ex. 2.99" value="{{$price->monthly_price}}">
                                </div>
                                <div id="monthly_price_error"></div>
                                @error('monthly_price')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="yearly_price">Yearly Price<span class="text-danger">*</span> <small>( 365 Days )</small></label>
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">{{ $price->country->currency->symbol }}</span>
                                  </div>
                                  <input type="number" class="form-control" name="yearly_price" aria-label="Amount (to the nearest dollar)" required step="0.01" data-parsley-type="number" min="1" data-parsley-min="1" id="yearly_price" data-parsley-gt="#monthly_price" data-parsley-errors-container="#yearly_price_error" placeholder="Ex. 20.99" value="{{$price->yearly_price}}">
                                </div>
                                <div id="yearly_price_error"></div>
                                @error('yearly_price')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.price.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                    </div>

                </form>

				</div>
			</div>	
		</div>
	</div>
</div>
@endsection