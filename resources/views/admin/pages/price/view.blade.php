@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminviewprice')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                            <tr>
                                <th class="text-nowrap" scope="row">Plan</th>
                                <td>{{$price->plan->name}}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Country</th>
                                <td>{{$price->country->country_name ." | ". $price->country->currency->code}} - {{ ($price->country->currency->stripe_supported == 1)?"Stripe":""}} {{ ($price->country->currency->paypal_supported == 1)?" | PayPal":""}}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Monthly Price <small>( 30 Days )</small></th>
                                <td>{{ $price->country->currency->symbol." ".$price->monthly_price}}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Yearly Price <small>( 365 Days )</small></th>
                                <td>{{ $price->country->currency->symbol." ".$price->yearly_price}}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Stripe Monthly Id</th>
                                <td>{{ $price->stripe_monthly_id }}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Stripe Yearly Id</th>
                                <td>{{ $price->stripe_yearly_id }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>	
		</div>
	</div>
</div>
@endsection