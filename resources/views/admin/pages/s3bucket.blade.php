@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">S3 Bucket</h4>
            </div>
		</div>
	</div>
    @include('admin.include.flash-message')
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
					<div id="fm" style="height: 600px;"></div>
				</div>
			</div>	
		</div>
	</div>
</div>


@endsection
@section('script')
@endsection