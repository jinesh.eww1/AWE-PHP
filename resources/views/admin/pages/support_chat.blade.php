@extends('admin.layouts.master')
@section('content')

<link href="{{ URL::asset('assets/css/chat.css')}}" rel="stylesheet" type="text/css" />

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">Support Chat</h4>
            </div>
		</div>
	</div>
    @include('admin.include.flash-message')
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
						<div class="container">
							<div class="messaging">
						      <div class="inbox_msg">
						        <div class="inbox_people">
						          
						          <div class="inbox_chat">
											@php 


											$default = '/images/default.png'; $readonly = 'readonly';
												if(!empty($recent_chat) && count($recent_chat)>0){
													$readonly = '';


													foreach ($recent_chat as $key => $value) {


														if($value->sender_id != 1){
															$profile_image = $value->sender_user->profile_picture_full_url;

															$name = $value->sender_user->first_name.' '.$value->sender_user->last_name;
															$user_id = $value->sender_id;
														}else{
															$profile_image = $value->receiver_user->profile_picture_full_url;
														
															$name = $value->receiver_user->first_name.' '.$value->receiver_user->last_name;
															$user_id = $value->receiver_id;

														}

														if($key == 0){
																if(!isset($_GET['user'])){
																	$selected_user = $user_id;
																}else{
																	$selected_user = $_GET['user'];
																}
														}
														
														$redirect_url = url('/admin/support?user='.$user_id);
												@endphp

						            <div class="chat_list <?php if($selected_user == $user_id){ echo "active_chat"; } ?>" data-id='{{$user_id}}' id="chat_{{$user_id}}" onclick="window.location = '{{$redirect_url}}';" >
						              <div class="chat_people">
						                <!--<div class="chat_img"> 
						                	<img src="{{ $profile_image }}" onerror="this.src='{{$default}}'" alt="{{ $name }}">
						                </div> -->
						                <div class="chat_ib">
						                  <h5>{{ $name }} <span class="text-danger new-msg-dot" style="display:none;">●</span> <span class="chat_date">{{ $value->created_date_time }}</span></h5>
						                  <p class="latest_message_{{$user_id}}">{{ strlen($value->message) > 50 ? substr($value->message,0,50)."..." : $value->message }}</p>
						                </div>
						              </div>
						            </div>
						          @php  } } @endphp
						          </div>
						        </div>
						        <div class="mesgs">
						          <div class="msg_history">
						           	@if(!empty($chat_history))
						           		@foreach ( $chat_history as $key => $value )
										   			@if($value->sender_id != 1)	
										   				@php $user_id = $value->sender_id; @endphp 
												   		<div class="incoming_msg">
									              <!--<div class="incoming_msg_img"> <img src="{{$value->sender_user->profile_picture_full_url}}" alt="{{$value->sender_user->first_name.' '.$value->sender_user->last_name}}" onerror="this.src='{{$default}}'"> </div> -->
									              <div class="received_msg">
									                <div class="received_withd_msg">
									                  <p>{{$value->message}}</p>
									                  <span class="time_date">{{ $value->created_date_time }}</span></div>
									              </div>
									            </div>
									          @else
									          @php $user_id = $value->receiver_id; @endphp 
									            <div class="outgoing_msg">
									              <div class="sent_msg">
									                <p>{{$value->message}}</p>
									                <span class="time_date">{{ $value->created_date_time }}</span> </div>
									            </div>
														@endif
												  @endforeach
						           	@endif

						          </div>
						          <div class="type_msg">
						            <div class="input_msg_write">
						              <input type="text" class="write_msg" {{$readonly}} placeholder="Type a message" />
						              <input type="hidden" class="user_id" id="user_id_{{!empty($user_id)?$user_id:''}}" value="{{ !empty($user_id)?$user_id:'' }}" />
						              <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
						            </div>
						          </div>
						        </div>
						      </div>
						      
							</div>
						</div>
				</div>
			</div>	
		</div>
	</div>
</div>


<script src="https://cdn.socket.io/4.5.0/socket.io.min.js" integrity="sha384-7EyYLQZgWBi67fBtVxw60/OWl1kjsfrPFcaU0pp0nAh+i8FD068QogUvg85Ewy1k" crossorigin="anonymous"></script>

<script type="text/javascript">

	var socket = io("https://awemovies.com:3000/");
	var selected_user = `<?php echo (isset($selected_user))?$selected_user:0; ?>`;
	var BASEURL = `<?php echo url('/admin/support'); ?>`;
	console.log(selected_user);	

	socket.on("connect", function() {
    
    socket.emit('connect_user', { user_id: 1});
    console.log("Admin connetcted");
});


$(document).ready(function() {

	$('.msg_history').scrollTop($('.msg_history')[0].scrollHeight);

	socket.on('receiver_message', function(data) {
    console.log("receiver_message");
    console.log(data);


  	if(data.send_by == 'me'){

    		var html = '<div class="outgoing_msg"><div class="sent_msg"><p>'+ data.message +'</p><span class="time_date">'+data.created_date_time+'</span> </div></div>';
				$('.msg_history').append(html);
				$('.write_msg').val('');
				
				if(data.message.length > 50){ data.message.substring(0,50)+"..."; }else{ data.message }

				$('.latest_message_'+data.receiver_id).empty();
				$('.latest_message_'+data.receiver_id).html((data.message.length > 50)?data.message.substring(0,50)+"...":data.message);


    }else{

    	if(data.sender_id == selected_user){
    		var html = '<div class="incoming_msg"><div class="received_msg"><div class="received_withd_msg"><p>'+data.message+'</p><span class="time_date">'+data.created_date_time+'</span></div></div></div>';
    		$('.msg_history').append(html);
    	}else{

    		if($("#chat_" + data.sender_id).length == 0) {
    			//window.location = 'https://africanworldentertainment.com/admin/support?user=3';
    			var new_chat = 
							'<div class="chat_list" data-id='+data.sender_id+' id="chat_'+data.sender_id+'" onclick="window.location=`'+BASEURL+'?user='+data.sender_id+'`;" >'+
	              '<div class="chat_people">'+
	                '<div class="chat_ib">'+
	                  '<h5>User '+data.receiver_user.first_name+'<span class="text-danger new-msg-dot" >●</span> <span class="chat_date">'+data.created_date_time+'</span></h5>'+
	                  '<p class="latest_message_'+data.sender_id+'">'+data.message.substring(0,50);+'</p>'+
	                '</div>'+
	              '</div>'+
	            '</div>';	
				  $( ".chat_list.active_chat" ).after( new_chat );
				}else{
					$("#chat_" + data.sender_id + " .new-msg-dot").show();
					$('.latest_message_'+data.sender_id).html((data.message.length > 50)?data.message.substring(0,50)+"...":data.message);
				}
    	}

    }
    $('.msg_history').scrollTop($('.msg_history')[0].scrollHeight);

  });

	var callback = function() {
		var message_val = $.trim($('.write_msg').val());
		var user_id = $('.user_id').val();

		console.log(message_val);

		if(message_val != '' && user_id !=''){

			user_id = parseInt(user_id);

			socket.emit('send_message', { sender_id: 1, receiver_id:user_id, message: message_val});

			
		

		}else{
			$('.write_msg').val('');
		}
	};


	$("input").keypress(function() {
	    if (event.which == 13) callback();
	});

	$('.msg_send_btn').click(callback);

});

function chat_history(id)
{	
		if(id == $('.user_id').val()){
			return false;
		}
		
    $('#loader').show();
    $.ajax({
          url: location.origin+'/admin/get_chat_history_data',
          type: "GET",
          dataType: "JSON",
          data:{
            "id":id
          },
          success: function (data) {
            var result = JSON.parse(JSON.stringify(data.data))

            var html = ''; var user_id = '';
            var default_img ="/images/default.png";

					    $.each(result, function (key, val) {
					      if(val['sender_id'] != 1){
					      	user_id = val['sender_id'];
					      	var created_date = val['created_date_time'];

					      	html += '<div class="incoming_msg"><div class="incoming_msg_img"> <img src="'+val['sender_user']['profile_picture_full_url']+'" alt="'+val['sender_user']['first_name']+' '+val['sender_user']['last_name']+'" onerror="this.src='+default_img+'" style="display: inline-block;width: 50px;height: 50px;"> </div><div class="received_msg"><div class="received_withd_msg"><p>'+val['message']+'</p><span class="time_date">'+created_date+'</span></div></div></div>';
					      }else{
					      	user_id = val['receiver_id'];

					      	var created_date = val['created_date_time'];

									html += '<div class="outgoing_msg"><div class="sent_msg"><p>'+val['message']+'</p><span class="time_date">'+created_date+'</span> </div></div>';
					      }
					    });
						
					    //console.log(html);
						$('.msg_history').empty();
						$('.msg_history').html(html);
						$('.user_id').val(user_id);
						$('.chat_list').removeClass('active_chat');
						$('#chat_'+user_id).addClass('active_chat');
						$('.msg_history').scrollTop($('.msg_history')[0].scrollHeight);

						$('#loader').hide();
          }
      });         
}



</script>
@endsection
@section('script')
@endsection