@extends('website.layouts.master')
@section('content')
<style type="text/css">
  .bg-body {
    background: linear-gradient(rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.89)) !important;
  } 
  .top-padding{
  	padding-top: 70px !important;
    padding-bottom: 50px !important;
  }
</style>
	<section class="top-padding text-white">
		<div class="container">
			<div class="mb-4 mx-2"><img src="{{ asset('assets/website/image/awe-home.png') }}" class="img-fluid box-radius" height="100px" width="100px"></div>
		<div class="row mx-2">
				<div class="col-md-12">
					
					<?php 
						echo !empty($terms_conditions)?$terms_conditions:'';
					?>
				</div>
			</div>
		</div>
	</section>	

@endsection
