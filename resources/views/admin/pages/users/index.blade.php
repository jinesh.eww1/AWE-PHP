@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					{{ Breadcrumbs::render('users') }}
					
				</div>
				<h4 class="page-title">{{$dateTableTitle}}</h4>
			</div>
			<div class="btn-group float-right mt-2 mb-2">
				<a type="button" target="_blank" href="{{ route('admin.users.export',['type'=>'excel']) }}"
					class="btn btn-sm btn-secondary waves-effect waves-light mr-2">XLSX Export</a>
				<a type="button" target="_blank" href="{{ route('admin.users.export',['type'=>'csv']) }}"
					class="btn btn-sm btn-secondary waves-effect waves-light mr-2">CSV Export</a>
				<a type="button" target="_blank" href="{{ route('admin.users.export',['type'=>'pdf']) }}"
					class="btn btn-sm btn-secondary waves-effect waves-light mr-2">PDF Export</a>
			</div>
		</div>
	</div>
	@include('admin.include.flash-message')
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
				<form class="form-inline mb-2" method="post" action="{{route('admin.users.index')}}" id="filter-form">
                        <div class="form-group">
                            <select class="form-control select2" name="status" id="orders">
                                <option value="">All</option>
                                <option value="1">Active</option>
                                <option value="2">Inactive</option>
                            </select>
                        </div>
                        
                        <button type="submit" class="btn btn-primary waves-effect waves-light ml-1" ><i class="fa fa-search"></i></button>
                    </form>
					@include('admin.include.table')
				</div>
			</div>	
		</div>
	</div>
</div>

@endsection
@section('script')
@include('admin.include.table_script')
@endsection		