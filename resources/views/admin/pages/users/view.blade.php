@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('viewuser')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
        </div>
        
        @if($user['parent_id'] == 0)
        <div class="col-12 text-right mb-1">
            <a href="{{ route('admin.user.edit',$user['id']) }}" class="btn btn-secondary btn-sm waves-effect m-l-5">Edit</a>
        </div>
        @endif

    </div>
    @php
        $default = '/images/default.png';
    @endphp
    <div class="row">

        <div class="col-lg-4 col-xl-4">
            <div class="card-box text-center">
                <img src="{{$user['profile_picture_full_url'] }}" onerror="this.src='{{$default}}'"  class="rounded-circle avatar-lg img-thumbnail"
                    alt="profile-image">

                <div class="text-left mt-3">

                    <p class="text-muted mb-2 font-13"><strong>First Name :</strong> <span class="ml-2">{{$user['first_name']}}</span></p>
                    <p class="text-muted mb-2 font-13"><strong>Last Name :</strong> <span class="ml-2">{{!empty($user['last_name'])?$user['last_name']:''}}</span></p>
                    <p class="text-muted mb-2 font-13"><strong>Phone :</strong><span class="ml-2">{{ !empty($user['phone_formatted'])?$user['phone_formatted']:'' }}</span></p>
                    <p class="text-muted mb-2 font-13"><strong>Email :</strong> <span class="ml-2">{{$user['email']}}</span></p>
                    <p class="text-muted mb-2 font-13"><strong>Referral Code :</strong> <span class="ml-2">{{ !empty($user['reference_code'])?$user['reference_code']:'' }}</span></p>
                     
                        
                    <p class="text-muted mb-2 font-13"><strong>Used Referral Code :</strong> <span class="ml-2">{{ !empty($user['used_referral_code'])?$user['used_referral_code']:'' }}</span>

                        @if(!empty($user['used_refferal']))
                         <a target="_blank" href="{{route('admin.user.show', $user['used_refferal']['id'])}}" >( {{$user['used_refferal']['first_name']}} {{!empty($user['used_refferal']['last_name'])?$user['used_refferal']['last_name']:''}} )</a>
                        @endif
                    </p>

                    <?php if($user['parent_id']==0) { ?>
                        <p class="text-muted mb-2 font-13"><strong>User Type :</strong> <span class="ml-2"> Parent </span></p>
                    <?php }else{  ?> 
                        <p class="text-muted mb-2 font-13"><strong>User Type :</strong> <span class="ml-2"> Sub User </span></p>
                    <?php } ?>

                    @if($user['parent_id'] == 0)
                    <p class="text-muted mb-2 font-13"><strong>Stripe Id :</strong> <span class="ml-2">{{ !empty($user['stripe_id'])?$user['stripe_id']:'' }} </span></p>
                    @endif
                    
                    <?php if($user['is_child']==1) { ?>
                        <p class="text-muted mb-2 font-13"><strong>Is Child? :</strong> <span class="ml-2"> Yes </span></p>
                    <?php }else{ ?>
                        <p class="text-muted mb-2 font-13"><strong>Is Child? :</strong> <span class="ml-2"> No </span></p>
                    <?php } ?>
                    
                    <?php if($user['parent_id']!=0) { ?>
                        <p class="text-muted mb-2 font-13"><strong>Parent User :</strong> <span class="ml-2"> 
                            <a href="{{route('admin.user.show', $user['parent']['id'])}}" >{{!empty($user['parent']['first_name'])?$user['parent']['first_name']:''}} {{!empty($user['parent']['last_name'])?$user['parent']['last_name']:''}} </a> </span></p>
                    <?php } ?>

                    <p class="text-muted mb-2 font-13"><strong>Status :</strong> <span class="ml-2">
                        <?php if($user['status']==0){ echo "Pending"; }elseif($user['status']==1){echo "Active"; }elseif ($user['status'] == 2 && !is_null($user['deactivated_time'])){ echo "Restore"; }else{ echo "Inactive"; }?></span></p>

                    <p class="text-muted mb-2 font-13"><strong>Language :</strong> <span class="ml-2"> {{ $user['language']['type'] }} </span></p>

                </div>
            </div> <!-- end card-box -->
        </div> <!-- end col-->


        @if($user['parent_id'] == 0)
        <div class="col-md-8">
            <div class="card-box">
                <div class="page-title-box text-left">
                    <h4 class="page-title">Payment History</h4>
                </div>
                
                <table class="table">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Mode</th>
                    <th scope="col">Plan Name</th>
                    <th scope="col">Type</th>
                    <th scope="col">Price</th>
                    <th scope="col">Transaction DateTime</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if(!empty($user['paymenthistory']) && count($user['paymenthistory'])>0){
                        foreach ($user['paymenthistory'] as $key => $value) { ?>
                        <tr>
                          <td scope="row">{{ $value['id'] }}</td>
                          <td scope="row">{{ ucfirst($value['mode']) }}</td>
                          <td scope="row"><?php echo '<a href="'.route('admin.plan.show',$value['plan']['id']).'" class="mr-2">'.ucfirst($value['plan']['name']['en']).'</a>'; ?></td>

                          <td scope="row"><?php 
                                if($value['type'] == 1){
                                echo "Monthly";
                                }elseif($value['type'] == 2){
                                echo "Yearly";
                                }else{
                                echo '';
                                }
                          ?></td>
                          <td scope="row">
                                @if($value['type'] == 1)
                                {{ config('app.currency').$value['plan']['monthly_price'] }}
                                @elseif($value['type'] == 2)
                                {{ config('app.currency').$value['plan']['yearly_price'] }}
                                @endif
                            </td>
                          <td scope="row">{{ date('d F Y h:i A',strtotime($value['created_at'])) }}</td>
                          <td scope="row"><a title="View" href="{{ route('admin.payment_history.show',$value['id']) }}" class="mr-2"><i class="fa fa-eye"></i></a></td>
                        </tr>
                    <?php } } else{ ?> 
                        <tr>
                          <td colspan="7" class="text-center"><strong>No data found.</strong></td>
                        </tr>
                    <?php } ?>
                    
                </tbody>
              </table>      
            </div>
        </div>
        @endif

    </div>

    <?php  if(!empty($user['subuser']) && count($user['subuser'])>0){ ?>
    <div class="row page-title-box col-lg-12">
        <h4 class="page-title ">Sub User</h4>
    </div>
    <?php } ?>

    <div class="row">
            <?php  if(!empty($user['subuser']) && count($user['subuser'])>0){
                foreach ($user['subuser'] as $key => $value) { ?>
                    <div class="col-lg-3 col-xl-3">
                        <div class="card-box text-center">
                            <img src="{{$value['profile_picture_full_url'] }}" onerror="this.src='{{$default}}'"  class="rounded-circle avatar-lg img-thumbnail"
                                alt="profile-image">

                            <h4 class="mb-0"><a target="_blank" href="{{route('admin.user.show', $value['id'])}}" >
                                @if($value['is_child'] != 0)
                                    <i class="fa fa-child" aria-hidden="true"></i>
                                @endif
                            {{ $value['first_name']}} {{$value['last_name'] }}</a></h4>

                        </div> <!-- end card-box -->
                    </div> <!-- end col-->

        <?php } } ?>
    </div>

    <div class="row">

        <?php if($user['parent_id']==0) { ?>
        <div class="col-md-12">
            <div class="card-box">
                <div class="page-title-box text-left">
                    <h4 class="page-title">Devices</h4>
                </div>
                
                <table class="table">
                <thead>
                  <tr>
                    <th scope="col">UDID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Location</th>
                    <th scope="col">Address</th>
                    <th scope="col">DateTime</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if(!empty($devices) && count($devices)>0){
                        foreach ($devices as $key => $value) { ?>
                        <tr>
                          <td scope="row">{{ $value->udid }}</td>
                          <td scope="row">{{ $value->name }}</td>
                          <td scope="row">{{ $value->city }}, {{ $value->state }}, {{ $value->country }}</td>
                          <td scope="row" class="text-center">
                            @if(!empty($value["latitude"]) && !empty($value["longitude"]))
                                <a class="text-muted mb-2 font-13" target="_blank" href='https://maps.google.com/?q={{$value["latitude"]}},{{$value["longitude"]}}'><i class="fa fa-map-marker" aria-hidden="true"></i> </a> 
                            @endif
                          </td>
                          <td scope="row">{{ date('d F Y h:i A',strtotime($value->created_at)) }}</td>
                        </tr>
                    <?php } } else{ ?> 
                        <tr>
                          <td colspan="5" class="text-center"><strong>No data found.</strong></td>
                        </tr>
                    <?php } ?>
                    
                </tbody>
              </table>      
            </div>
        </div>
        <?php } ?>

        
        <div class="col-md-12">
            <div class="card-box">
                <div class="page-title-box text-left">
                    <h4 class="page-title">Viewing Activity</h4>
                </div>

                <table class="table">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Content Name</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if(!empty($user['activity_log']) && count($user['activity_log'])>0){
                        $user['activity_log'] = array_reverse($user['activity_log']); 
                        foreach ($user['activity_log'] as $key => $value) { ?>
                        <tr>
                          <td>{{ date('m-d-Y H:i A',strtotime($value['created_at'])) }}</td>
                          <td><a target="_blank" href="{{route('admin.content.show',$value['content']['id'])}}">{{ $value['content']['name']['en'] }}</a>
                            <?php  if($value['content']['content_type'] == 2){ echo ' - <a href="'.route('admin.video.show',$value['content']['video']['id']).'">'.$value['content']['video']['episode_name']['en'].'</a>'; } ?>
                          </td>
                        </tr>
                    <?php } } else{ ?> 
                        <tr>
                          <td colspan="2" class="text-center"><strong>No data found.</strong></td>
                        </tr>
                    <?php } ?>
                    
                </tbody>
              </table>      
            </div>
        </div>

        <div class="col-md-12">
            <div class="card-box ">
                <div class="page-title-box text-left">
                    <h4 class="page-title">My List</h4>
                </div>
                
                <table class="table">
                <thead>
                  <tr>
                    <th>Content Name</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if(!empty($user['mylist']) && count($user['mylist'])>0){ 
                        $user['mylist'] = array_reverse($user['mylist']);
                        foreach ($user['mylist'] as $key => $value) { ?>
                        <tr>
                          <td class="text-left"><a target="_blank" href="{{route('admin.content.show',$value['content']['id'])}}">{{ $value['content']['name']['en'] }}</a></td>
                        </tr>
                    <?php } }else{ ?> 
                        <tr>
                          <td class="text-center"><strong>No data found.</strong></td>
                        </tr>
                    <?php } ?>
                    
                </tbody>
              </table>      
            </div>
        </div>
        
        <div class="col-md-12">
            @php 
            $user_age_restricted = array();
            if(!empty($user['useragerestriction']) && count($user['useragerestriction'])>0){
                foreach ($user['useragerestriction'] as $key => $value) {
                    $user_age_restricted[$key] = "<a target='_blank' href='".route('admin.ratings.show',$value['ageratings']['id'])."'>".$value['ageratings']['name']['en']."</a>";
                }
            }
            @endphp

            <div class="card-box ">
                <div class="page-title-box text-left">
                    <h4 class="page-title">User Age Restricted</h4>
                </div>
                
                <p class="text-muted mb-2 font-13"><span class="ml-2">@php echo implode(", ",$user_age_restricted); @endphp</span></p>
            </div>
        </div>

        <div class="col-md-12">
            @php 
            $user_age_restricted_content = array();
            if(!empty($user['userrestrictedcontent']) && count($user['userrestrictedcontent'])>0){
                foreach ($user['userrestrictedcontent'] as $key => $value) {
                    $user_age_restricted_content[$key] = "<a target='_blank' href='".route('admin.content.show',$value['content']['id'])."'>".$value['content']['name']['en']."</a>";
                }
            }
            @endphp

            <div class="card-box ">
                <div class="page-title-box text-left">
                    <h4 class="page-title">User Age Restricted Content</h4>
                </div>
                
                <p class="text-muted mb-2 font-13"><span class="ml-2">@php echo implode(", ",$user_age_restricted_content); @endphp</span></p>
            </div>
        </div>

        <div class="col-md-12">
            @php 
            $user_preferred_category = array();
            if(!empty($user['usergenres']) && count($user['usergenres'])>0){
                foreach ($user['usergenres'] as $key => $value) {
                    $user_preferred_category[$key] = "<a target='_blank' href='".route('admin.genre.show',$value['genre_id'])."'>".$value['genre']['name']['en']."</a>";
                }
            }
            @endphp

            <div class="card-box ">
                <div class="page-title-box text-left">
                    <h4 class="page-title">User Preferred Category</h4>
                </div>
                
                <p class="text-muted mb-2 font-13"><span class="ml-2">@php echo implode(", ",$user_preferred_category); @endphp</span></p>
            </div>
        </div>


    </div>


</div>

@endsection
@section('script')

@endsection 