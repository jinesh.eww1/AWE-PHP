@extends('admin.layouts.master')
@section('content')

<link rel="stylesheet" href="https://releases.transloadit.com/uppy/v2.13.0/uppy.min.css">

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminaddvideo')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
        </div>
	</div>
	<div class="row">
        <?php
        //dd($errors->all());
        ?>
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                
                <form method="POST" action={{route('admin.video.store')}} enctype="multipart/form-data" id="submit">
                <input type="hidden" name="content_type" id="content_type" value="">
                @csrf
                @method('POST')

                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Content<span class="text-danger">*</span></label>
                                <select class="form-control select2" onchange="sessionval(this)" name="content_id" required id="video_content_id" data-parsley-errors-container="#video_content_id_error">
                                    <option disabled selected>Select Content</option>
                                    @foreach ($content_data as $value)
                                        <option value="{{ $value['id'] }}"> {{ $value['name'] }} </option>
                                    @endforeach
                                </select>
                                <div class="error" id="video_content_id_error"></div>
                                @error('content_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" id="video_session_label">Season</label>
                                <select class="form-control select2" name="season_id" id="video_session_id" data-parsley-errors-container="#video_session_id_error">
                                    <option disabled selected>Select Season</option>
                                </select>
                                <div class="error" id="video_session_id_error"></div>
                                @error('season_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="tv_show_display_section">

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_name_en">Video Name English<span class="text-danger">*</span></label>
                                    <input type="text" name="episode_name_en" required placeholder="Enter video name english" class="form-control" id="episode_name_en">
                                    @error('episode_name_en')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_synopsis_en">Video Synopsis English<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="episode_synopsis_en" placeholder="Enter video synopsis english" id="episode_synopsis_en" required></textarea>
                                    @error('episode_synopsis_en')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_name_md">Video Name Mandarin<span class="text-danger">*</span></label>
                                    <input type="text" name="episode_name_md" required placeholder="Enter video name mandarin" class="form-control" id="episode_name_md">
                                    @error('episode_name_md')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_synopsis_md">Video Synopsis Mandarin<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="episode_synopsis_md" placeholder="Enter video synopsis mandarin" id="episode_synopsis_md" required></textarea>
                                    @error('episode_synopsis_md')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_name_hi">Video Name Hindi<span class="text-danger">*</span></label>
                                    <input type="text" name="episode_name_hi" required placeholder="Enter video name hindi" class="form-control" id="episode_name_hi">
                                    @error('episode_name_hi')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_synopsis_hi">Video Synopsis Hindi<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="episode_synopsis_hi" id="episode_synopsis_hi" placeholder="Enter video synopsis hindi"  required></textarea>
                                    @error('episode_synopsis_hi')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_name_sp">Video Name Spanish<span class="text-danger">*</span></label>
                                    <input type="text" name="episode_name_sp" required placeholder="Enter video name spanish" class="form-control" id="episode_name_sp">
                                    @error('episode_name_sp')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_synopsis_sp">Video Synopsis Spanish<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="episode_synopsis_sp" id="episode_synopsis_sp" placeholder="Enter video synopsis spanish" required></textarea>
                                    @error('episode_synopsis_sp')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_name_fr">Video Name French<span class="text-danger">*</span></label>
                                    <input type="text" name="episode_name_fr" required placeholder="Enter video name french" class="form-control" id="episode_name_fr">
                                    @error('episode_name_fr')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_synopsis_fr">Video Synopsis French<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="episode_synopsis_fr" id="episode_synopsis_fr" placeholder="Enter video synopsis french" required></textarea>
                                    @error('episode_synopsis_fr')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subtitle_en">Subtitle English <small>( vtt file only )</small></label>
                                <input type="file" name="subtitle_en" class="form-control" placeholder="select file" accept=".vtt">
                                @error('subtitle_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="audio_en">Audio English <small>( mp3 file only )</small></label>
                                <input type="file" name="audio_en" class="form-control" placeholder="select file" accept=".mp3">
                                @error('audio_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subtitle_md">Subtitle Mandarin <small>( vtt file only )</small></label>
                                <input type="file" name="subtitle_md" class="form-control" placeholder="select file" accept=".vtt">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="audio_md">Audio Mandarin <small>( mp3 file only )</small></label>
                                <input type="file" name="audio_md" class="form-control" placeholder="select file" accept=".mp3">
                                @error('audio_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subtitle_hi">Subtitle Hindi <small> ( vtt file only )</small></label>
                                <input type="file" name="subtitle_hi" class="form-control" placeholder="select file" accept=".vtt">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="audio_hi">Audio Hindi <small>( mp3 file only )</small></label>
                                <input type="file" name="audio_hi" class="form-control" placeholder="select file" accept=".mp3">
                                @error('audio_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subtitle_sp">Subtitle Spanish <small> ( vtt file only )</small></label>
                                <input type="file" name="subtitle_sp" class="form-control" placeholder="select file" accept=".vtt">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="audio_sp">Audio Spanish <small>( mp3 file only )</small></label>
                                <input type="file" name="audio_sp" class="form-control" placeholder="select file" accept=".mp3">
                                @error('audio_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subtitle_fr">Subtitle French<small> ( vtt file only )</small></label>
                                <input type="file" name="subtitle_fr" class="form-control" placeholder="select file" accept=".vtt">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="audio_fr">Audio French <small>( mp3 file only )</small></label>
                                <input type="file" name="audio_fr" class="form-control" placeholder="select file" accept=".mp3">
                                @error('audio_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>


                    <div class="row">

                        @php
                            $default = url(asset('images/logo.png'));
                        @endphp
                        <div class="col-md-6">
                            <div class="tv_show_display_section">
                                <div class="form-group">
                                    <label for="name">Thumbnail image<span class="text-danger">*</span></label>
                                    <input type="file" name="thumbnail_image"  class="form-control" id="thumbnail_image" required data-parsley-trigger="change"  data-parsley-max-file-size="5" data-parsley-filemimetypes="image/jpeg, image/png" accept="image/*" data-parsley-file-mime-types-message="Only allowed jpeg & png files" onchange="readURL1(this);" required>
                                    @error('thumbnail_image')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        
                        <div class="tv_show_display_section">
                            <div class="col-lg-6 ">
                                <div class="form-group">
                                    <img class="border rounded p-0"  src="" onerror="this.src='{{$default}}'" alt="your image" style="height: 119px;width: 142px; object-fit: contain;" id="blah1"/>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="browseFile">Video File<span class="text-danger">*</span><small>( mp4 file only )</small></label>

                                <div class="grid">
                                  <div class="column-full">
                                    <div class="UppyInput"></div>
                                    <div class="UppyInput-Progress"></div>
                                  </div>
                                </div>

                                <div class="uploaded-files px-1">
                                </div>

                                <input type="hidden" name="video_path" id="video_path">
                                <input type="hidden" name="video_full_link" id="video_full_link">

                                @error('video_path')
                                    <div class="error">{{ $message }}</div>
                                @enderror

                            </div>
                        </div>

                    </div>
                

                    <div class="form-group text-right m-b-0" style="padding-top: 15px;">
                        <button class="btn btn-primary waves-effect waves-light" type="submit" id="video_upload_btn">
                            Submit
                        </button> 
                        <a href="{{ route('admin.video.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a> 
                    </div>

                </form>

                </div>
            </div>  
        </div>
    </div>
</div>
<script src="https://releases.transloadit.com/uppy/v2.13.0/uppy.min.js" type="module"></script>
<script src="https://releases.transloadit.com/uppy/v2.13.0/uppy.legacy.min.js" nomodule></script>
<script src="https://releases.transloadit.com/uppy/locales/v2.1.1/en_US.min.js"></script>

<script>

window.addEventListener('DOMContentLoaded', function () {
  'use strict';
  var uppy = new Uppy.Core({
    debug: true,
    autoProceed: true,
    restrictions:{
        maxNumberOfFiles: 1,
        allowedFileTypes : ['video/mp4', 'video/*']
    },
    onBeforeFileAdded:(currentFile, files) => {
        const modifiedFile = {
        ...currentFile,
        name:  Date.now()
      }
      return modifiedFile+'.mp4'
    },
  });

  uppy.use(Uppy.FileInput, { target: '.UppyInput', pretty: false })
  .use(Uppy.StatusBar, {
    target: '.UppyInput-Progress',
    hideUploadButton: true,
    hideAfterFinish: false,
  });

  uppy.use(Uppy.AwsS3Multipart, {
      limit: 4,
      companionUrl: 'http://127.0.0.1:8000/',
    })

  //uppy.use(Uppy.Tus, { endpoint: 'https://tusd.tusdemo.net/files/' });
  uppy.on('upload-success', function (file, response) {
    console.log("response", response);
    console.log("file", file);
    var url = response.uploadURL;
    var fileName = file.name;
    console.log("get aws name key", file.s3Multipart.key);
    $('#video_path').val(file.s3Multipart.key);
    $('#video_full_link').val(url);
    document.querySelector('.uploaded-files').innerHTML += '<a href="' + url + '" target="_blank"><b>' + fileName + '</b> </a>video uploaded on AWS s3 bucket!';
  });

  uppy.on('upload-error', (file, error, response) => {
      console.log('error with file:', file.id)
      console.log('error message:', error)
    })

  uppy.on('restriction-failed', (file, error) => {
        console.log('restriction error with file:', file.id)
      console.log('restriction error message:', error)
})

  window.uppy = uppy;
});
</script>

<script type="text/javascript">

function sessionval(sel)
{
    var content_id = sel.value;
    var site_url = location.origin;
    $('#loader').show();
        $.ajax({
              url: site_url+'/admin/get_season_data',
              type: "GET",
              dataType: "JSON",
              data:{
                "id":content_id
              },
              success: function (data) {
                
                //var result = JSON.parse(JSON.stringify(data))

                $('#video_content_id_error').empty();
                $('#video_session_id').attr('disabled',false); 
                $('#video_upload_btn').attr('disabled',false); 
                $('#video_session_id').removeAttr('required');
                $('#video_session_label').html('Season');
                

                var select_option = '<option disabled selected>Select Season</option>';
                
                if(data.status == true){
                    
                    $('#content_type').val(data.data.content_type);

                    if(data.data.content_type == 1){
                        $('#video_session_id').attr('disabled',true); 
                        $('.tv_show_display_section').css('display','none');

                        $('#episode_name_en').removeAttr('required');
                        $('#episode_synopsis_en').removeAttr('required');
                        $('#episode_name_md').removeAttr('required');
                        $('#episode_synopsis_md').removeAttr('required');
                        $('#episode_name_hi').removeAttr('required');
                        $('#episode_synopsis_hi').removeAttr('required');
                        $('#episode_name_sp').removeAttr('required');
                        $('#episode_synopsis_sp').removeAttr('required');
                        $('#episode_name_fr').removeAttr('required');
                        $('#episode_synopsis_fr').removeAttr('required');
                        $('#thumbnail_image').removeAttr('required');

                    }else{
                        $.each(data.data.season, function( index, value ) {
                          select_option += '<option value="'+value.id+'">'+value.season['en']+'</option>';
                        });
                        
                        $('#video_session_id').attr('required','required');
                        $('#video_session_label').html('Season<span class="text-danger">*</span>');

                        $('.tv_show_display_section').css('display','');

                        $('#episode_name_en').attr('required','required');
                        $('#episode_synopsis_en').attr('required','required');
                        $('#episode_name_md').attr('required','required');
                        $('#episode_synopsis_md').attr('required','required');
                        $('#episode_name_hi').attr('required','required');
                        $('#episode_synopsis_hi').attr('required','required');
                        $('#episode_name_sp').attr('required','required');
                        $('#episode_synopsis_sp').attr('required','required');
                        $('#episode_name_fr').attr('required','required');
                        $('#episode_synopsis_fr').attr('required','required');
                        $('#thumbnail_image').attr('required','required');
                         
                    }

                }else{
                    if(data.message != ''){
                        $('#video_content_id_error').html(data.message);
                        $('#video_upload_btn').attr('disabled',true); 
                        $('#video_session_id').attr('disabled',true);
                        

                        $('.tv_show_display_section').css('display','none');

                        $('#episode_name_en').removeAttr('required');
                        $('#episode_synopsis_en').removeAttr('required');
                        $('#episode_name_md').removeAttr('required');
                        $('#episode_synopsis_md').removeAttr('required');
                        $('#episode_name_hi').removeAttr('required');
                        $('#episode_synopsis_hi').removeAttr('required');
                        $('#episode_name_sp').removeAttr('required');
                        $('#episode_synopsis_sp').removeAttr('required');
                        $('#episode_name_fr').removeAttr('required');
                        $('#episode_synopsis_fr').removeAttr('required');
                        $('#thumbnail_image').removeAttr('required');
                        
                    }else{
                        $('#video_content_id_error').empty();
                        $('#video_session_id').attr('disabled',false);
                        $('#video_upload_btn').attr('disabled',false);
                        
                    }
                }
               
                $('#video_session_id').empty();    
                $('#video_session_id').append(select_option);
                
                $('#loader').hide();

              }
          });  
        
}

</script>
@endsection
