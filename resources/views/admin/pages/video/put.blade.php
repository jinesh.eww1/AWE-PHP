@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admineditvideo')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                <form action="{{ route('admin.video.update',$video->id) }}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="content_type" id="content_type" value="{{ $video->content->content_type }}">
                @csrf
                @method('PUT')

                @php

                if($video->content->content_type == 2){
                    $episode_name = $video->getTranslations('episode_name');
                    $episode_synopsis = $video->getTranslations('episode_synopsis');
                }else{
                    $episode_name = '';
                    $episode_synopsis = '';
                }

                @endphp

                    <div class="row">
                        

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Content<span class="text-danger">*</span></label>
                                <select class="form-control select2" name="content_id" required id="video_content_id" disabled onchange="sessionval(this)" data-parsley-errors-container="#video_content_id_error">
                                    <option disabled selected>Select Content</option>
                                    @foreach ($content_data as $value)
                                        <option value="{{ $value['id'] }}" <?php if($value['id'] == $video->content_id){ echo "selected"; } ?> > {{ $value['name'] }} </option>
                                    @endforeach
                                </select>
                                <div id="video_content_id_error"></div>
                                @error('content_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Season @if(count($season_data)>0)<span class="text-danger">*</span>@endif</label>
                                <select class="form-control select2" @if(count($season_data)>0) required @else disabled @endif name="season_id" id="video_session_id">
                                    <option disabled selected>Select Season</option>
                                    @foreach ($season_data as $value)
                                        <option value="{{ $value['id'] }}" <?php if($value['id'] == $video->season_id){ echo "selected"; } ?> > {{ $value['season'] }} </option>
                                    @endforeach
                                </select>
                                @error('season_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="tv_show_display_section">
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_name_en">Video Name English<span class="text-danger">*</span></label>
                                    <input type="text" name="episode_name_en" required placeholder="Enter video name english" class="form-control" id="episode_name_en" value="{{ !empty($episode_name['en'])?$episode_name['en']:''  }}">
                                    @error('episode_name_en')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_synopsis_en">Video Synopsis English<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="episode_synopsis_en" placeholder="Enter video synopsis english" id="episode_synopsis_en" required> {{ !empty($episode_synopsis['en'])?$episode_synopsis['en']:''  }} </textarea>
                                    @error('episode_synopsis_en')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_name_md">Video Name Mandarin<span class="text-danger">*</span></label>
                                    <input type="text" name="episode_name_md" required placeholder="Enter video name mandarin" class="form-control" id="episode_name_md" value="{{ !empty($episode_name['md'])?$episode_name['md']:''  }}">
                                    @error('episode_name_md')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_synopsis_md">Video Synopsis Mandarin<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="episode_synopsis_md" placeholder="Enter video synopsis mandarin" id="episode_synopsis_md" required>{{ !empty($episode_synopsis['md'])?$episode_synopsis['md']:''  }} </textarea>
                                    @error('episode_synopsis_md')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_name_hi">Video Name Hindi<span class="text-danger">*</span></label>
                                    <input type="text" name="episode_name_hi" required placeholder="Enter video name hindi" class="form-control" id="episode_name_hi" value="{{ !empty($episode_name['hi'])?$episode_name['hi']:''  }}">
                                    @error('episode_name_hi')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_synopsis_hi">Video Synopsis Hindi<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="episode_synopsis_hi" id="episode_synopsis_hi" placeholder="Enter video synopsis hindi"  required>{{ !empty($episode_synopsis['hi'])?$episode_synopsis['hi']:''  }} </textarea>
                                    @error('episode_synopsis_hi')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_name_sp">Video Name Spanish<span class="text-danger">*</span></label>
                                    <input type="text" name="episode_name_sp" required placeholder="Enter video name spanish" class="form-control" id="episode_name_sp" value="{{ !empty($episode_name['sp'])?$episode_name['sp']:''  }}">
                                    @error('episode_name_sp')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_synopsis_sp">Video Synopsis Spanish<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="episode_synopsis_sp" id="episode_synopsis_sp" placeholder="Enter video synopsis spanish" required>{{ !empty($episode_synopsis['sp'])?$episode_synopsis['sp']:''  }} </textarea>
                                    @error('episode_synopsis_sp')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_name_fr">Video Name French<span class="text-danger">*</span></label>
                                    <input type="text" name="episode_name_fr" required placeholder="Enter video name french" class="form-control" id="episode_name_fr" value="{{ !empty($episode_name['fr'])?$episode_name['fr']:''  }}">
                                    @error('episode_name_fr')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="episode_synopsis_fr">Video Synopsis French<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="episode_synopsis_fr" id="episode_synopsis_fr" placeholder="Enter video synopsis french" required>{{ !empty($episode_synopsis['fr'])?$episode_synopsis['fr']:''  }} </textarea>
                                    @error('episode_synopsis_fr')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="row">
                        @php
                            $default = url(asset('images/logo.png'));
                        @endphp
                        <div class="col-md-6">
                            <div class="tv_show_display_section">
                                <div class="form-group">
                                    <label for="name">Thumbnail image<span class="text-danger">*</span></label>
                                    
                                    <input type="file" name="thumbnail_image"  class="form-control" id="thumbnail_image" data-parsley-trigger="change"  data-parsley-max-file-size="5" data-parsley-filemimetypes="image/jpeg, image/png" accept="image/*" data-parsley-file-mime-types-message="Only allowed jpeg & png files" onchange="readURL1(this);">
                                    @error('thumbnail_image')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                
                                
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <img class="border rounded p-0"  src="{{!empty($video->thumbnail_image)?env('AWS_S3_URL').'video/'.$video->thumbnail_image:''}}" onerror="this.src='{{$default}}'" alt="your image" style="height: 119px;width: 142px; object-fit: contain;" id="blah1"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-right m-b-0" style="padding-top: 15px;">
                        <button class="btn btn-primary waves-effect waves-light" id="video_upload_btn" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.video.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                    </div>  
                </form>
				</div>
			</div>	
		</div>
	</div>
</div>

<script type="text/javascript">

$(document).ready(function() {

  if( $('#content_type').val() == 1 ){

    $('.tv_show_display_section').css('display','none');

    $('#episode_name_en').removeAttr('required');
    $('#episode_synopsis_en').removeAttr('required');
    $('#episode_name_md').removeAttr('required');
    $('#episode_synopsis_md').removeAttr('required');
    $('#episode_name_hi').removeAttr('required');
    $('#episode_synopsis_hi').removeAttr('required');
    $('#episode_name_sp').removeAttr('required');
    $('#episode_synopsis_sp').removeAttr('required');
    $('#episode_name_fr').removeAttr('required');
    $('#episode_synopsis_fr').removeAttr('required');

  }else{
    
    $('.tv_show_display_section').css('display','');

    $('#episode_name_en').attr('required','required');
    $('#episode_synopsis_en').attr('required','required');
    $('#episode_name_md').attr('required','required');
    $('#episode_synopsis_md').attr('required','required');
    $('#episode_name_hi').attr('required','required');
    $('#episode_synopsis_hi').attr('required','required');
    $('#episode_name_sp').attr('required','required');
    $('#episode_synopsis_sp').attr('required','required');
    $('#episode_name_fr').attr('required','required');
    $('#episode_synopsis_fr').attr('required','required');
  }

});

function sessionval(sel)
{
    var content_id = sel.value;
    var site_url = location.origin;
    $('#loader').show();
        $.ajax({
              url: site_url+'/admin/get_season_data',
              type: "GET",
              dataType: "JSON",
              data:{
                "id":content_id
              },
              success: function (data) {
                var result = JSON.parse(JSON.stringify(data.data))

                var select_option = '<option disabled selected>Select Season</option>';
                $.each(result, function( index, value ) {
                  select_option += '<option value="'+value.id+'">'+value.season['en']+'</option>';
                });
                
                $('#video_session_id').empty();    
                $('#video_session_id').append(select_option);
                $('#loader').hide();

              }
          });         
}



var myVideos = [];

window.URL = window.URL || window.webkitURL;

document.getElementById('browseFile').onchange = setFileInfo;

function setFileInfo() {
  var files = this.files;
  myVideos.push(files[0]);
  var video = document.createElement('video');
  video.preload = 'metadata';

  video.onloadedmetadata = function() {
    window.URL.revokeObjectURL(video.src);
    var duration = video.duration;
    myVideos[myVideos.length - 1].duration = duration;
    updateInfos();
  }  

  video.src = URL.createObjectURL(files[0]);
}

</script>

<script src="{{ URL::asset('assets/js/resumable.js')}}"></script>
@endsection