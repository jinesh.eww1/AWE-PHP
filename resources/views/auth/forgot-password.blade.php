

@extends('admin.layouts.master-without-nav')

@section('body')

<body>

    @endsection

    @section('content')

    
    
    <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card">
                            <div class="card-body p-4">

                                <div class="text-center w-75 m-auto">
                                    <a href="{{ route('login') }}">
                                        <span><img src="{{asset('images/logo.png')}}" alt="" height="100"></span>
                                    </a>
                                    <p class="text-muted mb-4 mt-3">{{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }} </p>
                                </div>
                                
                                @include('admin.include.flash-message')

                            <!-- <x-auth-session-status class="text-success" :status="session('status')" /> -->

                            <!-- <x-auth-validation-errors class="text-danger" :errors="$errors" /> -->

                            <form method="POST" action="{{ route('password.email') }}">

                                @csrf

                                <div class="form-group mb-3">

                                    <label for="emailaddress">Email address</label>

                                    <input class="form-control" type="email" id="email" required name="email" autofocus placeholder="Enter your email address">

                                </div>                            

                                <div class="form-group mb-0 text-center">

                                    <button class="btn btn-primary btn-block" type="submit"> Email Password Reset Link </button>

                                </div>

                            </form>

                            </div> <!-- end card-body -->

                        </div>

                        <!-- end card -->

                            <!-- end row -->

                            </div> <!-- end col -->

                        </div>

                        <!-- end row -->

                    </div>

                    <!-- end container -->

                </div>

                <!-- end page -->

                <footer class="footer footer-alt">

                    {{date('Y')}} &copy; AWE

                </footer>

                @endsection
