@extends('website.layouts.master')
@section('content')
    <style type="text/css">
        .parsley-errors-list,
        .parsley-required,
        .parsley-equalto {
            float: left;
            text-align: left;
        }

        li {
            list-style: none;
        }

        .alert {
            position: unset !important;
        }
    </style>
    <section>
        <div class="container">
            <div class="mt-md-5 py-5">
                <a href="{{ route('forgot.password') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a>
            </div>
            <div class="text-center pt-5 mt-5">
                <div class="row justify-content-center">
                    <div class="col-md-4 text-start ms-md-5">
                        <h5 class="text-yellow mt-4 py-3"><?php if (Session::get('app_string')) {
                            echo Session::get('app_string.change_password.new_password_label');
                        } else {
                            echo CommonHelper::multi_language('change_password', 'new_password_label')->multi_language_value->language_value;
                        } ?></h5>
                        <div class="text-forgot font-20 me-md-5"><?php if (Session::get('app_string')) {
                            echo Session::get('app_string.change_password.recovery_password');
                        } else {
                            echo CommonHelper::multi_language('change_password', 'recovery_password')->multi_language_value->language_value;
                        } ?></div>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-md-4 my-3 block-movie">
                        @include('website.include.flash-message')
                    </div>
                </div>

                <?php /*<div class="row justify-content-center">
                                <div class="col-md-4 my-3 block-movie">
                                    <x-auth-validation-errors class="mb-4 text-danger" :errors="$errors"  style="::marker:none"/>
                                </div>
                            </div>*/
                ?>

                <div class="row justify-content-center">
                    <div class="col-md-4 text-start ms-md-5 text-white">
                        {{ $request->email }}
                    </div>
                </div>

                <form method="POST" action="{{ route('password.update') }}" class="password-border my-4">
                    @csrf

                    <input type="hidden" name="token" value="{{ $request->route('token') }}">
                    <input type="hidden" name="email" value="{{ $request->email }}">

                    <div class="d-flex justify-content-center my-4">
                        <div class="password-box-width position-relative">
                            <input class="form-control bg-transparent text-white font-18" data-parsley-length="[8, 15]"
                                type="password" required name="password" placeholder="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.new_password_label');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'new_password_label')->multi_language_value->language_value;
                                } ?>"
                                id="password_field"data-parsley-whitespace="trim"
                                data-parsley-required-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.password_required');
                                } else {
                                    echo CommonHelper::multi_language('login', 'password_required')->multi_language_value->language_value;
                                } ?>" 
                                data-parsley-length-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.max_15_password');
                                } else {
                                    echo CommonHelper::multi_language('login', 'max_15_password')->multi_language_value->language_value;
                                } ?>"
                                
                                >
                            <span class="hide_pass"><img src="{{ asset('assets\website\image/Icon-Hide.png') }}"
                                    class="hide-icon"></span>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center my-4">
                        <div class="password-box-width position-relative">
                            <input class="form-control bg-transparent text-white font-18" data-parsley-length="[8, 15]"data-parsley-whitespace="trim" type="password" required
                                name="password_confirmation" id="password_confirmation" placeholder="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.confirm_password_label');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'confirm_password_label')->multi_language_value->language_value;
                                } ?>"
                                data-parsley-equalto="#password_field"
                                data-parsley-required-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.register.c_password_required');
                                } else {
                                    echo CommonHelper::multi_language('register', 'c_password_required')->multi_language_value->language_value;
                                } ?>" 
                                data-parsley-length-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.max_15_password');
                                } else {
                                    echo CommonHelper::multi_language('login', 'max_15_password')->multi_language_value->language_value;
                                } ?>"
                                data-parsley-equalto-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.new_password_confirm_password_same');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'new_password_confirm_password_same')->multi_language_value->language_value;
                                } ?>"
                                >
                            <span class="con_hide_pass"><img src="{{ asset('assets\website\image/Icon-Hide.png') }}"
                                    class="hide-icon"></span>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center my-5">
                        <button type="submit"
                            class="btn bg-color p-3 fw-bold text-white font-23 box-radius password-box-width"><?php if (Session::get('app_string')) {
                                echo Session::get('app_string.account_profile.save');
                            } else {
                                echo CommonHelper::multi_language('account_profile', 'save')->multi_language_value->language_value;
                            } ?></button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
