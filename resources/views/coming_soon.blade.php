<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Coming Soon</title>
		<link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	</head>
	<body>
		<div class="d-flex" style="height: 100vh;">
		    <h5 class="p-2 align-self-center w-100 text-center">Website Coming Soon!</h5>
		  </div>
	</body>
</html>