<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>{{config('app.name')}}</title>
	<link rel="icon" href="{{ URL::asset('images/favicon.ico')}}" type="image/gif" sizes="16x13">
	<link rel="stylesheet" href="{{ URL::asset('assets/front/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{ URL::asset('assets/front/css/style.css')}}">
	<link rel="stylesheet" href="{{ URL::asset('assets/front/css/responsive.css')}}">
	<link rel="stylesheet" href="{{ URL::asset('assets/front/css/slick.css')}}">
</head>
<body style="background-color: black;">
	<section class="top-padding">
		<div class="container text-white">
			<div class="row mx-2">
				<div class="col-md-12">
					<div class="text-center pb-lg-5">
						<h1 class="text-white font-weight-bold border-bottom pb-3 mb-5 d-inline-block"> About Us</h1>
					</div>					

					<p>AWE Movies is a movie streaming service  provided by African World Entertainment Inc. We are registered in the State of New Jersey, in the United States. Our platform provides a great opportunity for people all over the world to learn about and enjoy African contemporary lifestyle as presented in our movies and shows. Members are able to download movies to iOS, android and iPAD when there is internet service and watch them later when there is no service. Great tool for people to erase boring periods and make every moment of their life count because life should be exciting.</p>

					<p>
						We hope that our platform will help our members learn about life beyond the Atlantic Ocean even when they have never visited before or visited in the remote past. We hope to help some members reconnect and most importantly we hope to inspire and empower members with knowledge of the African World.
					</p>

					<p>
						Welcome to African World Entertainment Inc.
					</p>

				</div>
			</div>
		</div>
	</section>
</body>
</html>
<script src="{{ URL::asset('assets/front/js/jquery.min.js')}}"></script>
<script src="{{ URL::asset('assets/front/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('assets/front/js/popper.min.js')}}"></script>
<script src="{{ URL::asset('assets/front/js/slick.min.js')}}"></script>