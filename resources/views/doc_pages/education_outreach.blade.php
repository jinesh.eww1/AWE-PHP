<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>{{config('app.name')}}</title>
	<link rel="icon" href="{{ URL::asset('images/favicon.ico')}}" type="image/gif" sizes="16x13">
	<link rel="stylesheet" href="{{ URL::asset('assets/front/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{ URL::asset('assets/front/css/style.css')}}">
	<link rel="stylesheet" href="{{ URL::asset('assets/front/css/responsive.css')}}">
	<link rel="stylesheet" href="{{ URL::asset('assets/front/css/slick.css')}}">
</head>
<body>
	<section class="top-padding">
		<div class="container">
			<div class="row mx-2">
				<div class="col-md-12">
					<div class="text-center pb-lg-5">
						<h1 class="t-blue font-weight-bold border-bottom1 pb-3 mb-5 d-inline-block"> Education Outreach</h1>
					</div>					
					<!-- <b><p>What is Lorem Ipsum?</p></b> -->
					<p>The NC ABC Commission’s Education Outreach Section runs the underage drinking prevention initiative TalkitOutNC.org. They also provide (free) in-person and online responsible alcohol seller/server training and approve outside training resources to help permit applicants obtain their ‘proof of training’.</p>

					<p>Applicants applying for retail or delivery permits are required to provide proof of Responsible Alcohol Seller/Server training prior to obtaining their ABC permit. Acceptable proof of training may be in the form of a certificate of training, transcript, or other document provided by the course provider, or corporate/delivery permittee. The applicant may also have the course provider sign a form attesting to completion of this training.</p>
					
					<p>If the applicant is representing a corporate chain, the corporation will be required to provide to the NC ABC Commission a copy of their training manual which meets at least these minimum course content requirements.</p>
					
					<p>If the applicant is representing a delivery service, the delivery service will be required to provide to the NC ABC Commission a copy of their training manual which meets at least these minimum course content requirements.</p>
					
				</div>
			</div>
		</div>
	</section>
</body>
</html>
<script src="{{ URL::asset('assets/front/js/jquery.min.js')}}"></script>
<script src="{{ URL::asset('assets/front/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('assets/front/js/popper.min.js')}}"></script>
<script src="{{ URL::asset('assets/front/js/slick.min.js')}}"></script>
<script src="{{ URL::asset('assets/front/js/custom.js')}}"></script>