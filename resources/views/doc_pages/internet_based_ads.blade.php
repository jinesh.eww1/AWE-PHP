<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>{{config('app.name')}}</title>
	<link rel="icon" href="{{ URL::asset('images/favicon.ico')}}" type="image/gif" sizes="16x13">
	<link rel="stylesheet" href="{{ URL::asset('assets/front/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{ URL::asset('assets/front/css/style.css')}}">
	<link rel="stylesheet" href="{{ URL::asset('assets/front/css/responsive.css')}}">
	<link rel="stylesheet" href="{{ URL::asset('assets/front/css/slick.css')}}">
</head>
<body>
	<section class="top-padding">
		<div class="container">
			<div class="row mx-2">
				<div class="col-md-12">
					<div class="text-center pb-lg-5">
						<h1 class="t-blue font-weight-bold border-bottom1 pb-3 mb-5 d-inline-block"> Internet Based Ads</h1>
					</div>					
					<!-- <b><p>What is Lorem Ipsum?</p></b> -->
					<p>We at Dalcom Consulting Inc. may use the information we collect from you to create a unique experience for you on our site and mobile apps (our "Platform"). Information about your preferences and browsing history may be collected and linked by us or by third parties across our Platform, as well as other sites and apps, to facilitate the display of advertisements that are relevant to your interests. For example, if you have shown a preference for a type of beverage while using Dalcom Consulting Inc., you may be served an advertisement relating to that type of beverage. Such interest-based advertisements are placed on our Platform as well as on sites and apps operated by third parties.</p>

					<p>You can opt out of interest-based advertising delivered by third parties that follow the Digital Advertising Alliance's Self-Regulatory Principles for Online Behavioral Advertising at https://youradchoices.com/control. If you would like to know more about how Dalcom Consulting Inc. collects, uses and shares your personal information, please see our Privacy Notice.</p>
					
					
				</div>
			</div>
		</div>
	</section>
</body>
</html>
<script src="{{ URL::asset('assets/front/js/jquery.min.js')}}"></script>
<script src="{{ URL::asset('assets/front/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('assets/front/js/popper.min.js')}}"></script>
<script src="{{ URL::asset('assets/front/js/slick.min.js')}}"></script>
<script src="{{ URL::asset('assets/front/js/custom.js')}}"></script>