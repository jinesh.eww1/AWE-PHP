@yield('script')

    <script type="text/javascript" src="{{ asset('assets/website/js/js.slim.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/website/js/popper.min.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('assets/website/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/website/js/jquery.min.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('assets/website/js/slick.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/website/js/slick.min.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('assets/website/js/bootstrap.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/website/js/custom.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/notiflix/notiflix-2.1.2.js')}}"></script>
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
    
<script src="{{ asset('assets/website/js/firebase.js') }}"></script>
<script>
$(document).ready(function(){
    fnBrowserDetect();
});

    var firebaseConfig = {
        apiKey: "AIzaSyBZebYt4QGwwtRNHdwkSXR75Y_L291-gYA",
        authDomain: "awe-project-341306.firebaseapp.com",
        projectId: "awe-project-341306",
        storageBucket: "awe-project-341306.appspot.com",
        messagingSenderId: "804988135511",
        appId: "1:804988135511:web:dd2d481c60afbc50145b12"
    };
      
    firebase.initializeApp(firebaseConfig);
    const messaging = firebase.messaging();
  
    function initFirebaseMessagingRegistration() {
            messaging
            .requestPermission()
            .then(function () {
                return messaging.getToken()
            })
            .then(function(token) { //console.log(token);
                $('#device_token').val(token);
            }).catch(function (err) {
                console.log('User Chat Token Error'+ err);
            });
     }
$(document).ready(function(){
    initFirebaseMessagingRegistration(); 
});

</script>

    @if(Request::path() == '/' || Request::path() == 'login' || Request::path() == 'signup' || Request::path() == 'account-profile' || Request::path() == 'forgot/password')
    <link rel="stylesheet" href="{{ asset('assets/website/css/intlTelInput.css') }}" />
    <script src="{{ asset('assets/website/js/intlTelInput.min.js') }}"></script>
    <script type="text/javascript">

       const phoneInputField = document.querySelector("#phone");
       const phoneInput = window.intlTelInput(phoneInputField, {
        //preferredCountries:["in"],
        preferredCountries:["us"],
         utilsScript:
           "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
       });


        function process() {
        const phoneNumber = phoneInput.getNumber();
        const selectedCountryData = phoneInput.getSelectedCountryData();
         

         $('#country_code').val(phoneInput.getSelectedCountryData().dialCode);
         $('#country_iso2').val(selectedCountryData.iso2);

        }
    </script>
    @endif

       <div id="loader" style="width: 100%; height: 100%; position: fixed;display: block;top: 0;left: 0;text-align: center;opacity: 1;background-color: #ffffff73;z-index: 99; display: none;">
            <lottie-player src="https://assets10.lottiefiles.com/packages/lf20_vqa0ahvi.json" background="transparent" speed="1" autoplay loop class="loader-img"></lottie-player>
       </div>


    <div class="modal fade" id="staticBackdrop" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content text-center p-4">
                <div class="modal-header">
                    <h5 class="modal-title text-dark" id="staticBackdropLabel"><?php if(Session::get('app_string')){ echo Session::get('app_string.logout.logout_popup2'); }else{ echo CommonHelper::multi_language('logout','logout_popup2')->multi_language_value->language_value; } ?></h5>
                </div>
                <div class="justify-content-center mt-5 mb-2">
                    
                    <a href="javascript:void(0)"><button type="button" class="btn font-18 text-yellow border-color col-4 box-radius" data-bs-dismiss="modal"><?php if(Session::get('app_string')){ echo Session::get('app_string.logout.cancel'); }else{ echo CommonHelper::multi_language('logout','cancel')->multi_language_value->language_value; 
                } ?></button></a>

                    <a href="{{ route('logout') }}"><button type="button" class="btn font-18 bg-color text-white col-4 box-radius ms-4" data-bs-dismiss="modal"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.logout'); }else{ echo CommonHelper::multi_language('settings','logout')->multi_language_value->language_value; 
                } ?></button></a>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="update_profile_popup" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered text-center">
            <div class="modal-content text-center p-4">
                <div class="modal-header" style="border-bottom: none;">
                    <h5 class="modal-title" id="staticBackdropLabel"><?php if(Session::get('app_string')){ echo Session::get('app_string.app_titles_and_messages.profile_proceed_further'); }else{ echo CommonHelper::multi_language('app_titles_and_messages','profile_proceed_further')->multi_language_value->language_value; 
                } ?></h5>
                </div>
                <div class="justify-content-center mt-5 mb-2">
                    <a href="{{ route('account.profile') }}"><button type="button" class="btn font-18 bg-color text-white col-4 box-radius ms-4 col-md-6" data-bs-dismiss="modal"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.ok'); }else{ echo CommonHelper::multi_language('movie_details','ok')->multi_language_value->language_value; 
                } ?></button></a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="parent_subscription_popup" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered text-center">
            <div class="modal-content text-center p-4">
                <div class="modal-header" style="border-bottom: none;">
                    <h5 class="modal-title" id="staticBackdropLabel"><?php if(Session::get('app_string')){ echo Session::get('app_string.subscription.subscription_required_for_view'); }else{ echo CommonHelper::multi_language('subscription','subscription_required_for_view')->multi_language_value->language_value; 
                } ?></h5>
                </div>
                <div class="justify-content-center mt-5 mb-2">
                    <a href="{{ route('all.plans') }}"><button type="button" class="btn font-18 bg-color text-white col-4 box-radius ms-4 col-md-6" data-bs-dismiss="modal"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.ok'); }else{ echo CommonHelper::multi_language('movie_details','ok')->multi_language_value->language_value; 
                } ?></button></a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="sub_subscription_popup" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered text-center">
            <div class="modal-content text-center p-4">
                <div class="modal-header" style="border-bottom: none;">
                    <h5 class="modal-title" id="staticBackdropLabel"><?php if(Session::get('app_string')){ echo Session::get('app_string.subscription.no_active_subscription'); }else{ echo CommonHelper::multi_language('subscription','no_active_subscription')->multi_language_value->language_value; 
                } ?></h5>
                </div>
                <div class="justify-content-center mt-4 mb-2">
                    <button type="button" class="btn font-18 bg-color text-white col-4 box-radius ms-4 col-md-6" data-bs-dismiss="modal"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.ok'); }else{ echo CommonHelper::multi_language('movie_details','ok')->multi_language_value->language_value; 
                } ?></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="screen_limit_popup" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered text-center">
            <div class="modal-content text-center p-4">
                <div class="modal-header" style="border-bottom: none;">
                    <h5 class="modal-title" id="staticBackdropLabel"><?php if(Session::get('app_string')){ echo Session::get('app_string.screen_limit.screen_limit_validation_string'); }else{ echo CommonHelper::multi_language('screen_limit','screen_limit_validation_string')->multi_language_value->language_value; 
                } ?></h5>
                </div>
                <div class="justify-content-center mt-5 mb-2">
                    <a href="javascript:void(0)"><button type="button" class="btn font-18 bg-color text-white col-4 box-radius ms-4 col-md-6" data-bs-dismiss="modal"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.ok'); }else{ echo CommonHelper::multi_language('movie_details','ok')->multi_language_value->language_value; 
                } ?></button></a>
                </div>
            </div>
        </div>
    </div>

@if(str_contains(Request::path(), 'content/details') || str_contains(Request::path(), 'home'))
    <script type="text/javascript">
        $(document).ready(function(){
            
            $('.liked_function').click( function(){
                
                var id = $(this).attr('data-id');
                var status = $(this).attr('data-status');

                var phone_verified = "{{Session::get('user_data.phone_verified')}}";
               
                if(phone_verified == 0){
                    $('#update_profile_popup').modal('show');
                    return false;
                }

                $('#loader').show();
                $.ajax({
                    type: "get",
                    dataType: "json",
                    url: "{{ route('content-liked')}}",
                    data:{id:id,status:status},
                    success: function (returnData) {

                        if(returnData.status == true){
                            if(status ==1){
                                $('.total_likes_div').empty().html(`<div class="d-flex align-items-center justify-content-between like-mobile my-3 total_likes_div"><div><img src="{{ asset('assets/website/image/thumbs-up.png') }}" class="likes_dislikes_img"><span class="text-white font-14 mx-1 total_likes"></span></div><div class="text-white font-14 mx-1">{{ !empty($data['year'])?$data['year']:(!empty($home['data']['main']['year'])?$home['data']['main']['year']:'') }}</div><div class="text-white font-14 mx-1">{{ !empty($data['age_rating_name'])?$data['age_rating_name']:(!empty($home['data']['main'])?$home['data']['main']['age_rating_name']:'') }}</div></div>`);

                                $('.likes_dislikes_img').attr('src',"{{ asset('assets/website/image/thumbs-up.png') }}");
                                $('.total_likes').html(returnData.data.likes);
                                $('.liked_function').attr('data-status',0);    
                                $('.liked_function').addClass('bg-color');
                                $('.disliked_function').removeClass('bg-color');
                                if($('.disliked_function').attr('data-status') == 0){
                                    $('.disliked_function').attr('data-status',1);
                                }
                                
                                $('.liked_lbl').empty().html(`<?php echo (Session::get('app_string.movie_details.liked'))?Session::get('app_string.movie_details.liked'):CommonHelper::multi_language('movie_details','liked')->multi_language_value->language_value; ?>`);
                                
                                $('.disliked_lbl').empty().html(`<?php echo (Session::get('app_string.movie_details.dislike'))?Session::get('app_string.movie_details.dislike'):CommonHelper::multi_language('movie_details','dislike')->multi_language_value->language_value; ?>`);
                            }else{
                                if(returnData.data.likes>0){
                                    $('.total_likes_div').empty().html(`<div class="d-flex align-items-center justify-content-between like-mobile my-3 total_likes_div"><div><img src="{{ asset('assets/website/image/thumbs-up.png') }}" class="likes_dislikes_img"><span class="text-white font-14 mx-1 total_likes"></span></div><div class="text-white font-14 mx-1">{{ !empty($data['year'])?$data['year']:(!empty($home['data']['main']['year'])?$home['data']['main']['year']:'') }}</div><div class="text-white font-14 mx-1">{{ !empty($data['age_rating_name'])?$data['age_rating_name']:(!empty($home['data']['main'])?$home['data']['main']['age_rating_name']:'') }}</div></div>`);
                                    $('.total_likes').html(returnData.data.likes);
                               }else{
                                $('.total_likes_div').empty().html(`<div class="d-flex align-items-center justify-content-between like-mobile my-3 total_likes_div"><div class="text-white font-14 mx-1">{{ !empty($data['year'])?$data['year']:(!empty($home['data']['main']['year'])?$home['data']['main']['year']:'') }}</div><div class="text-white font-14 mx-1">{{ !empty($data['age_rating_name'])?$data['age_rating_name']:(!empty($home['data']['main'])?$home['data']['main']['age_rating_name']:'') }}</div></div>`);
                                    $('.likes_dislikes_img').attr('src',"");
                                    $('.total_likes').html("");
                               }
                                
                                $('.liked_function').attr('data-status',1);
                                $('.liked_function').removeClass('bg-color');

                                if($('.disliked_function').attr('data-status') == 0){
                                    $('.disliked_function').attr('data-status',1);
                                }
                                
                                $('.liked_lbl').empty().html(`<?php echo (Session::get('app_string.movie_details.like'))?Session::get('app_string.movie_details.like'):CommonHelper::multi_language('movie_details','like')->multi_language_value->language_value; ?>`);
                            }
                            
                            $('#loader').hide();
                            
                            // Notiflix.Notify.Success(returnData.message);
                        }else{
                            Notiflix.Notify.Failure(returnData.message);
                        }

                    },
                    error: function (data) {
                    },
                });

            });


            $('.disliked_function').click( function(){
                
                var id = $(this).attr('data-id');
                var status = $(this).attr('data-status');
                
                var phone_verified = "{{Session::get('user_data.phone_verified')}}";
               
                if(phone_verified == 0){
                    $('#update_profile_popup').modal('show');
                    return false;
                }

                $('#loader').show();

                $.ajax({
                    type: "get",
                    dataType: "json",
                    url: "{{ route('content-disliked')}}",
                    data:{id:id,status:status},
                    success: function (returnData) {

                        if(returnData.status == true){
                           
                            if(status ==1){
                               if(returnData.data.likes>0){
                                $('.total_likes_div').empty().html(`<div class="d-flex align-items-center justify-content-between like-mobile my-3 total_likes_div"><div><img src="{{ asset('assets/website/image/thumbs-up.png') }}" class="likes_dislikes_img"><span class="text-white font-14 mx-1 total_likes"></span></div><div class="text-white font-14 mx-1">{{ !empty($data['year'])?$data['year']:(!empty($home['data']['main']['year'])?$home['data']['main']['year']:'') }}</div><div class="text-white font-14 mx-1">{{ !empty($data['age_rating_name'])?$data['age_rating_name']:(!empty($home['data']['main'])?$home['data']['main']['age_rating_name']:'') }}</div></div>`);
                                    $('.total_likes').html(returnData.data.likes);
                               }else{
                                $('.total_likes_div').empty().html(`<div class="d-flex align-items-center justify-content-between like-mobile my-3 total_likes_div"><div class="text-white font-14 mx-1">{{ !empty($data['year'])?$data['year']:(!empty($home['data']['main']['year'])?$home['data']['main']['year']:'') }}</div><div class="text-white font-14 mx-1">{{ !empty($data['year'])?$data['year']:(!empty($home['data']['main']['year'])?$home['data']['main']['year']:'') }}</div></div>`);

                                    $('.likes_dislikes_img').attr('src',"");
                                    $('.total_likes').html("");
                               }
                                $('.disliked_function').attr('data-status',0);    
                                $('.disliked_function').addClass('bg-color');
                                $('.liked_function').removeClass('bg-color');
                                if($('.liked_function').attr('data-status') == 0){
                                    $('.liked_function').attr('data-status',1);
                                }

                                $('.disliked_lbl').empty().html(`<?php echo (Session::get('app_string.movie_details.disliked'))?Session::get('app_string.movie_details.disliked'):CommonHelper::multi_language('movie_details','disliked')->multi_language_value->language_value; ?>`);

                                $('.liked_lbl').empty().html(`<?php echo (Session::get('app_string.movie_details.like'))?Session::get('app_string.movie_details.like'):CommonHelper::multi_language('movie_details','like')->multi_language_value->language_value; ?>`);

                            }else{
                                $('.disliked_function').attr('data-status',1);
                                $('.disliked_function').removeClass('bg-color');

                                if($('.liked_function').attr('data-status') == 0){
                                    $('.liked_function').attr('data-status',1);
                                }
                                $('.disliked_lbl').empty().html(`<?php echo (Session::get('app_string.movie_details.dislike'))?Session::get('app_string.movie_details.dislike'):CommonHelper::multi_language('movie_details','dislike')->multi_language_value->language_value; ?>`);
                            }
                            $('#loader').hide();
                            // Notiflix.Notify.Success(returnData.message);
                        }else{
                            Notiflix.Notify.Failure(returnData.message);
                        }

                    },
                    error: function (data) {
                    },
                });

            });


            $('.mylist_function').click( function(){
                
                var id = $(this).attr('data-id');
                var status = $(this).attr('data-status');
                
                var phone_verified = "{{Session::get('user_data.phone_verified')}}";
               
                if(phone_verified == 0){
                    $('#update_profile_popup').modal('show');
                    return false;
                }
                $('#loader').show();

                $.ajax({
                    type: "get",
                    dataType: "json",
                    url: "{{ route('content-my-list')}}",
                    data:{id:id,status:status},
                    success: function (returnData) {

                        if(returnData.status == true){
                           
                            if(status ==1){
                                $('.mylist_function').attr('data-status',0);    
                                $('.mylist_function').addClass('bg-color');
                                
                            }else{
                                $('.mylist_function').attr('data-status',1);
                                $('.mylist_function').removeClass('bg-color');
                            }
                            $('#loader').hide();
                            Notiflix.Notify.Success(returnData.message);
                        }else{
                            Notiflix.Notify.Failure(returnData.message);
                        }

                    },
                    error: function (data) {
                    },
                });

            }); 
        });
    </script>
@endif
<script type="text/javascript">
        $(document).ready(function(){

            $.getJSON("https://api.ipify.org?format=json", function(data) {
                $("#device_ip").val(data.ip);
            });

            $(".alert").slideDown(300).delay(4000).slideUp(250);
            $('form').parsley();

            $('.hide_pass').click( function(){
                if($(this).find('img').attr('class') == 'show-icon'){
                    $('.hide_pass').empty();
                    $('.hide_pass').html("<img src='{{ asset("assets/website/image/Icon-Hide.svg") }}' class='hide-icon' />");
                    $('#password_field').attr('type','password');
                }else{
                    $('.hide_pass').empty();
                    $('.hide_pass').html("<img src='{{ asset("assets/website/image/Icon-show.png") }}' class='show-icon' />");
                    $('#password_field').attr('type','text');
                }
            });
            
            $('.old_hide_pass').click( function(){
                if($(this).find('img').attr('class') == 'show-icon'){
                    $('.old_hide_pass').empty();
                    $('.old_hide_pass').html("<img src='{{ asset("assets/website/image/Icon-Hide.svg") }}' class='hide-icon' />");
                    $('#current_password').attr('type','password');
                }else{
                    $('.old_hide_pass').empty();
                    $('.old_hide_pass').html("<img src='{{ asset("assets/website/image/Icon-show.png") }}' class='show-icon' />");
                    $('#current_password').attr('type','text');
                }
            });
            
            $('.con_hide_pass').click( function(){
                if($(this).find('img').attr('class') == 'show-icon'){
                    $('.con_hide_pass').empty();
                    $('.con_hide_pass').html("<img src='{{ asset("assets/website/image/Icon-Hide.svg") }}' class='hide-icon' />");
                    $('#password_confirmation').attr('type','password');
                }else{
                    $('.con_hide_pass').empty();
                    $('.con_hide_pass').html("<img src='{{ asset("assets/website/image/Icon-show.png") }}' class='show-icon' />");
                    $('#password_confirmation').attr('type','text');
                }
            });
 

        });

      //paste only digits
      function paste_only_digit(input){
        let value = input.value;
        let numbers = value.replace(/[^0-9]/g, "");
        input.value = numbers;
      }

        function getLocation() {
            if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(showPosition);
            } else {
              alert("Geolocation is not supported by this browser.");
            }
        }

        function fnBrowserDetect(){
            
            $("#loader").show();
             var browserName = (function() {
                var test = function(regexp) {
                    return regexp.test(window.navigator.userAgent)
                }
                switch (true) {
                    case test(/edg/i):
                        return "Edge";
                    case test(/trident/i):
                        return "Internet Explorer";
                    case test(/firefox|fxios/i):
                        return "Firefox";
                    case test(/opr\//i):
                        return "Opera";
                    case test(/ucbrowser/i):
                        return "UC Browser";
                    case test(/samsungbrowser/i):
                        return "Samsung Browser";
                    case test(/chrome|chromium|crios/i):
                        return "Chrome";
                    case test(/safari/i):
                        return "Safari";
                    default:
                        return "No browser detection";
                }
            })();
            
           $('#device_name').val(browserName); 
           $('#acp_device_name').empty().html(browserName);

           $('#loader').hide();

        }

        function showPosition(position) {

            $('#latitude').val(position.coords.latitude);
            $('#longitude').val(position.coords.longitude);

        }


        function length_count() {

            if($('#email_field').val().length > 5){
                if($.isNumeric($('#email_field').val())){
                    $('.phone_field').css('display','');
                    $('.email_div').css('display','none');
                    $('#phone').val($('#email_field').val()).focus();
                    $('#email_field').val('');
                    $('#email_field').removeAttr('required');
                }
            }

        }

        function notifications() {
            $('#loader').show();

            $.ajax({
                type: "get",
                dataType: "json",
                url: "{{ route('notifications')}}",
                //data:{id:id},
                success: function (returnData) {
                    $('#loader').hide();
                    //console.log(returnData);
                    var html =''; 
                    var view_more =''; 
                    if(returnData != '' && returnData.length > 0){
                        $(".notification_sec").empty();
                        var len = returnData.length;
                        var logo_img = '{{ asset("assets/website/image/awe-home.png") }}';
                        for( var i = 0; i<len; i++){
                            if(i<10){
                                var title = returnData[i]['title'];
                                var message = returnData[i]['message'];
                                var time_ago = returnData[i]['time_ago'];
                                
                                html +='<li><div class=" align-items-start justify-content-between "><div class="d-flex align-items-start mt-3"><img src='+logo_img+' class="img-fluid notification-data ms-2"><div class="px-2"><label>'+title+'</label><p style="margin-bottom: 3px; color:#8b8b8b;">'+message+'</p><span style="font-size: 10px;color:#8b8b8b;">'+time_ago+'</span></div></div></div><hr></li>';
                            }else{
                                var view_more ='<li class="text-center"><a href="javascript:void(0)" class="text-white"><div class=" align-items-start justify-content-between "> Load More... </div></a><hr></li>';
                            }
                        }

                        $(".notification_sec").html(html+view_more);
                    }

                },
                error: function (data) {
                },
            });
        }


        function phone_length_count() {

            if($('#phone').val() == ''){
                
                $('.phone_field').css('display','none');
                $('.email_div').css('display','');
                $('#email_field').attr('required','required').focus();
                $('#phone').val('');
                $('#country_code').val('');
                $('#country_iso2').val('');
                
            }
            
        }

    </script>

<script src="{{ asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
<script type="text/javascript">
    window.ParsleyValidator.addValidator('notequalto',
        function(value, requirement) {
            return value !== $(requirement).val();
    }, 32);

    window.ParsleyValidator.addValidator('validateFullWidthCharacters', {
        requirementType: 'string',
        validateString: function (value, requirement) {
            regex = /^([a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6})$/gm;
            return regex.test(value);
        }
    });

function removeSpaces(string) {
    
    var remove_string = string.split(' ').join('');
    return remove_string;
}
</script>