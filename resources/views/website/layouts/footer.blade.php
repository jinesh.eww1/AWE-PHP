<style type="text/css">
.list-unstyled {
    padding-left: 0 !important;
}
</style>
<footer>
    <section>
        <div class="container">
            <div class="row pt-md-5">
                <div class="col-lg-4 col-12 ">
                    <img src="{{ asset('assets/website/image/awe-home.png') }}" height="100px" width="100px" class="box-radius">
                    <?php /*<div class="font-18 text-footer pt-md-3 mt-5 pb-2">

                    </div>*/ ?>
                    <div class="mt-4 my-md-5">
                        <a target="_blank" href="https://www.facebook.com/africanworldentertainment"><img
                                src="{{ asset('assets/website/image/fb-yellow.png') }}"></a>
                        <a target="_blank" href="https://twitter.com/Africanworldent"><img
                                src="{{ asset('assets/website/image/twitter.png') }}" class="ms-4"></a>
                        <a target="_blank" href="https://www.tiktok.com/@awemovies"><img
                                src="{{ asset('assets/website/image/card/tiktok.png') }}" class="ms-4"></a>
                        <a target="_blank" href="https://www.instagram.com/awe_org/"><img
                                src="{{ asset('assets/website/image/card/instagram.png') }}" class="ms-4"></a>
                        <a target="_blank" href="https://www.youtube.com/channel/UC3ZymP875LwoFg6YgfgV9Vg"><img
                                src="{{ asset('assets/website/image/card/youtube.png') }}" class="ms-4"></a>
                    </div>
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-2 col-sm-4 col-12 ">
                    <h6 class="fw-bold text-white">
                        <?php if(Session::get('app_string')){ echo Session::get('app_string.footer_section.links'); }else{ echo CommonHelper::multi_language('footer_section','links')->multi_language_value->language_value; } ?>
                    </h6>
                    <ul class="list-unstyled font-18">
                        <li class="mt-3 pt-3"><a href="{{ route('home','movie') }}"
                                class="text-footer text-decoration-none"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.home'); }else{ echo CommonHelper::multi_language('home','home')->multi_language_value->language_value; } ?></a>
                        </li>
                        <li class="mt-3"><a href="{{ route('home','tv-shows') }}"
                                class="text-footer text-decoration-none"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.tv_show'); }else{ echo CommonHelper::multi_language('home','tv_show')->multi_language_value->language_value; } ?></a>
                        </li>
                        <li class="mt-3"><a href="{{ route('home','movie') }}"
                                class="text-footer text-decoration-none"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.movies'); }else{ echo CommonHelper::multi_language('home','movies')->multi_language_value->language_value; } ?></a>
                        </li>

                    </ul>
                </div>
                <div class="col-lg-3 col-sm-4 col-12 col-4 ">
                    <h6 class="fw-bold text-white">
                        <?php if(Session::get('app_string')){ echo Session::get('app_string.footer_section.about'); }else{ echo CommonHelper::multi_language('footer_section','about')->multi_language_value->language_value; } ?>
                    </h6>
                    <ul class="list-unstyled font-18">
                        <li class="mt-3 pt-3"><a target="_blank" href="{{ url('/terms-conditions') }}"
                                class="text-footer text-decoration-none"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.terms_conditions'); }else{ echo CommonHelper::multi_language('settings','terms_conditions')->multi_language_value->language_value; } ?></a>
                        </li>
                        <li class="mt-3"><a target="_blank" href="{{ url('/privacy-policy') }}"
                                class="text-footer text-decoration-none"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.privacy_policy'); }else{ echo CommonHelper::multi_language('settings','privacy_policy')->multi_language_value->language_value; } ?></a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2 col-sm-4 col-12 ">
                    <h6 class="fw-bold text-white">
                        <?php if(Session::get('app_string')){ echo Session::get('app_string.footer_section.get_in_touch'); }else{ echo CommonHelper::multi_language('footer_section','get_in_touch')->multi_language_value->language_value; } ?>
                    </h6>
                    <?php /*<div class="row mt-3 pt-3">
                        <div class="col-md-2 col-1">
                            <img src="{{ asset('assets\website\image/location.png') }}">
                        </div>
                        <div class="col-md-10 col-11 text-footer font-18 fw-light">
                            408 Bethel Rd, Suite B-3,  Somers Point, NJ 08244
                        </div>
                    </div>*/ ?>
                    <div class="row pt-3 mt-3">
                        <div class="col-md-2 col-1">
                            <img src="{{ asset('assets\website\image/contact.png') }}">
                        </div>
                        <div class="col-md-10 col-11 font-18 ">
                            <a href="tel:18884453670" class="text-footer text-decoration-none">1 (888) 445-3670</a>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-2 col-1">
                            <img src="{{ asset('assets\website\image/email.png') }}">
                        </div>
                        <div class="col-md-10 col-11 font-18 text-break pe-0">
                            <a href="mailto:info@awemovies.com"
                                class="text-footer text-decoration-none">info@awemovies.com</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="font-14 text-white fw-light mb-4">
                <p>The copyright license of the movies and contents contained therein was duly obtained from the owners
                    and all rights in respect of our movies and contents are reserved.Any redistribution or reproduction
                    of part or all of the contents in any form is prohibited and you may not, except with our express
                    written permission, distribute or commercially exploit the content. Nor may you transmit it or store
                    it in any other app, website, or other forms of electronic retrieval system.</p>
                <p>Copyright © African World entertainment (AWEmovies), {{ date('Y') }} All right reserved</p>
            </div>
        </div>
    </section>
</footer>