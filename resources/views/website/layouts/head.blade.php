@yield('css')
<?php 
    $genre = Session::get('category'); //CommonHelper::genre_data(Session::get("user_data.id"));
?>

<style type="text/css">
.user-menu::after {
    /*content: none !important;*/
    /* margin-left: -0.745em !important; */
}

.notification_sec {
    width: 300px;
    margin-left: -120px;
    height: 470px;
    overflow-y: auto;
    margin-top: 15px !important;
    background-color: black;
    color: white;
}

.search-data-show li {
    margin: 10px 10px;
}

.search-data-show {
    margin-top: 15px;
    left: 0;
    right: 0;
    border-radius: 10px;
    overflow-y: auto;
}
</style>

<header class="about-header">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
            <div class="container position-relative pt-md-0 pt-5 my-md-0 my-3">
                <a class="navbar-brand" href="{{ route('home','movie') }}"><img class="head_icon_rounded" src="{{ asset('assets/website/image/awe-home.png') }}" height="50px" width="50px"></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-between" id="mynavbar">
                    <ul class="navbar-nav ps-xl-5 ms-xl-5 nav-font me-auto">
                        <li class="nav-item px-2 d-inline-flex">
                            <a class="nav-link active" href="{{ route('home','movie') }}">
                                <?php if(Session::get('app_string')){ echo Session::get('app_string.home.movies'); 
                                    }else{ echo CommonHelper::multi_language('home','movies')->multi_language_value->language_value; } ?></a>
                        </li>
                        <li class="nav-item px-2 d-inline-flex">
                            <a class="nav-link"
                                href="{{ route('home','tv-shows') }}"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.tv_show'); }else{ echo CommonHelper::multi_language('home','tv_show')->multi_language_value->language_value; } ?></a>
                        </li>
                        <li class="nav-item px-2 d-inline-flex">
                            <a class="nav-link "
                                href="{{ route('my-list') }}"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.my_list'); }else{ echo CommonHelper::multi_language('home','my_list')->multi_language_value->language_value; } ?></a>
                        </li>
                        <li class="nav-item dropdown px-2 d-inline-flex">
                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                data-bs-toggle="dropdown"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.categories'); }else{ echo CommonHelper::multi_language('home','categories')->multi_language_value->language_value; } ?></a>
                            <ul class="dropdown-menu p-1">
                                <li class="text-center"><a
                                        class="text-decoration-none text-yellow text-center font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.categories'); }else{ echo CommonHelper::multi_language('home','categories')->multi_language_value->language_value; } ?></a>
                                </li>
                                <li>
                                    <hr class="dropdown-divider my-1">
                                </li>
                                <div style="column-count: 2;" class="m-2">
                                    <?php if(!empty($genre)){ 
                                            $content_type = !empty($type)?$type:'movie';
                                            foreach ($genre as $key => $value) { ?>
                                    <li class="">
                                        @if (isset($category_type) && $category_type == base64_encode($value['id']))
                                            <a class="dropdown-item bg-color" 
                                                href="{{ route('view.more', [$content_type, base64_encode($value['id'])]) }}">
                                                <span>{{ $value['name'] }}</span>

                                            </a>
                                            @else
                                            <a class="dropdown-item"
                                                href="{{ route('view.more', [$content_type, base64_encode($value['id'])]) }}">
                                                {{ $value['name'] }}

                                            </a>
                                        @endif
                                    </li>
                                    <?php } } ?>

                                </div>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="d-flex align-items-center search-btn px-md-0 px-2">

                    @if(Route::current()->getName() =='popular-search')
                    
                        <form class="position-relative">
                            <div class="serch-img">
                                <input class="form-control rounded-pill p-search border-color text-white" type="text"
                                    placeholder="<?php if(Session::get('app_string')){ echo Session::get('app_string.search.search'); }else{ echo CommonHelper::multi_language('search','search')->multi_language_value->language_value; } ?>"
                                    id="search_filter">

                            </div>
                            <ul id="searchResult" class="list-unstyled bg-white position-absolute search-data-show">
                            </ul>
                        </form>
                    
                    @endif
                    @if(Route::current()->getName() !='popular-search')
                    <a href="{{ route('popular-search') }}?type=web" class="mx-2"><img src="{{ asset('assets/website/image/card/search_icon.png') }}" class="img-fluid px-lg-4 px-md-3 px-2" style=" height: 30px;"></a>
                    <div class="d-flex align-items-center position-relative">

                        @endif
                        <div class="dropdown mx-2">
                            <a href="javascript:void(0);" onclick="notifications();" class="nav-link dropdown-toggle text-white user-menu" role="button" data-bs-toggle="dropdown"><img src="{{ asset('assets/website/image/bell.png') }}" style="padding-right: 15px;" class="px-2"></a>

                            <ul class="dropdown-menu p-1 notification_sec">
                                <?php /* $notification = CommonHelper::notification_data(Session::get('user_data.id'));

                                    if(!empty($notification) && count($notification) > 0){
                                        foreach ($notification as $key => $value) { ?>
                                        <li>
                                        <div class=" align-items-start justify-content-between ">
                                            <div class="d-flex align-items-start mt-3">
                                                <img src="{{ asset('assets/website/image/awe-home.png') }}" class="img-fluid notification-data ms-2">
                                                <div class="px-2">
                                                    <label>{{ $value['title'] }}</label>
                                                    <p style="margin-bottom: 3px;">{{ $value['message'] }}</p>
                                                    <span style="font-size: 12px;color: #ccc;">{{ $value['time_ago'] }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        </li>
                                <?php } }else{ */ ?>
                                <li>
                                    <div class="text-center mt-5">
                                        <img src="{{ asset('assets/website/image/card/AWE_NothingToShow_IMAGE.png') }}">
                                        <p class="mt-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.nodata.nothing_to_show_here'); }else{ echo CommonHelper::multi_language('nodata','nothing_to_show_here')->multi_language_value->language_value; } ?></p>
                                        <p><?php if(Session::get('app_string')){ echo Session::get('app_string.nodata.no_notification_till_now'); }else{ echo CommonHelper::multi_language('nodata','no_notification_till_now')->multi_language_value->language_value; } ?></p>
                                  </div>
                                </li>
                                <?php //} ?>
                            </ul>
                        </div>

                        <div class="dropdown mx-2">
                            <a class="nav-link dropdown-toggle text-white user-menu" href="#" role="button"
                                data-bs-toggle="dropdown">

                                <?php if(!empty(Session::get('user_data.profile_picture_full_url'))){ ?>
                                <img src="{{ Session::get('user_data.profile_picture_full_url') }}" class=""onerror="this.src='{{ asset('/assets/website/image/awe-home.png') }}'"
                                    style="height: 30px !important;width: 30px !important;border-radius: 15% !important;object-fit:cover">
                                <?php }else{ ?>
                                <img src="{{ asset('assets/website/image/user.png') }}" class="img-fluid ">
                                <?php } ?>
                            </a>
                            <ul class="dropdown-menu p-1" style="margin-top:15px;margin-left: -110px;">
                                <?php if(Session::has('token')){ ?>

                                <li><a class="dropdown-item text-yellow font-18"
                                        href="{{ route('whos.watching') }}"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.who_watching'); }else{ echo CommonHelper::multi_language('movie_details','who_watching')->multi_language_value->language_value; } ?></a>
                                </li>

                                <li><a class="dropdown-item text-yellow font-18"
                                        href="{{ route('profile') }}"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.settings'); }else{ echo CommonHelper::multi_language('settings','settings')->multi_language_value->language_value; } ?></a>
                                </li>

                                <li data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                    <a class="dropdown-item text-yellow font-18" href="javascript:void(0)">
                                    <?php if(Session::get('app_string')){ echo Session::get('app_string.settings.logout'); }else{ echo CommonHelper::multi_language('settings','logout')->multi_language_value->language_value; } ?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>