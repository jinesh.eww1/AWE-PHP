<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AWE</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/website/font/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/website/css/bootstrap.main.css') }}">
    <link rel="stylesheet"type="text/css"  href="{{ asset('assets/website/css/slick.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/website/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/website/css/responsive.css') }}">
    <link href="{{ URL::asset('assets/libs/notiflix/notiflix-2.1.2.css')}}" rel="stylesheet" type="text/css" />
    
    <script src="{{ URL::asset('assets/js/jquery.min.js')}}"></script>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">
</head>

<body class="bg-body">

    @yield('content')

     <!-- class="about-footer" -->

    
    @include('website.layouts.footer-script')

</body>

</html>
