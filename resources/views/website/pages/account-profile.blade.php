@extends('website.layouts.master')
@section('content')

    @include('website.include.flash-message')

    <style type="text/css">
        .iti__flag-container {
            height: unset !important;
        }
    </style>
    <section>
        <div class="container">
            <div class="mt-md-5 py-5 mb-md-4">
                <a href="{{ route('profile') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span
                    class="text-white font-18"><?php if (Session::get('app_string')) {
                        echo Session::get('app_string.settings.account_profile');
                    } else {
                        echo CommonHelper::multi_language('settings', 'account_profile')->multi_language_value->language_value;
                    } ?></span>
            </div>
            <form action="{{ route('account.profile') }}" method="post">
                <div class="row">
                    <div class="col-lg-4 col-md-6 my-4">
                        <label class="text-white font-21 mb-4"><?php if (Session::get('app_string')) {
                            echo Session::get('app_string.account_profile.language');
                        } else {
                            echo CommonHelper::multi_language('account_profile', 'language')->multi_language_value->language_value;
                        } ?></label>
                        <div class="row align-items-center all-noti p-3 position-relative">
                            <div class="col-md-2 col-2 p-0 text-center">
                                <img class="img-fluid" src="{{ asset('assets/website/image/language-icon.png') }}"
                                    alt="">
                            </div>
                            <div class="col-md-10 col-10 p-0 select-language">
                                <select class="text-white font-18" name="location" style="width: 100% !important;">
                                    @if (!empty($language))
                                        @foreach ($language as $key => $value)
                                            <?php if (app()->getLocale() == $value['api_key']) {
                                                $selected_lan = 'selected';
                                            } else {
                                                $selected_lan = '';
                                            } ?>
                                            <option value="{{ $value['api_key'] }}" data-id="{{ $value['id'] }}"
                                                {{ $selected_lan }}>{{ $value['type'] }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 my-4">
                        <label class="text-white font-21 mb-4"><?php if (Session::get('app_string')) {
                            echo Session::get('app_string.account_profile.profile');
                        } else {
                            echo CommonHelper::multi_language('account_profile', 'profile')->multi_language_value->language_value;
                        } ?></label>
                        <div class="row align-items-center all-noti p-3">
                            <div class="col-md-2 col-2 p-0 text-center">
                                <img class="img-fluid" src="{{ asset('assets/website/image/mobile-icon.png') }}"
                                    alt="">
                            </div>
                            <div class="col-md-8 col-8 p-0">
                                <?php if(!empty($user['country_code']) && !empty($user['phone'])){ ?>
                                <input class="bg-transparent border-0 text-white outline-line-none font-20 w-100" type="text"
                                    readonly value="{{ $user['country_code'] . $user['phone'] }} ">
                                <?php }else{ ?>
                                <input class="bg-transparent border-0 text-white outline-line-none font-20 w-100"type="tel"oninput="paste_only_digit(this)"
                                    name="phone"data-parsley-length="[6, 15]" maxlength="15" data-parsley-type="integer"
                                    placeholder="<?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.register.mobile');
                                    } else {
                                        echo CommonHelper::multi_language('register', 'mobile')->multi_language_value->language_value;
                                    } ?>" id="phone" onkeyup="phone_length_count()"
                                    data-parsley-errors-container="#phone_error"
                                    data-parsley-required-message="<?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.forgot_password.enter_email_mobile_number');
                                    } else {
                                        echo CommonHelper::multi_language('forgot_password', 'enter_email_mobile_number')->multi_language_value->language_value;
                                    } ?>" 
                                    data-parsley-type-message="<?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.login.email_invalid');
                                    } else {
                                        echo CommonHelper::multi_language('login', 'email_invalid')->multi_language_value->language_value;
                                    } ?>"
                                     data-parsley-length-message="<?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.register.mobile_number_min_length');
                                    } else {
                                        echo CommonHelper::multi_language('register', 'mobile_number_min_length')->multi_language_value->language_value;
                                    } ?>">                                   
                                    
                                <input type="hidden" name="country_code" id="country_code" value="">
                                <input type="hidden" name="country_iso2" id="country_iso2" value="">
                                <input type="hidden" name="type" value="account-profile">
                                <?php } ?>
                            </div>

                            <div class="col-md-2 col-2 text-end">
                                <?php if(!empty($user['country_code']) && !empty($user['phone'])){ ?>
                                <img class="img-fluid" src="{{ asset('assets/website/image/send-otp.png') }}"
                                    alt="">
                                <?php } ?>
                            </div>

                        </div>
                        <div class="error" id="phone_error"></div>
                    </div>
                    <div class="col-lg-4 col-md-6 my-4">
                        <label class="text-white font-21 mb-4"><?php if (Session::get('app_string')) {
                            echo Session::get('app_string.register.email');
                        } else {
                            echo CommonHelper::multi_language('register', 'email')->multi_language_value->language_value;
                        } ?></label>
                        <div class="row align-items-center all-noti p-3">
                            <div class="col-md-2 col-2 p-0 text-center">
                                <img class="img-fluid" src="{{ asset('assets/website/image/email-icon.png') }}"
                                    alt="">
                            </div>
                            <div class="col-md-8 col-8 p-0">
                                <?php if(!empty($user['email'])){ ?>
                                <input class="bg-transparent border-0 text-white outline-line-none font-20 w-100" type="email"
                                    readonly value="{{ $user['email'] }} ">
                                <?php }else{ ?>
                                <input class="bg-transparent border-0 text-white outline-line-none font-20 w-100" type="email"
                                    name="email">
                                <?php } ?>
                            </div>
                            <div class="col-md-2 col-2 text-end">
                                <img class="img-fluid" src="{{ asset('assets/website/image/send-otp.png') }}"
                                    alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 my-4">
                        <label class="text-white font-21 mb-4"><?php if (Session::get('app_string')) {
                            echo Session::get('app_string.account_profile.devices');
                        } else {
                            echo CommonHelper::multi_language('account_profile', 'devices')->multi_language_value->language_value;
                        } ?></label>
                        <a href="{{ route('register-device') }}" class="text-decoration-none">
                            <div class="row align-items-center all-noti p-3 position-relative">
                                <div class="col-md-2 col-2 p-0 text-center">
                                    <img class="img-fluid" src="{{ asset('assets/website/image/card/tv_icone.png') }}"
                                        alt="">
                                </div>
                                <div class="col-md-10 col-10 p-0 device-show">
                                    <span class="text-white font-21" id="acp_device_name"></span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="d-flex justify-content-center my-5 pt-4">
                    <div class="col-lg-3 col-md-6 col-10">
                        <button class="form-control btn bg-color px-4 py-3 box-radius font-23 text-white" type="submit"
                            onclick="process()"><?php if (Session::get('app_string')) {
                                echo Session::get('app_string.account_profile.save');
                            } else {
                                echo CommonHelper::multi_language('account_profile', 'save')->multi_language_value->language_value;
                            } ?></button>
                    </div>
                </div>
                @csrf()
            </form>
        </div>
    </section>
    <script>
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
@endsection
