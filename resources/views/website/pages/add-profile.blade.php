@extends('website.layouts.master')
@section('content')
@include('website.include.flash-message')

<section>
    <div class="container">
        <div class="mt-md-5 py-5">
            <a href="{{ url()->previous() }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.add_Profile.Create _profile'); }else{ echo  CommonHelper::multi_language('add_Profile','Create _profile')->multi_language_value->language_value; } ?></span>
        </div>
        <form method="POST" action="{{ route('account.add') }}?type=profile" class="position-relative mt-5 pt-3" enctype="multipart/form-data"  >
            
            <div class="row justify-content-center">
                <div class="manage-profile-edit btn bg-white box-radius position-relative">
                    <div class="profile-edit-icon">
                        <img class="img-fluid p-4 mt-4"  src="{{ asset('assets/website/image/profile1-edit.png') }}" alt="" id="blah1">
                        <input type="file" name="profile_picture" style="display:none;" onchange="readURL1(this);" id="profile_image" accept="image/*"  data-parsley-errors-container="#profile_picture_error" >
                    </div>
                    <div class="error" id="profile_picture_error" style="margin-top:15px;"></div>
                </div>

            </div>
            <div class="row justify-content-center edit-profile mt-4 gap-2">
                <div class="col-md-2 col-10 mt-3 px-md-0">
                    <input data-parsley-length="[2,25]" maxlength="25" class="form-control bg-transparent px-4 py-3 font-21 text-yellow" required type="text" name="first_name" placeholder="<?php if(Session::get('app_string')){ echo Session::get('app_string.add_Profile.first_name'); }else{ echo  CommonHelper::multi_language('add_Profile','first_name')->multi_language_value->language_value; } ?>" data-parsley-pattern="^[a-zA-Z\s]*$"
                    data-parsley-required-message="<?php if (Session::get('app_string')) {
                        echo Session::get('app_string.add_Profile.enter_first_name');
                    } else {
                        echo CommonHelper::multi_language('add_Profile', 'enter_first_name')->multi_language_value->language_value;
                    } ?>" 
                    data-parsley-length-message="<?php if (Session::get('app_string')) {
                        echo Session::get('app_string.add_Profile.first_name_min_length');
                    } else {
                        echo CommonHelper::multi_language('add_Profile', 'first_name_min_length')->multi_language_value->language_value;
                    } ?>"
                    data-parsley-pattern-message="<?php if (Session::get('app_string')) {
                        echo Session::get('app_string.add_Profile.please_enter_valid_first_name');
                    } else {
                        echo CommonHelper::multi_language('add_Profile', 'please_enter_valid_first_name')->multi_language_value->language_value;
                    } ?>">
                </div>
                <div class="col-md-2 col-10 mt-3 px-md-0">
                    <input data-parsley-length="[2,25]"maxlength="25" class="form-control bg-transparent px-4 py-3 font-21 text-yellow" required type="text" name="last_name" placeholder="<?php if(Session::get('app_string')){ echo Session::get('app_string.add_Profile.last_name'); }else{ echo  CommonHelper::multi_language('add_Profile','last_name')->multi_language_value->language_value; } ?>"
                    data-parsley-pattern="^[a-zA-Z\s]*$"
                    data-parsley-required-message="<?php if (Session::get('app_string')) {
                        echo Session::get('app_string.add_Profile.enter_last_name');
                    } else {
                        echo CommonHelper::multi_language('add_Profile', 'enter_last_name')->multi_language_value->language_value;
                    } ?>" 
                    data-parsley-length-message="<?php if (Session::get('app_string')) {
                        echo Session::get('app_string.add_Profile.last_name_min_length');
                    } else {
                        echo CommonHelper::multi_language('add_Profile', 'last_name_min_length')->multi_language_value->language_value;
                    } ?>"
                    data-parsley-pattern-message="<?php if (Session::get('app_string')) {
                        echo Session::get('app_string.add_Profile.please_enter_valid_last_name');
                    } else {
                        echo CommonHelper::multi_language('add_Profile', 'please_enter_valid_last_name')->multi_language_value->language_value;
                    } ?>" >
                </div>
            </div>
            <div class="row justify-content-center align-items-center my-4 pt-2">
                <div class="col-lg-4 col-md-6 col-10"> 
                    <div class="row justify-content-center align-items-center all-noti p-2 box-shadow-notification">
                        <div class="col-lg-9 col-md-9 col-8 px-md-0">
                            <p class="text-white font-21 p-3"><?php if(Session::get('app_string')){ echo Session::get('app_string.add_Profile.for_children'); }else{ echo  CommonHelper::multi_language('add_Profile','for_children')->multi_language_value->language_value; } ?></p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-4">
                            <label class="switch float-end">
                                <input type="checkbox" name="is_child" >
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button class="btn profile-save-btn text-white bg-color font-20 fw-bold p-3 my-4 box-radius" type="submit"><?php if(Session::get('app_string')){ echo Session::get('app_string.account_profile.save'); }else{ echo CommonHelper::multi_language('account_profile','save')->multi_language_value->language_value; } ?></button>
            </div>
            @csrf();
        </form>   
    </div>
</section>

<script type="text/javascript">
    $(".profile-edit-icon").click(function () {
        $("#profile_image").click();
    });

    function readURL1(input) {
    if (input.files && input.files[0]) {
        var ext = input.files[0]['name'].split('.').pop();

        if($.inArray(ext, ['png','jpg','jpeg','svg']) == -1) {
            Notiflix.Notify.Failure('<?php if (Session::get('app_string')) {echo Session::get('app_string.add_Profile.invalid_file_type');} else {echo CommonHelper::multi_language('add_Profile', 'invalid_file_type')->multi_language_value->language_value;} ?>');
            return false;
        }

        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1').attr('src', e.target.result);
            $('.blah1').attr('href', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

</script>
@endsection
