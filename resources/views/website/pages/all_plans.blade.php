@extends('website.layouts.master')
@section('content')
@include('website.include.flash-message')
<style>
    .plan-top-radius{
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
    }
    .plan-bottom-radius{
        border-bottom-left-radius: 10px;
        border-bottom-right-radius: 10px;
    }
    .choose-plan-overview-for-mobile{
        display: none;
    }
    @media only screen and (max-width: 768px) and (min-width: 320px){
        .choose-plan-overview-for-mobile{
            display: block;
        }
        .choose-the-plan-overview{
            display: none;
        }
        .carousel-control-prev{
            left: -7%;
            opacity: 1;
        }
        .carousel-control-next {
            right: -7%;
            opacity: 1;
        }
    }
</style>
<section>
    <div class="container">

        <div class="row justify-content-xl-between align-items-center">

            <div class="col-xl-4 col-lg-3 col-md-4 mt-md-5 my-4">
                <a href="{{ route('home','movie') }}">
                    <img class="img-fluid box-radius" src="{{ asset('assets/website/image/awe-home.png') }}" height="80px" width="80px" alt="">
                </a>
            </div>

            <div class="col-xl-3 col-lg-4 col-md-6 mt-md-5 my-4 px-md-0 ms-lg-5 ms-lg-4 col-sm-6" >
                <div class="bg-white p-1 box-radius d-inline-block">
                    <button class="btn font-14 fw-bold box-radius px-lg-5 px-md-5 px-4 btn-white text-white bg-color" id="monthly_btn"><?php if(Session::get('app_string')){ echo Session::get('app_string.payment_method.monthly'); }else{ echo CommonHelper::multi_language('payment_method','monthly')->multi_language_value->language_value; } ?></button>
                    <button class="btn font-14 fw-bold box-radius px-lg-5 px-md-5 px-4" id="yearly_btn"><?php if(Session::get('app_string')){ echo Session::get('app_string.payment_method.yearly'); }else{ echo CommonHelper::multi_language('payment_method','yearly')->multi_language_value->language_value; } ?></button>
                </div>   
            </div>
            <div class="col-xl-4 col-lg-4 col-md-2 mt-md-5 my-4 text-end col-sm-6">
                @if(url()->previous() == route('view.plan') || url()->previous() == route('profile'))
                @else
                 <a href="{{ route('home','movie') }}" class="font-20 text-white">{{ __('message.plan.skip') }} <i class="fa fa-angle-right" aria-hidden="true"></i> </a>   
                @endif
            </div>
        </div>
        <div class="choose-the-plan-overview">
            <div class="row mt-md-5 my-4 pt-2 justify-content-around">
                <div class="col-md-4 font-25 text-white mb-md-0 mb-5"><a href="{{ url()->previous() }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> {{ __('message.plan.choose_plan') }}</div>

                 @php if(!empty($data['data']) && count($data['data'])>0){ 
                    $plan_name_array = array_column($data['data'],'name');
                    $monthly_price_array = array_column($data['data'],'monthly_price');
                    $yearly_price_array = array_column($data['data'],'yearly_price');
                    $currency_sign_array = array_column($data['data'],'currency_sign');
                    
                 } @endphp

                <div class="col-md-7 col-12 px-md-0">
                    <div class="row justify-content-between">
                        @php foreach ($plan_name_array as $key => $value) { @endphp
                            <div class="col-md-3 col-4">
                                <button class="btn font-20 bg-transparent border-white text-white p-2 px-xl-5 px-0 box-radius col-12">{{$value}}</button>
                            </div>    
                        @php } @endphp
                        

                    </div>
                </div>
            </div>
            <div class="row my-4 justify-content-around">
                <div class="col-md-4 font-25 text-white"></div>
                <div class="col-md-7 text-center"><div class="btn font-20 bg-transparent fw-bold text-white px-4 price_label" >{{ __('message.plan.monthly_price') }}</div></div>
            </div>
            <div class="row my-4 justify-content-around">
                <div class="col-md-4 font-25 text-white"></div>
                <div class="col-md-7 px-md-0 col-11 box-radius bg-white">
                    <div class="row justify-content-between monthly_price_div">
                       @php foreach ($monthly_price_array as $key => $value) { @endphp 
                            <div class="col-md-3 col-4">
                                <span class="btn px-3 px-lg-5 font-20 p-2 fw-bold">{{$currency_sign_array[$key]}}{{$value}}</span>
                            </div>
                        @php } @endphp
                    </div>

                    <div class="row justify-content-between yearly_price_div" style="display:none;">
                        @php foreach ($yearly_price_array as $key => $value) { @endphp 
                            <div class="col-md-3 col-4">
                                <span class="btn px-3 px-lg-5 font-20 p-2 fw-bold">{{$currency_sign_array[$key]}}{{$value}}</span>
                            </div>
                        @php } @endphp
                    </div>
                </div>
            </div>
            <div class="row my-4 justify-content-around align-items-center">
                <div class="col-md-4 ps-xl-5">
                    <ul class="list-unstyled abut-payment-plan">
                        @php 
                        if(!empty($data['data']) && count($data['data'])>0){
                            foreach ($data['data'] as $key => $value) {
                                if($key == 0){
                                    foreach ($value['attribute'] as $akey => $avalue) { 
                        @endphp
                        
                            <li class="font-20 text-white p-3">{{ $avalue['alias'] }}</li>            
                        
                        @php } } } } @endphp

                    </ul>
                </div>

                <div class="col-md-7 col-12 px-md-0">
                    <div class="row justify-content-between">
                        @php 
                        if(!empty($data['data']) && count($data['data'])>0){
                            foreach ($data['data'] as $key => $value) {
                               
                        @endphp
                        <div class="col-md-3 col-4 px-md-0 ">
                            <div class="row justify-content-center">
                                <div class="col-10 bg-white box-radius">
                                    <ul class="list-unstyled text-center ps-0">
                                        <?php $available = ''; foreach ($value['attribute'] as $akey => $avalue) {

                                            if($avalue['value'] == 1 && ($avalue['name'] == 'hd_available' || $avalue['name'] == 'ultra_hd_available' || $avalue['name'] == 'watch_lp_tv' || $avalue['name'] == 'watch_mp_t'|| $avalue['name'] == 'cancel'|| $avalue['name'] == 'unlimited_m_s')){

                                                $available = '<i class="fa fa-check p-2 show-about-plan-list" aria-hidden="true"></i>';
                                            }elseif ($avalue['name'] == 'screens') {
                                                $available = '<span class="p-2 show-about-plan-text font-18 fw-bold">'.$avalue['value'].'</span>';
                                            }else{
                                                $available = '<i class="fa fa-times p-2 show-about-plan-close" aria-hidden="true"></i>';
                                            }
                                            
                                        ?>
                                            <li class="p-3"><?php echo $available; ?></li>
                                        <?php  } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                         @php  } }  @endphp

                    </div>
                </div>
            </div>

            <?php 

                if(Session::get('app_string')){ 
                    $btn_name = Session::get('app_string.subscription.buy_now'); 
                }else{ 
                    $btn_name = CommonHelper::multi_language('subscription','buy_now')->multi_language_value->language_value; 
                }


                if(!empty($user['plan_id']) && $user['plan_id'] == 1 &&  !empty($user['plan_type']) &&  $user['plan_type'] == 1){
                    $basic_btn = '<button disabled="disabled" class="btn border-white text-white p-2 px-xl-5 px-0 box-radius col-12 plan_buy_btn" id = "basic_btn" data-plan-id="1">'.$btn_name.'</button>';
                }else{
                    $basic_btn = '<button class="btn border-white text-white p-2 px-xl-5 px-0 box-radius col-12 bg-color plan_buy_btn"  id = "basic_btn"  data-plan-id="1">'.$btn_name.'</button>';
                }

                if (!empty($user['plan_id']) && $user['plan_id'] == 2 && !empty($user['plan_type']) &&  $user['plan_type'] == 1) {
                    $sta_btn = '<button disabled="disabled"  class="btn border-white text-white p-2 px-xl-5 px-0 box-radius col-12 plan_buy_btn"  id = "sta_btn" data-plan-id="2">'.$btn_name.'</button>';
                }else{
                    $sta_btn = '<button class="btn border-white text-white p-2 px-xl-5 px-0 box-radius col-12 bg-color plan_buy_btn" id = "sta_btn" data-plan-id="2">'.$btn_name.'</button>';
                }

                if (!empty($user['plan_id']) && $user['plan_id'] == 3 && !empty($user['plan_type']) &&  $user['plan_type'] == 1) {
                    $pre_btn = '<button disabled="disabled" id = "pre_btn" class="btn border-white text-white p-2 px-xl-5 px-0 box-radius col-12 plan_buy_btn" data-plan-id="3">'.$btn_name.'</button>';
                }else{
                    $pre_btn = '<button  id = "pre_btn" class="btn border-white text-white p-2 px-xl-5 px-0 box-radius col-12 bg-color plan_buy_btn" data-plan-id="3">'.$btn_name.'</button>';
                }
            ?>
            <div class="row mt-md-5 my-4 justify-content-around">
                <div class="col-md-4 font-25 text-white"></div>
                <div class="col-md-7 col-12 px-md-0">
                    <div class="row justify-content-between">
                        <div class="col-md-3 col-4"><?php echo $basic_btn; ?></div>
                        <div class="col-md-3 col-4 text-center"><?php echo $sta_btn; ?></div>
                        <div class="col-md-3 col-4 text-end"><?php echo $pre_btn; ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="choose-plan-overview-for-mobile my-5">
            <div class="row mt-md-5 my-2 pt-2 justify-content-around">
                            <div class="col-md-4 font-25 text-white mb-md-0 mb-4"><a href="{{ url()->previous() }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> {{ __('message.plan.choose_plan') }}</div>

                             @php if(!empty($data['data']) && count($data['data'])>0){ 
                                $plan_name_array = array_column($data['data'],'name');
                                $monthly_price_array = array_column($data['data'],'monthly_price');
                                $yearly_price_array = array_column($data['data'],'yearly_price');
                                $currency_sign_array = array_column($data['data'],'currency_sign');
                                
                             } @endphp

                        </div>
            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row mt-0 my-3 justify-content-around">
                            <div class="col-md-4 font-25 text-white"></div>
                            <div class="col-md-7 text-center"><div class="btn font-20 bg-transparent fw-bold text-white px-4 price_label" >{{ __('message.plan.monthly_price') }}</div></div>
                        </div>
                        
                        <div class="row justify-content-center monthly_price_div mx-0 mb-5">
                           @php foreach ($monthly_price_array as $key => $value) { @endphp 
                                <div class="col-sm-4 col-6 box-radius bg-white">
                                    <span class="btn px-lg-5 font-20 p-2 fw-bold">{{$currency_sign_array[$key]}}{{$value}}</span>
                                </div>
                            @php } @endphp
                        </div>

                        <div class="row justify-content-center mx-0 mb-5 yearly_price_div" style="display:none;">
                            @php foreach ($yearly_price_array as $key => $value) { @endphp 
                                <div class="col-sm-4 col-6 box-radius bg-white">
                                    <span class="btn px-lg-5 font-20 p-2 fw-bold">{{$currency_sign_array[$key]}}{{$value}}</span>
                                </div>
                            @php } @endphp
                        </div>

                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">HD Available</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3 plan-top-radius">
                               <i class="fa fa-times p-2 show-about-plan-close" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Ultra HD Available</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3">
                              <i class="fa fa-times p-2 show-about-plan-close" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Watch on your laptop and TV</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3">
                              <i class="fa fa-check p-2 show-about-plan-list" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Watch on your Mobile Phones & Tablets</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3">
                               <i class="fa fa-check p-2 show-about-plan-list" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Screens you can watch on at same time</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3">
                                <i class="fa fa-times p-2 show-about-plan-close" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Unlimited movies and TV shows</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3 plan-bottom-radius">
                               <i class="fa fa-check p-2 show-about-plan-list" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-center mx-0 mt-5">
                            <div class="col-sm-3 col-6">
                                <a href="payment.php"><button class="btn bg-color text-white p-2 px-xl-5 px-0 box-radius col-12">Buy Now</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row mt-0 my-3 justify-content-around">
                            <div class="col-md-4 font-25 text-white"></div>
                            <div class="col-md-7 text-center"><div class="btn font-20 bg-transparent fw-bold text-white px-4 price_label" >{{ __('message.plan.monthly_price') }}</div></div>
                        </div>
                        <div class="row justify-content-center monthly_price_div mx-0 mb-5">
                                <div class="col-sm-4 col-6 box-radius bg-white">
                                    <span class="btn px-lg-5 font-20 p-2 fw-bold">{{$currency_sign_array[$key]}} {{$value}}</span>
                                </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">HD Available</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3 plan-top-radius">
                               <i class="fa fa-times p-2 show-about-plan-close" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Ultra HD Available</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3">
                              <i class="fa fa-times p-2 show-about-plan-close" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Watch on your laptop and TV</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3">
                              <i class="fa fa-check p-2 show-about-plan-list" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Watch on your Mobile Phones & Tablets</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3">
                               <i class="fa fa-check p-2 show-about-plan-list" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Screens you can watch on at same time</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3">
                                <i class="fa fa-times p-2 show-about-plan-close" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Unlimited movies and TV shows</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3 plan-bottom-radius">
                               <i class="fa fa-check p-2 show-about-plan-list" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-center mx-0 mt-5">
                            <div class="col-sm-3 col-6">
                                <a href="payment.php"><button class="btn bg-color text-white p-2 px-xl-5 px-0 box-radius col-12">Buy Now</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row mt-0 my-3 justify-content-around">
                            <div class="col-md-4 font-25 text-white"></div>
                            <div class="col-md-7 text-center"><div class="btn font-20 bg-transparent fw-bold text-white px-4 price_label" >{{ __('message.plan.monthly_price') }}</div></div>
                        </div>
                        <div class="row justify-content-center monthly_price_div mx-0 mb-5">
                                <div class="col-sm-4 col-6 box-radius bg-white">
                                    <span class="btn px-lg-5 font-20 p-2 fw-bold">{{$currency_sign_array[$key]}} {{$value}}</span>
                                </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">HD Available</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3 plan-top-radius">
                               <i class="fa fa-times p-2 show-about-plan-close" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Ultra HD Available</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3">
                              <i class="fa fa-times p-2 show-about-plan-close" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Watch on your laptop and TV</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3">
                              <i class="fa fa-check p-2 show-about-plan-list" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Watch on your Mobile Phones & Tablets</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3">
                               <i class="fa fa-check p-2 show-about-plan-list" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Screens you can watch on at same time</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3">
                                <i class="fa fa-times p-2 show-about-plan-close" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-start align-items-center mx-0">
                            <div class="col-9 ps-xl-5">
                                <div class="text-white p-2">Unlimited movies and TV shows</div>
                            </div>
                             <div class="col-sm-2 col-3 px-md-0 bg-white text-center p-3 plan-bottom-radius">
                               <i class="fa fa-check p-2 show-about-plan-list" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row justify-content-center mx-0 mt-5">
                            <div class="col-sm-3 col-6">
                                <a href="payment.php"><button class="btn bg-color text-white p-2 px-xl-5 px-0 box-radius col-12">Buy Now</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
    </div>

    <form class="row justify-content-center" action="{{ route('payment.method')}}" method="post" >
        @csrf()

        <input type="hidden" name="plan_type" value="1" id="plan_type">
        <input type="hidden" name="plan_id" value="" id="plan_id">

    </form>


</section>

<script type="text/javascript">
    $("#monthly_btn").click(function(){
        $("#monthly_btn").addClass('btn-white text-white bg-color');
        $("#yearly_btn").removeClass('btn-white text-white bg-color');
        $(".price_label").empty().html("{{ __('message.plan.monthly_price') }}");
        $(".monthly_price_div").css('display','');
        $(".yearly_price_div").css('display','none');

        $('#plan_type').val(1);


        var plan_id ='{{ !empty($user["plan_id"])?$user["plan_id"]:"" }}';
        var plan_type = '{{ !empty($user["plan_type"])?$user["plan_type"]:"" }}';

        if(plan_id ==1 && plan_type == 1){
            $('#basic_btn').attr('disabled','disabled');
            $("#basic_btn").removeClass('bg-color');
        }else{
            $('#basic_btn').removeAttr('disabled');
            $("#basic_btn").addClass('bg-color');
        }

        if(plan_id ==2 && plan_type == 1){
            $('#sta_btn').attr('disabled','disabled');
            $("#sta_btn").removeClass('bg-color');
        }else{
            $('#sta_btn').removeAttr('disabled');
            $("#sta_btn").addClass('bg-color');
        }

        if(plan_id ==3 && plan_type == 1){
            $('#pre_btn').attr('disabled','disabled');
            $("#pre_btn").removeClass('bg-color');
        }else{
            $('#pre_btn').removeAttr('disabled');
            $("#pre_btn").addClass('bg-color');
        }

    });

    $("#yearly_btn").click(function(){
        $("#yearly_btn").addClass('btn-white text-white bg-color');
        $("#monthly_btn").removeClass('btn-white text-white bg-color');
        $(".price_label").empty().html('{{ __("message.plan.yearly_price") }}');
        $(".monthly_price_div").css('display','none');
        $(".yearly_price_div").css('display','');

        $('#plan_type').val(2);

        var plan_id ='{{ !empty($user["plan_id"])?$user["plan_id"]:"" }}';
        var plan_type = '{{ !empty($user["plan_type"])?$user["plan_type"]:"" }}';

        if(plan_id ==1 && plan_type == 2){
            $('#basic_btn').attr('disabled','disabled');
            $("#basic_btn").removeClass('bg-color');
        }else{
            $('#basic_btn').removeAttr('disabled');
            $("#basic_btn").addClass('bg-color');
        }

        if(plan_id ==2 && plan_type == 2){
            $('#sta_btn').attr('disabled','disabled');
            $("#sta_btn").removeClass('bg-color');
        }else{
            $('#sta_btn').removeAttr('disabled');
            $("#sta_btn").addClass('bg-color');
        }

        if(plan_id ==3 && plan_type == 2){
            $('#pre_btn').attr('disabled','disabled');
            $("#pre_btn").removeClass('bg-color');
        }else{
            $('#pre_btn').removeAttr('disabled');
            $("#pre_btn").addClass('bg-color');
        }
    });

    $(".plan_buy_btn").click(function(){

        var phone_verified = "{{Session::get('user_data.phone_verified')}}";
               
        if(phone_verified == 0){
            $('#update_profile_popup').modal('show');
            return false;
        }
        
        $('#plan_id').val($(this).attr('data-plan-id'));

        $("form:first").submit();
    });
</script>

@endsection
