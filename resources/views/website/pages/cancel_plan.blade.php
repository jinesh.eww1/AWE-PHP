@extends('website.layouts.master')
@section('content')

@include('website.include.flash-message')

<section>
    <div class="container">
        <div class="mt-md-5 pt-5 py-4">
            <a href="{{ route('view.plan') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.subscription'); }else{ echo CommonHelper::multi_language('settings','subscription')->multi_language_value->language_value; } ?></span>
        </div>
        
        <div class="row justify-content-center mt-5">
            <div class="col-lg-4 col-md-6 col-12 bg-subscrip bg-white">
                <div class="mt-2">
                    <div class="font-23 text-yellow mt-5 p-2 text-center"><?php if(Session::get('app_string')){ echo Session::get('app_string.subscription.plan_cancelled'); }else{ echo CommonHelper::multi_language('subscription','plan_cancelled')->multi_language_value->language_value; } ?></div>
                </div>

                <div class="mt-5 mb-5 text-center">
                    <a href="{{ route('home','movie') }}" class="btn text-white bg-color py-2 px-5 font-18 box-radius"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.home'); }else{ echo CommonHelper::multi_language('home','home')->multi_language_value->language_value; } ?></a>
                    <a href="{{ route('all.plans') }}" class="btn text-white bg-color py-2 px-5 font-18 box-radius" style="margin-left: 20px !important;"><?php if(Session::get('app_string')){ echo Session::get('app_string.subscription.see_all_plans'); }else{ echo CommonHelper::multi_language('subscription','see_all_plans')->multi_language_value->language_value; } ?></a>
                </div>
            </div>
        </div>
 


    </div>
</section>

@endsection