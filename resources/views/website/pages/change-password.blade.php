@extends('website.layouts.master')
@section('content')
    @include('website.include.flash-message')
    <style type="text/css">
        .parsley-errors-list {
            text-align: left;
        }
    </style>
    <section>
        <div class="container">
            <div class="mt-md-5 py-5">
                <a href="{{ route('profile') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a>
            </div>
            <div class="text-center">
                <div>
                    <img class="img-fluid" src="{{ asset('assets/website/image/change-password.png') }}" alt="">
                </div>
                <h6 class="text-white mt-4 py-3"><?php if (Session::get('app_string')) {
                    echo Session::get('app_string.settings.change_password');
                } else {
                    echo CommonHelper::multi_language('settings', 'change_password')->multi_language_value->language_value;
                } ?></h6>
                <div class="text-forgot"><?php if (Session::get('app_string')) {
                    echo Session::get('app_string.change_password.new_previously_password_different');
                } else {
                    echo CommonHelper::multi_language('change_password', 'new_previously_password_different')->multi_language_value->language_value;
                } ?></div>
                <form method="post" action="{{ route('user-change-password') }}" class="password-border my-5">
                    @csrf()
                    <div class="d-flex justify-content-center my-4">
                        <div class="password-box-width position-relative">
                            <input class="form-control bg-transparent text-white font-14" data-parsley-length="[8, 15]"maxlength="15"
                                required type="password" name="current_password" id="current_password"
                                placeholder="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.current_pass_label');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'current_pass_label')->multi_language_value->language_value;
                                } ?>"
                                data-parsley-required-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.current_password');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'current_password')->multi_language_value->language_value;
                                } ?>" 
                                data-parsley-length-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.current_password_min_length');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'current_password_min_length')->multi_language_value->language_value;
                                } ?>"
                                >
                            <span class="old_hide_pass"><img class="img-fluid"
                                    src="{{ asset('assets/website/image/Icon-Hide.png') }}" alt=""></span>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center my-4">
                        <div class="password-box-width position-relative">
                            <input class="form-control bg-transparent text-white font-14" data-parsley-length="[8, 15]"
                            maxlength="15"
                                required type="password" name="password" placeholder="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.new_password_label');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'new_password_label')->multi_language_value->language_value;
                                } ?>"
                                id="password_field" data-parsley-required data-parsley-notequalto="#current_password"
                                data-parsley-notequalto-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.Current_new_password_different');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'Current_new_password_different')->multi_language_value->language_value;
                                } ?>"
                                data-parsley-required-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.new_password');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'new_password')->multi_language_value->language_value;
                                } ?>" 
                                data-parsley-length-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.new_password_min_length');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'new_password_min_length')->multi_language_value->language_value;
                                } ?>">
                            <span class="hide_pass"><img class="img-fluid"
                                    src="{{ asset('assets/website/image/Icon-Hide.png') }}" alt=""></span>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center my-4">
                        <div class="password-box-width position-relative">
                            <input class="form-control bg-transparent text-white font-14" data-parsley-length="[8, 15]"maxlength="15"
                                required type="password" name="password_confirmation" placeholder="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.confirm_password_label');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'confirm_password_label')->multi_language_value->language_value;
                                } ?>"
                                data-parsley-equalto="#password_field" id="password_confirmation"
                                data-parsley-required-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.confirm_password');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'confirm_password')->multi_language_value->language_value;
                                } ?>" 
                                data-parsley-length-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.confirm_password_min_length');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'confirm_password_min_length')->multi_language_value->language_value;
                                } ?>"
                                data-parsley-equalto-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.change_password.new_password_confirm_password_same');
                                } else {
                                    echo CommonHelper::multi_language('change_password', 'new_password_confirm_password_same')->multi_language_value->language_value;
                                } ?>"
                                >
                            <span class="con_hide_pass"> <img class="img-fluid"
                                    src="{{ asset('assets/website/image/Icon-Hide.png') }}" alt=""></span>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center my-4">
                        <button type="submit"
                            class="btn bg-color p-3 fw-bold text-white font-23 box-radius password-box-width"><?php if (Session::get('app_string')) {
                                echo Session::get('app_string.account_profile.save');
                            } else {
                                echo CommonHelper::multi_language('account_profile', 'save')->multi_language_value->language_value;
                            } ?></button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
