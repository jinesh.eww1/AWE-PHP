@extends('website.layouts.master')
@section('content')

<style type="text/css">  
input::placeholder {
  color: #FFFFFF;
  opacity: 0.5;
}
.chat-inbox {
  max-height: 80vh;
  overflow-y: auto;
}
.mb-5{
margin-bottom: 20rem!important;
}
</style>

    <section class="bg-back">
      <div class="container">
        <div class="row">
          <div class="chat-box d-flex align-items-end mx-auto height-full pb-4 text-white position-relative"> 
            <div class="w-100">
              <a href="{{ route('profile')}}" class="text-decoration-none font-18 position-absolute top-0 py-4 ps-3 text-white w-100 about-chat-back"><img src="{{ asset('assets/website/image/back-button.svg') }}" class="img-fluid me-3" alt="Go Back to Previous Page" width="24" height="24"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.chat_support'); }else{ echo CommonHelper::multi_language('settings','chat_support')->multi_language_value->language_value; } ?></a>
              <div class="position-relative">
                <div class="chat-inbox msg_history ">

                  <?php $user_id = Session::get('user_data.id');
                  
                    if(isset($data['data']) && count($data['data'])>0){ 
                      foreach (array_reverse($data['data']) as $key => $value) { 
                        if($value['receiver_id'] == $user_id) {  ?>
                            <div class="w-75">
                              <div class="bg-gray">
                                <p class="mb-0 text-break">{{ $value['message'] }}</p>
                              </div>
                              <p class="mt-2 poppin">{{ $value['created_date_time'] }}</p>
                            </div>
                        <?php }else{ ?>
                            <div class="w-75 ms-auto">
                              <div class="bg-yellow">
                                <p class="mb-0 text-break">{{ $value['message'] }}</p>
                              </div>
                              <p class="text-end mt-2 poppin">{{ $value['created_date_time'] }}</p>
                            </div>
                  <?php } } }else{ ?> 
                      <div class="text-center chat_img_fix">
                        <img class="mb-1" src="{{ asset('assets/website/image/card/AWE_NothingToShow_IMAGE.png') }}">
                        <p class="mt-5"><?php if(Session::get('app_string')){ echo Session::get('app_string.chat_support.no_chat_available'); }else{ echo CommonHelper::multi_language('chat_support','no_chat_available')->multi_language_value->language_value; } ?></p>
                      </div>
                  <?php } ?>

                </div>
               
              </div>

              <div class="footer-chat">
                
                <input class="font-regular input_box bg-back font-18 write_msg" type="text" name="message" placeholder="<?php if(Session::get('app_string')){ echo Session::get('app_string.chat_support.type_msg'); }else{ echo CommonHelper::multi_language('chat_support','type_msg')->multi_language_value->language_value; } ?>">
                <button type="button" class="btn_send msg_send_btn"><img src="{{ asset('assets/website/image/send.svg') }}" class="img-fluid bg-black" alt="Press to Send" width="25"></button>
                 
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


<script src="https://cdn.socket.io/4.5.0/socket.io.min.js" integrity="sha384-7EyYLQZgWBi67fBtVxw60/OWl1kjsfrPFcaU0pp0nAh+i8FD068QogUvg85Ewy1k" crossorigin="anonymous"></script>


<script type="text/javascript">

  var socket = io("https://awemovies.com:3000/");
  var selected_user = `<?php echo (isset($user_id))?$user_id:0; ?>`;
  var BASEURL = `<?php echo url('/chat-support'); ?>`;
  //console.log(selected_user); 

  socket.on("connect", function() {
      var id ='{{ Session::get("user_data.id")}}';
      socket.emit('connect_user', { user_id: id});
      console.log("User connetcted");
  });
  
  
$(document).ready(function() {

  $('.msg_history').scrollTop($('.msg_history')[0].scrollHeight);



  socket.on('receiver_message', function(data) {
   
    if(data.send_by == 'me'){

        var html = '<div class="w-75 ms-auto"><div class="bg-yellow"><p class="mb-0 text-break">'+ data.message +'</p></div><p class="text-end mt-2 poppin">'+data.created_date_time+'</p></div>';
        $('.msg_history').append(html);
        $('.write_msg').val('');
        
    }else{

      var html = '<div class="w-75"><div class="bg-gray"><p class="mb-0 text-break">'+ data.message +'</p></div><p class="mt-2 poppin">'+data.created_date_time+'</p></div>';
        $('.msg_history').append(html);
        $('.write_msg').val('');
        
    }

    $('.msg_history').scrollTop($('.msg_history')[0].scrollHeight);

  });


  var callback = function() {
    
    var message_val = $.trim($('.write_msg').val());
    var user_id = `<?php echo (isset($user_id))?$user_id:0; ?>`;

    if(message_val != '' && user_id !=''){
      user_id = parseInt(user_id);
      socket.emit('send_message', { sender_id: user_id, receiver_id:1, message: message_val});
      $(".chat_img_fix").remove();
    }else{
      $('.write_msg').val('');
    }

  };


  $("input").keypress(function() {
      if (event.which == 13) callback();
  });

  $('.msg_send_btn').click(callback);

});

  var ENDPOINT = "{{ url('/') }}";
  var page = 1;
  var last_page = "{{ !empty($data['last_page'])?$data['last_page']:1 }}";
  $('.chat-inbox').scroll(function () {
      if ($('.chat-inbox').scrollTop() == 0 && page < last_page) {
        page++;
        appendData();
      }
  });
  function appendData() {
    $('#loader').show();
    $.ajax({
        url: ENDPOINT + "/chat-support?page=" + page,
        datatype: "json",
        type: "get",
    })
    .done(function (response) {
      $('#loader').hide();
      if(response.status == true){
        var html = '';

          $.each(response.data.data.reverse(), function (key, val) {
              if(val.sender_id == "{{Session::get('user_data.id')}}"){
                html +='<div class="w-75 ms-auto"><div class="bg-yellow"><p class="mb-0 text-break">'+ val.message +'</p></div><p class="text-end mt-2 poppin">'+val.created_date_time+'</p></div>'; 
              }else{
                html += '<div class="w-75"><div class="bg-gray"><p class="mb-0 text-break">'+ val.message +'</p></div><p class="mt-2 poppin">'+val.created_date_time+'</p></div>';
              }
          });

          $(".msg_history").prepend(html);
          $(".msg_history").scrollTop(parseInt(response.data.data.length)*67)

      }
      
    })
    .fail(function (jqXHR, ajaxOptions, thrownError) {
        console.log('Server error occured');
    });

  }
</script>

@endsection