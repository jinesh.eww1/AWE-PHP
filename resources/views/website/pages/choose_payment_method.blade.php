    @extends('website.layouts.master')
@section('content')

<link rel="stylesheet" href="{{ asset('assets/website/css/addons.css') }}" />

<style>
    .parsley-errors-list.filled{
        margin-bottom: 5px;
        margin-top: 0px;
    }
</style>
<section>
    <div class="container">
        <div class="mt-md-5 py-5">
            <a href="{{ route('all.plans') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.choose_payment_method.choose_payment_method'); }else{ echo CommonHelper::multi_language('choose_payment_method','choose_payment_method')->multi_language_value->language_value; } ?></span>
        </div>
        <div class="row justify-content-center mt-5">
            <div class="col-md-4 mb-md-0 mb-5 d-flex d-md-block nav nav-tabs"  id="nav-tab" role="tablist">
                <div class="btn bg-white box-radius width-pay p-3 mb-0 mb-md-4 ms-3 position-relative active">
                    <a href="javascript:void(0)" id="nav-home-tab" data-toggle="tab" role="tab" aria-controls="nav-home" aria-selected="true">
                        <div class="p-1 px-3 text-start payment-arrow-icon">
                            <img class="img-fluid" src="{{ asset('assets/website/image/stripe.svg') }}" alt="">
                        </div>
                    </a>
                </div>
                <div class="btn bg-white box-radius width-pay p-3 mt-0 mt-md-2 ms-3 position-relative">
                    <form class="row justify-content-center" id="paypal_plan_form" action="{{ route('subscription.plan.overview')}}" method="post" >
                        @csrf()

                        <input type="hidden" name="plan_type" value="{{ $post_data['plan_type'] }}" id="plan_type">
                        <input type="hidden" name="plan_id" value="{{ $post_data['plan_id'] }}" id="plan_id">
                        <input type="hidden" name="payment_method" value="paypal" id="payment_method">

                        <a class="paypal_plan_buy_btn" href="javascript:void(0)" id="nav-profile-tab" data-toggle="tab" role="tab" aria-controls="nav-profile" aria-selected="false">
                            <div class="p-1 px-md-3 text-start">
                                <img class="img-fluid" src="{{ asset('assets/website/image/paypal.svg') }}" alt="">
                            </div>
                        </a>

                    </form>
                </div>
            </div>
            <div class="col-md-8 col-11">
                    <div class="bg-white box-radius tab-content  mb-md-5 pb-md-5" id="nav-tabContent">
                        <div class="tab-pane active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="px-4 pt-4">
                                <img class="img-fluid" src="{{ asset('assets/website/image/stripe.png') }}" alt="">
                            </div>
                            <div class="row">
                                <div class="col-xl-7 col-lg-7 col-12">
                                    <form class="row justify-content-center" id="stripe_plan_form"
                                        action="{{ route('subscription.plan.overview') }}" method="post">
                                        @csrf()

                                        <input type="hidden" name="selected_card" value="0" id="selected_card">
                                        <input type="hidden" name="plan_type" value="{{ $post_data['plan_type'] }}"
                                            id="plan_type">
                                        <input type="hidden" name="plan_id" value="{{ $post_data['plan_id'] }}"
                                            id="plan_id">
                                        <input type="hidden" name="payment_method" value="stripe" id="payment_method">
                                        <input type="hidden" name="card_name" value="" id="card_name">

                                        <div class="p-4">
                                            <div class="card-payment mt-4">
                                                <input class="form-control font-verify" type="text" required
                                                    name="name" placeholder="<?php if (Session::get('app_string')) {
                                                        echo Session::get('app_string.choose_payment_method.card_holder_name');
                                                    } else {
                                                        echo CommonHelper::multi_language('choose_payment_method', 'card_holder_name')->multi_language_value->language_value;
                                                    } ?>"
                                                    data-parsley-length="[2, 20]" maxlength="20"data-parsley-pattern="^[a-zA-Z\s]*$"
                                                    data-parsley-required-message="<?php if (Session::get('app_string')) {
                                                        echo Session::get('app_string.choose_payment_method.please_enter_card_holder_name');
                                                    } else {
                                                        echo CommonHelper::multi_language('choose_payment_method','please_enter_card_holder_name')->multi_language_value->language_value;
                                                    } ?>" 
                                                    data-parsley-length-message="<?php if (Session::get('app_string')) {
                                                        echo Session::get('app_string.choose_payment_method.card_holder_min_validation');
                                                    } else {
                                                        echo CommonHelper::multi_language('choose_payment_method', 'card_holder_min_validation')->multi_language_value->language_value;
                                                    } ?>"
                                                    data-parsley-pattern-message="<?php if (Session::get('app_string')) {
                                                        echo Session::get('app_string.choose_payment_method.please_enter_valid_card_holder_name');
                                                    } else {
                                                        echo CommonHelper::multi_language('choose_payment_method', 'please_enter_valid_card_holder_name')->multi_language_value->language_value;
                                                    } ?>"
                                                    
                                                    >
                                            </div>
                                            <div class="card-payment mt-3">
                                                <input class="form-control font-verify" data-parsley-type="number"maxlength="16"onkeypress='validate(event)'
                                                    type="text" required name="number" placeholder="<?php if (Session::get('app_string')) {
                                                        echo Session::get('app_string.choose_payment_method.card_number');
                                                    } else {
                                                        echo CommonHelper::multi_language('choose_payment_method', 'card_number')->multi_language_value->language_value;
                                                    } ?>"
                                                   
                                                    data-parsley-required-message="<?php if (Session::get('app_string')) {
                                                        echo Session::get('app_string.choose_payment_method.please_enter_card_number');
                                                    } else {
                                                        echo CommonHelper::multi_language('choose_payment_method', 'please_enter_card_number')->multi_language_value->language_value;
                                                    } ?>" 
                                                    
                                                    data-parsley-type-message="<?php if (Session::get('app_string')) {
                                                        echo Session::get('app_string.choose_payment_method.please_enter_valid_card_number');
                                                    } else {
                                                        echo CommonHelper::multi_language('choose_payment_method', 'please_enter_valid_card_number')->multi_language_value->language_value;
                                                    } ?>"
                                                    >
                                            </div>
                                            <div class="card-payment mt-3">
                                                <input class="form-control font-verify" type="text" required
                                                    name="exp_month_year" placeholder="<?php if (Session::get('app_string')) {
                                                        echo Session::get('app_string.choose_payment_method.expiry_date_place_holder');
                                                    } else {
                                                        echo CommonHelper::multi_language('choose_payment_method', 'expiry_date_place_holder')->multi_language_value->language_value;
                                                    } ?>"
                                                    id="datepicker" 
                                                    data-parsley-required-message="<?php if (Session::get('app_string')) {
                                                        echo Session::get('app_string.choose_payment_method.please_enter_date');
                                                    } else {
                                                        echo CommonHelper::multi_language('choose_payment_method', 'please_enter_date')->multi_language_value->language_value;
                                                    } ?>"
                                                    >
                                            </div>
                                            <div class="card-payment mt-3">
                                                <input class="form-control font-verify" data-parsley-type="number"
                                                    type="password" required name="cvv" onkeypress='validate(event)'
                                                    minlength="3" maxlength="3" data-parsley-length="[3, 3]"
                                                    placeholder="<?php if (Session::get('app_string')) {
                                                        echo Session::get('app_string.choose_payment_method.security_code');
                                                    } else {
                                                        echo CommonHelper::multi_language('choose_payment_method', 'security_code')->multi_language_value->language_value;
                                                    } ?>"
                                                    data-parsley-required-message="<?php if (Session::get('app_string')) {
                                                        echo Session::get('app_string.choose_payment_method.please_enter_card_security_number');
                                                    } else {
                                                        echo CommonHelper::multi_language('choose_payment_method', 'please_enter_card_security_number')->multi_language_value->language_value;
                                                    } ?>"
                                                    data-parsley-type-message="<?php if (Session::get('app_string')) {
                                                        echo Session::get('app_string.payment_method.enter_valid_cvv');
                                                    } else {
                                                        echo CommonHelper::multi_language('payment_method', 'enter_valid_cvv')->multi_language_value->language_value;
                                                    } ?>"
                                                    data-parsley-length-message="<?php if (Session::get('app_string')) {
                                                        echo Session::get('app_string.choose_payment_method.please_enter_valid_card_security_number');
                                                    } else {
                                                        echo CommonHelper::multi_language('choose_payment_method', 'please_enter_valid_card_security_number')->multi_language_value->language_value;
                                                    } ?>"
                                                    >
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-xl-5 col-lg-5 col-12">
                                    <div class="p-xl-4 p-lg-0 p-4">

                                        <?php if($api_data['status'] == true){ ?>

                                        <div class="d-flex justify-content-between align-items-center mt-md-4">
                                            <div>
                                                <?php if($api_data['data']['brand'] == 'visa'){ ?>
                                                <img class="img-fluid p-2 card-pay-border card_image"
                                                    src="{{ asset('assets/website/image/visa-card.png') }}"
                                                    alt="">
                                                <?php }elseif ($api_data['data']['brand'] == 'mastercard') { ?>
                                                <img class="img-fluid p-2 card-pay-border card_image"
                                                    src="{{ asset('assets/website/image/master-card.png') }}"
                                                    alt="">
                                                <?php }elseif ($api_data['data']['brand'] == 'amex') { ?>
                                                <img class="img-fluid p-2 card-pay-border card_image" height="30px"
                                                    width="10px"
                                                    src="{{ asset('assets/website/image/card/Amex.png') }}"
                                                    alt="">
                                                <?php }elseif ($api_data['data']['brand'] == 'diners') { ?>
                                                <img class="img-fluid p-2 card-pay-border card_image"
                                                    src="{{ asset('assets/website/image/card/Diners_Club.png') }}"
                                                    alt="">
                                                <?php }elseif ($api_data['data']['brand'] == 'discover') { ?>
                                                <img class="img-fluid p-2 card-pay-border card_image"
                                                    src="{{ asset('assets/website/image/card/Disciver.png') }}"
                                                    alt="">
                                                <?php }elseif ($api_data['data']['brand'] == 'jcb') { ?>
                                                <img class="img-fluid p-2 card-pay-border card_image"
                                                    src="{{ asset('assets/website/image/card/jcb.png') }}"
                                                    alt="">
                                                <?php }elseif ($api_data['data']['brand'] == 'unionpay') { ?>
                                                <img class="img-fluid p-2 card-pay-border card_image"
                                                    src="{{ asset('assets/website/image/card/unionpay-svgrepo-com.png') }}"
                                                    alt="">
                                                <?php } ?>

                                                <span class="card-pay-color ms-md-3 ms-0">**** **** ****
                                                    {{ $api_data['data']['account_number_last4'] }}</span>
                                            </div>
                                            <div>
                                                <label class="card-pay-radio">
                                                    <input type="checkbox" name="radio" id="existing_card">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="text-center mt-4">
                        <button class="btn profile-save-btn text-white bg-color font-20 fw-bold p-3 my-4 box-radius"
                            id="stripe_payment_btn" type="button"><?php if (Session::get('app_string')) {
                                echo Session::get('app_string.choose_payment_method.pay_now');
                            } else {
                                echo CommonHelper::multi_language('choose_payment_method', 'pay_now')->multi_language_value->language_value;
                            } ?></button>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
        $("#datepicker").datepicker( {
            startDate: '-0m',
            format: "mm/yy",
            viewMode: "months", 
            minViewMode: "months"
        });
    function validate(evt) {
        var theEvent = evt || window.event;

        // Handle paste
        if (theEvent.type === 'paste') {
            key = event.clipboardData.getData('text/plain');
        } else {
            // Handle key press
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key)
    ;
        }
        var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }

        $("#existing_card").click(function() {

            if ($("#existing_card").prop('checked') == true) {
                $('input[name=name]').removeAttr('required').attr('readonly', 'readonly').val('');
                $('input[name=number]').removeAttr('required').attr('readonly', 'readonly').val('');
                $('input[name=exp_month_year]').removeAttr('required').attr('readonly', 'readonly').val('');
                $('input[name=cvv]').removeAttr('required').attr('readonly', 'readonly').val('');
                $('#selected_card').val(1);

            } else {
                $('input[name=name]').attr('required', 'required').removeAttr('readonly');
                $('input[name=number]').attr('required', 'required').removeAttr('readonly');
                $('input[name=exp_month_year]').attr('required', 'required').removeAttr('readonly');
                $('input[name=cvv]').attr('required', 'required').removeAttr('readonly');
                $('#selected_card').val(0);
            }

        });


        $(".paypal_plan_buy_btn").click(function() {

            $("#paypal_plan_form").submit();
        });

        function getCardType(number) {
            const numberFormated = number.replace(/\D/g, '')
            var patterns = {
                visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
                mastercard: /^5[1-5][0-9]{14}$/,
                amex: /^3[47][0-9]{13}$/,
                ELO: /^((((636368)|(438935)|(504175)|(451416)|(636297))\d{0,10})|((5067)|(4576)|(4011))\d{0,12})$/,
                AURA: /^(5078\d{2})(\d{2})(\d{11})$/,
                jcb: /^(?:2131|1800|35\d{3})\d{11}$/,
                diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
                discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
                HIPERCARD: /^(606282\d{10}(\d{3})?)|(3841\d{15})$/,
                ELECTRON: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
                MAESTRO: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
                DANKORT: /^(5019)\d+$/,
                INTERPAYMENT: /^(636)\d+$/,
                unionpay: /^(62|88)\d+$/,
            }
            for (var key in patterns) {
                if (patterns[key].test(numberFormated)) {
                    return key
                }
            }
        }

        $("#stripe_payment_btn").click(function() {

            var card_name = getCardType($('input[name=number]').val());

            var card_number = $('input[name=number]').val();
            var name = $('input[name=name]').val();
            var exp_month_year = $('input[name=exp_month_year]').val();
            var cvv = $('input[name=cvv]').val();

            if (card_number == '' || name == '' || exp_month_year == '' || cvv == '') {
                $("#stripe_plan_form").submit();
            } else {
                if (card_name != '' && card_name != null || $("#existing_card").prop('checked') == true) {
                    $('#card_name').val(card_name);
                    $("#stripe_plan_form").submit();
                } else {
                    Notiflix.Notify.Failure("Invalid card details");
                }
            }

        });
    </script>
@endsection
