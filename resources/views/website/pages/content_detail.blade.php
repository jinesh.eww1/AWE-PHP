@extends('website.layouts.master')
@section('content')
@include('website.include.flash-message')
@include('website.layouts.head')


<?php 
    if(isset($data['poster']) && !empty($data['poster'])){
        $main_baner = $data['poster'];
    }else{
        $main_baner = asset('assets/website/image/awe-home.png');
    }
?>
<style type="text/css">
    .bg-body {
      background: linear-gradient(rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.89));
    }
    #singlevideoModal .video-js .vjs-tech, #singlevideoModal .modal-header.video_data, #singlevideoModal .video-js.vjs-default-skin{height: 100vh !important;}
</style>

    <section class="position-relative about-banner-video about-video-margin">
    <input type="hidden" name="device_name" id="device_name">
    <div class="video-sec">
            @if(Session::get('user_data.autoplay_previews') == 1)

                <video src="{{ $data['trailer']}}" autoplay muted loop id="bgVideo" poster="{{ $data['detail_poster'] }}"></video>
                <a id="MuteButton" class="muted text-decoration-none" onclick="toggleMute();" style="background-color:transparent;"><img src="{{ asset('assets/website/image/card/volume-high-slash.png') }}"></a>
                
            @else
                <video  src="" loop autoplay poster="{{ $data['detail_poster'] }}">
            @endif
            </div>
        <div class="container">
            <div class="mt-md-5 pt-md-5 font-18 pb-5 video-uper-text">
                <h1 class="text-white fw-bold mb-3">{{ $data['name']}}   
                    {{-- <a href="javascript:void(0)" class="text-decoration-none"> --}}
                    <?php if($data['is_free'] == 1){ ?>
                        <label
                        class="btn bg-color border-color font-20 text-white free_label mx-3"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.free'); }else{ echo CommonHelper::multi_language('home','free')->multi_language_value->language_value; } ?></label>
                        <?php } ?>
                    </h1>
                    @if($data['likes']>0)
                    <div class="d-flex align-items-center justify-content-between like-mobile my-3 total_likes_div">
                        <div>
                            <img src="{{ asset('assets\website\image/thumbs-up.png') }}" class="likes_dislikes_img">
                            <span class="text-white font-14 mx-1 total_likes">{{$data['likes']}} </span>
                        </div>
                        <div class="text-white font-14 mx-1">{{ $data['year'] }}</div>
                        <div class="text-white font-14 mx-1">{{ $data['age_rating_name'] }}</div>
                    </div>
                    @else
                    <div class="d-flex align-items-center justify-content-between like-mobile my-3 total_likes_div">
                        <div class="text-white font-14 mx-1">{{ $data['year'] }}</div>
                        <div class="text-white font-14 mx-1">{{ $data['age_rating_name'] }}</div>
                    </div>
                    @endif
                    <div class="row mx-0"><p class="text-white col-md-5 fw-light mb-1 more" id="movie-content">{{ $data['synopsis'] }}</p></div>
                <div class="mt-4 text-center text-sm-start">
                    <button class="bg-white btn play-btn my-2 my-lg-0" data-toggle="modal" data-target="#basicModal" id="trailer_play_btn">
                        <img src="{{ asset('assets/website/image/play-btn.png') }}" class="mb-1">
                        <span
                            class="font-23 ms-3"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.trailer'); }else{ echo CommonHelper::multi_language('movie_details','trailer')->multi_language_value->language_value; } ?></span>
                    </button>
                    <button class="bg-color btn ms-md-4 play-btn my-2" data-toggle="modal" data-target="#videoModal" id="video_play_btn">
                        <img src="{{ asset('assets/website/image/play-btn-w.png') }}" class="mb-2">
                        <span
                            class="font-23 text-white ms-3"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.play'); }else{ echo CommonHelper::multi_language('movie_details','play')->multi_language_value->language_value; } ?></span>
                    </button>

                </div>

                <div class="mt-3 text-center text-sm-start">
                    <?php 
                        $mylistcolor = '';$mlstatus = 1;
                        if($data['my_list'] == true){
                            $mylistcolor = 'bg-color'; $mlstatus = 0;
                        }
                        ?>
                    <button class="btn {{ $mylistcolor }} about-forgot border-color w-131 mylist_function"
                        data-id="{{$data['id']}}" data-status="{{$mlstatus}}">
                        <img src="{{ asset('assets\website\image/plus.png') }}">
                        <span
                            class="text-white font-14"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.my_list'); }else{ echo CommonHelper::multi_language('home','my_list')->multi_language_value->language_value; } ?></span>
                    </button>

                    <?php 
                        $likecolor = '';$lstatus = 1;
                        if($data['liked'] == true){
                            $likecolor = 'bg-color'; 
                            $lstatus = 0;
                        }
                        ?>

                    <button class="btn {{ $likecolor }} about-forgot border-color w-131 ms-md-3 liked_function" data-id="{{!empty($data['id'])?$data['id']:''}}" data-status="{{$lstatus}}">
                            <img src="{{ asset('assets\website\image/like.png') }}">
                            <span class="text-white font-14 liked_lbl mx-1">
                                <?php if(Session::get('app_string')){ echo ($data['liked'])?Session::get('app_string.movie_details.liked'):Session::get('app_string.movie_details.like'); }else{ echo ($data['liked'])?CommonHelper::multi_language('movie_details','liked')->multi_language_value->language_value:CommonHelper::multi_language('movie_details','like')->multi_language_value->language_value; } ?></span>
                        </button>

                    <?php 
                        $dlikecolor = '';$dlstatus = 1;
                        if($data['disliked'] == true){
                            $dlikecolor = 'bg-color'; $dlstatus = 0;
                        }
                        ?>
                    <button class="btn {{ $dlikecolor }} about-forgot border-color w-131 ms-md-3 my-2 disliked_function" data-id="{{!empty($data['id'])?$data['id']:''}}" data-status="{{$dlstatus}}">
                            <img src="{{ asset('assets\website\image/dislike.png') }}">
                            <span class="text-white font-14 disliked_lbl mx-1"><?php if(Session::get('app_string')){ echo ($data['disliked'])?Session::get('app_string.movie_details.disliked'):Session::get('app_string.movie_details.dislike'); }else{ echo ($data['disliked'])?CommonHelper::multi_language('movie_details','disliked')->multi_language_value->language_value:CommonHelper::multi_language('movie_details','dislike')->multi_language_value->language_value; } ?></span>
                        </button>
                </div>

                <div class="font-18 fw-light col-md-12 text-white mt-4 pt-2">
                    <?php 
                        $starring = array();
                            foreach ($data['content_cast'] as $key => $value) {
                                $starring[$key] = $value['artist']['name'];
                            }
                            if(!empty($starring) && count($starring)>0){ $starring = implode(", ", $starring); }else{ $starring = ''; }

                            $genres = array();
                            foreach ($data['content_genres'] as $key => $value) {
                                $genres[$key] = $value['genre']['name'];
                            }
                            if(!empty($genres) && count($genres)>0){ $genres = implode(", ", $genres); }else{ $genres = ''; }
                            
                            $tags = array();
                            foreach ($data['content_tags'] as $key => $value) {
                                $tags[$key] = $value['tags']['name'];
                            }
                            if(!empty($tags) && count($tags)>0){ $tags = implode(", ", $tags); }else{ $tags = ''; }

                            $subtitle = array();
                            foreach ($data['subtitle'] as $key => $value) {
                                $subtitle[$key] = $value['language'];
                            }
                            if(!empty($subtitle) && count($subtitle)>0){ $subtitle = implode(", ", $subtitle); }else{ $subtitle = ''; }
                            
                            $audio = array();
                            foreach ($data['audio'] as $key => $value) {
                                $audio[$key] = $value['language'];
                            }
                            if(!empty($audio) && count($audio)>0){ $audio = implode(", ", $audio); }else{ $audio = ''; }
                        
                        ?>
                    <div class="mb-2">
                        <label
                            class="text-yellow fw-bold"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.starring'); }else{ echo CommonHelper::multi_language('movie_details','starring')->multi_language_value->language_value; } ?>
                            :</label><span class="fw-light"> {{ $starring }}</span>
                    </div>

                    <div class="mb-2">
                        <label
                            class="text-yellow fw-bold"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.genres'); }else{ echo CommonHelper::multi_language('movie_details','genres')->multi_language_value->language_value; } ?>
                            :</label><span class="fw-light"> {{ $genres }}</span>
                    </div>

                    <div class="mb-2">
                        <label
                            class="text-yellow fw-bold"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.tags'); }else{ echo CommonHelper::multi_language('movie_details','tags')->multi_language_value->language_value; } ?>
                            :</label><span class="fw-light"> {{ $tags }}</span>
                    </div>
                    <div class="mb-2">
                        <label
                            class="text-yellow fw-bold"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.languages'); }else{ echo CommonHelper::multi_language('movie_details','languages')->multi_language_value->language_value; } ?>
                            : </label><span class="fw-light"> <?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.audio'); }else{ echo CommonHelper::multi_language('movie_details','audio')->multi_language_value->language_value; } ?> ( {{ !empty($audio)?$audio:"English" }} ) 
                                <?php 
                                if(!empty($subtitle)){
                                if(Session::get('app_string')){ 
                                    echo Session::get('app_string.movie_details.subtitles'); 
                                }else{ 
                                    echo CommonHelper::multi_language('movie_details','subtitles')->multi_language_value->language_value; } ?> ( {{ $subtitle }} ) 
                                <?php } ?></span>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $eplised_class=$mllised_class='';
    if($data['content_type'] == 2){
        $eplised_class = 'listed-a';
    }else{
        $mllised_class = 'listed-a';
    }
?>
<main>
    <section class="mb-5 pb-lg-4">
        <div class="container">
            <ul class="font-23 d-inline-flex list-unstyled mt-5">
                <?php if($data['content_type'] == 2){ ?>
                <li  class="me-5"><a onclick="episodes_dis();"  href="javascript:void(0)" class=" {{$eplised_class}} text-decoration-none text-white episodes_dis"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.episodes'); }else{ echo CommonHelper::multi_language('movie_details','episodes')->multi_language_value->language_value; } ?></a></li>
                <?php } ?>
                <li><a onclick="more_like_dis()" href="javascript:void(0)" class="{{$mllised_class}} text-decoration-none text-white fw-light more_like_dis"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.more_like_this'); }else{ echo CommonHelper::multi_language('movie_details','more_like_this')->multi_language_value->language_value; } ?></a></li>
            </ul>
            <?php if($data['content_type'] == 2){ ?>
            <?php  $season_data = array_column($data['content_seasons'],'season','id');
            if(!empty($season_data)){
            ?>
            <div class="option episodes_class" >
                <select class="text-white border-0 season_ddl" onchange="session_episode();">
                    <?php foreach ($season_data as $key => $value) { ?>
                        <option value="{{ $key }}">{{ $value }}</option>
                    <?php } ?>
                </select>
            </div>
            <?php } ?>
            <div class="row mt-4 episodes_class" >
                <?php if(count($data['content_seasons'])>0){
                    foreach ($data['content_seasons'] as $key => $value) { 
                        foreach ($value['video'] as $vkey => $vvalue) { ?>
                        <div style="cursor:pointer;" class="col-md-3 mb-4 hide_seassion session_{{$vvalue['season_id']}}" @if($key != 0) style="display: none;" @endif>
                            <div class="row font-12 single_episode_play" data-video-id="{{ $vvalue['id'] }}" data-video-watched-duration="{{ $vvalue['watched_duration_secs'] }}" 
                            <?php if(Session::get('user_data.allow_download_preference') == 1) { ?>
                                data-video-url ="{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net','https://awe-dev.s3.us-east-2.amazonaws.com',$vvalue['video']) }}" 
                            <?php }else{ ?> 
                                data-video-url ="{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net','https://awe-dev.s3.us-east-2.amazonaws.com',$vvalue['low_quality_video']) }}" 
                            <?php } ?>
                            data-video-check = "single_episode" data-toggle="modal" data-target="#singlevideoModal" >

                                <div class="col-lg-3 col-3">
                                    <img src="{{ $vvalue['thumbnail_image'] }}" class="img-fluid">
                                </div>
                                <div class="col-lg-7 col-7 text-white">
                                    {{$vvalue['episode_name']}}
                                    <p class="fw-light">{{$vvalue['duration']}}</p>
                                </div>
                                
                            </div>
                            <div class="font-12 text-white smore">{{$vvalue['episode_synopsis']}}</div> 
                        </div>        
                <?php } } } ?>
             
            </div>
             <?php } ?>

            <div class="more_like_this" @if($data['content_type'] == 2) style="display:none;" @endif >
                <div class="about-grid-selction text-center">

                <?php if(!empty($more_like_this)){
                    foreach ($more_like_this as $key => $value) { ?>
                        <div class="mt-2 hover-img-list position-relative">
                            <?php if($value['is_free'] == 1){ ?>
                                @switch(Session::get('locale'))
                            @case("sp")
                                <img src="{{ asset('assets/website/image/card/Spanish.png') }}" class="img-fluid free_tag_img">
                            @break
                            @case("md")
                                <img src="{{ asset('assets/website/image/card/Mandarin.png') }}" class="img-fluid free_tag_img">
                            @break
                            @case("hi")
                                <img src="{{ asset('assets/website/image/card/hindi.png') }}" class="img-fluid free_tag_img">
                            @break
                            @case("fr")
                                <img src="{{ asset('assets/website/image/card/French.png') }}" class="img-fluid free_tag_img">
                            @break
                            @default
                                <img src="{{ asset('assets/website/image/card/free.png') }}" class="img-fluid free_tag_img">
                        @endswitch
                            <?php } ?>
                            <img src="{{ $value['poster'] }}" class="img-fluid img-size box-radius">
                            <a href="{{ route('content-details',$value['slug'])}}"
                                class="btn play-hover-list w-100 h-100 position-absolute"><img
                                    src="{{ asset('assets/website/image/play.png') }}" class="img-fluid"></a>
                        </div>
                <?php } } ?>

                </div>
            </div>
        </div>
    </section>
</main>




{{-- player model start --}}
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog" style="max-width:100% !important;">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close modal-video-close cancel_video_play_btn" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-header video_data">
                <video id="trailer_video"  class="video-js vjs-default-skin" controls crossorigin playsinline data-poster="{{$data['poster']}}"  width="1920px" height="940px">
                    <source src="{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net', 'https://awe-dev.s3.us-east-2.amazonaws.com',$data['trailer']) }}" type="video/mp4" size="720" />
                </video>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
    <div class="modal-dialog m-0" style="max-width:100% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close modal-video-close cancel_video_play_btn" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-header video_data">
                  <video id="my_video_1" class="video-js vjs-default-skin" controls crossorigin playsinline data-poster="{{$data['poster']}}" type='application/x-mpegURL'  width="1920px" height="940px">
                    @if(Session::get('user_data.allow_download_preference') == 1)

                    <source src="{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net', 'https://awe-dev.s3.us-east-2.amazonaws.com',$data['video'][0]['video']) }}" type='application/x-mpegURL'>
                     @else
                        <source src="{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net', 'https://awe-dev.s3.us-east-2.amazonaws.com',$data['video'][0]['low_quality_video']) }}" type='application/x-mpegURL'>

                    @endif
                  </video>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="singlevideoModal" tabindex="-1" role="dialog" aria-labelledby="singlevideoModal" aria-hidden="true">
    <div class="modal-dialog m-0" style="max-width:100% !important;height:100vh;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close modal-video-close cancel_video_play_btn" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-header video_data">
                  <video id="single_video_play" class="video-js vjs-default-skin" controls autoplay crossorigin playsinline data-poster="{{$data['poster']}}" type='application/x-mpegURL' width="1920px" height="940px">

                  </video>
            </div>

        </div>
    </div>
</div>

{{-- player model end--}}
{{-- Player script start --}}


<link rel="stylesheet" href="{{ asset('assets/website/css/video-js.css') }}" />
<script type="text/javascript" src="{{ asset('assets/website/js/video.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/website/js/videojs-seek-buttons.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('assets/website/css/videojs-seek-buttons.css') }}" />
<script type="text/javascript" src="{{ asset('assets/website/js/videojs-media-sources.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/website/js/videojs-playlist.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('assets/website/css/video-js.min.css') }}" />

<script src="https://cdn.socket.io/4.5.0/socket.io.min.js" integrity="sha384-7EyYLQZgWBi67fBtVxw60/OWl1kjsfrPFcaU0pp0nAh+i8FD068QogUvg85Ewy1k" crossorigin="anonymous"></script>

<script type="text/javascript">

var options = {
    playbackRates: [1, 1.25, 1.5, 1.75, 2],
    controlBar: {
        volumePanel: {
            inline: false
        }
    }
    };
var movie_video = videojs('my_video_1',options);
movie_video.seekButtons({
            forward: 15,
            back: 15
        });

//movie_video.hlsQualitySelector();

var trailer_video = videojs('trailer_video',options);
trailer_video.seekButtons({
            forward: 15,
            back: 15
        });


var single_movie_video = videojs('single_video_play',options);
single_movie_video.seekButtons({
            forward: 15,
            back: 15
        });

function toggleMute() {
    var button = document.getElementById("MuteButton");
    var video = document.getElementById("bgVideo");

    if (video.muted) {
        $('#MuteButton').empty().html(`<img src="{{ asset('assets/website/image/card/volumn-up.png') }}">`);
        video.muted = false;
    } else {
        $('#MuteButton').empty().html(`<img src="{{ asset('assets/website/image/card/volume-high-slash.png') }}">`);
        video.muted = true;
    }

    button.classList.toggle('muted');
}

    $(window).keyup(function(e) {
        if (e.key === "Escape") {

            clearInterval(window.cwtimer);
            clearInterval(window.scwtimer);

            trailer_video.pause();
            single_movie_video.pause(); 
            movie_video.pause();

            movie_video.on("pause", function (e) {
                clearInterval(window.cwtimer);
            });

            single_movie_video.on("pause", function (e) {
                clearInterval(window.scwtimer);
            });

        }
        
    });
 
    $('.modal-video-close').on('click', function(){
        trailer_video.pause();
        movie_video.pause();
        clearInterval(window.cwtimer);
    });


    $('#trailer_play_btn').on('click', function(){
        trailer_video.play();
    });

    $('.cancel_video_play_btn').on('click', function(){
        
        clearInterval(window.cwtimer);
        clearInterval(window.scwtimer);

        single_movie_video.on("pause", function (e) {
            clearInterval(window.scwtimer);
        });

        trailer_video.pause();
        single_movie_video.pause(); 
        movie_video.pause();
        
        videojs('my_video_1').pause();
        $('#videoModal').modal('hide'); 

        videojs('single_video_play').pause();
        $('#singlevideoModal').modal('hide');

    });



    $('#video_play_btn').on('click', function(){

        /*if('{{ Session::get("user_data.plan_id") }}' == '' && '{{ $data["is_free"] }}' == 0){
            
            movie_video.pause();
            movie_video.on("pause", function () {
                clearInterval(window.cwtimer);
            });
            
            $('.cancel_video_play_btn').click();
            $('#parent_subscription_popup').modal('show');
            
            return false;

        }*/

        if("{{ $data['content_type'] }}" == 2){

            var types = {
                mp4: "video/mp4",
                webm: "video/webm",
                m3u: "application/x-mpegURL"
              };

            if('{{ Session::get("user_data.autoplay") }}' == 1){

                var vPlaylist = [
                @foreach ($data['content_seasons'] as $key => $value)
                 @foreach ($value['video'] as $vkey => $vvalue) 
                    @if(Session::get('user_data.allow_download_preference') == 1)
                        {
                        sources: [{
                            src: "{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net', 'https://awe-dev.s3.us-east-2.amazonaws.com',$vvalue['video']) }}",
                            type: types.m3u
                        }]
                        },
                    @else

                        {
                        sources: [{
                            src: "{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net', 'https://awe-dev.s3.us-east-2.amazonaws.com',$vvalue['low_quality_video']) }}",
                            type: types.m3u
                        }]
                        },

                    @endif
                @endforeach
                @endforeach
                ];

            }else{

                var vPlaylist = [{
                        sources: [{
                            src: "{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net', 'https://awe-dev.s3.us-east-2.amazonaws.com',$data['video'][0]['low_quality_video']) }}",
                            type: types.m3u
                        }]},

                ];
            }
                

                var rate = 1; // playback rate
                movie_video.ready(function () {
                  this.playlist(vPlaylist);
                  this.playlist.autoadvance(0);
                  this.playlist.repeat(false); // Allow skipping back to first video in playlist.
                });
                movie_video.on("beforeplaylistitem", function () {
                  rate = this.playbackRate();
                });
                movie_video.on("playlistitem", function () {
                 this.playbackRate(rate);
                });


            var content_id =''; var season_id = ''; var video_id = '';
            movie_video.play();
            movie_video.on("play", function (e) {

                //console.log(movie_video.src());
                //console.log(single_video_url);    

                @foreach ($data['content_seasons'] as $key => $value)
                    @foreach ($value['video'] as $vkey => $vvalue) 

                    
                        if($.trim(movie_video.src()) === "{{str_replace('https://d3ob6u4l0x8h65.cloudfront.net', 'https://awe-dev.s3.us-east-2.amazonaws.com',$vvalue['video'])}}"){
                            content_id = "{{$vvalue['content_id']}}";
                            season_id = "{{$vvalue['season_id']}}";
                            video_id = "{{$vvalue['id']}}";
                        }
                    
                    @endforeach
                @endforeach
                

                /*var sfseconds = 1;
                var socket = io("https://awemovies.com:3000/");
                socket.on("connect", function() {
                  var id = parseInt('{{ Session::get("user_data.id")}}');
                  socket.emit('connect_user', { user_id: id});
                  console.log("User connetcted");
                });

                var user_id = parseInt('{{ Session::get("user_data.id") }}');
                var parent_id = parseInt('{{ Session::get("user_data.parent_id") }}');
                var content_id = parseInt(content_id);
                var season_id = parseInt(season_id);
                var video_id = parseInt(video_id);
                var udid = '{{ Session::get("device_token") }}';
                var device_name = $('#device_name').val();
                var plan_id = parseInt('{{ !empty(Session::get("user_data.plan_id"))?Session::get("user_data.plan_id"):0 }}');

                
                socket.on('watching', function(data) {
                    if(data.message == 'stop_watching' && data.udid == '{{ Session::get("device_token") }}'){
                        movie_video.pause();
                        movie_video.on("pause", function () {
                            clearInterval(window.cwtimer);
                        });
                        $('.cancel_video_play_btn').click();
                        //$('#videoModal').modal('hide');
                        $('#screen_limit_device_name').empty().html(data.watching_data.data[0]['device_name']+' : '+data.watching_data.data[0]['name']);
                        $('#screen_limit_popup').modal('show');
                    }
                });

                socket.emit('watching', { user_id: user_id, parent_id:parent_id, content_id: content_id, season_id: season_id, video_id: video_id, seconds: sfseconds, udid: udid, device_name: device_name, plan_id: plan_id});


                window.cwtimer = setInterval(function() {
                    
                    var content_id =''; var season_id = ''; var video_id = '';
                     @foreach ($data['content_seasons'] as $key => $value)
                        @foreach ($value['video'] as $vkey => $vvalue) 

                        if($.trim(movie_video.src()) === "{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net','https://awe-dev.s3.us-east-2.amazonaws.com',$vvalue['video']) }}"){

                            content_id = "{{$vvalue['content_id']}}";
                            season_id = "{{$vvalue['season_id']}}";
                            video_id = "{{$vvalue['id']}}";

                        }

                        @endforeach
                    @endforeach


                    //console.log(movie_video.src());

                    var sfseconds = Math.round(movie_video.currentTime());;
                    var socket = io("https://awemovies.com:3000/");
                    socket.on("connect", function() {
                      var id = parseInt('{{ Session::get("user_data.id")}}');
                      socket.emit('connect_user', { user_id: id});
                      console.log("User connetcted");
                    });

                    var user_id = parseInt('{{ Session::get("user_data.id") }}');
                    var parent_id = parseInt('{{ Session::get("user_data.parent_id") }}');
                    var content_id = parseInt(content_id);
                    var season_id = parseInt(season_id);
                    var video_id = parseInt(video_id);
                    var udid = '{{ Session::get("device_token") }}';
                    var device_name = $('#device_name').val();
                    var plan_id = parseInt('{{ !empty(Session::get("user_data.plan_id"))?Session::get("user_data.plan_id"):0 }}');

                    socket.on('watching', function(data) {
                        if(data.message == 'stop_watching' && data.udid == '{{ Session::get("device_token") }}'){
                            movie_video.pause();
                            movie_video.on("pause", function () {
                                clearInterval(window.cwtimer);
                            });
                            
                            $('.cancel_video_play_btn').click();
                            //$('#videoModal').modal('hide');
                            $('#screen_limit_device_name').empty().html(data.watching_data.data[0]['device_name']+' : '+data.watching_data.data[0]['name']);
                            $('#screen_limit_popup').modal('show');
                        }
                    });

                    socket.emit('watching', { user_id: user_id, parent_id:parent_id, content_id: content_id, season_id: season_id, video_id: video_id, seconds: sfseconds, udid: udid, device_name: device_name, plan_id: plan_id});
                
                }, 10000);*/


            });
        }


        if("{{ $data['content_type'] }}" == 1){

            movie_video.currentTime("{{!empty($data['video'][0]['watched_duration_secs'])?$data['video'][0]['watched_duration_secs']:''}}");
            movie_video.play();
            
            var fseconds = 1;
            var socket = io("https://awemovies.com:3000/");
            socket.on("connect", function() {
              var id = parseInt('{{ Session::get("user_data.id")}}');
              socket.emit('connect_user', { user_id: id});
              console.log("User connetcted");
            });

            var user_id = parseInt('{{ Session::get("user_data.id") }}');
            var parent_id = parseInt('{{ Session::get("user_data.parent_id") }}');
            var content_id = parseInt("{{!empty($data['video'][0]['content_id'])?$data['video'][0]['content_id']:''}}");
            var season_id = parseInt("{{!empty($data['video'][0]['season_id'])?$data['video'][0]['season_id']:0}}");
            var video_id = parseInt("{{!empty($data['video'][0]['id'])?$data['video'][0]['id']:''}}");
            var udid = '{{ Session::get("device_token") }}';
            var device_name = $('#device_name').val();
            var plan_id = parseInt('{{ !empty(Session::get("user_data.plan_id"))?Session::get("user_data.plan_id"):0 }}');


            socket.on('watching', function(data) {
                
                if(data.message == 'stop_watching' && data.udid == '{{ Session::get("device_token") }}'){
                    movie_video.pause();
                    movie_video.on("pause", function () {
                        clearInterval(window.cwtimer);
                    });

                    $('.cancel_video_play_btn').click();
                   // $('#videoModal').modal('hide');

                    $('#screen_limit_device_name').empty().html(data.watching_data.data[0]['device_name']+' : '+data.watching_data.data[0]['name']);
                    $('#screen_limit_popup').modal('show');
                }
            });

            socket.emit('watching', { user_id: user_id, parent_id:parent_id, content_id: content_id, season_id: season_id, video_id: video_id, seconds: fseconds, udid: udid, device_name: device_name, plan_id: plan_id});
        
            movie_video.on("play", function () {

            window.cwtimer = setInterval(function() {
                if (!movie_video.paused()) {
                        var seconds = Math.round(movie_video.currentTime());
                        var socket = io("https://awemovies.com:3000/");
                        socket.on("connect", function() {
                          var id = parseInt('{{ Session::get("user_data.id")}}');
                          socket.emit('connect_user', { user_id: id});
                          console.log("User connetcted");
                        });

                        var user_id = parseInt('{{ Session::get("user_data.id") }}');
                        var parent_id = parseInt('{{ Session::get("user_data.parent_id") }}');
                        var content_id = parseInt("{{!empty($data['video'][0]['content_id'])?$data['video'][0]['content_id']:''}}");
                        var season_id = parseInt("{{!empty($data['video'][0]['season_id'])?$data['video'][0]['season_id']:0}}");
                        var video_id = parseInt("{{!empty($data['video'][0]['id'])?$data['video'][0]['id']:''}}");
                        var udid2 = '{{ Session::get("device_token") }}';
                        var device_name = $('#device_name').val();
                        var plan_id = parseInt('{{ !empty(Session::get("user_data.plan_id"))?Session::get("user_data.plan_id"):0 }}');


                        socket.on('watching', function(data) {
                            
                            if(data.message == "stop_watching"  && data.udid == '{{ Session::get("device_token") }}'){
                                
                                movie_video.pause();
                                movie_video.on("pause", function () {
                                    clearInterval(window.cwtimer);
                                });

                                $('.cancel_video_play_btn').trigger('click');
                                $('#videoModal').modal('hide');

                                $('#screen_limit_device_name').empty().html(data.watching_data.data[0]['device_name']+' : '+data.watching_data.data[0]['name']);
                                $('#screen_limit_popup').modal('show');
                            }
                        });
                        socket.emit('watching', { user_id: user_id, parent_id:parent_id, content_id: content_id, season_id: season_id, video_id: video_id, seconds: seconds, udid: udid2, device_name: device_name, plan_id: plan_id});
            
                }else{
                    clearInterval(window.cwtimer);
                }
            }, 10000); 

        });
        }

    });


$('.single_episode_play').on('click', function(){


    var single_video_id = "";
    var single_video_url = "";
    if($(this).attr('data-video-check') == 'single_episode'){
        single_video_id = $(this).attr('data-video-id');
        single_video_url = $(this).attr('data-video-url');
        single_video_watched_duration = $(this).attr('data-video-watched-duration');
    }


    /*console.log(single_video_id);
    console.log(single_video_url);
    console.log(single_video_watched_duration);

    return false;*/

        /*if('{{ Session::get("user_data.plan_id") }}' == '' && '{{ $data["is_free"] }}' == 0){
            
            single_movie_video.pause();
            single_movie_video.on("pause", function () {
                clearInterval(window.cwtimer);
            });
            
            $('.cancel_video_play_btn').click();
            
            $('#parent_subscription_popup').modal('show');
            
            return false;

        }*/



        if('{{ Session::get("user_data.autoplay") }}' == 1 && "{{ $data['content_type'] }}" == 2){

            var types = {
              mp4: "video/mp4",
              webm: "video/webm",
              m3u: "application/x-mpegURL"
            };

            var svPlaylist = [{ sources: [] }, ];

            single_movie_video.ready(function () {
              this.playlist(svPlaylist);
              this.playlist.autoadvance(1);
              this.playlist.repeat(false); // Allow skipping back to first video in playlist.
            });

            svPlaylist = [{
                sources: [{
                        src: single_video_url,
                        type: types.m3u
                    }]
                },
            ];

            var rate = 1;

            single_movie_video.ready(function () {
              this.playlist(svPlaylist);
              this.playlist.autoadvance(1);
              this.playlist.repeat(false); // Allow skipping back to first video in playlist.
            });
            single_movie_video.on("beforeplaylistitem", function () {
              rate = this.playbackRate();
            });
            single_movie_video.on("playlistitem", function () {
             this.playbackRate(rate);
            });

            single_movie_video.currentTime(single_video_watched_duration);
            
            var content_id =''; var season_id = ''; var video_id = '';

            //console.log(single_movie_video.src());

            single_movie_video.on("play", function (e) {
                
                @foreach ($data['content_seasons'] as $key => $value)
                    @foreach ($value['video'] as $vkey => $vvalue) 

                        if(single_video_id == "{{$vvalue['id']}}" && single_video_url === "{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net', 'https://awe-dev.s3.us-east-2.amazonaws.com',(Session::get('user_data.allow_download_preference') == 1)?$vvalue['video']:$vvalue['low_quality_video']) }}"){
                            content_id = "{{$vvalue['content_id']}}";
                            season_id = "{{$vvalue['season_id']}}";
                            video_id = "{{$vvalue['id']}}";
                        }
                    
                    @endforeach
                @endforeach


                /*console.log(content_id);
                console.log(season_id);
                console.log(video_id);
                console.log(single_movie_video.src());*/

                /*var sfseconds = 1;
                var socket = io("https://awemovies.com:3000/");
                socket.on("connect", function() {
                  var id = parseInt('{{ Session::get("user_data.id")}}');
                  socket.emit('connect_user', { user_id: id});
                  console.log("User connetcted");
                });

                var user_id = parseInt('{{ Session::get("user_data.id") }}');
                var parent_id = parseInt('{{ Session::get("user_data.parent_id") }}');
                var content_id = parseInt(content_id);
                var season_id = parseInt(season_id);
                var video_id = parseInt(video_id);
                var udid = '{{ Session::get("device_token") }}';
                var device_name = $('#device_name').val();
                var plan_id = parseInt('{{ !empty(Session::get("user_data.plan_id"))?Session::get("user_data.plan_id"):0 }}');

                
                socket.on('watching', function(data) {
                    if(data.message == 'stop_watching' && data.udid == '{{ Session::get("device_token") }}'){
                        single_movie_video.pause();
                        single_movie_video.on("pause", function () {
                            clearInterval(window.cwtimer);
                        });
                        $('.cancel_video_play_btn').click();
                        $('#screen_limit_device_name').empty().html(data.watching_data.data[0]['device_name']+' : '+data.watching_data.data[0]['name']);
                        $('#screen_limit_popup').modal('show');
                    }
                });

                socket.emit('watching', { user_id: user_id, parent_id:parent_id, content_id: content_id, season_id: season_id, video_id: video_id, seconds: sfseconds, udid: udid, device_name: device_name, plan_id: plan_id});



                window.scwtimer = setInterval(function() {
                    
                    var content_id =''; var season_id = ''; var video_id = '';
                     @foreach ($data['content_seasons'] as $key => $value)
                        @foreach ($value['video'] as $vkey => $vvalue) 

                        if(single_video_id == "{{$vvalue['id']}}" && single_video_url === "{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net', 'https://awe-dev.s3.us-east-2.amazonaws.com',(Session::get('user_data.allow_download_preference') == 1)?$vvalue['video']:$vvalue['low_quality_video']) }}"){

                            content_id = "{{$vvalue['content_id']}}";
                            season_id = "{{$vvalue['season_id']}}";
                            video_id = "{{$vvalue['id']}}";

                        }

                        @endforeach
                    @endforeach


                    console.log(content_id);
                    console.log(season_id);
                    console.log(video_id);
                    console.log(single_movie_video.src());


                    var sfseconds = Math.round(single_movie_video.currentTime());;
                    var socket = io("https://awemovies.com:3000/");
                    socket.on("connect", function() {
                      var id = parseInt('{{ Session::get("user_data.id")}}');
                      socket.emit('connect_user', { user_id: id});
                      console.log("User connetcted");
                    });

                    var user_id = parseInt('{{ Session::get("user_data.id") }}');
                    var parent_id = parseInt('{{ Session::get("user_data.parent_id") }}');
                    var content_id = parseInt(content_id);
                    var season_id = parseInt(season_id);
                    var video_id = parseInt(video_id);
                    var udid = '{{ Session::get("device_token") }}';
                    var device_name = $('#device_name').val();
                    var plan_id = parseInt('{{ !empty(Session::get("user_data.plan_id"))?Session::get("user_data.plan_id"):0 }}');

                    socket.on('watching', function(data) {
                        if(data.message == 'stop_watching' && data.udid == '{{ Session::get("device_token") }}'){
                            single_movie_video.pause();
                            single_movie_video.on("pause", function () {
                                clearInterval(window.cwtimer);
                            });
                            
                            $('.cancel_video_play_btn').click();
                            //$('#videoModal').modal('hide');
                            $('#screen_limit_device_name').empty().html(data.watching_data.data[0]['device_name']+' : '+data.watching_data.data[0]['name']);
                            $('#screen_limit_popup').modal('show');
                        }
                    });

                    socket.emit('watching', { user_id: user_id, parent_id:parent_id, content_id: content_id, season_id: season_id, video_id: video_id, seconds: sfseconds, udid: udid, device_name: device_name, plan_id: plan_id});
                
                }, 10000);*/

                

            });

        }

    });
</script>

<script>
$(document).ready(function() {

    var showChar = 200; // How many characters are shown by default
    var ellipsestext = "";
    var moretext = "<?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.show_more'); }else{ echo CommonHelper::multi_language('movie_details','show_more')->multi_language_value->language_value; } ?>";
    var lesstext = "<?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.show_less'); }else{ echo CommonHelper::multi_language('movie_details','show_less')->multi_language_value->language_value; } ?>";

    $('.more').each(function() {
        var content = $(this).html();

        if (content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses ">' + ellipsestext +
                '&nbsp;</span><span class="morecontent "><span>' + h +
                '</span>&nbsp;&nbsp;<a href="" class="morelink text-yellow">' + moretext +
                '</a></span>';

            $(this).html(html);
        }

    });

    $('.smore').each(function() {
        var content = $(this).html();

        if (content.length > 100) {

            var c = content.substr(0, 100);
            var h = content.substr(100, content.length - 100);

            var html = c + '<span class="moreellipses ">' + ellipsestext +
                '&nbsp;</span><span class="morecontent "><span>' + h +
                '</span>&nbsp;&nbsp;<a href="" class="morelink text-yellow">' + moretext +
                '</a></span>';

            $(this).html(html);
        }

    });

    $(".morelink").click(function() {
        if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
});


function episodes_dis() {
  $('.episodes_dis').addClass('listed-a');
  $('.more_like_dis').removeClass('listed-a');
  $('.episodes_class').css('display','flex');
  //$('.episodes_class').addClass('d-flex');
  $('.more_like_this').css('display','none');
}

function more_like_dis() {
  $('.episodes_dis').removeClass('listed-a');
  $('.more_like_dis').addClass('listed-a');
  $('.episodes_class').css('display','none');
  $('.more_like_this').css('display','block');
}

function session_episode() {
  var id = $('.season_ddl').val();
  $('.hide_seassion').css('display','none');
  $('.session_'+id).css('display','block');
}

</script>

@include('website.layouts.footer')
@endsection