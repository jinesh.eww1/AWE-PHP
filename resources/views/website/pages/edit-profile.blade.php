@extends('website.layouts.master')
@section('content')
    @include('website.include.flash-message')

    <section>
        <div class="container">
            <div class="mt-md-5 pt-5 mb-2">
                <a href="{{ route('manage-profile') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a>
                <span class="text-white font-18"><?php if (Session::get('app_string')) {
                    echo Session::get('app_string.edit_profile.edit_profile');
                } else {
                    echo CommonHelper::multi_language('edit_profile', 'edit_profile')->multi_language_value->language_value;
                } ?></span>
            </div>
            <form method="POST" action="{{ route('edit-profile', base64_encode($user['data']['id'])) }}"
                class="position-relative mt-5 pt-3" enctype="multipart/form-data">
                @csrf()

                <input type="hidden" name="user_id" value="{{ $user['data']['id'] }}">

                <div>
                    <div id="edit-img-bg" class="manage-profile-edit position-relative profile-edit-icon profile-edit mx-auto">
                        <img class="img-fluid box-radius" src="{{ $user['data']['profile_picture_full_url'] }}"onerror="this.src='{{ asset('/assets/website/image/awe-home.png') }}'"
                            alt="" id="blah1">
                        <input type="file" name="profile_picture" style="display:none;" onchange="readURL1(this);"
                            id="profile_image" accept="image/*" data-parsley-errors-container="#profile_picture_error">
                    </div>
                    <div class="error" id="profile_picture_error" style="margin-top:15px;"></div>
                </div>

                <div class="row justify-content-center edit-profile mt-4">
                    <div class="col-xl-3 col-md-5 col-10">
                        <div class="row justify-content-center align-items-top">
                            <div class="col-md-6 col-10 mt-3 ps-md-0">
                                <input data-parsley-length="[2,25]" required data-parsley-pattern="^[a-zA-Z\s]*$" maxlength="25"
                                    class="form-control bg-transparent p-3 text-yellow" type="text" name="first_name"
                                    placeholder="<?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.add_Profile.first_name');
                                    } else {
                                        echo CommonHelper::multi_language('add_Profile', 'first_name')->multi_language_value->language_value;
                                    } ?>" value="{{ $user['data']['first_name'] }}"
                                    data-parsley-required-message="<?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.add_Profile.enter_first_name');
                                    } else {
                                        echo CommonHelper::multi_language('add_Profile', 'enter_first_name')->multi_language_value->language_value;
                                    } ?>" 
                                    data-parsley-length-message="<?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.add_Profile.first_name_min_length');
                                    } else {
                                        echo CommonHelper::multi_language('add_Profile', 'first_name_min_length')->multi_language_value->language_value;
                                    } ?>"
                                    data-parsley-pattern-message="<?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.add_Profile.please_enter_valid_first_name');
                                    } else {
                                        echo CommonHelper::multi_language('add_Profile', 'please_enter_valid_first_name')->multi_language_value->language_value;
                                    } ?>"
                                    >
                            </div>
                            <div class="col-md-6 col-10 mt-3 ps-md-0">
                                <input data-parsley-length="[2,25]" required data-parsley-pattern="^[a-zA-Z\s]*$"maxlength="25"
                                    class="form-control bg-transparent p-3 text-yellow" type="text" name="last_name"
                                    placeholder="<?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.add_Profile.last_name');
                                    } else {
                                        echo CommonHelper::multi_language('add_Profile', 'last_name')->multi_language_value->language_value;
                                    } ?>"
                                    value="{{ !empty($user['data']['last_name']) ? $user['data']['last_name'] : '' }}"
                                    data-parsley-required-message="<?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.add_Profile.enter_last_name');
                                    } else {
                                        echo CommonHelper::multi_language('add_Profile', 'enter_last_name')->multi_language_value->language_value;
                                    } ?>" 
                                    data-parsley-length-message="<?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.add_Profile.last_name_min_length');
                                    } else {
                                        echo CommonHelper::multi_language('add_Profile', 'last_name_min_length')->multi_language_value->language_value;
                                    } ?>"
                                    data-parsley-pattern-message="<?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.add_Profile.please_enter_valid_last_name');
                                    } else {
                                        echo CommonHelper::multi_language('add_Profile', 'please_enter_valid_last_name')->multi_language_value->language_value;
                                    } ?>"
                                    
                                    >
                            </div>
                        </div>
                    </div>
                </div>

                <?php //if($user['data']['is_child'] != 1 && Session::get('user_data.id') != $user['data']['id']){ ?>
                <?php if((Session::get('user_data.id') != $user['data']['id'] && Session::get('user_data.is_child') == 0) && $user['data']['parent_id'] != 0){ ?>
                <div class="row justify-content-center align-items-center mt-4">
                    <div class="col-xl-3 col-md-5 col-10">
                        <div class="row justify-content-center align-items-center all-noti  box-shadow-notification">
                            <div class="col-lg-9 col-md-9 col-8 px-md-0">
                                <p class="text-white font-14 p-3"><?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.add_Profile.for_children');
                                } else {
                                    echo CommonHelper::multi_language('add_Profile', 'for_children')->multi_language_value->language_value;
                                } ?></p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-4">
                                <label class="switch1 float-end">
                                    <?php if ($user['data']['is_child'] == 1) {
                                        $check = 'checked="checked"';
                                    } else {
                                        $check = '';
                                    }
                                    ?>
                                    <input type="checkbox" name="is_child" {{ $check }}>
                                    <span class="slider1 round1"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }else{ ?>
                <input type="hidden" name="is_child" value="{{ $user['data']['is_child'] }}">
                <?php } ?>


                <div class="row justify-content-center align-items-center mt-4 pt-3">
                    <div class="text-center text-white mb-3"><?php if (Session::get('app_string')) {
                        echo Session::get('app_string.edit_profile.profile_parental_controls');
                    } else {
                        echo CommonHelper::multi_language('edit_profile', 'profile_parental_controls')->multi_language_value->language_value;
                    } ?></div>
                    <div class="profile-list-width">
                        <ul class="about-user-profile text-white list-unstyled">
                            <a href="{{ route('preferred-categories', base64_encode($user['data']['id'])) }}"
                                class="text-decoration-none">
                                <li class="p-3 px-4 back-arrow-profile position-relative">
                                    <img class="img-fluid" src="{{ asset('assets/website/image/categories.png') }}"><span
                                        class="font-14 ms-4"><?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.edit_profile.prefered_category');
                                        } else {
                                            echo CommonHelper::multi_language('edit_profile', 'prefered_category')->multi_language_value->language_value;
                                        } ?></span>
                                </li>
                            </a>
                            <a href="{{ route('profile-lock', base64_encode($user['data']['id'])) }}"
                                class="text-decoration-none">
                                <li class="p-3 px-4 back-arrow-profile position-relative d-flex align-items-center">
                                    <img class="img-fluid" src="{{ asset('assets/website/image/profile-lock.png') }}">
                                    <span class="font-14 ms-4"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.edit_profile.profile_lock');
                                    } else {
                                        echo CommonHelper::multi_language('edit_profile', 'profile_lock')->multi_language_value->language_value;
                                    } ?><br>
                                        <?php if(!empty($user['data']['lock_password'])){ ?>
                                        <span class="font-12 text-yellow"><?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.edit_profile.on');
                                        } else {
                                            echo CommonHelper::multi_language('edit_profile', 'on')->multi_language_value->language_value;
                                        } ?></span></span>
                                    <?php }else{ ?>
                                    <span class="font-12 text-yellow"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.edit_profile.off');
                                    } else {
                                        echo CommonHelper::multi_language('edit_profile', 'off')->multi_language_value->language_value;
                                    } ?></span></span>
                                    <?php } ?>
                                </li>
                            </a>
                            <a href="{{ route('playback-setting', base64_encode($user['data']['id'])) }}"
                                class="text-decoration-none">
                                <li class="p-3 px-4 back-arrow-profile position-relative d-flex align-items-center">
                                    <img class="img-fluid"
                                        src="{{ asset('assets/website/image/playback-setting.png') }}"><span
                                        class="font-14 ms-4"><?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.edit_profile.playback_settings');
                                        } else {
                                            echo CommonHelper::multi_language('edit_profile', 'playback_settings')->multi_language_value->language_value;
                                        } ?><br>

                                    <?php if((!empty($user['data']['autoplay']) && $user['data']['autoplay'] == 1) || (!empty($user['data']['autoplay_previews']) && $user['data']['autoplay_previews'] == 1)){ ?>
                                        
                                        <span class="font-12 text-yellow"><?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.edit_profile.on');
                                        } else {
                                            echo CommonHelper::multi_language('edit_profile', 'on')->multi_language_value->language_value;
                                        } ?></span></span>
                                    <?php }else{ ?>
                                    <span class="font-12 text-yellow"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.edit_profile.off');
                                    } else {
                                        echo CommonHelper::multi_language('edit_profile', 'off')->multi_language_value->language_value;
                                    } ?></span></span>
                                    <?php } ?>

                                </li>
                            </a>
                            <?php if(Session::get('user_data.parent_id') == 0){ ?>
                            <a href="{{ route('viewing-restrictions', base64_encode($user['data']['id'])) }}"
                                class="text-decoration-none">
                                <li class="p-3 px-4 back-arrow-profile position-relative">
                                    <img class="img-fluid" src="{{ asset('assets/website/image/view.png') }}"><span
                                        class="font-14 ms-4"><?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.edit_profile.viewing_restrictions');
                                        } else {
                                            echo CommonHelper::multi_language('edit_profile', 'viewing_restrictions')->multi_language_value->language_value;
                                        } ?></span>
                                </li>
                            </a>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="text-center my-4">

                        <?php if(Session::get('user_data.id') != $user['data']['id'] && $user['data']['parent_id'] != 0){ ?>
                        <img class="img-fluid" src="{{ asset('assets/website/image/del-btn-yellow.png') }}">

                        <!-- <a href="{{ route('account-remove', base64_encode($user['data']['id'])) }}" class="text-yellow"> -->
                        <a href="javascript:void(0)" class="text-yellow" data-bs-toggle="modal"
                            data-bs-target="#deleteBackdrop">
                            <span class="font-14 ms-2">
                                <?php
                                if ($user['data']['parent_id'] > 0) {
                                    if (Session::get('app_string')) {
                                        echo Session::get('app_string.edit_profile.delete_profile');
                                    } else {
                                        echo CommonHelper::multi_language('edit_profile', 'delete_profile')->multi_language_value->language_value;
                                    }
                                } else {
                                    if (Session::get('app_string')) {
                                        echo Session::get('app_string.edit_profile.delete_account');
                                    } else {
                                        echo CommonHelper::multi_language('edit_profile', 'delete_account')->multi_language_value->language_value;
                                    }
                                } ?></span>
                        </a>
                        <?php }elseif (Session::get('user_data.id') == $user['data']['id'] && $user['data']['parent_id'] == 0) { ?>
                        <img class="img-fluid" src="{{ asset('assets/website/image/del-btn-yellow.png') }}">

                        <!-- <a href="{{ route('account-remove', base64_encode($user['data']['id'])) }}" class="text-yellow"> -->
                        <a href="javascript:void(0)" class="text-yellow" data-bs-toggle="modal"
                            data-bs-target="#deleteBackdrop">
                            <span class="font-14 ms-2">
                                <?php
                                if ($user['data']['parent_id'] > 0) {
                                    if (Session::get('app_string')) {
                                        echo Session::get('app_string.edit_profile.delete_profile');
                                    } else {
                                        echo CommonHelper::multi_language('edit_profile', 'delete_profile')->multi_language_value->language_value;
                                    }
                                } else {
                                    if (Session::get('app_string')) {
                                        echo Session::get('app_string.edit_profile.delete_account');
                                    } else {
                                        echo CommonHelper::multi_language('edit_profile', 'delete_account')->multi_language_value->language_value;
                                    }
                                } ?></span>
                        </a>
                        <?php } ?>


                        <div class="modal fade" id="deleteBackdrop" tabindex="-1" aria-labelledby="deleteBackdropLabel"
                            aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content text-center p-4">
                                    <div class="modal-header">
                                        @if(Session::get('user_data.id') == $user['data']['id'] && Session::get('user_data.parent_id') == 0)
    
                                            <h5 class="modal-title text-dark" id="deleteBackdropLabel"><?php if (Session::get('app_string')) {
                                                echo Session::get('app_string.edit_profile.account_delete_popup');
                                            } else {
                                                echo CommonHelper::multi_language('edit_profile', 'account_delete_popup')->multi_language_value->language_value;
                                            } ?>
                                            </h5>

                                        @else

                                            <h5 class="modal-title text-dark" id="deleteBackdropLabel"><?php if (Session::get('app_string')) {
                                                echo Session::get('app_string.edit_profile.profile_delete_popup');
                                            } else {
                                                echo CommonHelper::multi_language('edit_profile', 'profile_delete_popup')->multi_language_value->language_value;
                                            } ?>
                                            </h5>

                                        @endif

                                    </div>
                                    <div class="justify-content-center mt-5 mb-2">

                                        <a href="javascript:void(0)"><button type="button"
                                                class="btn font-18 text-yellow border-color col-4 box-radius"
                                                data-bs-dismiss="modal"><?php if (Session::get('app_string')) {
                                                    echo Session::get('app_string.logout.cancel');
                                                } else {
                                                    echo CommonHelper::multi_language('logout', 'cancel')->multi_language_value->language_value;
                                                } ?></button></a>

                                        <a href="{{ route('account-remove', base64_encode($user['data']['id'])) }}"><button
                                                type="button"
                                                class="btn font-18 bg-color text-white col-4 box-radius ms-4"
                                                data-bs-dismiss="modal"><?php if (Session::get('app_string')) {
                                                    echo Session::get('app_string.downloads.delete');
                                                } else {
                                                    echo CommonHelper::multi_language('downloads', 'delete')->multi_language_value->language_value;
                                                } ?></button></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="my-md-5 my-3 text-center">
                        <button class="btn text-white font-20 fw-bold profile-save-btn bg-color p-3 box-radius"
                            type="submit"><?php if (Session::get('app_string')) {
                                echo Session::get('app_string.account_profile.save');
                            } else {
                                echo CommonHelper::multi_language('account_profile', 'save')->multi_language_value->language_value;
                            } ?></button>
                    </div>
                </div>


            </form>
        </div>
    </section>


    <script type="text/javascript">
        $(".profile-edit").click(function() {
            $("#profile_image").click();
        });

        function readURL1(input) {
            if (input.files && input.files[0]) {
                var ext = input.files[0]['name'].split('.').pop();
                if ($.inArray(ext, ['png', 'jpg', 'jpeg','svg']) == -1) {
                    Notiflix.Notify.Failure('<?php if (Session::get('app_string')) {echo Session::get('app_string.add_Profile.invalid_file_type');} else {echo CommonHelper::multi_language('add_Profile', 'invalid_file_type')->multi_language_value->language_value;} ?>');                   
                    return false;
                }

                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah1').attr('src', e.target.result);
                    $('.blah1').attr('href', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
