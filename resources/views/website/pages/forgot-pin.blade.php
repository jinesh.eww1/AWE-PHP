@extends('website.layouts.master')
@section('content')
    @include('website.include.flash-message')


    <style type="text/css">
        .hide-icon,
        .show-icon {
            content: '';
            position: absolute;
            /*background: url('../image/Icon-Hide.png') no-repeat;*/
            width: 28px;
            height: 28px;
            top: 26px;
            right: 40px;
        }
    </style>
    <section>
        <div class="container">
            <div class="mt-md-5 py-5 mb-md-4">
                <a href="{{ url()->previous() }}"><img
                        src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span
                    class="text-white font-18"><?php if (Session::get('app_string')) {
                        echo Session::get('app_string.profile_lock.forgot_pin_title');
                    } else {
                        echo CommonHelper::multi_language('profile_lock', 'forgot_pin_title')->multi_language_value->language_value;
                    } ?></span>
            </div>
            <form method="post" action="{{ route('forgot-pin', base64_encode($id)) }}" class="position-relative">
                @csrf()

                <input type="hidden" name="user_id" value="{{ $user['id'] }}">

                <div class="d-flex justify-content-center  my-4 pt-2">
                    <div class="manage-profile-edit btn bg-white box-radius position-relative">
                        <p class="font-date text-yellow fw-bold text-start my-2 ps-1">{{ $user['first_name'] }}</p>
                        <img class="img-fluid img-size p-4" style="height: 85% !important;"
                            src="{{ $user['profile_picture_full_url'] }}" alt=""onerror="this.src='{{ asset('/assets/website/image/awe-home.png') }}'">
                    </div>
                </div>
                <div class="row justify-content-center text-center pt-2 mt-md-5">
                    <div class="text-white font-date col-lg-4 col-md-8 col-11">
                        <?php if (Session::get('app_string')) {
                            echo Session::get('app_string.forgot_password.edit_profile_lock');
                        } else {
                            echo CommonHelper::multi_language('forgot_password', 'edit_profile_lock')->multi_language_value->language_value;
                        } ?>
                    </div>
                </div>
                <div class="row justify-content-center edit-profile my-5">
                    <div class="col-md-4 col-10 mt-2 position-relative">
                        <div class="lock-icon">
                            <input class="form-control bg-transparent p-4 font-20 text-white ps-5 forgot_pin" minlength="8" maxlength="15"
                                type="password" name="password" placeholder="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.password');
                                } else {
                                    echo CommonHelper::multi_language('login', 'password')->multi_language_value->language_value;
                                } ?>" data-parsley-length="[8, 15]" required="required" id="password_field"
                                data-parsley-required-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.password_required');
                                } else {
                                    echo CommonHelper::multi_language('login', 'password_required')->multi_language_value->language_value;
                                } ?>" 
                                data-parsley-length-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.min_8_password');
                                } else {
                                    echo CommonHelper::multi_language('login','min_8_password')->multi_language_value->language_value;
                                } ?>"
                                >

                            <span class="hide_pass"><img src="{{ asset('assets/website/image/Icon-Hide.png') }}"
                                    class="hide-icon"></span>

                        </div>
                    </div>
                </div>

                <div class="text-center pt-4">
                    <button class="btn profile-save-btn text-white bg-color font-20 fw-bold p-3 my-4 box-radius"
                        type="submit"><?php if (Session::get('app_string')) {
                            echo Session::get('app_string.profile_lock.continue');
                        } else {
                            echo CommonHelper::multi_language('profile_lock', 'continue')->multi_language_value->language_value;
                        } ?></button>
                </div>
            </form>
        </div>
    </section>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.forgot_pin').on("cut copy paste", function(e) {
                e.preventDefault();
            });
        });
    </script>
@endsection
