@extends('website.layouts.master')
@section('content')
@include('website.include.flash-message')

<section>
        <div class="container">
            <div class="mt-md-5 py-5 mb-md-4">
                <a href="{{ route('index') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a>
            </div>

            <div class="text-center mt-5 pt-3">
                <img src="{{ asset('assets/website/image/forgot-img.png') }}">
                <h6 class="text-white font mt-3 pt-3"><?php if(Session::get('app_string')){ echo Session::get('app_string.forgot_password.forgot_password'); }else{ echo CommonHelper::multi_language('forgot_password','forgot_password')->multi_language_value->language_value; } ?></h6>

                <p class="text-forgot mt-3">
                    <?php if(Session::get('app_string')){ echo Session::get('app_string.forgot_password.secondary_title'); }else{ echo CommonHelper::multi_language('forgot_password','secondary_title')->multi_language_value->language_value; } ?>
                </p>

            </div>            

            <form action="{{ route('forgot.password') }}" method="POST" onsubmit="process()">
                @csrf()
                <div class="row justify-content-center forgot-phone">
                    <div class="col-md-4 my-3 block-movie email_div">

                        <input type="email" id="email_field" required name="email" placeholder="<?php if(Session::get('app_string')){ echo Session::get('app_string.forgot_password.email_mobile_number'); }else{ echo CommonHelper::multi_language('forgot_password','email_mobile_number')->multi_language_value->language_value; } ?>" class="about-forgot form-control p-4 rounded text-white" onkeyup="this.value=removeSpaces(this.value); length_count()" data-parsley-required-message="<?php if (Session::get('app_string')) {
                            echo Session::get('app_string.forgot_password.enter_email_mobile_number');
                        } else {
                            echo CommonHelper::multi_language('forgot_password', 'enter_email_mobile_number')->multi_language_value->language_value;
                        } ?>" 
                        data-parsley-type-message="<?php if (Session::get('app_string')) {
                            echo Session::get('app_string.login.email_invalid');
                        } else {
                            echo CommonHelper::multi_language('login', 'email_invalid')->multi_language_value->language_value;
                        } ?>"
                        >
                    </div>

                    <div class="col-md-4 my-3 block-movie phone_field" style="display:none;">

                        <input type="tel" style="width:450px !important; padding-left: 50px !important;" name="phone" data-parsley-type="integer" class="about-forgot form-control p-4 rounded text-white" minlength="6" maxlength="15" placeholder="Enter your mobile number" id="phone" onkeyup="this.value=removeSpaces(this.value); phone_length_count()" oninput="paste_only_digit(this)"

                        data-parsley-required-message="<?php if (Session::get('app_string')) {
                            echo Session::get('app_string.forgot_password.enter_email_mobile_number');
                        } else {
                            echo CommonHelper::multi_language('forgot_password', 'enter_email_mobile_number')->multi_language_value->language_value;
                        } ?>" 
                        data-parsley-type-message="<?php if (Session::get('app_string')) {
                            echo Session::get('app_string.login.email_invalid');
                        } else {
                            echo CommonHelper::multi_language('login', 'email_invalid')->multi_language_value->language_value;
                        } ?>"
                        data-parsley-length-message="<?php if (Session::get('app_string')) {
                            echo Session::get('app_string.register.mobile_number_min_length');
                        } else {
                            echo CommonHelper::multi_language('register','mobile_number_min_length')->multi_language_value->language_value;
                        } ?>">
                        <input type="hidden" name="country_code" id="country_code" value="">
                    </div>

                    <div class="row justify-content-center">
                        <button type="submit" class="btn bg-color p-3 fw-bold text-white font-18 box-radius col-md-4 my-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.forgot_password.send'); }else{ echo CommonHelper::multi_language('forgot_password','send')->multi_language_value->language_value; } ?></button> 
                    </div>
                </div>
            </form> 
        </div>
    </section>

@endsection
