@extends('website.layouts.master')
@section('content')
    @include('website.include.flash-message')

    @if (Session::has('token'))
        @include('website.layouts.head')
    @endif


    <style type="text/css">
        .about-question-input {
            opacity: unset !important;
        }
    </style>
    <section class="mt-4 pt-4">
        <div class="container my-5 pb-5">
            <div class="text-white fw-bold font-28">
                @if (Session::has('token'))
                    <a href="{{ route('profile') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a>
                @endif <?php if (Session::get('app_string')) {
                    echo Session::get('app_string.settings.help');
                } else {
                    echo CommonHelper::multi_language('settings', 'help')->multi_language_value->language_value;
                } ?>
            </div>
            <div>
                <div class="accordion accordion-flush" id="accordionFlushExample">

                    <?php 

                    if(!empty($data) && count($data) > 0){ 
                foreach ($data as $key => $value) { ?>

                    <div class="accordion-item mt-4">

                        <h2 class="accordion-header" id="flush-headingTwo">
                            <button class="accordion-button collapsed font-18 text-white" type="button"
                                data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo{{ $value['id'] }}"
                                aria-expanded="false" aria-controls="flush-collapseTwo{{ $value['id'] }}">
                                {{ $value['question'] }}
                            </button>
                        </h2>

                        <div id="flush-collapseTwo{{ $value['id'] }}" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">

                            <div class="accordion-body">

                                <p class="terms-text font-20">
                                    {{ $value['answer'] }}
                                </p>
                                <p class="text-yellow font-20"><?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.settings.was_this_answer_helpful');
                                } else {
                                    echo CommonHelper::multi_language('settings', 'was_this_answer_helpful')->multi_language_value->language_value;
                                } ?></p>
                                <button class="px-4 py-2 rounded-pill text-yellow font-20 border-color px-4 bg-transparent"
                                    data-bs-toggle="modal" data-bs-target="#help_response"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.settings.yes');
                                    } else {
                                        echo CommonHelper::multi_language('settings', 'yes')->multi_language_value->language_value;
                                    } ?></button>

                                <button class="px-4 py-2 rounded-pill text-white font-20 border-color px-4 ms-3 bg-color"
                                    data-bs-toggle="collapse"
                                    data-bs-target="#feedback_{{ $value['id'] }}"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.settings.no');
                                    } else {
                                        echo CommonHelper::multi_language('settings', 'no')->multi_language_value->language_value;
                                    } ?></button>

                                <form method="post" id="feedback_from_{{ $value['id'] }}">
                                    <div class="mt-4 collapse" id="feedback_{{ $value['id'] }}">
                                        <div style="display: grid;">
                                            <textarea data-parsley-required class="terms-text w-100 p-2 px-3" id="feedback_text{{ $value['id'] }}"
                                                placeholder="<?php if (Session::get('app_string')) {
                                                    echo Session::get('app_string.help_screen.add_feedback_you_want');
                                                } else {
                                                    echo CommonHelper::multi_language('help_screen', 'add_feedback_you_want')->multi_language_value->language_value;
                                                } ?>"
                                                data-parsley-required-message="<?php if (Session::get('app_string')) {
                                                    echo Session::get('app_string.help_screen.please_provide_your_feedback');
                                                } else {
                                                    echo CommonHelper::multi_language('help_screen', 'please_provide_your_feedback')->multi_language_value->language_value;
                                                } ?>" 
                                                ></textarea>
                                                
                                            <button type="button"
                                                class="btn text-white bg-color p-3 fw-bold mt-2 feedback_btn"
                                                data-id="{{ $value['id'] }}"><?php if (Session::get('app_string')) {
                                                    echo Session::get('app_string.help_screen.submit');
                                                } else {
                                                    echo CommonHelper::multi_language('help_screen', 'submit')->multi_language_value->language_value;
                                                } ?></button>
                                        </div>
                                    </div>
                                </form>

                            </div>

                        </div>

                    </div>

                    <?php } } ?>

                </div>

                <div class="mt-5 position-relative about-question">
                    <div class="font-20 text-white fw-bold p-4 text-start about-click"><?php if (Session::get('app_string')) {
                        echo Session::get('app_string.help_screen.cant_find_que');
                    } else {
                        echo CommonHelper::multi_language('help_screen', 'cant_find_que')->multi_language_value->language_value;
                    } ?></div>
                    <p class="text-white position-absolute ask-us"><?php if (Session::get('app_string')) {
                        echo Session::get('app_string.help_screen.ask_to_us');
                    } else {
                        echo CommonHelper::multi_language('help_screen', 'ask_to_us')->multi_language_value->language_value;
                    } ?></p>
                    <div class="px-lg-5 pb-lg-4 p-3 " style="display:block !important;">

                        <form method="post" id="ask_form">
                            @csrf()
                            <input type="text" name="question" id="question"
                                class="w-100 about-question-input text-bold p-lg-4 p-3" required
                                data-parsley-required-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.help_screen.Please_enter_the_feedback_text');
                                } else {
                                    echo CommonHelper::multi_language('help_screen','Please_enter_the_feedback_text')->multi_language_value->language_value;
                                } ?>" 
                                >
                            <div class="text-center mt-4 pt-3 mb-2">
                                <button type="button"
                                    class="btn text-white bg-color p-3 fw-bold click-submit"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.help_screen.submit');
                                    } else {
                                        echo CommonHelper::multi_language('help_screen', 'submit')->multi_language_value->language_value;
                                    } ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div class="modal fade" id="help_response" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered text-center">
            <div class="modal-content text-center p-4">
                <div class="modal-header" style="border-bottom: none; display: unset !important;">
                    <h5 class="modal-title" id="staticBackdropLabel"><?php if (Session::get('app_string')) {
                        echo Session::get('app_string.help_screen.thanks_for_your_feedback');
                    } else {
                        echo CommonHelper::multi_language('help_screen', 'thanks_for_your_feedback')->multi_language_value->language_value;
                    } ?></h5>
                </div>
                <div class="justify-content-center mt-5 mb-2">
                    <a href="javascript:void(0)">
                        <button type="button" id="help_popup_btn"
                            class="btn font-18 bg-color text-white col-4 box-radius ms-4 col-md-6"
                            data-bs-dismiss="modal"><?php if (Session::get('app_string')) {
                                echo Session::get('app_string.movie_details.ok');
                            } else {
                                echo CommonHelper::multi_language('movie_details', 'ok')->multi_language_value->language_value;
                            } ?></button></a>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        $(document).ready(function() {
            $("input").keypress(function() {
                if (event.which == 13) {
                    return false;
                }
            });
        });


        $('#help_popup_btn').click(function() {
            $('.accordion-collapse').removeClass('show');
        });
        
        $('.feedback_btn').click(function() {

            var id = $(this).attr('data-id');
            var feedback_text = $('#feedback_text' + id).val();
            var user_id = '{{ !empty(Session::get('user_data.id')) ? Session::get('user_data.id') : 0 }}';

            feedback_text = $.trim(feedback_text);

            if (feedback_text == '') {
                $('#feedback_from_' + id).submit();
                return false;
            }

            $('#loader').show();
            $.ajax({
                type: "get",
                dataType: "json",
                url: "{{ route('feedback') }}",
                data: {
                    'id': id,
                    "feedback_text": feedback_text,
                    'user_id': user_id
                },

                success: function(returnData) {
                    $('#loader').hide();
                    if (returnData.status == true) {
                        $('#feedback_text' + id).val('');
                        $('.accordion-collapse').removeClass('show');
                        $('#help_response').modal('show');
                    } else {
                        Notiflix.Notify.Failure(returnData.message);
                    }
                },
                error: function(data) {},
            });


        });



        $('.click-submit').click(function() {

            var question = $.trim($('#question').val());
            var token = $('input[name=_token]').val();

            if (question == '') {
                $('#ask_form').submit();
                return false;
            }
            $('#loader').show();
            $.ajax({
                type: "post",
                dataType: "json",
                url: "{{ route('ask-to-us') }}",
                data: {
                    'question': question,
                    "_token": token
                },
                success: function(returnData) {
                    $('#loader').hide();
                    if (returnData == 1) {
                        
                        $('#question').val('');
                        $('#help_response').modal('show');
                    }
                },
                error: function(data) {},
            });


        });
    </script>

    @if (Session::has('token'))
        @include('website.layouts.footer')
    @endif
@endsection
