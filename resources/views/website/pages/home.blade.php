@extends('website.layouts.master')
<?php header("Access-Control-Allow-Origin: *"); ?>
@section('content')
@include('website.include.flash-message')
@include('website.layouts.head')
<?php header("Access-Control-Allow-Origin: *"); ?>

<style type="text/css">
    .bg-body {
      background: linear-gradient(rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.89));
    }
</style>

        <?php
        
            if(isset($home['data']['main']['detail_poster']) && !empty($home['data']['main']['detail_poster'])){
                $main_baner = $home['data']['main']['detail_poster'];
            }else{
                $main_baner = asset('assets/website/image/awe-home.png');
            }
        ?>

        <section class="bg-body-home about-banner-video about-video-margin" style="background: url('{{$main_baner}}');">
        <input type="hidden" name="device_name" id="device_name">

            @if(!empty($home['data']['main']))
            <div class="container">
                <div class="font-18 banner-section">
                    <h1 class="text-white fw-bold mb-3">{{ !empty($home['data']['main']['name'])?$home['data']['main']['name']:'' }} 
                        <?php if(!empty($home['data']['main']['is_free']) && $home['data']['main']['is_free'] == 1){ ?>
                            <label class="btn bg-color border-color font-20 text-white free_label mx-3"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.free'); }else{ echo CommonHelper::multi_language('home','free')->multi_language_value->language_value; } ?></label>
                        <?php } ?>
                    </h1>

                    
                    @if($home['data']['main']['likes']>0)
                    <div class="d-flex align-items-center justify-content-between like-mobile my-3 total_likes_div">
                        <div>
                            <img src="{{ asset('assets\website\image/thumbs-up.png') }}" class="likes_dislikes_img">
                            <span class="text-white font-14 mx-1 total_likes">{{$home['data']['main']['likes']}} </span>
                        </div>
                        <div class="text-white font-14 mx-1">{{ $home['data']['main']['year'] }}</div>
                        <div class="text-white font-14 mx-1">{{ $home['data']['main']['age_rating_name'] }}</div>
                    </div>
                    @else
                    <div class="d-flex align-items-center justify-content-between like-mobile my-3 total_likes_div">
                        <div class="text-white font-14 mx-1">{{$home['data']['main']['year'] }}</div>
                        <div class="text-white font-14 mx-1">{{ $home['data']['main']['age_rating_name'] }}</div>
                    </div>
                    @endif
                    
                    <div class="row mx-0"><p class="text-white col-md-5 fw-light mb-1 more" id="movie-content">{{ !empty($home['data']['main']['synopsis'])?$home['data']['main']['synopsis']:'' }}</p></div>

                    <div class="mt-4 text-center text-sm-start">
                        <button class="bg-white btn play-btn my-2 my-lg-0" data-toggle="modal" data-target="#basicModal" id="trailer_play_btn">
                            <img src="{{ asset('assets\website\image/play-btn.png') }}" class="mb-1">
                            <span class="font-23 ms-3"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.trailer'); }else{ echo CommonHelper::multi_language('movie_details','trailer')->multi_language_value->language_value; } ?></span>
                        </button>

                        <button class="bg-color btn ms-md-4 play-btn my-2" data-toggle="modal" data-target="#videoModal" id="video_play_btn">
                            <img src="{{ asset('assets\website\image/play-btn-w.png') }}" class="mb-2">
                            <span class="font-23 text-white ms-3"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.play'); }else{ echo CommonHelper::multi_language('movie_details','play')->multi_language_value->language_value; } ?></span>
                        </button>

                    </div>
                    <div class="mt-3 text-center text-sm-start">
                        <?php 
                        $mylistcolor = '';$mlstatus = 1;
                        if(!empty($home['data']['main']['my_list']) && $home['data']['main']['my_list'] == true){
                            $mylistcolor = 'bg-color'; $mlstatus = 0;
                        }
                        ?>
                        <button class="btn {{ $mylistcolor }} about-forgot border-color w-131 mylist_function" data-id="{{!empty($home['data']['main']['id'])?$home['data']['main']['id']:''}}" data-status="{{$mlstatus}}">
                            <img src="{{ asset('assets\website\image/plus.png') }}">
                            <span class="text-white font-14 mx-1"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.my_list'); }else{ echo CommonHelper::multi_language('home','my_list')->multi_language_value->language_value; } ?></span>
                        </button>

                        <?php 
                        $likecolor = '';$lstatus = 1;
                        if(!empty($home['data']['main']['liked']) && $home['data']['main']['liked'] == true){
                            $likecolor = 'bg-color'; 
                            $lstatus = 0;
                        }
                        ?>
                        <button class="btn {{ $likecolor }} about-forgot border-color w-131 ms-md-3 liked_function" data-id="{{!empty($home['data']['main']['id'])?$home['data']['main']['id']:''}}" data-status="{{$lstatus}}">
                            <img src="{{ asset('assets\website\image/like.png') }}">
                            <span class="text-white font-14 liked_lbl mx-1">
                                <?php if(Session::get('app_string')){ echo ($home['data']['main']['liked'])?Session::get('app_string.movie_details.liked'):Session::get('app_string.movie_details.like'); }else{ echo ($home['data']['main']['liked'])?CommonHelper::multi_language('movie_details','liked')->multi_language_value->language_value:CommonHelper::multi_language('movie_details','like')->multi_language_value->language_value; } ?></span>
                        </button>

                        <?php 
                        $dlikecolor = '';$dlstatus = 1;
                        if(!empty($home['data']['main']['disliked']) && $home['data']['main']['disliked'] == true){
                            $dlikecolor = 'bg-color'; $dlstatus = 0;
                        }
                        ?>
                        <button class="btn {{ $dlikecolor }} about-forgot border-color w-131 ms-md-3 my-2 disliked_function" data-id="{{!empty($home['data']['main']['id'])?$home['data']['main']['id']:''}}" data-status="{{$dlstatus}}">
                            <img src="{{ asset('assets\website\image/dislike.png') }}">
                            <span class="text-white font-14 disliked_lbl mx-1"><?php if(Session::get('app_string')){ echo ($home['data']['main']['disliked'])?Session::get('app_string.movie_details.disliked'):Session::get('app_string.movie_details.dislike'); }else{ echo ($home['data']['main']['disliked'])?CommonHelper::multi_language('movie_details','disliked')->multi_language_value->language_value:CommonHelper::multi_language('movie_details','dislike')->multi_language_value->language_value; } ?></span>
                        </button>
                    </div>
                </div>
            </div>
            @endif
        </section>

<main class="about-main pb-lg-5 mb-5">
    <section class="mb-lg-5">
        <div class="container">
            <?php 
                if(!empty($home['data']['data']) && count($home['data']['data'])>0){
                    foreach ($home['data']['data'] as $key => $value) { 

                    if($value['name'] == 'Recommended'){ ?>
                        <div class="mt-5 pt-lg-5">
                            
                                <div class="">
                                    <span class="font-28 text-white"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.recommended'); }else{ echo CommonHelper::multi_language('home','recommended')->multi_language_value->language_value; } ?></span>
                                </div>
                                <div class="mt-5">
                                    <div class="text-center slider-home">
                                        
                                        <?php foreach ($value['content'] as $rckey => $rcvalue) { ?>
                                            <div class="col-md-2 hover-img-list position-relative">
                                                <div class="d-flex justify-content-center">
                                                    <?php if($rcvalue['is_free'] == 1){ ?>
                                                        @switch(Session::get('locale'))
                                                            @case("sp")
                                                                <img src="{{ asset('assets/website/image/card/Spanish.png') }}" class="img-fluid rec_free_tag_img">
                                                            @break
                                                            @case("md")
                                                                <img src="{{ asset('assets/website/image/card/Mandarin.png') }}" class="img-fluid rec_free_tag_img">
                                                            @break
                                                            @case("hi")
                                                                <img src="{{ asset('assets/website/image/card/hindi.png') }}" class="img-fluid rec_free_tag_img">
                                                            @break
                                                            @case("fr")
                                                                <img src="{{ asset('assets/website/image/card/French.png') }}" class="img-fluid rec_free_tag_img">
                                                            @break
                                                            @default
                                                                <img src="{{ asset('assets/website/image/card/free.png') }}" class="img-fluid rec_free_tag_img">
                                                        @endswitch
                                                        
                                                    <?php } ?>
                                                    <img src="{{ $rcvalue['poster'] }}" class="img-fluid rec_image img-height box-radius">
                                                    <a href="{{ route('content-details',$rcvalue['slug'])}}" class="btn play-hover-list w-100 h-100 position-absolute"><img src="{{ asset('assets/website/image/play.png') }}" class="img-fluid"></a>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        

                                    </div>
                                </div>
                            
                        </div>
                    <?php }else{ ?>
                        
                <div class="pt-lg-3 mt-md-4">
                    <div class="my-4">
                        <span class="font-28 text-white">{{ $value['name'] }}</span>
                        @if($value['view_more'] == true)<a href="{{ route('view.more',[$type,base64_encode($value['id'])]) }}" class="text-yellow float-end mt-2"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.view_more'); }else{ echo CommonHelper::multi_language('home','view_more')->multi_language_value->language_value; } ?> </a>@endif
                    </div>
                    <div class="">
                        <div class="about-grid-selction text-center">
                            <?php foreach ($value['content'] as $ckey => $cvalue) { ?>
                                <div class="hover-img-list position-relative">
                                    <?php if($cvalue['is_free'] == 1){ ?>
                                        @switch(Session::get('locale'))
                                            @case("sp")
                                                <img src="{{ asset('assets/website/image/card/Spanish.png') }}" class="img-fluid home_free_tag_img">
                                            @break
                                            @case("md")
                                                <img src="{{ asset('assets/website/image/card/Mandarin.png') }}" class="img-fluid home_free_tag_img">
                                            @break
                                            @case("hi")
                                                <img src="{{ asset('assets/website/image/card/hindi.png') }}" class="img-fluid home_free_tag_img">
                                            @break
                                            @case("fr")
                                                <img src="{{ asset('assets/website/image/card/French.png') }}" class="img-fluid home_free_tag_img">
                                            @break
                                            @default
                                                <img src="{{ asset('assets/website/image/card/free.png') }}" class="img-fluid home_free_tag_img">
                                        @endswitch
                                    <?php } ?>
                                    <img src="{{ $cvalue['poster'] }}" class="img-fluid img-height box-radius">
                                    <a href="{{ route('content-details',$cvalue['slug'])}}" class="btn play-hover-list w-100 h-100 position-absolute"><img src="{{ asset('assets/website/image/play.png') }}" class="img-fluid"></a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            <?php } } } ?>
        </div>
    </section>  
</main>


{{-- player model start --}}
@if(!empty($home['data']['main']))
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog" style="max-width:100% !important;">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close modal-video-close cancel_video_play_btn" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-header video_data">
                <video id="trailer_video" class="video-js vjs-default-skin" controls crossorigin playsinline data-poster="{{!empty($home['data']['main']['detail_poster'])?$home['data']['main']['detail_poster']:''}}"  width="600" height="240">
                    <source src="{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net', 'https://awe-dev.s3.us-east-2.amazonaws.com', $home['data']['main']['trailer']) }}" type="video/mp4" size="720" />
                </video>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal">
    <div class="modal-dialog m-0" style="max-width:100% !important;">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close modal-video-close cancel_video_play_btn" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-header video_data">
                  <video id="my_video_1" class="video-js vjs-default-skin" controls crossorigin playsinline data-poster="{{!empty($home['data']['main']['detail_poster'])?$home['data']['main']['detail_poster']:''}}"  width="600" height="240">

                    @if(Session::get('user_data.allow_download_preference') == 1)
                        <source src="{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net', 'https://awe-dev.s3.us-east-2.amazonaws.com', $home['data']['main']['video'][0]['video'] ) }}" label="SD" type="application/x-mpegURL">
                    @else
                        <source src="{{ str_replace('https://d3ob6u4l0x8h65.cloudfront.net', 'https://awe-dev.s3.us-east-2.amazonaws.com', $home['data']['main']['video'][0]['low_quality_video'] ) }}" label="SD" type="application/x-mpegURL">
                    @endif
                        
                  </video>
            </div>
        </div>
    </div>
</div>
@endif
{{-- player model end--}}
{{-- Player script start --}}


@if(!empty($home['data']['main']))
<link rel="stylesheet" href="{{ asset('assets/website/css/video-js.css') }}" />
<script type="text/javascript" src="{{ asset('assets/website/js/video.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/website/js/videojs-seek-buttons.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('assets/website/css/videojs-seek-buttons.css') }}" />
<script type="text/javascript" src="{{ asset('assets/website/js/videojs-media-sources.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/website/js/videojs-playlist.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('assets/website/css/video-js.min.css') }}" />

<script type="text/javascript" src="{{ asset('assets/website/js/socket.io.min.js') }}"></script>
 
<script type="text/javascript">

var options = {
    playbackRates: [1, 1.25, 1.5, 1.75, 2],
    controlBar: {
        volumePanel: {
            inline: false
        }
    }
    };
var movie_video = videojs('my_video_1',options);
movie_video.seekButtons({
            forward: 15,
            back: 15
        });

//movie_video.hlsQualitySelector();

var trailer_video = videojs('trailer_video',options);
trailer_video.seekButtons({
            forward: 15,
            back: 15
        });


    $(document).keyup(function(e) {
        if (e.key === "Escape") { 
            if($('#videoModal').hasClass("show")){
            }else{
                
                movie_video.pause();
                clearInterval(window.cwtimer);
            }
        }
        if($('#basicModal').hasClass("show")){
        }else{
            
            trailer_video.pause();
            clearInterval(window.cwtimer);
        }
    });
   
    $('#trailer_play_btn').on('click', function(){
        
        trailer_video.play();
    });

     $('.cancel_video_play_btn').on('click', function(){
        
        clearInterval(window.cwtimer);
        
        trailer_video.pause();

        videojs('my_video_1').pause();
        $('#videoModal').modal('hide'); 

    });

    $('#video_play_btn').on('click', function(){

        if("{{$home['data']['main']['content_type']}}" != '' && "{{$home['data']['main']['content_type']}}" == 2){
            window.location.href = "{{ route('content-details',$home['data']['main']['slug']) }}";
            return false;
        }

        if('{{ Session::get("user_data.plan_id") }}' == '' && '{{ !empty($home["data"]["main"]["is_free"])?$home["data"]["main"]["is_free"]:"" }}' == 0){
            
            movie_video.pause();
            movie_video.on("pause", function () {
                clearInterval(window.cwtimer);
            });

            $('.cancel_video_play_btn').click();

            $('#parent_subscription_popup').modal('show');
            
            return false;
        }


        movie_video.currentTime("{{!empty($home['data']['main']['video'][0]['watched_duration_secs'])?$home['data']['main']['video'][0]['watched_duration_secs']:0}}");
        movie_video.play();

        movie_video.on("play", function () {

            var fseconds = 1;
            var socket = io("https://awemovies.com:3000/");
            socket.on("connect", function() {
              var id = parseInt('{{ Session::get("user_data.id")}}');
              socket.emit('connect_user', { user_id: id});
              console.log("User connetcted");
            });

            var user_id = '{{ Session::get("user_data.id") }}';
            var parent_id = '{{ Session::get("user_data.parent_id") }}';
            var content_id = "{{!empty($home['data']['main']['video'][0]['content_id'])?$home['data']['main']['video'][0]['content_id']:0}}";
            var season_id = "{{!empty($home['data']['main']['video'][0]['season_id'])?$home['data']['main']['video'][0]['season_id']:0}}";
            var video_id = "{{!empty($home['data']['main']['video'][0]['id'])?$home['data']['main']['video'][0]['id']:0}}";
            var udid = '{{ Session::get("device_token") }}';
            var device_name = $('#device_name').val();
            var plan_id = '{{ Session::get("user_data.plan_id") }}';
            var plan_id = parseInt('{{ !empty(Session::get("user_data.plan_id"))?Session::get("user_data.plan_id"):0 }}');


            socket.on('watching', function(data) {
                
                if(data.message == 'stop_watching' && data.udid == '{{ Session::get("device_token") }}'){
                    movie_video.pause();
                    movie_video.on("pause", function () {
                        clearInterval(window.cwtimer);
                    });

                    $('.cancel_video_play_btn').click();
                   // $('#videoModal').modal('hide');

                    $('#screen_limit_device_name').empty().html(data.watching_data.data[0]['device_name']+' : '+data.watching_data.data[0]['name']);
                    $('#screen_limit_popup').modal('show');
                }
            });

            socket.emit('watching', { user_id: user_id, parent_id:parent_id, content_id: content_id, season_id: season_id, video_id: video_id, seconds: fseconds, udid: udid, device_name: device_name, plan_id: plan_id});

            window.cwtimer = setInterval(function() {
                var seconds = movie_video.currentTime();
                var socket = io("https://awemovies.com:3000/");
                socket.on("connect", function() {
                  var id ='{{ Session::get("user_data.id")}}';
                  socket.emit('connect_user', { user_id: id});
                  console.log("User connetcted");
                });

                var user_id = '{{ Session::get("user_data.id") }}';
                var parent_id = '{{ Session::get("user_data.parent_id") }}';
                var content_id = "{{!empty($home['data']['main']['video'][0]['content_id'])?$home['data']['main']['video'][0]['content_id']:0}}";
                var season_id = "{{!empty($home['data']['main']['video'][0]['season_id'])?$home['data']['main']['video'][0]['season_id']:0}}";
                var video_id = "{{!empty($home['data']['main']['video'][0]['id'])?$home['data']['main']['video'][0]['id']:0}}";
                var udid = '{{ Session::get("device_token") }}';
                var device_name = $('#device_name').val();
                var plan_id = '{{ Session::get("user_data.plan_id") }}';

                socket.on('watching', function(data) {
                    if(data.message == 'stop_watching' && data.udid == '{{ Session::get("device_token") }}'){
                        
                        movie_video.pause();
                        $('#videoModal').modal('hide');
                        $('#screen_limit_popup').modal('show');
                    }
                });

                socket.emit('watching', { user_id: user_id, parent_id:parent_id, content_id: content_id, season_id: season_id, video_id: video_id, seconds: seconds, udid: udid, device_name: device_name, plan_id: plan_id});
            }, 10000);    
        });
        

        movie_video.on("pause", function () {
            clearInterval(window.cwtimer);
        });
        
    });

    if (document.pictureInPictureElement !== null) {
        
        movie_video.on("play", function () {
            window.pipcwtimer = setInterval(function() {
                var seconds = movie_video.currentTime();
                var socket = io("https://awemovies.com:3000/");
                socket.on("connect", function() {
                  var id ='{{ Session::get("user_data.id")}}';
                  socket.emit('connect_user', { user_id: id});
                  console.log("User connetcted");
                });

                var user_id = '{{ Session::get("user_data.id") }}';
                var parent_id = '{{ Session::get("user_data.parent_id") }}';
                var content_id = "{{!empty($home['data']['main']['video'][0]['content_id'])?$home['data']['main']['video'][0]['content_id']:''}}";
                var season_id = "{{!empty($home['data']['main']['video'][0]['season_id'])?$home['data']['main']['video'][0]['season_id']:''}}";
                var video_id = "{{!empty($home['data']['main']['video'][0]['id'])?$home['data']['main']['video'][0]['id']:''}}";
                
                var udid = '{{ Session::get("device_token") }}';
                var device_name = $('#device_name').val();
                var plan_id = '{{ Session::get("user_data.plan_id") }}';

                socket.on('watching', function(data) {
                    if(data.message == 'stop_watching' && data.udid == '{{ Session::get("device_token") }}'){
                        movie_video.pause();
                        $('#videoModal').modal('hide');
                        $('#screen_limit_popup').modal('show');
                    }
                });

                socket.emit('watching', { user_id: user_id, parent_id:parent_id, content_id: content_id, season_id: season_id, video_id: video_id, seconds: seconds, udid: udid, device_name: device_name, plan_id: plan_id});
            }, 10000);    
        });
        

        movie_video.on("pause", function () {
            clearInterval(window.pipcwtimer);
        });

        
    }

$(document).ready(function() {
    var showChar = 200; // How many characters are shown by default
    var ellipsestext = "";
    var moretext = "<?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.show_more'); }else{ echo CommonHelper::multi_language('movie_details','show_more')->multi_language_value->language_value; } ?>";
    var lesstext = "<?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.show_less'); }else{ echo CommonHelper::multi_language('movie_details','show_less')->multi_language_value->language_value; } ?>";

    $('.more').each(function() {
        var content = $(this).html();

        if (content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses ">' + ellipsestext +
                '&nbsp;</span><span class="morecontent "><span>' + h +
                '</span>&nbsp;&nbsp;<a href="" class="morelink text-yellow">' + moretext +
                '</a></span>';

            $(this).html(html);
        }

    });
    $(".morelink").click(function() {
        if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });
});
</script>

@endif

@include('website.layouts.footer')
@endsection
