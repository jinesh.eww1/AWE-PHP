@extends('website.layouts.master')
@section('content')
@include('website.include.flash-message')


<style type="text/css">
    
.hide-icon, .show-icon {
    content: '';
    position: absolute;
    /*background: url('../image/Icon-Hide.png') no-repeat;*/
    width: 28px;
    height: 28px;
    top: 26px;
    right: 40px;
}

</style>
<section>
    <div class="container">
        <div class="mt-md-5 py-5 mb-md-4">
            
            @if(!empty($type))
                <a href="{{ route('whos.watching') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> 
            @else
                <a href="{{ route('manage-profile') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> 
            @endif
            
            <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.edit_profile.profile_lock'); }else{ echo CommonHelper::multi_language('edit_profile','profile_lock')->multi_language_value->language_value; } ?></span>
        </div>
        <form method="post" id="lock_profile_form" action="{{ route('lock-profile',base64_encode($id))}}" class="position-relative">
            @csrf()
            
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">
            <input type="hidden" name="type" value="{{ $type }}">

            <input type="hidden" name="device_name" id="device_name" value="{{ $type }}">
            <input type="hidden" name="ip" id="device_ip">

            <?php /*<input type="hidden" name="latitude" id="latitude" value="">
            <input type="hidden" name="longitude" id="longitude" value="">*/ ?>

            <div class="d-flex justify-content-center  my-4 pt-2">
                <div class="manage-profile-edit btn bg-white box-radius position-relative">
                     <p class="font-date text-yellow fw-bold text-start mt-2 ps-1 mb-0">{{ $user['first_name'] }}</p>
                     
                        <img class="img-fluid px-4 py-3" src="{{ $user['profile_picture_full_url'] }}" id="manage-profile-img" alt=""onerror="this.src='{{ asset('/assets/website/image/awe-home.png') }}'">
                    
                </div>
            </div>


            <div class="row justify-content-center text-center pt-2 mt-md-5">
                <div class="text-white font-date col-lg-4 col-md-8 col-11">
                    <?php if(Session::get('app_string')){ echo Session::get('app_string.profile_lock.account_password_to_edit_profile'); }else{ echo CommonHelper::multi_language('profile_lock','account_password_to_edit_profile')->multi_language_value->language_value; } ?>
                </div>
            </div>
            <div class="row justify-content-center edit-profile my-5">
                <div class="col-md-4 col-10 mt-2 position-relative">
                    <div class="lock-icon">
                        <input class="form-control bg-transparent p-4 font-20 text-white ps-5" type="password" name="password" placeholder="<?php if(Session::get('app_string')){ echo Session::get('app_string.login.password'); }else{ echo CommonHelper::multi_language('login','password')->multi_language_value->language_value; } ?>" data-parsley-length="[4,4]"maxlength="4" pattern="[0-9]*" inputmode="numeric"data-parsley-type="digits"  onkeypress='validate(event)' required id="password_field"
                        data-parsley-required-message="<?php if (Session::get('app_string')) {
                            echo Session::get('app_string.login.password_required');
                        } else {
                            echo CommonHelper::multi_language('login', 'password_required')->multi_language_value->language_value;
                        } ?>" 
                        data-parsley-length-message="<?php if (Session::get('app_string')) {
                            echo Session::get('app_string.profile_lock.pin_min_value_validation');
                        } else {
                            echo CommonHelper::multi_language('profile_lock', 'pin_min_value_validation')->multi_language_value->language_value;
                        } ?>" 
                        >

                        <span class="hide_pass"><img src="{{ asset('assets/website/image/Icon-Hide.png') }}" class="hide-icon"></span>

                    </div>
                </div>
            </div>
            <div class="text-center pt-4">
                <a href="{{ route('forgot-pin',base64_encode($id))}}" class="text-white font-20"><?php if(Session::get('app_string')){ echo Session::get('app_string.profile_lock.forgot_pin'); }else{ echo CommonHelper::multi_language('profile_lock','forgot_pin')->multi_language_value->language_value; } ?></a>
            </div>

            <div class="text-center pt-4">
                <button class="btn profile-save-btn text-white bg-color font-20 fw-bold p-3 my-4 box-radius" type="submit"><?php if(Session::get('app_string')){ echo Session::get('app_string.profile_lock.continue'); }else{ echo CommonHelper::multi_language('profile_lock','continue')->multi_language_value->language_value; } ?></button>
            </div>
        </form>
    </div>
</section>

<script type="text/javascript">

    $(document).on('keyup keypress', 'input', function(e) {
          if(e.which == 13) {
            $('#lock_profile_form').submit();
          }
        });


    function validate(evt) {
        var theEvent = evt || window.event;

        // Handle paste
        if (theEvent.type === 'paste') {
            key = event.clipboardData.getData('text/plain');
        } else {
            // Handle key press
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key)
    ;
        }
        var regex = /[0-9]/;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }
</script>
@endsection
