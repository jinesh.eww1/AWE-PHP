@extends('website.layouts.master')
@section('content')

    <script type="text/javascript"
        src="https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js"></script>

    <style type="text/css">
        #phone {
            padding-right: 53px !important;
        }

        .alert {
            position: unset !important;
        }
    </style>


    <section>
        <div class="container">
            <div class="text-center pt-lg-5">
                <a href="javascript:void(0)"><img src="{{ asset('assets/website/image/awe-home.png') }}" class="img-fluid mt-5 box-radius"
                        height="110px" width="110px"></a>
                <h1 class="text-white font-28 mt-3 fw-bold"><?php if (Session::get('app_string')) {
                    echo Session::get('app_string.login.welcome');
                } else {
                    echo CommonHelper::multi_language('login', 'welcome')->multi_language_value->language_value;
                } ?></h1>

                <div class="option-hr d-inline-block">
                    <select class="text-white" id="language_ddl">
                        @if (!empty($language))
                            @foreach ($language as $key => $value)
                                <?php if (app()->getLocale() == $value['api_key']) {
                                    $selected_lan = 'selected';
                                } else {
                                    $selected_lan = '';
                                } ?>
                                <option value="{{ $value['api_key'] }}" data-id="{{ $value['id'] }}" {{ $selected_lan }}>
                                    {{ $value['type'] }}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
            </div>
            <div class="row justify-content-center pb-5">
                <div class="col-11 col-md-10 col-lg-6 bg-white mt-4 box-radius">
                    <div class="row text-center py-3 mt-md-4 font-18">
                        <div class="col-md-6 signin-border fw-bold p-3"><?php if (Session::get('app_string')) {
                            echo Session::get('app_string.login.sign_in');
                        } else {
                            echo CommonHelper::multi_language('login', 'sign_in')->multi_language_value->language_value;
                        } ?></div>
                        <div class="col-md-6 p-3 signup-border">
                            <a href="{{ route('signup') }}" class="text-decoration-none  text-muted"><?php if (Session::get('app_string')) {
                                echo Session::get('app_string.register.sign_up');
                            } else {
                                echo CommonHelper::multi_language('register', 'sign_up')->multi_language_value->language_value;
                            } ?></a>
                        </div>
                    </div>

                    @include('website.include.flash-message')

                    <form class="row justify-content-center" id="signin_form" action="{{ route('index') }}" method="post"
                        onsubmit="process()">
                        @csrf()

                        <?php /*<input type="hidden" name="latitude" id="latitude">
                        <input type="hidden" name="longitude" id="longitude">*/ ?>

                        <input type="hidden" name="ip" id="device_ip">
                        <input type="hidden" name="device_name" id="device_name">
                        <input type="hidden" name="device_token" id="device_token" value="">

                        <div class="mt-4 col-md-8 email_div">
                            <label class="form-label"><?php if (Session::get('app_string')) {
                                echo Session::get('app_string.login.enter_email_phone');
                            } else {
                                echo CommonHelper::multi_language('login', 'enter_email_phone')->multi_language_value->language_value;
                            } ?><span class="text-danger">
                                    *</span></label><br>
                            <input type="email" name="email" value="{{ old('email') }}" required
                            data-parsley-validate-full-width-characters="true"
                                class="form-control border-color px-4 py-3 box-radius " placeholder="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.enter_email_phone');
                                } else {
                                    echo CommonHelper::multi_language('login', 'enter_email_phone')->multi_language_value->language_value;
                                } ?>"
                                id="email_field" onkeyup="this.value=removeSpaces(this.value); length_count()"
                                data-parsley-validate-full-width-characters-message="<?php if (Session::get('app_string')) { echo Session::get('app_string.please_enter_valid_email.app_titles_and_messages'); } else { echo CommonHelper::multi_language('please_enter_valid_email', 'app_titles_and_messages')->multi_language_value->language_value;} ?>"
                                data-parsley-required-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.forgot_password.enter_email_mobile_number');
                                } else {
                                    echo CommonHelper::multi_language('forgot_password', 'enter_email_mobile_number')->multi_language_value->language_value;
                                } ?>" 
                                data-parsley-type-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.email_invalid');
                                } else {
                                    echo CommonHelper::multi_language('login', 'email_invalid')->multi_language_value->language_value;
                                } ?>"
                                                                  
                                >

                        </div>

                        <div class="mt-4 col-md-8 phone_field" style="display:none">
                            <label class="form-label"><?php if (Session::get('app_string')) {
                                echo Session::get('app_string.register.mobile');
                            } else {
                                echo CommonHelper::multi_language('register', 'mobile')->multi_language_value->language_value;
                            } ?><span class="text-danger">
                                    *</span></label><br>
                            <input type="tel" name="phone" inputmode="numeric" value="{{ old('phone') }}" data-parsley-type="integer"
                                class="form-control border-color px-4 py-3 box-radius" minlength="6" maxlength="15"
                                placeholder="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.enter_email_phone');
                                } else {
                                    echo CommonHelper::multi_language('login', 'enter_email_phone')->multi_language_value->language_value;
                                } ?>" id="phone" onkeyup="this.value=removeSpaces(this.value); phone_length_count()" 
                                 data-parsley-required-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.forgot_password.enter_email_mobile_number');
                                } else {
                                    echo CommonHelper::multi_language('forgot_password', 'enter_email_mobile_number')->multi_language_value->language_value;
                                } ?>" 
                                data-parsley-type-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.email_invalid');
                                } else {
                                    echo CommonHelper::multi_language('login', 'email_invalid')->multi_language_value->language_value;
                                } ?>"
                                data-parsley-length-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.register.mobile_number_min_length');
                                } else {
                                    echo CommonHelper::multi_language('register','mobile_number_min_length')->multi_language_value->language_value;
                                } ?>">
                            <input type="hidden" name="country_code" id="country_code" value="">
                            <input type="hidden" name="country_iso2" id="country_iso2" value="">
                        </div>

                        <div class="mt-4 col-md-8 position-relative">
                            <label class="form-label"><?php if (Session::get('app_string')) {
                                echo Session::get('app_string.login.password');
                            } else {
                                echo CommonHelper::multi_language('login', 'password')->multi_language_value->language_value;
                            } ?><span class="text-danger"> *</span></label>
                            <input type="password" autocomplete="off" data-parsley-length="[8, 15]" maxlength="15" name="password"
                                value="{{ old('password') }}" required
                                class="form-control border-color px-4 py-3 box-radius" placeholder="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.enter_password');
                                } else {
                                    echo CommonHelper::multi_language('login', 'enter_password')->multi_language_value->language_value;
                                } ?>"
                                id="password_field"
                                data-parsley-required-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.password_required');
                                } else {
                                    echo CommonHelper::multi_language('login', 'password_required')->multi_language_value->language_value;
                                } ?>" 
                                data-parsley-length-message="<?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.min_8_password');
                                } else {
                                    echo CommonHelper::multi_language('login', 'min_8_password')->multi_language_value->language_value;
                                } ?>"
                                 >
                            <span class="hide_pass"><img src="{{ asset('assets\website\image/Icon-Hide.png') }}"
                                    class="hide-icon"></span>
                        </div>
                        <div class="col-md-7 mt-3">
                            <a href="{{ route('forgot.password') }}"
                                class="text-decoration-none text-forgot float-end"><?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.forgot_password.forgot_password');
                                } else {
                                    echo CommonHelper::multi_language('forgot_password', 'forgot_password')->multi_language_value->language_value;
                                } ?></a>
                        </div>
                        <div class="col-md-8 mt-5">
                            <button type="button" onclick="signup_function()"
                                class="form-control btn bg-color px-4 py-3 fw-bold text-white font-18 box-radius"
                                style="background-color: #D5B115;"><?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.sign_in');
                                } else {
                                    echo CommonHelper::multi_language('login', 'sign_in')->multi_language_value->language_value;
                                } ?></button>
                        </div>
                    </form>
                    <div class="text-center mt-3">
                        <p class="text-forgot font-14"><?php if (Session::get('app_string')) {
                            echo Session::get('app_string.login.or_continue');
                        } else {
                            echo CommonHelper::multi_language('login', 'or_continue')->multi_language_value->language_value;
                        } ?></p>
                        <div class="text-white mb-4" style="display: inline-flex;">
                            <a href="javascript:void(0)" class="me-3" onclick="google_signup()"><img
                                    src="{{ asset('assets/website/image/google.svg') }}"></a>
                            <a href="javascript:void(0)" class="me-3" onclick="facebook_signup()"><img
                                    src="{{ asset('assets/website/image/facebook.svg') }}"></a>
                            <!-- <a href="{{ route('apple') }}"><img src="{{ asset('assets/website/image/card/apple.png') }}"></a> -->
                            <div id="appleid-signin" data-border="true" data-mode="logo-only" data-border-radius="50"
                                data-width="130" data-height="30" data-color="white"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <form class="row justify-content-center" id="social_form" action="" method="get">

        <?php /*<input type="hidden" name="latitude" id="social_latitude">
        <input type="hidden" name="latitude" id="social_latitude">*/ ?>

        <input type="hidden" name="ip" id="social_device_ip">
        <input type="hidden" name="device_name" id="social_device_name">
        <input type="hidden" name="device_token" id="social_device_token">

    </form>

    <script type="text/javascript" src="{{ asset('assets/website/js/jwt-decode.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            //getLocation();
        });

        
         function google_signup() {
            //$('#social_latitude').val($('#latitude').val());
            //$('#social_longitude').val($('#longitude').val());
            $('#social_device_ip').val($('#device_ip').val());
            $('#social_device_name').val($('#device_name').val());
            $('#social_device_token').val($('#device_token').val());

            $('#social_form').attr('action','{{ route("google") }}');
            $('#social_form').submit();
        }

        function facebook_signup() {
            //$('#social_latitude').val($('#latitude').val());
            //$('#social_longitude').val($('#longitude').val());
            $('#social_device_ip').val($('#device_ip').val());
            $('#social_device_name').val($('#device_name').val());
            $('#social_device_token').val($('#device_token').val());

            $('#social_form').attr('action','{{ route("facebook") }}');
            $('#social_form').submit();
        }


$('input').keydown(function(event) {
    if (event.keyCode == 13) {
        signup_function();
        return false;
    }
});

        function signup_function() {
            
            /*var device_token = $('#device_token').val();

            if(device_token == ''){
                Notiflix.Notify.Failure(`<?php //if (Session::get('app_string')) { echo Session::get('app_string.app_titles_and_messages.allow_notification_permission'); } else { echo CommonHelper::multi_language('app_titles_and_messages', 'allow_notification_permission')->multi_language_value->language_value;} ?>`);
                
                return false;

            }*/

            $('#signin_form').submit();
        
        }

        $("#language_ddl").change(function() {
            //alert($(this).val());
            var location = $(this).val();
            $('#loader').show();
            $.ajax({
                type: "get",
                dataType: "json",
                url: "{{ route('language.change') }}",
                data: {
                    'location': location
                },
                success: function(returnData) {
                    if (returnData.status == true) {
                        //Notiflix.Notify.Success(returnData.message);    
                        window.location.reload();
                        setTimeout(function(){
                            $('#loader').hide();
                        },4000);
                    }

                },
                error: function(data) {},
            });

        });



        window.AppleID.auth.init({
            clientId: "com.awe.web",
            scope: "name email",
            redirectURI: "{{ route('apple.callback') }}",
            state: new Date().toISOString(),
            usePopup: true,
        });

        document.addEventListener(
            "AppleIDSignInOnSuccess",
            appleLoginSuccessful
        );

        document.addEventListener(
            "AppleIDSignInOnFailure",
            appleLoginFailed
        );

        function appleLoginSuccessful(e) {
            var token = e.detail.authorization.id_token;
            var decoded = jwt_decode(token);
            var data = {
                email: decoded.email,
                id: decoded.sub,
                _token: "{{ csrf_token() }}",
                //latitude: $('#latitude').val(),
                //longitude: $('#longitude').val(),
                ip: $('#device_ip').val(),
                device_name: $('#device_name').val(),
                device_token: $('#device_token').val()
            };
            $.ajax({
                url: "{{ route('apple.callback') }}",
                type: "POST",
                data: data,
                success: function(data) {
                    if (data.status) {
                        Notiflix.Notify.Success('Success');
                        window.location.href = data.redirect_url;
                    } else {
                        Notiflix.Notify.Failure(data.message);
                    }
                }
            });
        }

        function appleLoginFailed(e) {}
    </script>
@endsection
