@extends('website.layouts.master')
@section('content')
@include('website.include.flash-message')

<section>
    <div class="container">
        <div class="mt-md-5 py-5">
            <a href="{{ route('profile') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.manage_profile'); }else{ echo CommonHelper::multi_language('settings','manage_profile')->multi_language_value->language_value; } ?></span>
        </div>
        <div class="mt-5 pt-md-3 text-center">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8 col-md-12">
                    <div class="row justify-content-center">
                        <?php if(!empty($data) && count($data)>0){ 
                        		foreach ($data as $key => $value) {
                                    if(!empty($value['lock_password'])){
                        				$url = route('lock-profile',base64_encode($value['id']));
                        			}else{
                        				$url = route('edit-profile',base64_encode($value['id']));
                        			}
                        		?> 

                			<div class="manage-profile-edit btn bg-white m-4 box-radius position-relative">
	                            <a href="{{$url}}" class="text-decoration-none p-edit-icon">
	                                <p class="font-date text-yellow fw-bold text-start my-2 ps-1">{{ strlen($value['first_name']) > 10 ? substr($value['first_name'],0,10)."..." : $value['first_name'] }}
                                    <?php if($value['is_child'] == 1){ ?>
                                        <span class="font-18 text-yellow fw-bold text-end mt-1" style="float: right;padding-right: 11px;"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.kids'); }else{ echo CommonHelper::multi_language('home','kids')->multi_language_value->language_value; } ?></span>
                                    <?php } ?>
                                    </p>
	                                <img class="img-fluid" src="{{ $value['profile_picture_full_url'] }}" alt="" style="height: 65% !important;width: 135px;object-fit:cover" onerror="this.src='{{ asset('/assets/website/image/awe-home.png') }}'">
	                            </a>
                                <?php if(!empty($value['lock_password'])){ ?>
                                        <img class="manage-lock-icon" src="{{ asset('assets/website/image/card/lock.png') }}">
                                    <?php } ?>

	                        </div>
                        
                        <?php } } ?>
                        
                        <?php if(count($data)<5){ ?>
                            <div class="manage-profile-edit btn m-3 box-radius position-relative">
    	                        <a href="{{ route('account.add')}}" class="text-decoration-none">
    	                        	<div class="manage-profile-edit btn bg-white box-radius">
    	                                <img class="img-fluid mt-5 pt-2" src="{{ asset('assets/website/image/add-button-2.png') }}" alt="">
    	                                <p class="font-21 text-dark fw-bold mt-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.add_profile'); }else{ echo CommonHelper::multi_language('settings','add_profile')->multi_language_value->language_value; } ?></p>
    	                        	</div>
    	                        </a>
                            </div>
                    	<?php } ?>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection