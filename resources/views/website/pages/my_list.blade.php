@extends('website.layouts.master')
@section('content')
@include('website.include.flash-message')
    @include('website.layouts.head')

<style type="text/css">
    .bg-body {
      background: linear-gradient(rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.89));
    }
</style>

    <section class="mt-5 pt-5">
        <div class="container">
            <div class="mt-4 pt-lg-4 font-28 text-white text-center text-sm-start fw-bold">
                <?php if (Session::get('app_string')) {
                    echo Session::get('app_string.home.my_list');
                } else {
                    echo CommonHelper::multi_language('home', 'my_list')->multi_language_value->language_value;
                } ?>
            </div>

            <?php if(!empty($my_list['data']['data']) && count($my_list['data']['data'])>0){ ?>
            <div class="about-grid-selction">
                <?php foreach ($my_list['data']['data'] as $key => $value) {  ?>

                <div class="mt-4 hover-img-list position-relative">
                    <?php if($value['content']['is_free'] == 1){ ?>
                        @switch(Session::get('locale'))
                            @case("sp")
                                <img src="{{ asset('assets/website/image/card/Spanish.png') }}" class="img-fluid free_tag_img">
                            @break
                            @case("md")
                                <img src="{{ asset('assets/website/image/card/Mandarin.png') }}" class="img-fluid free_tag_img">
                            @break
                            @case("hi")
                                <img src="{{ asset('assets/website/image/card/hindi.png') }}" class="img-fluid free_tag_img">
                            @break
                            @case("fr")
                                <img src="{{ asset('assets/website/image/card/French.png') }}" class="img-fluid free_tag_img">
                            @break
                            @default
                                <img src="{{ asset('assets/website/image/card/free.png') }}" class="img-fluid free_tag_img">
                        @endswitch
                    <?php } ?>
                    <img src="{{ $value['content']['poster'] }}" class="img-fluid img-height box-radius">
                    <a href="{{ route('content-details', $value['content']['slug']) }}"
                        class="btn play-hover-list w-100 h-100 position-absolute"><img
                            src="{{ asset('assets/website/image/play.png') }}" class="img-fluid"></a>
                </div>
                <?php } ?>

            </div>

            <?php if(!empty($my_list['data']['data']) && count($my_list['data']['data'])>0){ ?>
            @if($my_list['data']['last_page']>1)    
            <nav aria-label=".." class="mt-5">
                <ul class="pagination justify-content-center">
                  
                    <?php for ($i=1; $i <= $my_list['data']['last_page']; $i++) { 
						if(!empty(request()->get('page'))){
							$page = request()->get('page');
				    	}else{
							$page = 1;
				    	}
				    	if($page == $i){
							 $url = 'JavaScript:void(0)"'; 
							 $active = "active";
						 }else{ 
							$url = URL('/my-list').'?type=web&page='.$i; 
							$active='';
						 }
						?>
					@if($i==1)
					 <li class="page-item mx-2">
                        <a class="page-link bg-transparent text-white border-0" href="{{ $url }}" tabindex="-1"
                            aria-disabled="true">
                            < </a>
                    </li>
					@endif
                    <li class="page-item mx-2 {{ $active }}"><a class="page-link"
                            href="{{ $url }}">{{ $i }}</a></li>
                    <?php }  ?>

                    <li class="page-item mx-2">
                        <a class="page-link bg-transparent text-white border-0" href="{{ $url }}">></a>
                    </li>
                </ul>
            </nav>
            @endif
            <?php } ?>


            <?php }else{ ?>
            <div class="row justify-content-center pb-md-5 mb-5">
                <div class="text-center mt-5 mb-5">
                    <img src="{{ asset('assets/website/image/card/AWE_NothingToShow_IMAGE.png') }}">
                    <p class="text-white font-18 mt-4"><?php if (Session::get('app_string')) {
                        echo Session::get('app_string.nodata.nothing_to_show_here');
                    } else {
                        echo CommonHelper::multi_language('nodata', 'nothing_to_show_here')->multi_language_value->language_value;
                    } ?></p>
                    <p class="text-white font-18"><?php if (Session::get('app_string')) {
                        echo Session::get('app_string.nodata.empty_list');
                    } else {
                        echo CommonHelper::multi_language('nodata', 'empty_list')->multi_language_value->language_value;
                    } ?></p>

                </div>
            </div>
            <?php } ?>

        </div>

        <?php /*{{ $my_list->links() }}*/ ?>

    </section>




    @include('website.layouts.footer')
@endsection
