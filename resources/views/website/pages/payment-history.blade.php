@extends('website.layouts.master')
@section('content')

@include('website.layouts.head')

<section class="mt-5 pt-5">
<div class="container">
    <a href="{{ route('profile')}}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a>
  <div class="pt-lg-4 font-28 text-yellow text-center text-sm-start fw-bold">
      <?php if(Session::get('app_string')){ echo Session::get('app_string.app_titles_and_messages.payment_history'); }else{ echo CommonHelper::multi_language('app_titles_and_messages','payment_history')->multi_language_value->language_value; }; ?>
    </div>
  <div class="row">
<?php if(!empty($payment_history) && count($payment_history)>0){ ?>
    <table class="table table-hover text-left text-white">
    <thead>
      <tr>
        <th><?php if(Session::get('app_string')){ echo Session::get('app_string.payment_history.purchase_date'); }else{ echo CommonHelper::multi_language('payment_history','purchase_date')->multi_language_value->language_value; }; ?></th>
        <th><?php if(Session::get('app_string')){ echo Session::get('app_string.payment_history.period'); }else{ echo CommonHelper::multi_language('payment_history','period')->multi_language_value->language_value; }; ?></th>
        <th><?php if(Session::get('app_string')){ echo Session::get('app_string.payment_history.plan_name'); }else{ echo CommonHelper::multi_language('payment_history','plan_name')->multi_language_value->language_value; }; ?></th>
        <th><?php if(Session::get('app_string')){ echo Session::get('app_string.payment_history.plan_type'); }else{ echo CommonHelper::multi_language('payment_history','plan_type')->multi_language_value->language_value; }; ?></th>
        <th><?php if(Session::get('app_string')){ echo Session::get('app_string.payment_history.payment_method'); }else{ echo CommonHelper::multi_language('payment_history','payment_method')->multi_language_value->language_value; }; ?></th>
        <th><?php if(Session::get('app_string')){ echo Session::get('app_string.payment_history.total'); }else{ echo CommonHelper::multi_language('payment_history','total')->multi_language_value->language_value; }; ?></th>
      </tr>
    </thead>
    <tbody>
      <?php 
      $data=Session::get('user_data');
         $symbol = Symfony\Component\Intl\Currencies::getSymbol($data['currency_code']);
         foreach ($payment_history as $key => $value) {
            $monthly_price = strval(round(Currency::convert()->from('USD')->to($data['currency_code'])->amount($value->plan->monthly_price)->get()));
            if($value->type == 1){
              if(Session::get('app_string')){ $type = Session::get('app_string.payment_method.monthly'); }else{ $type = CommonHelper::multi_language('payment_method','monthly')->multi_language_value->language_value; };
              $day = '+30 day';
              $price = $symbol.$monthly_price;
            }elseif ($value->type == 2) {
              $day = '+365 day';
              $price = $symbol .$yearly_price;
              if(Session::get('app_string')){ $type = Session::get('app_string.payment_method.yearly'); }else{ $type = CommonHelper::multi_language('payment_method','yearly')->multi_language_value->language_value; }
            }else{
              $price = $symbol .$monthly_price;
              $day = '+30 day';

              if(Session::get('app_string')){ $type = Session::get('app_string.payment_method.monthly'); }else{ $type = CommonHelper::multi_language('payment_method','monthly')->multi_language_value->language_value; };
            }
      ?>
        <tr> 
          <td>{{ date('d-m-Y',strtotime($value->created_at)) }}</td>
          <td>{{ date('d-m-Y',strtotime($value->created_at)) }} - {{ date('d-m-Y',strtotime($value->created_at.$day)) }}</td>
          <td>{{ $value->plan->name }}</td>
          <td>{{ $type }}</td>
          <td>{{ $value->mode }}</td>
          <td>{{ $price }}</td>
        </tr>
      <?php } ?> 
      
    </tbody>
  </table>
  <?php }else{ ?> 
    <div class="row justify-content-center d-flix">
          <div class="text-center mt-5 mb-5">
            <img src="{{ asset('assets/website/image/card/AWE_NothingToShow_IMAGE.png') }}">
                      <p class="text-white font-18 mt-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.nodata.nothing_to_show_here'); }else{ echo CommonHelper::multi_language('nodata','nothing_to_show_here')->multi_language_value->language_value; } ?></p>
                      <p class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.nodata.empty_list'); }else{ echo CommonHelper::multi_language('nodata','empty_list')->multi_language_value->language_value; } ?></p>

          </div>
        </div>
  <?php } ?>
  </div>
</div>
</section>

@include('website.layouts.footer')
@endsection