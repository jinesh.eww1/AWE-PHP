@extends('website.layouts.master')
@section('content')
@include('website.include.flash-message')

<section>
    <div class="container">
        <div class="mt-md-5 py-5 mb-md-4">
            <a href="{{ route('edit-profile',base64_encode($id))}}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.edit_profile.playback_settings'); }else{ echo CommonHelper::multi_language('edit_profile','playback_settings')->multi_language_value->language_value; } ?></span>
        </div>
        <form method="post" action="{{ route('playback-setting',base64_encode($id)) }}" class="position-relative">
            @csrf()
            <input type="hidden" name="user_id" value="{{ $user['data']['id'] }}">
            <div class="text-center">
                <img class="img-fluid" src="{{ asset('assets/website/image/playback-wallpaper.png') }}" alt="">
                <div class="text-yellow font-date mt-md-5 pt-4 fw-bold"><?php if(Session::get('app_string')){ echo Session::get('app_string.playback_settings.autoplay_controls'); }else{ echo CommonHelper::multi_language('playback_settings','autoplay_controls')->multi_language_value->language_value; } ?></div>
            </div>
            <div class="d-md-flex justify-content-center gap-5 mt-5">
                <div class="d-flex  align-items-center">
                    <div class="play-checkbox">
                        <?php 
                            $checked = $achecked = '';
                            if($user['data']['autoplay'] == 1){ $checked = 'checked="checked"'; } 
                            if($user['data']['autoplay_previews'] == 1){ $achecked = 'checked="checked"'; } 
                        ?>
                        <input id="play-checkbox" name="autoplay" type="checkbox" {{ $checked }}>
                        <label for="play-checkbox"></label>
                        <svg width="15" height="14" viewBox="0 0 15 14" fill="none">
                            <path d="M2 8.36364L6.23077 12L13 2"></path>
                        </svg>
                    </div>
                    <div class="font-22 text-white ms-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.playback_settings.autoplay_next_episode'); }else{ echo CommonHelper::multi_language('playback_settings','autoplay_next_episode')->multi_language_value->language_value; } ?></div>
                </div>
                <div class="d-flex align-items-center mt-md-0 mt-4">
                    <div class="play-checkbox">
                        <input id="play-checkbox" name="autoplay_previews" type="checkbox" {{ $achecked }}>
                        <label for="play-checkbox"></label>
                        <svg width="15" height="14" viewBox="0 0 15 14" fill="none">
                            <path d="M2 8.36364L6.23077 12L13 2"></path>
                        </svg>
                    </div>
                    <div class="font-22 text-white ms-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.playback_settings.autoplay_previews_while_browsing'); }else{ echo CommonHelper::multi_language('playback_settings','autoplay_previews_while_browsing')->multi_language_value->language_value; } ?></div>
                </div>
            </div>
            <div class="text-center pt-5">
                <button class="btn profile-save-btn text-white bg-color font-date fw-bold p-3 my-4 box-radius" type="submit"><?php if(Session::get('app_string')){ echo Session::get('app_string.account_profile.save'); }else{ echo CommonHelper::multi_language('account_profile','save')->multi_language_value->language_value; } ?></button>
            </div>
        </form>
    </div>
</section>

@endsection
