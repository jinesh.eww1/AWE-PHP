@extends('website.layouts.master')
@section('content')

@include('website.include.flash-message')

@include('website.layouts.head')

<style type="text/css">
    .bg-body {
      background: linear-gradient(rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.89));
    }
</style>

<section class="mt-5 pt-5">
     
	<div class="container">
		<div class="mt-4 pt-lg-4 font-28 text-white text-center text-sm-start fw-bold pt-3">
			<?php if(Session::get('app_string')){ echo Session::get('app_string.search.popular_searches'); }else{ echo CommonHelper::multi_language('search','popular_searches')->multi_language_value->language_value; } ?>
		</div>
		<div class="row">
			
			<?php if(!empty($data) && count($data)>0){ 
                $genres ='';
				foreach ($data as $key => $value) {
			?>
				<div class="col-lg-4 col-md-6 my-3 py-3 position-relative">
					<img src="{{ $value['poster'] }}" class="img-fluid img-size box-radius" style="width: 470px !important;height: 200px !important;max-width: 100% !important;">
					<a href="{{ route('content-details',$value['slug']) }}"><button class="btn download-btn py-2 px-3" style="text-align: left;">
                        <img src="{{ asset('assets/website/image/play.png') }}">
                        <div style="display: inline-grid;">
                        <span class="fw-bold font-18 text-white ms-3"> {{ $value['name'] }}</span>
                        <?php $genres = array_column($value['genres'],'name');  ?>
                        <span class="font-14 text-white ms-3">{{ !empty($genres)?implode(', ', $genres):'' }}</span>
                        </div>
                    </button></a>
				</div>
			<?php } } ?>


		</div>
	</div>
</section>

<script type="text/javascript">
    
    $(document).ready(function(e){


    var visibleWord = document.getElementById('searchResult');

    if (!visibleWord) {
        return true;
    }

    // continue with the code... 
    var nextWord = visibleWord.nextSibling;


        $(document).on('keyup keypress', 'input', function(e) {
          if(e.which == 13) {
            e.preventDefault();
            return false;
          }
        });


        var typingTimer;                //timer identifier
        var doneTypingInterval = 3000;  //time in ms, 5 seconds for example
        var $input = $('#search_filter');

        //on keyup, start the countdown
        $input.on('keyup', function () {
          clearTimeout(typingTimer);
          typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });

        //on keydown, clear the countdown 
        $input.on('keydown', function () {
          clearTimeout(typingTimer);
        });

        //user is "finished typing," do something
        function doneTyping () {
          //do something
          var search = $("#search_filter").val();

          if(search != "" && search.length >= 3){

                    $.ajax({
                        url: "{{ route('search')}}",
                        type: 'get',
                        data: {search:search},
                        dataType: 'json',
                        success:function(response){

                            var len = response.data.length;
                            $("#searchResult").empty();
                            var html ='';
                            for( var i = 0; i<len; i++){
                                var id = response['data'][i]['id'];
                                var name = response['data'][i]['name'];
                                var slug = response['data'][i]['slug'];
                                html +="<li><a class='text-yellow font-18' href='{{ url('/')}}/content/details/"+slug+"?search=1'>"+name+"</a></li>";
                            }
                            $("#searchResult").html(html);
                            $("#searchResult").addClass('show');
                        },
                        error:function(response){
                             var html ='<li class="text-yellow font-18 text-center">'+response.responseText+'</li>';
                             $("#searchResult").empty();
                             $("#searchResult").html(html);
                             $("#searchResult").addClass('show');
                        }
                    });
                }else{
                    $("#searchResult").empty();
                    $("#searchResult").removeClass('show');
                }
        }


        });
</script>

@include('website.layouts.footer')
@endsection
