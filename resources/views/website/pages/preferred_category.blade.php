@extends('website.layouts.master')
@section('content')

<section>
    <div class="container">
        <div class="mt-md-5 my-4">
            <a href="{{ route('home','movie')}}"><img src="{{ asset('assets/website/image/awe-home.png') }}" class="img-fluid box-radius"
                        height="50px" width="50px"></a>

            <h5 class="text-white mt-4 pt-3"><?php if(Session::get('app_string')){ echo Session::get('app_string.preferred_category.preferred_category'); }else{ echo CommonHelper::multi_language('preferred_category','preferred_category')->multi_language_value->language_value; } ?></h5>
        </div>
        <form action="{{ route('preferred.category') }}" method="post">
            @csrf()
            <input type="hidden" name="user_id" value="{{ Session::get('user_data.id') }}">
            <div class="row justify-content-center">
                <div class="col-lg-7 col-md-10 px-0 ms-md-5">
                    <div class="mt-md-5 pt-3 ms-md-3">
                        @php

                        foreach ($data as $key => $value) {
                            if($value['selected'] == true){ $checked = 'checked'; }else{ $checked = ''; }
                            if($key == 0){ $first_select = 'checked'; $required = 'required'; }else{ $first_select = $required = ''; }
                        @endphp
                        <div class="d-flex justify-content-between align-items-center px-4 py-3 ps-md-5 my-md-2">
                            <div class="text-white font-date">{{ $value['name'] }}</div>
                            <div class="cbx">
                                <input class="checkbox cbx_{{$value['id']}}" id="cbx" {{$first_select}} {{$required}}  type="checkbox" {{ $checked }} name="preferred_category[]" value="{{ $value['id'] }}" data-parsley-errors-container="#checkbox_error">
                                <label for="cbx"></label>
                                <svg width="15" height="14" viewBox="0 0 15 14" fill="none">
                                    <path d="M2 8.36364L6.23077 12L13 2"></path>
                                </svg>
                                <div class="error" id="checkbox_error" style="width: max-content;"></div>
                            </div>
                        </div>
                        @php 
                            }
                        @endphp
                    </div>
                </div>
                <div class="my-5 py-2 pb-md-5 text-center">
                    <button class="btn text-white font-20 fw-bold profile-save-btn bg-color p-3 box-radius" type="submit"><?php if(Session::get('app_string')){ echo Session::get('app_string.account_profile.save'); }else{ echo CommonHelper::multi_language('account_profile','save')->multi_language_value->language_value; } ?></button>
                </div>
            </div>
        </form>
    </div>
</section>


<script type="text/javascript">
    $('.checkbox').click(function() {
        var value = $(this).val();
        var count = $('input[type="checkbox"]:checked').length;
        
        if (count == 0) {
            $('.cbx_'+value).click();
        }
    });
</script>

@endsection
