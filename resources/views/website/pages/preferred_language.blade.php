@extends('website.layouts.master')
@section('content')

<section>
    <div class="container">
        <div class="mt-md-5 py-5 my-4">
            <a href="{{ route('all.plans') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.app_language.preference_selection'); }else{ echo CommonHelper::multi_language('app_language','preference_selection')->multi_language_value->language_value; } ?></span>
        </div>
        <form action="{{ route('preferred.language') }}" method="POST">
            @csrf();
            <input type="hidden" name="user_id" value='{{ Session::get("user_id") }}'>
            <div class="row justify-content-center">
                <div class="col-lg-7 col-md-10 px-0 me-md-5">
                    <div class="d-block d-md-flex justify-content-center align-items-center mt-md-4 ps-3">
                        <img class="img-fluid text-yellow me-5" src="{{ asset('assets/website/image/language-logo.png') }}" alt="">
                        <div>
                            <div class="text-yellow font-28"><?php if(Session::get('app_string')){ echo Session::get('app_string.app_language.your_preferred_language'); }else{ echo CommonHelper::multi_language('app_language','your_preferred_language')->multi_language_value->language_value; } ?></div>
                            <p class="text-forgot font-22"><?php if(Session::get('app_string')){ echo Session::get('app_string.app_language.select_your_language'); }else{ echo CommonHelper::multi_language('app_language','select_your_language')->multi_language_value->language_value; } ?></p>
                        </div>
                    </div>

                    <?php 
                        if(isset($data) && !empty($data)){ 
                            foreach ($data as $key => $value) { 
                                if($value['preferred'] == true){ $checked = 'checked'; }else{ $checked = ''; }
                    ?>
                            <div class="d-flex justify-content-between align-items-center mt-md-5 ms-3 pt-3">
                                <div class="d-flex align-items-center">
                                    <img class="img-fluid" src="{{ $value['image'] }}" alt="">
                                    <span class="font-date text-white ms-3 ms-md-5">{{ $value['type'] }}</span>
                                </div>
                                <div class="cbx me-3 me-md-0">
                                    <input type="radio" id="test1" value="{{ $value['id'] }}" name="language_id" {{ $checked }}>
                                    <label for="test1"></label>
                                    <svg width="15" height="14" viewBox="0 0 15 14" fill="none">
                                        <path d="M2 8.36364L6.23077 12L13 2"></path>
                                    </svg>
                                </div>
                            </div>
                    <?php } } ?>
                    


                    <div class="my-5 py-md-5 pb-md-5 text-center">
                            <button class="btn text-white font-20 fw-bold profile-save-btn bg-color p-3 box-radius" type="submit"><?php if(Session::get('app_string')){ echo Session::get('app_string.account_profile.save'); }else{ echo CommonHelper::multi_language('account_profile','save')->multi_language_value->language_value; } ?></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

@endsection
