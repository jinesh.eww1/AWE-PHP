@extends('website.layouts.master')
@section('content')
@include('website.include.flash-message')

<style type="text/css">
   .group1{
        background: transparent;
        color: white;
        width: 84px;
        height: 84px;
        font-size: 33px;
    }
</style>

<section>
    <div class="container">
        <div class="mt-md-5 py-5 mb-md-4">
            <a href="{{ route('edit-profile',base64_encode($id)) }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.edit_profile.profile_lock'); }else{ echo CommonHelper::multi_language('edit_profile','profile_lock')->multi_language_value->language_value; } ?></span>
        </div>
        <form method="post" action="{{ route('profile-lock',base64_encode($id)) }}" class="position-relative profile_lock">
            @csrf()
            <input type="hidden" name="user_id" value="{{ $id }}">
            <div class="text-center">
                <img class="img-fluid" src="{{ asset('assets/website/image/password-wallpaper.png') }}" alt="">
                <div class="text-white font-date mt-md-5 pt-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.profile_lock.set_profile_lock'); }else{ echo CommonHelper::multi_language('profile_lock','set_profile_lock')->multi_language_value->language_value; } ?></div>
            </div>
          
            @if($user['data']['lock_password']!=NULL)
            <div class="row justify-content-center align-items-center mt-4 pt-2">
                <div class="col-lg-3 col-md-6 col-10">
                    <div class="row justify-content-center align-items-center all-noti box-shadow-notification">
                        <div class="col-lg-9 col-md-9 col-8 px-md-0">
                            <p class="text-white font-14 p-3"><?php if(Session::get('app_string')){ echo Session::get('app_string.profile_lock.disable_pin'); }else{ echo CommonHelper::multi_language('profile_lock','disable_pin')->multi_language_value->language_value; } ?></p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-4">
                            <label class="switch1 float-end">
                                <input type="checkbox" id="disable_pin_checkbox" name="disable_pin" >
                                <span class="slider1 round1"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <?php 
                if(!empty($user['data']['lock_password'])){
                    $lock_password = str_split((string)$user['data']['lock_password']);                   
                }
            ?>
            <div class="d-flex justify-content-center gap-md-4 gap-3 my-md-5 pt-1 mt-5">
                

                <div class="otp-vrification" id="otp" style="display: contents;">

                <?php if(isset($_GET['type']) && $_GET['type'] == 'forgot-pin'){ 
                        $type = 'type = "text"';
                        $value1 = (isset($lock_password[0]) && $lock_password[0] != NULL)?$lock_password[0]:'';
                        $value2 = (isset($lock_password[1]) && $lock_password[1] != NULL)?$lock_password[1]:'';
                        $value3 = (isset($lock_password[2]) && $lock_password[2] != NULL)?$lock_password[2]:'';
                        $value4 = (isset($lock_password[3]) && $lock_password[3] != NULL)?$lock_password[3]:'';
                    }else{
                        $type = "type = 'password'";
                        $value1=$value2 = $value3 = $value4 = '';
                    } ?>
                    <input class="form-control text-center group1 fw-bold font-18" <?php echo $type; ?> data-parsley-type="digits" name="pin_1" maxlength="1"pattern="[0-9]*" inputmode="numeric" value="<?php echo $value1; ?>" id="first" data-parsley-errors-container="#pin_error" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  data-parsley-ui-enabled='false' data-parsley-group='pin-grp' >

                    <input class="form-control text-center group1 fw-bold font-18" <?php echo $type; ?> data-parsley-type="digits" name="pin_2" maxlength="1" value="<?php echo $value2; ?>" id="second" data-parsley-errors-container="#pin_error"pattern="[0-9]*" inputmode="numeric" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  data-parsley-ui-enabled='false' data-parsley-group='pin-grp' >

                    <input class="form-control text-center group1 fw-bold font-18" <?php echo $type; ?> data-parsley-type="digits" name="pin_3" maxlength="1" value="<?php echo $value3; ?>" id="third" data-parsley-errors-container="#pin_error" pattern="[0-9]*" inputmode="numeric"oninput="this.value=this.value.replace(/[^0-9]/g,'');"  data-parsley-ui-enabled='false' data-parsley-group='pin-grp' >

                    <input class="form-control text-center group1 fw-bold font-18" <?php echo $type; ?> data-parsley-type="digits" name="pin_4" maxlength="1" value="<?php echo $value4; ?>" id="fourth" data-parsley-errors-container="#pin_error"pattern="[0-9]*" inputmode="numeric" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  data-parsley-ui-enabled='false' data-parsley-group='pin-grp' >

                </div>

                
            </div>

            <div class="d-flex justify-content-center my-md-1 pt-1 mt-1">
                <div class="error" id="pin_error"></div>
            </div>

            <div class="text-center">
                    <button class="btn profile-save-btn text-white bg-color font-20 fw-bold p-3 my-4 box-radius" onclick="pin_validation();" type="button">
                        @if(!empty($user['data']['lock_password']))
                        <?php if(Session::get('app_string')){ echo Session::get('app_string.profile_lock.change_pin'); }else{ echo CommonHelper::multi_language('profile_lock','change_pin')->multi_language_value->language_value; } ?>
                    @else
                    <?php if(Session::get('app_string')){ echo Session::get('app_string.account_profile.save'); }else{ echo CommonHelper::multi_language('account_profile','save')->multi_language_value->language_value; } ?>
                @endif</button>
            </div>
        </form>
    </div>
</section>


<script>
$(document).ready(function() {
    $('.group1').on("cut copy paste", function(e) {
        e.preventDefault();
    });
    var addressFields = $("[data-parsley-group='pin-grp']");
addressFields.each(function(index, element) {
  $(element).parsley().on('field:validated', function(parsleyField) {
    var fieldOptions = parsleyField.actualizeOptions().options;
    var classHandler = fieldOptions.classHandler(parsleyField);
    var container = $(fieldOptions.errorsContainer);
    //classHandler.removeClass(fieldOptions.successClass);
    //classHandler.removeClass(fieldOptions.errorClass);
    var valid = parsleyField.parent.isValid(fieldOptions.group);
    if(valid) {
      container.html("");
    } else {
      $('#pin_error').html(`<?php if(Session::get('app_string')){ echo Session::get('app_string.profile_lock.valid_pin'); }else{ echo CommonHelper::multi_language('profile_lock','valid_pin')->multi_language_value->language_value; } ?>`).css('color','red');
    }
  });
});

});
    function pin_validation() {

        var pin1 = $('#first').val();
        var pin2 = $('#second').val();
        var pin3 = $('#third').val();
        var pin4 = $('#fourth').val();

        if ($('#disable_pin_checkbox').is(':checked')) {
            $('.profile_lock').submit();
            return false;
        }

        if(pin1 == '' || pin2 == '' || pin3 == '' || pin4 == ''){        
            $('#pin_error').css('color','red');
            $('#pin_error').html(`<?php if(Session::get('app_string')){ echo Session::get('app_string.profile_lock.enter_pin'); }else{ echo CommonHelper::multi_language('profile_lock','enter_pin')->multi_language_value->language_value; } ?>`);
            return false;
        }else{
            $('.profile_lock').submit();
        }
            
    }

    function OTPInput() {
  const inputs = document.querySelectorAll('#otp > *[id]');
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener('keydown', function(event) {
      if (event.key === "Backspace") {
        inputs[i].value = '';
        if (i !== 0)
          inputs[i - 1].focus();
          event.preventDefault();
      } else {
        if (i === inputs.length - 1 && inputs[i].value !== '') {
          return true;
        } else if (event.keyCode > 47 && event.keyCode < 58) {
          inputs[i].value = event.key;
          if (i !== inputs.length - 1)
            inputs[i + 1].focus();
          event.preventDefault();
        }else if (event.keyCode > 95 && event.keyCode < 106) {
          inputs[i].value = event.key;
          if (i !== inputs.length - 1)
            inputs[i + 1].focus();
          event.preventDefault();
        }
      }
    });
  }
}
OTPInput();
</script>

@endsection
