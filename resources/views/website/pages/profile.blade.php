@extends('website.layouts.master')
@section('content')

@include('website.include.flash-message')
<?php 
    //dd(Session::get('app_string'));
?>
<section>
    <div class="container">
        <div class="my-5 pt-md-5">
            <a href="{{ route('home','movie') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.settings'); }else{ echo CommonHelper::multi_language('settings','settings')->multi_language_value->language_value; } ?></span>
        </div>
            <div class="row justify-content-center">
                <div class="col-md-9">
                    <div class="row justify-content-center" style=" border-radius: 9px;">
                        <div class="col-lg-1 col-md-3 col-6 my-2 about-w">
                            <div class="about-profile bg-white p-2 text-center float-end" >
                                <a href="javascript:void(0)" onclick="return account_profile({{count($data)}});" class="text-decoration-none" >
                                    <img class="img-fluid mt-2" src="{{ asset('assets/website/image/add-button.png') }}" >
                                    <p class="text-dark mt-2 mb-0"><span class="fw-bold text-break"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.add_profile'); }else{ echo CommonHelper::multi_language('settings','add_profile')->multi_language_value->language_value; } ?></span></p>
                                </a>
                                <a href="{{ route('manage-profile') }}" class="text-decoration-none border-line mt-md-0 mt-2">
                                    <img class="img-fluid mt-3 mt-md-2" src="{{ asset('assets/website/image/edit.png') }}">
                                    <p class="text-dark mt-lg-2 mt-0"><span  class="fw-bold text-break"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.manage_profile'); }else{ echo CommonHelper::multi_language('settings','manage_profile')->multi_language_value->language_value; } ?></span></p>
                                </a>
                            </div>
                        </div>

                        <?php if(!empty($data)){
                                foreach ($data as $key => $value) { 
                                    if(Session::get('user_data.id') == $value['id']){ 
                                        $all_noti = $value['notification']; 
                                        $parent_id = $value['parent_id'];                         
                        ?>

                            <div class="btn bg-white m-2 box-radius col-lg-2 col-md-3 col-6 my-2 position-relative" style="height:175px; width: 175px;">
                                    
                                    <?php if(Session::get('user_data.id') == $value['id']){ ?><img class="img-fluid position-absolute right-click-btn" src="{{ asset('assets/website/image/right-click.png') }}" alt=""> <?php } ?>

                                    <p class="font-18 text-yellow fw-bold text-start mb-2 mt-1">{{ strlen($value['first_name']) > 10 ? substr($value['first_name'],0,10)."..." : $value['first_name'] }} 
                                        <?php if($value['is_child'] == 1){ ?>
                                            <span class="font-18 text-yellow fw-bold text-end" style="float: right;"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.kids'); }else{ echo CommonHelper::multi_language('home','kids')->multi_language_value->language_value; } ?></span>
                                        <?php } ?>
                                    </p>
                                    <img class="img-fluid mb-2" id="whos_img" src="{{ $value['profile_picture_full_url'] }}"onerror="this.src='{{ asset('/assets/website/image/awe-home.png') }}'" alt="">
                            </div>

                        <?php } } 
                            foreach ($data as $key => $value) { 
                                if(Session::get('user_data.id') != $value['id']){ 
                        ?>
                        
                            <div class="btn bg-white m-2 box-radius col-lg-2 col-md-3 col-6 my-2 position-relative" style="height:175px; width: 175px;">
                                       
                                <p class="font-18 text-yellow fw-bold text-start mb-2 mt-1">{{ strlen($value['first_name']) > 10 ? substr($value['first_name'],0,10)."..." : $value['first_name'] }} 
                                    <?php if($value['is_child'] == 1){ ?>
                                        <span class="font-18 text-yellow fw-bold text-end" style="float: right;"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.kids'); }else{ echo CommonHelper::multi_language('home','kids')->multi_language_value->language_value; } ?></span>
                                    <?php } ?>
                                </p>
                                <img class="img-fluid mb-2" id="whos_img" src="{{ $value['profile_picture_full_url'] }}"onerror="this.src='{{ asset('/assets/website/image/awe-home.png') }}'" alt="">
                            </div>

                        <?php } } } ?>
                        
                    </div>
                </div>
            </div>
            <div class="row justify-content-center align-items-center mt-4 pt-3">
                <div class="col-lg-3 col-md-6 col-11">
                    <div class="row justify-content-center align-items-center all-noti box-shadow-notification">
                        <div class="col-lg-9 col-md-9 col-8">
                            <p class="text-white font-14 p-3"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.allow_notification'); }else{ echo CommonHelper::multi_language('settings','allow_notification')->multi_language_value->language_value; } ?></p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-4">
                            <label class="switch1">
                                <input type="checkbox" onchange="allow_notification();" name="allow_notification" <?php if($all_noti == 1){ echo "checked='checked'";} ?> >
                                <span class="slider1 round1"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center align-items-center mt-4 pt-3">
                <div class="profile-list-width">
                    <ul class="about-user-profile text-white list-unstyled">

                        <a href="{{ route('account.profile') }}" class="text-decoration-none">
                            <li class="p-4 back-arrow-profile position-relative">
                                <img class="img-fluid" src="{{ asset('assets/website/image/user-profile.png') }}"><span class="font-14 ms-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.account_profile'); }else{ echo CommonHelper::multi_language('settings','account_profile')->multi_language_value->language_value; } ?></span>
                            </li>
                        </a>
                        
                        <?php  if($parent_id <=0 ){ ?>
                        <a href="{{ route('view.plan') }}" class="text-decoration-none">
                            <li class="p-4 back-arrow-profile position-relative">
                                <img class="img-fluid" src="{{ asset('assets/website/image/subscrip.png') }}"><span class="font-14 ms-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.subscription'); }else{ echo CommonHelper::multi_language('settings','subscription')->multi_language_value->language_value; } ?></span>
                            </li>
                        </a>
                        @if(Session::get('user_data.social_id')==null)
                        <a href="{{ route('user-change-password') }}" class="text-decoration-none">
                            <li class="p-4 back-arrow-profile position-relative">
                                <img class="img-fluid" src="{{ asset('assets/website/image/pasword.png') }}"><span class="font-14 ms-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.change_password'); }else{ echo CommonHelper::multi_language('settings','change_password')->multi_language_value->language_value; } ?></span>
                            </li>
                        </a>
                        @endif
                        <a href="{{ route('payment-history') }}" class="text-decoration-none">
                            <li class="p-4 back-arrow-profile position-relative">
                                <i class="fa fa-usd" aria-hidden="true" style="font-size: x-large !important;"></i><span class="font-14 ms-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.app_titles_and_messages.payment_history'); }else{ echo CommonHelper::multi_language('app_titles_and_messages','payment_history')->multi_language_value->language_value; } ?></span>
                            </li>
                        </a>
                        <a href="{{ url('/logs/'.Session::get('user_data.id')) }}" class="text-decoration-none">
                            <li class="p-4 back-arrow-profile position-relative">
                                <img class="img-fluid" src="{{ asset('assets/website/image/action.png') }}"><span class="font-14 ms-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.viewing_activity'); }else{ echo CommonHelper::multi_language('settings','viewing_activity')->multi_language_value->language_value; } ?></span>
                            </li>
                        </a>

                        <?php if(Session::get("user_data.plan_id")  != ''){ ?>
                        <a href="{{ route('share-activity') }}" class="text-decoration-none">
                            <li class="p-4 back-arrow-profile position-relative">
                                <img class="img-fluid" src="{{ asset('assets/website/image/share.png') }}"><span class="font-14 ms-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.share_and_loyalty'); }else{ echo CommonHelper::multi_language('settings','share_and_loyalty')->multi_language_value->language_value; } ?></span>
                            </li>
                        </a>
                        <?php }else{ ?>
                            <a class="position-relative" href="javascript:void(0)">
                                <li class="p-4 back-arrow-profile position-relative"  data-bs-toggle="modal" data-bs-target="#share_loyalty_popup">
                                    <img class="img-fluid" src="{{ asset('assets/website/image/share.png') }}"><span class="font-14 ms-4" style="cursor:pointer;"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.share_and_loyalty'); }else{ echo CommonHelper::multi_language('settings','share_and_loyalty')->multi_language_value->language_value; } ?>
                                    </span>
                                </li>
                            </a>
                        <?php } } ?>
                        <a href="{{ url('/privacy-policy') }}" class="text-decoration-none"target="_blank">
                            <li class="p-4 back-arrow-profile position-relative">
                                <img class="img-fluid" src="{{ asset('assets/website/image/policy.png') }}"><span class="font-14 ms-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.privacy_policy'); }else{ echo CommonHelper::multi_language('settings','privacy_policy')->multi_language_value->language_value; } ?></span>
                            </li>
                        </a>
                        <a href="{{ route('help') }}" class="text-decoration-none">
                            <li class="p-4 back-arrow-profile position-relative">
                                <img class="img-fluid" src="{{ asset('assets/website/image/help.png') }}"><span class="font-14 ms-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.help'); }else{ echo CommonHelper::multi_language('settings','help')->multi_language_value->language_value; } ?></span>
                            </li>
                        </a>
                        <?php if($parent_id <=0 ){ ?>
                        <a href="{{ route('chat-support') }}" class="text-decoration-none">
                            <li class="p-4 back-arrow-profile position-relative">
                                <i class="fa fa-comments-o" aria-hidden="true" style="font-size: x-large !important;"></i><span class="font-14 ms-4"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.chat_support'); }else{ echo CommonHelper::multi_language('settings','chat_support')->multi_language_value->language_value; } ?></span>
                            </li>
                        </a>
                        <?php } ?>
                        <a class="position-relative">
                            <li class="p-4 back-arrow-profile position-relative"  data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                <img class="img-fluid" src="{{ asset('assets/website/image/logout.png') }} "><span class="font-14 ms-4" style="cursor:pointer;"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.logout'); }else{ echo CommonHelper::multi_language('settings','logout')->multi_language_value->language_value; } ?></span>
                            </li>
                            
                        </a>
                    </ul>
                </div>
                <?php /*<div class="my-5 text-center">
                    <button class="btn text-white font-20 fw-bold profile-save-btn bg-color p-3 box-radius" onclick="allow_notification();"><?php if(Session::get('app_string')){ echo Session::get('app_string.account_profile.save'); }else{ echo CommonHelper::multi_language('account_profile','save')->multi_language_value->language_value; } ?></button>
                </div>*/ ?>
            </div>  
    </div>
</section>




<div class="modal fade" id="share_loyalty_popup" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered text-center">
        <div class="modal-content text-center p-4">
            <div class="modal-header" style="border-bottom: none;">
                <h5 class="modal-title" id="staticBackdropLabel"><?php if(Session::get('app_string')){ echo Session::get('app_string.subscription.loyalty_after_first_purchase'); }else{ echo CommonHelper::multi_language('subscription','loyalty_after_first_purchase')->multi_language_value->language_value; 
            } ?></h5>
            </div>
            <div class="justify-content-center mt-5 mb-2">
                <a href="javascript:void(0)"><button type="button" class="btn font-18 bg-color text-white col-4 box-radius ms-4 col-md-6" data-bs-dismiss="modal" ><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.ok'); }else{ echo CommonHelper::multi_language('movie_details','ok')->multi_language_value->language_value; 
            } ?></button></a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

function allow_notification() {
    
    var status = 0;
    if($('input[name="allow_notification"]').prop('checked') == true){
        status = 1;
    }

    $('#loader').show();
    $.ajax({
        type: "get",
        dataType: "json",
        url: "{{ route('allow.notification') }}",
        data:{'status':status},
        success: function (returnData) {
            $('#loader').hide();
            if(returnData.status == true){
                Notiflix.Notify.Success(returnData.message);
            }else{
                Notiflix.Notify.Failure(returnData.message);
            }
        },
        error: function (data) {
        },
    });
}


function account_profile(data) {
    if(data < 5){
        window.location.href= "{{ route('account.add') }}?type=profile";
    }else{

        Notiflix.Notify.Failure("{{ CommonHelper::multi_language('add_Profile','max_limit_add_profile')->multi_language_value->language_value }}");
    }
}

</script>

                
@endsection
