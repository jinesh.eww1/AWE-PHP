@extends('website.layouts.master')
@section('content')
@include('website.include.flash-message')

<section>
    <div class="container">
        <div class="mt-md-5 pt-5 mb-md-3">
            <a href="{{ route('account.profile')}}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.account_profile.devices'); }else{ echo CommonHelper::multi_language('account_profile','devices')->multi_language_value->language_value; } ?></span>
        </div>
        <div class="pb-2">
            <p class="text-yellow font-device"><?php if(Session::get('app_string')){ echo Session::get('app_string.devices.your_registered_devices'); }else{ echo CommonHelper::multi_language('devices','your_registered_devices')->multi_language_value->language_value; } ?></p>
        </div>
        <div class="pt-3">
            <div class="row justify-content-center">

                <?php if(!empty($data['data']) && count($data['data'])>0){
                        
                        foreach ($data['data'] as $key => $value) { ?>
                        
                        @if($value['token'] == Session::get('device_token'))
                        <div class="col-lg-4 col-md-6 col-11">
                            <div class="row about-register-back p-4">
                                <div class="col-md-2 col-2 text-center">
                                    <?php if($value['type'] == 'web'){ ?>
                                        <img class="img-fluid" src="{{ asset('assets/website/image/tv-img.png') }}" alt="">
                                    <?php }else{ ?> 
                                        <img class="img-fluid" src="{{ asset('assets/website/image/mobile-img.png') }}" alt="">
                                    <?php } ?>
                                </div>
                                <div class="col-md-10 col-10">
                                    <div class="font-22">
                                        <span class="text-yellow">{{ $value['name'] }} </span>@switch($value['type'])
                                        @case('ios')
                                            <span class="text-forgot">(iOS) </span>
                                        @break
                                        @case('android')
                                            <span class="text-forgot">(Android) </span>
                                        @break
                                        @default
                                            <span class="text-forgot">(Web) </span>
                                    @endswitch
                                    </div>
                                    <div class="d-lg-flex d-block justify-content-between align-items-end remove-btn-block">
                                        <div class="fw-light font-22 mt-3">
                                            <span class="text-white "><?php if(Session::get('app_string')){ echo Session::get('app_string.devices.date_of_login'); }else{ echo CommonHelper::multi_language('devices','date_of_login')->multi_language_value->language_value; } ?>:</span><br>
                                            <span class="text-forgot">{{ date('F d,Y',strtotime($value['login_time'])) }}</span>
                                        </div>

                                        @if($value['token'] != Session::get('device_token'))
                                        <button class="text-yellow remove-btn-color btn p-2 m-1 px-3" data-bs-toggle="modal" data-bs-target="#exampleModal{{ $value['id'] }}"><?php if(Session::get('app_string')){ echo Session::get('app_string.devices.deregister'); }else{ echo CommonHelper::multi_language('devices','deregister')->multi_language_value->language_value; } ?></button>
                                        @endif
                                        <div class="modal fade" id="exampleModal{{ $value['id'] }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content text-center p-4">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel"><?php if(Session::get('app_string')){ echo Session::get('app_string.devices.sure_you_want_device_deregister'); }else{ echo CommonHelper::multi_language('devices','sure_you_want_device_deregister')->multi_language_value->language_value; } ?></h5>
                                                    </div>
                                                    <div class="justify-content-center mt-5 mb-2">
                                                        
                                                        <button type="button" class="btn font-18 text-yellow border-color col-4 box-radius" data-bs-dismiss="modal"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.no'); }else{ echo CommonHelper::multi_language('settings','no')->multi_language_value->language_value; } ?></button>

                                                        <form method="POST" action="{{ route('remove-device') }}" style="display: contents;">
                                                            <button type="submit" class="btn font-18 bg-color text-white col-4 box-radius ms-4" data-bs-dismiss="modal"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.yes'); }else{ echo CommonHelper::multi_language('settings','yes')->multi_language_value->language_value; } ?></button>
                                                            @csrf()
                                                            <input type="hidden" name="devices_id" value="{{ $value['id'] }}">
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                <?php }  

                foreach ($data['data'] as $key => $value) { ?>
                        
                        @if($value['token'] != Session::get('device_token'))
                        <div class="col-lg-4 col-md-6 col-11">
                            <div class="row about-register-back p-4">
                                <div class="col-md-2 col-2 text-center">
                                    <?php if($value['type'] == 'web'){ ?>
                                        <img class="img-fluid" src="{{ asset('assets/website/image/tv-img.png') }}" alt="">
                                    <?php }else{ ?> 
                                        <img class="img-fluid" src="{{ asset('assets/website/image/mobile-img.png') }}" alt="">
                                    <?php } ?>
                                </div>
                                <div class="col-md-10 col-10">
                                    <div class="font-22">
                                        <span class="text-yellow">{{ $value['name'] }} </span>@switch($value['type'])
                                        @case('ios')
                                            <span class="text-forgot">(iOS) </span>
                                        @break
                                        @case('android')
                                            <span class="text-forgot">(Android) </span>
                                        @break
                                        @default
                                            <span class="text-forgot">(Web) </span>
                                    @endswitch
                                    </div>
                                    <div class="d-lg-flex d-block justify-content-between align-items-end remove-btn-block">
                                        <div class="fw-light font-22 mt-3">
                                            <span class="text-white "><?php if(Session::get('app_string')){ echo Session::get('app_string.devices.date_of_login'); }else{ echo CommonHelper::multi_language('devices','date_of_login')->multi_language_value->language_value; } ?>:</span><br>
                                            <span class="text-forgot">{{ date('F d,Y',strtotime($value['login_time'])) }}</span>
                                        </div>

                                        @if($value['token'] != Session::get('device_token'))
                                        <button class="text-yellow remove-btn-color btn p-2 m-1 px-3" data-bs-toggle="modal" data-bs-target="#exampleModal{{ $value['id'] }}"><?php if(Session::get('app_string')){ echo Session::get('app_string.devices.deregister'); }else{ echo CommonHelper::multi_language('devices','deregister')->multi_language_value->language_value; } ?></button>
                                        @endif
                                        <div class="modal fade" id="exampleModal{{ $value['id'] }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content text-center p-4">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel"><?php if(Session::get('app_string')){ echo Session::get('app_string.devices.sure_you_want_device_deregister'); }else{ echo CommonHelper::multi_language('devices','sure_you_want_device_deregister')->multi_language_value->language_value; } ?></h5>
                                                    </div>
                                                    <div class="justify-content-center mt-5 mb-2">
                                                         <form method="POST" action="{{ route('remove-device') }}" style="display: contents;">
                                                            <button type="submit" class="btn font-18 text-yellow border-color col-4 box-radius " data-bs-dismiss="modal"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.yes'); }else{ echo CommonHelper::multi_language('settings','yes')->multi_language_value->language_value; } ?></button>
                                                            @csrf()
                                                            <input type="hidden" name="devices_id" value="{{ $value['id'] }}">
                                                        </form>

                                                        <button type="button" class="btn font-18 bg-color text-white col-4 box-radius ms-4" data-bs-dismiss="modal"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.no'); }else{ echo CommonHelper::multi_language('settings','no')->multi_language_value->language_value; } ?></button>                
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                <?php }  ?>

            <?php } ?>
            </div>
        </div>

        @if($data['last_page']>1)
        <nav aria-label=".." class="mt-5">
            <ul class="pagination justify-content-center">
                <?php for ($i=1; $i <= $data['last_page']; $i++) { 
                    if(!empty(request()->get('page'))){
                        $page = request()->get('page');
                    }else{
                        $page = 1;
                    }
                    if($page == $i){ $url = 'JavaScript:void(0)"'; $active = "active"; }else{ $url = URL('/register-device').'?type=web&page='.$i; $active=''; }
                ?>
                @if($i==1)
                <li class="page-item  mx-2">
                    <a class="page-link bg-transparent text-white border-0" href="{{ $url }}" tabindex="-1"
                        aria-disabled="true">
                        <</a>
                </li>
                @endif
                    <li class="page-item mx-2 {{ $active }}"><a class="page-link"  href="{{ $url }}">{{ $i }}</a></li>
                <?php } ?>

                <li class="page-item mx-2">
                <a class="page-link bg-transparent text-white border-0" href="{{ $url }}">></a>
                </li>
            </ul>
        </nav>
        @endif

            

    </div>
</section>
                
@endsection
