@extends('website.layouts.master')
@section('content')
@include('website.include.flash-message')

<style type="text/css">
    .cbx {
        width: 115px !important;
    }
</style>
<section>
    <div class="container">
        <div class="mt-md-5 py-5 my-4">
            <a href="{{ route('edit-profile',base64_encode($id))}}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.preferred_category.preferred_category'); }else{ echo CommonHelper::multi_language('preferred_category','preferred_category')->multi_language_value->language_value; } ?></span>
        </div>
        
        <form method="post" action="{{ route('preferred-categories',base64_encode($id)) }}" class="position-relative">
            @csrf()
            <input type="hidden" name="user_id" value="{{ $id }}">
        
            <div class="row justify-content-center">
                <div class="col-lg-7 col-md-10 px-0 ms-md-5">
                    <div  class="about-categories">

                        <?php

                         if(!empty($category['data']) && count($category['data'])>0){

                            foreach ($category['data'] as $key => $value) { 

                            if($value['selected'] == true){ $checked = 'checked'; }else{ $checked = ''; }
                            if($key == 0){ $required = 'required'; }else{ $required = ''; }

                        ?>

                            <div class="d-flex justify-content-between align-items-center p-4 px-md-5 my-md-2">
                                <div class="text-white font-date">{{ $value['name'] }}</div>
                                <div class="cbx">
                                    <input class="checkbox cbx_{{$value['id']}}" id="cbx" {{$required}}  type="checkbox" {{ $checked }} name="preferred_category[]" value="{{ $value['id'] }}" data-parsley-errors-container="#checkbox_error">
                                    <label for="cbx"></label>
                                    <svg width="15" height="14" viewBox="0 0 15 14" fill="none">
                                        <path d="M2 8.36364L6.23077 12L13 2"></path>
                                    </svg>
                                    <div class="error" id="checkbox_error" style="width: max-content;"></div>
                                </div>
                            </div>

                        <?php } } ?>

                    </div>
                </div>
                <div class="my-5 py-2 text-center">
                    <button class="btn text-white font-20 fw-bold profile-save-btn bg-color p-3 box-radius" type="submit"><?php if(Session::get('app_string')){ echo Session::get('app_string.account_profile.save'); }else{ echo CommonHelper::multi_language('account_profile','save')->multi_language_value->language_value; } ?> </button>
                </div>
            </div>
        </form>
    </div>
</section>

<script type="text/javascript">
    $('.checkbox').click(function() {
        var value = $(this).val();
        var count = $('input[type="checkbox"]:checked').length;
        
        if (count == 0) {
            $('.cbx_'+value).click();
        }
    });
</script>

@endsection
