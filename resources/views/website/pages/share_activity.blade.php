@extends('website.layouts.master')
@section('content')
@include('website.include.flash-message')

<section>
    <div class="container">
        <div class="mt-md-5 py-5">
            <a href="{{ route('profile')}}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.share_and_loyalty'); }else{ echo CommonHelper::multi_language('settings','share_and_loyalty')->multi_language_value->language_value; } ?></span>
        </div>
        <div class="text-center">
            <img class="img-fluid" src="{{ asset('assets/website/image/share-activity.png') }}" alt="">
            <div class="text-white fw-bold font-date my-5 pt-2 pb-3">
                <?php if(Session::get('app_string')){ echo Session::get('app_string.account_profile.received_loyal_offer'); }else{ echo CommonHelper::multi_language('account_profile','received_loyal_offer')->multi_language_value->language_value; } ?>
            </div>
            <div class="d-flex justify-content-center">
                <div class="bg-color mb-5 black-dote-bg position-relative box-radius">
                    <button class="btn fw-bold font-share-code text-white px-5 p-2">{{ !empty($reference_code)?$reference_code:'' }}</button>
                </div>
            </div>
            <div>
                <?php if(date('Y-m-d') > $referral_expiration_date){ ?>
                    <a href="javascript:void(0)" class="font-18 text-danger"><?php if(Session::get('app_string')){ echo Session::get('app_string.share_and_loyalty.code_expired'); }else{ echo CommonHelper::multi_language('share_and_loyalty','code_expired')->multi_language_value->language_value; } ?></a>
                <?php }else{ ?> 
                    <a href="javascript:void(0)" onclick="copy_clipboard();" class="font-18 text-white"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.share'); }else{ echo CommonHelper::multi_language('movie_details','share')->multi_language_value->language_value; } ?></a>                    
                <?php } ?>
            </div>
            <div class="mt-3"><a href="{{ url('/loyalty-reward-terms-condition') }}" class="font-18 text-white" target="_blank"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.terms_conditions'); }else{ echo CommonHelper::multi_language('settings','terms_conditions')->multi_language_value->language_value; } ?></a></div>
        </div>
    </div>


    <textarea style="display:none;" id="reference_code">Start watching all the same great TV shows and movies as I am. Please use this referral code to get the benefits.:- {{ !empty($reference_code)?$reference_code:'' }} 
    website : {{ url('/').'/signup?referral_code='}}{{ !empty($reference_code)?$reference_code:'' }}
    Android playstore link : https://play.google.com/store/apps/details?id=com.app.awe
    iOS playstore link : itms-apps://itunes.apple.com/app/id1609922315
    </textarea>

</section>

<script type="text/javascript">
    function copy_clipboard() {
      /* Get the text field */
      var copyText = $('#reference_code').val();
      console.log(copyText);
      /* Select the text field */
      //copyText.select();
      //copyText.setSelectionRange(0, 99999); /* For mobile devices */

      /* Copy the text inside the text field */
      navigator.clipboard.writeText(copyText);
      
      Notiflix.Notify.Success('Copy Text to Clipboard');

    }
</script>

@endsection
