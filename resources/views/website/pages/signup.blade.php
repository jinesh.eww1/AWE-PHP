@extends('website.layouts.master')
@section('content')
    <script type="text/javascript"
        src="https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js"></script>

    <style type="text/css">
        #phone {
            padding-right: 53px !important;
        }
        .alert {
            position: unset !important;
        }
        #checkbox_error .parsley-errors-list.filled{
            padding-left: 0px !important;
        }

    </style>
    <section>
        <div class="container">
            <div class="text-center pt-lg-5">
                <img src="{{ asset('assets/website/image/awe-home.png') }}" class="img-fluid mt-5 box-radius" height="110px"
                    width="110px">
                <p class="text-white mt-3"><?php if (Session::get('app_string')) {
                    echo Session::get('app_string.register.register_secondary_title');
                } else {
                    echo CommonHelper::multi_language('register', 'register_secondary_title')->multi_language_value->language_value;
                } ?></p>
            </div>
            <div class="row justify-content-center">
                <div class="col-11 col-md-10 col-lg-6 bg-white box-radius mb-5">
                    <div class="row text-center py-3 mt-md-4 font-18">
                        <div class="col-md-6 p-3 signup-border"><a href="{{ route('index') }}"
                                class="text-decoration-none  text-muted"><?php if (Session::get('app_string')) {
                                    echo Session::get('app_string.login.sign_in');
                                } else {
                                    echo CommonHelper::multi_language('login', 'sign_in')->multi_language_value->language_value;
                                } ?></a></div>
                        <div class="col-md-6 p-3 signin-border fw-bold"><?php if (Session::get('app_string')) {
                            echo Session::get('app_string.register.sign_up');
                        } else {
                            echo CommonHelper::multi_language('register', 'sign_up')->multi_language_value->language_value;
                        } ?></div>
                    </div>

                    @include('website.include.flash-message')

                    <form class="row justify-content-center" action="{{ route('signup') }}" id="signup_form" method="post"
                        onsubmit="process()">
                        @csrf()

                        <?php /*<input type="hidden" name="latitude" id="latitude">
                        <input type="hidden" name="longitude" id="longitude"> */ ?>
                        <input type="hidden" name="ip" id="device_ip">
                        <input type="hidden" name="device_name" id="device_name">
                        <input type="hidden" name="device_token" id="device_token">

                        <div class="col-md-11 px-4">
                            <div class="row justify-content-center">
                                <div class="mt-4 col-md-6  box-radius">
                                    <label class="form-label"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.add_Profile.first_name');
                                    } else {
                                        echo CommonHelper::multi_language('add_Profile', 'first_name')->multi_language_value->language_value;
                                    } ?><span class="text-danger">
                                            *</span></label>
                                    <input type="text" minlength="2" maxlength="15"
                                        class="form-control border-color box-radius" required name="first_name"
                                        placeholder="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.add_Profile.first_name');
                                        } else {
                                            echo CommonHelper::multi_language('add_Profile', 'first_name')->multi_language_value->language_value;
                                        } ?>" value="{{ old('first_name') }}"
                                        data-parsley-pattern="^[a-zA-Z\s]*$"
                                        data-parsley-required-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.add_Profile.enter_first_name');
                                        } else {
                                            echo CommonHelper::multi_language('add_Profile','enter_first_name')->multi_language_value->language_value;
                                        } ?>"
                                        data-parsley-length-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.add_Profile.first_name_min_length');
                                        } else {
                                            echo CommonHelper::multi_language('add_Profile','first_name_min_length')->multi_language_value->language_value;
                                        } ?>"
                                        data-parsley-pattern-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.add_Profile.please_enter_valid_first_name');
                                        } else {
                                            echo CommonHelper::multi_language('add_Profile','please_enter_valid_first_name')->multi_language_value->language_value;
                                        } ?>">
                                </div>
                                <div class="mt-4 col-md-6">
                                    <label class="form-label"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.add_Profile.last_name');
                                    } else {
                                        echo CommonHelper::multi_language('add_Profile','last_name')->multi_language_value->language_value;
                                    } ?><span class="text-danger">
                                            *</span></label>
                                    <input type="text" data-parsley-length="[2,15]"
                                        class="form-control border-color box-radius" required name="last_name"
                                        placeholder="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.add_Profile.last_name');
                                        } else {
                                            echo CommonHelper::multi_language('add_Profile','last_name')->multi_language_value->language_value;
                                        } ?>" value="{{ old('last_name') }}"
                                        data-parsley-pattern="^[a-zA-Z\s]*$"
                                        data-parsley-required-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.add_Profile.enter_last_name');
                                        } else {
                                            echo CommonHelper::multi_language('add_Profile','enter_last_name')->multi_language_value->language_value;
                                        } ?>"
                                        data-parsley-length-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.add_Profile.last_name_min_length');
                                        } else {
                                            echo CommonHelper::multi_language('add_Profile','last_name_min_length')->multi_language_value->language_value;
                                        } ?>"
                                        data-parsley-pattern-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.add_Profile.please_enter_valid_last_name');
                                        } else {
                                            echo CommonHelper::multi_language('add_Profile','please_enter_valid_last_name')->multi_language_value->language_value;
                                        } ?>"
                                        >
                                </div>
                                <div class="mt-4 col-md-6">
                                    <label class="form-label"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.register.email');
                                    } else {
                                        echo CommonHelper::multi_language('register', 'email')->multi_language_value->language_value;
                                    } ?><span class="text-danger">
                                            *</span></label>
                                    <input type="email" value="{{ old('email') }}" onfocus="this.value = this.value;"  onkeyup="this.value=removeSpaces(this.value);"
                                        class="form-control border-color box-radius" required name="email"
                                        data-parsley-validate-full-width-characters="true" data-parsley-type="email"
                                        
                                        placeholder="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.email');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'email')->multi_language_value->language_value;
                                        } ?>"
                                        data-parsley-required-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.login.email_required');
                                        } else {
                                            echo CommonHelper::multi_language('login', 'email_required')->multi_language_value->language_value;
                                        } ?>"
                                        
                                        data-parsley-validate-full-width-characters-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.app_titles_and_messages.please_enter_valid_email');
                                        } else {
                                            echo CommonHelper::multi_language('app_titles_and_messages', 'please_enter_valid_email')->multi_language_value->language_value;
                                        } ?>"

                                        data-parsley-type-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.app_titles_and_messages.please_enter_valid_email');
                                        } else {
                                            echo CommonHelper::multi_language('app_titles_and_messages', 'please_enter_valid_email')->multi_language_value->language_value;
                                        } ?>">
                                </div>
                                <div class="mt-4 col-md-6 position-relative">
                                    <label class="form-label">OTP<span class="text-danger"> *</span></label>
                                    <a href="JavaScript:void(0);" id="send_email_otp" onclick="send_email_otp();"
                                        class="text-decoration-none text-yellow font-12 float-end pt-2"><?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.send_otp');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'send_otp')->multi_language_value->language_value;
                                        } ?></a>
                                    <input type="tel" data-parsley-type="digits" maxlength="4"oninput="paste_only_digit(this)"
                                        minlength="4"onkeypress='validate(event)' data-parsley-equalto="#check_email_otp"
                                        class="form-control border-color box-radius" required id='email_otp'
                                        name="email_otp" placeholder="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.enter_email_otp');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'enter_email_otp')->multi_language_value->language_value;
                                        } ?>"
                                      
                                        data-parsley-equalto-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.valid_otp');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'valid_otp')->multi_language_value->language_value;
                                        } ?>"

                                        data-parsley-required-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.email_otp_required');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'email_otp_required')->multi_language_value->language_value;
                                        } ?>">

                                    <input type="hidden" class="check_email_otp" required name="check_email_otp"
                                        id="check_email_otp">
                                    <span class="verify_email"></span>
                                </div>

                                <div class="mt-4 col-md-6">
                                    <label class="form-label"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.register.mobile');
                                    } else {
                                        echo CommonHelper::multi_language('register', 'mobile')->multi_language_value->language_value;
                                    } ?><span class="text-danger">
                                            *</span></label>

                                    <input type="tel" data-parsley-type="digits"
                                        oninput="phone_copy(this)" oninput="paste_only_digit(this)"
                                        maxlength="15" data-parsley-length="[6, 15]"
                                        class="form-control border-color box-radius" required name="phone"
                                        id="phone"onkeypress='validate(event)' placeholder="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.mobile');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'mobile')->multi_language_value->language_value;
                                        } ?>"
                                        value="{{ old('phone') }}" data-parsley-required-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.mobile_required');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'mobile_required')->multi_language_value->language_value;
                                        } ?>"
                                        data-parsley-length-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.mobile_number_min_length');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'mobile_number_min_length')->multi_language_value->language_value;
                                        } ?>">
                                    <input type="hidden" name="country_code" id="country_code" value="">
                                    <input type="hidden" name="country_iso2" id="country_iso2" value="">

                                </div>
                                <div class="mt-4 col-md-6 position-relative">
                                    <label class="form-label">OTP<span class="text-danger"> *</span></label>
                                    <a href="JavaScript:void(0);" id="send_mobile_otp" onclick="send_mobile_otp();"
                                        class="text-decoration-none text-yellow font-12 float-end pt-2"><?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.send_otp');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'send_otp')->multi_language_value->language_value;
                                        } ?></a>
                                    <input type="tel" data-parsley-type="digits" maxlength="4" minlength="4"oninput="paste_only_digit(this)"
                                        class="form-control border-color box-radius"
                                        data-parsley-equalto="#check_mobile_otp" id="mobile_otp" required
                                        onkeypress='validate(event)' name="mobile_otp" placeholder="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.enter_mobile_otp');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'enter_mobile_otp')->multi_language_value->language_value;
                                        } ?>"
                                        data-parsley-equalto-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.valid_otp');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'valid_otp')->multi_language_value->language_value;
                                        } ?>"
                                        data-parsley-required-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.mobile_otp_required');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'mobile_otp_required')->multi_language_value->language_value;
                                        } ?>">
                                    <input type="hidden" class="check_mobile_otp" required name="check_mobile_otp"
                                        id="check_mobile_otp">
                                    <span class="verify_mobile"></span>

                                </div>
                                <div class="mt-4 col-md-6 position-relative">
                                    <label class="form-label"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.login.password');
                                    } else {
                                        echo CommonHelper::multi_language('login', 'password')->multi_language_value->language_value;
                                    } ?><span class="text-danger">
                                            *</span></label>
                                    <input type="password" autocomplete="off" minlength="8" maxlength="15"
                                        class="form-control border-color box-radius" required name="password"
                                        placeholder="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.login.password');
                                        } else {
                                            echo CommonHelper::multi_language('login', 'password')->multi_language_value->language_value;
                                        } ?>" id="password_field"
                                        data-parsley-whitespace="trim"
                                        data-parsley-required-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.login.password_required');
                                        } else {
                                            echo CommonHelper::multi_language('login', 'password_required')->multi_language_value->language_value;
                                        } ?>"
                                        data-parsley-length-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.login.min_8_password');
                                        } else {
                                            echo CommonHelper::multi_language('login', 'min_8_password')->multi_language_value->language_value;
                                        } ?>">
                                    <span class="hide_pass"><img src="{{ asset('assets\website\image/Icon-Hide.svg') }}"
                                            class="hide-icon"></span>
                                </div>
                                <div class="mt-4 col-md-6 position-relative">
                                    <label class="form-label"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.register.c_password');
                                    } else {
                                        echo CommonHelper::multi_language('register', 'c_password')->multi_language_value->language_value;
                                    } ?><span class="text-danger">
                                            *</span></label>
                                    <input type="password" minlength="8" maxlength="15"
                                        class="form-control border-color box-radius" required name="password_confirmation"
                                        id="password_confirmation" placeholder="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.c_password');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'c_password')->multi_language_value->language_value;
                                        } ?>"
                                        data-parsley-equalto="#password_field"
                                        data-parsley-required-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.c_password_required');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'c_password_required')->multi_language_value->language_value;
                                        } ?>"
                                        data-parsley-length-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.login.min_8_password');
                                        } else {
                                            echo CommonHelper::multi_language('login', 'min_8_password')->multi_language_value->language_value;
                                        } ?>"
                                        data-parsley-equalto-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.password_cpassword_same');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'password_cpassword_same')->multi_language_value->language_value;
                                        } ?>">
                                    <span class="con_hide_pass"><img
                                            src="{{ asset('assets\website\image/Icon-Hide.svg') }}"
                                            class="hide-icon"></span>
                                </div>
                                <div class="mt-4 col-md-12">
                                    <label class="form-label"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.register.referral_code');
                                    } else {
                                        echo CommonHelper::multi_language('register', 'referral_code')->multi_language_value->language_value;
                                    } ?></label>

                                    <input type="text" value="{{ !empty($_GET['referral_code'])?$_GET['referral_code']:old('referral_code') }}" name="referral_code"
                                        class="form-control border-color box-radius" placeholder="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.referral_code');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'referral_code')->multi_language_value->language_value;
                                        } ?>">
                                </div>
                                <div class="font-14 mt-4 pt-3 d-flex align-items-center">
                                    <input type="checkbox" name="checkbox" id="signup_checkbox" required class="check-large" value="1"
                                        data-parsley-errors-container="#checkbox_error"
                                        data-parsley-required-message="<?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.terms_condition_required');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'terms_condition_required')->multi_language_value->language_value;
                                        } ?>" 
                                        >

                                    <label style="cursor:pointer;" for="signup_checkbox" class="ps-3 font-12"><?php echo str_replace(CommonHelper::multi_language('register', 'terms_condition_show_name')->multi_language_value->language_value, '<a target="_blank" href="' . url('/terms-conditions') . '" class="text-yellow fw-bold">' . CommonHelper::multi_language('register', 'terms_condition_show_name')->multi_language_value->language_value . '</a>', CommonHelper::multi_language('register', 'agree_to_our_terms_conditions')->multi_language_value->language_value); ?> </label>

                                </div>
                                <div class="error" id="checkbox_error"></div>
                                @error('checkbox')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                                <div class="row justify-content-center mt-4">
                                    <button onclick="signup_function()" id="registration_btn"
                                        class="col-md-9 btn bg-color p-3 fw-bold text-white font-18 box-radius"
                                        type="button"><?php if (Session::get('app_string')) {
                                            echo Session::get('app_string.register.sign_up');
                                        } else {
                                            echo CommonHelper::multi_language('register', 'sign_up')->multi_language_value->language_value;
                                        } ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="text-center mt-3">
                        <p class="text-forgot font-14"><?php if (Session::get('app_string')) {
                            echo Session::get('app_string.login.or_continue');
                        } else {
                            echo CommonHelper::multi_language('login', 'or_continue')->multi_language_value->language_value;
                        } ?></p>
                        <div class="text-white mb-4" style="display: inline-flex;">
                            <a href="JavaScript:void(0)" class="me-3" onclick="goolge_social_form();"><img
                                    src="{{ asset('assets/website/image/google.png') }}"></a>
                            <a href="JavaScript:void(0)" class="me-3" onclick="fb_social_form();"><img
                                    src="{{ asset('assets/website/image/facebook.png') }}"></a>
                            <!-- <a href="{{ route('apple') }}"><img src="{{ asset('assets/website/image/card/apple.png') }}"></a> -->

                            <div id="appleid-signin" data-border="true" data-mode="logo-only" data-border-radius="50"
                                data-width="130" data-height="30" data-color="white"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <form class="row justify-content-center" id="social_form" action="" method="get">

        <?php /*<input type="hidden" name="latitude" id="social_latitude">
        <input type="hidden" name="latitude" id="social_latitude">*/ ?>

        <input type="hidden" name="ip" id="social_device_ip">
        <input type="hidden" name="device_name" id="social_device_name">
        <input type="hidden" name="device_token" id="social_device_token">

    </form>


    <script type="text/javascript" src="{{ asset('assets/website/js/jwt-decode.js') }}"></script>


    <script>
        function process(input){
  let value = input.value;
  let numbers = value.replace(/[^0-9]/g, "");
  input.value = numbers;
}

        function goolge_social_form() {
            //$('#social_latitude').val($('#latitude').val());
            //$('#social_longitude').val($('#longitude').val());

            $('#social_device_ip').val($('#device_ip').val());
            $('#social_device_name').val($('#device_name').val());
            $('#social_device_token').val($('#device_token').val());

            $('#social_form').attr('action','{{ route("google") }}');
            $('#social_form').submit();
        }

        function fb_social_form() {
            //$('#social_latitude').val($('#latitude').val());
            //$('#social_longitude').val($('#longitude').val());

            $('#social_device_ip').val($('#device_ip').val());
            $('#social_device_name').val($('#device_name').val());
            $('#social_device_token').val($('#device_token').val());

            $('#social_form').attr('action','{{ route("facebook") }}');
            $('#social_form').submit();
        }

        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
        
        //prevent characters and other special characters
        $(document).ready(function() {
            //getLocation();

            $('input[name="phone"]').on('keypress', function(e) {
                var $this = $(this).val().trim();
                var regex = new RegExp("^[0-9\b]+$");
                if (e.charCode < 49 && e.charCode > 47) {
                    if ($this.length == 0) {
                        e.preventDefault();
                        return false;
                    } else {
                        return true;
                    }

                }
            });

        });
        
        $('input').keydown(function(e) {
          if(e.which == 13) {
            signup_function();
            return false;
          }
        });

        function signup_function() {
            /*var device_token = $('#device_token').val();

            if(device_token == ''){
                Notiflix.Notify.Failure(`<?php //if (Session::get('app_string')) { echo Session::get('app_string.app_titles_and_messages.allow_notification_permission'); } else { echo CommonHelper::multi_language('app_titles_and_messages', 'allow_notification_permission')->multi_language_value->language_value;} ?>`);
                return false;
            }*/
            
            $('#signup_form').submit();
            
        }


        function time_out(type, sec) {

            var incomeTicker = sec;

            if (type == 'email') {
                //$('input[name="email"]').attr('readonly', 'readonly');
                window.email_timer = setInterval(function() {
                    if (incomeTicker > 0) {
                        incomeTicker--;
                        $('#send_email_otp').empty().html("00 : " + ("0" + incomeTicker).slice(-2));
                        $('#send_email_otp').removeAttr('onclick');
                        $('input[name="email_otp"]').removeAttr('readonly');

                    } else {

                        if (incomeTicker === 0) {
                            clearInterval(window.email_timer);
                        }

                        $('#send_email_otp').empty().html(
                            "{{ CommonHelper::multi_language('register', 'resend_otp')->multi_language_value->language_value }}"
                        );
                        $('#send_email_otp').attr('onclick', 'send_email_otp();');

                    }
                }, 1000);
            } else {
                //$('input[name="phone"]').attr('readonly', 'readonly');
                window.mobile_timer = setInterval(function() {
                    if (incomeTicker > 0) {
                        incomeTicker--;

                        $('#send_mobile_otp').empty().html("00 : " + ("0" + incomeTicker).slice(-2));
                        $('#send_mobile_otp').removeAttr('onclick');
                        $('input[name="mobile_otp"]').removeAttr('readonly');
                    } else {

                        if (incomeTicker === 0) {
                            clearInterval(window.mobile_timer);
                        }

                        $('#send_mobile_otp').empty().html(
                            "{{ CommonHelper::multi_language('register', 'resend_otp')->multi_language_value->language_value }}"
                        );
                        $('#send_mobile_otp').attr('onclick', 'send_mobile_otp();');
                        // $('input[name="mobile_otp"]').attr('readonly','readonly');

                    }

                }, 1000);
            }

        }


        function send_email_otp() {

            var email = $('input[name="email"]').val();
            if (email != '') {
                time_out('email', 60);
            } else {
                 Notiflix.Notify.Failure("<?php if (Session::get('app_string')) {echo Session::get('app_string.register.email_send_otp'); } else {echo CommonHelper::multi_language('register', 'email_send_otp')->multi_language_value->language_value;} ?>"); 

                return false;
            }
            $('input[name="email_otp"]').removeAttr('readonly');
            $('#loader').show();
            $.ajax({
                type: "get",
                dataType: "json",
                url: "/signup/email-otp/" + email,
                success: function(returnData) {

                    if (returnData.status == true) {
                        //Notiflix.Notify.Success('Email OTP : '+returnData.data.otp);
                        Notiflix.Notify.Success(returnData.message);
                        $('#check_email_otp').val(returnData.data.otp);
                        $("#loader").hide();
                    } else {
                        clearInterval(window.email_timer);
                        Notiflix.Notify.Failure(returnData.message);
                        $('#send_email_otp').empty().html(
                            "{{ CommonHelper::multi_language('register', 'resend_otp')->multi_language_value->language_value }}"
                        );
                        $('#send_email_otp').attr('onclick', 'send_email_otp();');
                        //$('#email_otp').attr('readonly', 'readonly');
                        $('input[name="email"]').removeAttr('readonly');
                        $("#loader").hide();
                    }

                },
                error: function(data) {},
            });
        }

        function send_mobile_otp() {

            var mobile = $('input[name="phone"]').val();
            if (mobile != '') {
                time_out('mobile', 60);
            } else {
                 Notiflix.Notify.Failure("<?php if (Session::get('app_string')) {echo Session::get('app_string.register.mobile_send_otp'); } else {echo CommonHelper::multi_language('register', 'mobile_send_otp')->multi_language_value->language_value;} ?>");
                return false;
            }
            $('#mobile_otp').removeAttr('readonly');
            $('#loader').show();
            $.ajax({
                type: "get",
                dataType: "json",
                url: "/signup/mobile-otp?mobile=" + mobile + "&code=" + phoneInput
                    .getSelectedCountryData().dialCode,
                success: function(returnData) {

                    if (returnData.status == true) {
                        //Notiflix.Notify.Success('Mobile OTP : '+returnData.data.otp);
                        Notiflix.Notify.Success(returnData.message);
                        $('#check_mobile_otp').val(returnData.data.otp);
                        $("#loader").hide();
                    } else {
                        clearInterval(window.mobile_timer);
                        Notiflix.Notify.Failure(returnData.message);
                        $('#send_mobile_otp').empty().html(
                            "{{ CommonHelper::multi_language('register', 'resend_otp')->multi_language_value->language_value }}"
                        );
                        $('#send_mobile_otp').attr('onclick', 'send_mobile_otp();');
                        //$('#mobile_otp').attr('readonly', 'readonly');
                        $('input[name="phone"]').removeAttr('readonly');
                        $("#loader").hide();
                    }
                },
                error: function(data) {},
            });
        }

        $('input[name="email_otp"]').keyup(function() {

            var check_email_otp = $('#check_email_otp').val();
            var email_otp = $(this).val().trim();

            if (email_otp.length == 4 && email_otp == check_email_otp) {
                $('.verify_email').empty();
                $('.verify_email').html(
                    '<img src="{{ asset('assets/website/image/send-otp.png') }}" class="obout-otp">');

                clearInterval(window.email_timer);
                $('input[name="email"],input[name="email_otp"]').attr('readonly', 'readonly');
                $('#send_email_otp').empty().removeAttr('onclick');
                $('input[name="email"]').removeAttr('readonly');

            } else {
                $('.verify_email').empty();
            }

        });

        $('input[name="email"]').change(function(){
            clearInterval(window.email_timer);
           $('input[name="email_otp"]').removeAttr('readonly').val('');
           $('.verify_email').empty();
           $('#check_email_otp').val('');
           $('#send_email_otp').html(`<?php if (Session::get('app_string')) { echo Session::get('app_string.register.send_otp'); } else { echo CommonHelper::multi_language('register', 'send_otp')->multi_language_value->language_value;} ?>`).attr('onclick', 'send_email_otp();');
        });

        $('input[name="mobile_otp"]').keyup(function() {

            var check_mobile_otp = $('#check_mobile_otp').val();
            var mobile_otp = $(this).val().trim();
            if (mobile_otp.length == 4 && mobile_otp == check_mobile_otp) {

                $('.verify_mobile').empty();
                $('.verify_mobile').html(
                    '<img src="{{ asset('assets/website/image/send-otp.png') }}" class="obout-otp">');

                clearInterval(window.mobile_timer);
                $('input[name="phone"],input[name="mobile_otp"]').attr('readonly', 'readonly');
                $('#send_mobile_otp').empty().removeAttr('onclick');
                $('input[name="phone"]').removeAttr('readonly');
            } else {
                $('.verify_mobile').empty();
            }

        });

        $('input[name="phone"]').change(function(){
            clearInterval(window.mobile_timer);
           $('input[name="mobile_otp"]').removeAttr('readonly').val('');
           $('.verify_mobile').empty();
           $('#check_mobile_otp').val('');
           $('#send_mobile_otp').html(`<?php if (Session::get('app_string')) { echo Session::get('app_string.register.send_otp'); } else { echo CommonHelper::multi_language('register', 'send_otp')->multi_language_value->language_value;} ?>`).attr('onclick', 'send_mobile_otp();');
        });


        window.AppleID.auth.init({
            clientId: "com.awe.web",
            scope: "name email",
            redirectURI: "{{ route('apple.callback') }}",
            state: new Date().toISOString(),
            usePopup: true,
        });

        document.addEventListener(
            "AppleIDSignInOnSuccess",
            appleLoginSuccessful
        );

        document.addEventListener(
            "AppleIDSignInOnFailure",
            appleLoginFailed
        );

        function appleLoginSuccessful(e) {
            var token = e.detail.authorization.id_token;
            var decoded = jwt_decode(token);
            var data = {
                email: decoded.email,
                id: decoded.sub,
                _token: "{{ csrf_token() }}",
                //latitude: $('#latitude').val(),
                //longitude: $('#longitude').val(),
                ip: $('#device_ip').val(),
                device_name: $('#device_name').val(),
                device_token: $('#device_token').val()
            };
            $.ajax({
                url: "{{ route('apple.callback') }}",
                type: "POST",
                data: data,
                success: function(data) {
                    if (data.status) {
                        Notiflix.Notify.Success('Success');
                        window.location.href = data.redirect_url;
                    } else {
                        Notiflix.Notify.Failure(data.message);
                    }
                }
            });
        }

        function appleLoginFailed(e) {}


        function phone_copy(input){
          let value = input.value;
          let numbers = value.replace(/[^0-9]/g, "");
          input.value = numbers;
        }
    </script>
@endsection
