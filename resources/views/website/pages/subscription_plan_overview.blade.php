@extends('website.layouts.master')
@section('content')

@if($post_data['payment_method'] == 'paypal')
<script src="https://www.paypal.com/sdk/js?client-id=AYiQqLene-VH7Ti0OcxD1_1H-Mb3IYg5DCSPoXusGxXNKSxtqMX_l-dSMZbSwoJHBrJffTI5wvR-CaCj&vault=true&intent=subscription" data-sdk-integration-source="button-factory" id="sub_script"></script>
@endif

<?php

   $plan_name=$plan_description=$plan_price = '';
    if(!empty($api_plan_detail)){
        foreach ($api_plan_detail['data'] as $key => $value) {
            
            if($value['id'] == $post_data['plan_id']){
                $plan_name = $value['name'];
                $plan_description = $value['description'];
                if($post_data['plan_type'] == 1){
                    $plan_price = $value['currency_sign'].' '.$value['monthly_price'].' <span class="fw-light font-date opactiy-80"> / '.CommonHelper::multi_language('payment_method','month')->multi_language_value->language_value.'</span>';    
                }else{
                    $plan_price = $value['currency_sign'].' '.$value['yearly_price'].' <span class="fw-light font-date opactiy-80"> / '.CommonHelper::multi_language('payment_method','year')->multi_language_value->language_value.'</span>';    
                }
                
            }
        }
    }
?>
<section>
    <div class="container">
        <div class="mt-md-5 pt-5">
            <a href="{{ route('all.plans') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.subscription.subscription_plan_overview'); }else{ echo CommonHelper::multi_language('subscription','subscription_plan_overview')->multi_language_value->language_value; } ?></span>
        </div>
        <div class="row justify-content-center my-5">
            <div class="col-lg-5 col-md-8 col-11 bg-white box-radius">
                <div class="p-4 mt-3 px-3 px-md-5 ">
                    <div class="font-18 plan-bottom pb-3">{{ __('message.plan.selected_plan') }}</div>
                    <div class="font-22 pt-3 text-yellow">{{ $plan_name }}</div>
                    <p class="fw-light opactiy-80 mt-2 font-verify">{{ $plan_description }}</p>
                    <div class="text-yellow doller-font pb-3"><?php echo $plan_price; ?></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mb-4">
            <div class="col-lg-5 col-md-8 col-11 bg-white box-radius">
                <div class="p-4 my-3 px-3 px-md-5">
                    <div class="plan-bottom font-18 pb-3">{{ __('message.plan.payment') }}</div>
                    <div class="d-flex justify-content-between align-items-center mt-4">
                        @if($post_data['payment_method'] == 'paypal')
                            <img class="img-fluid" src="{{ asset('assets/website/image/paypal.svg') }}" alt="">
                        @else
                        <div>
                            <?php if($post_data['card_name'] == 'visa'){ ?>
                                <img class="img-fluid p-2 card-pay-border card_image" src="{{ asset('assets/website/image/visa-card.png') }}" alt="">
                            <?php }elseif ($post_data['card_name'] == 'mastercard') { ?>
                                <img class="img-fluid p-2 card-pay-border card_image" src="{{ asset('assets/website/image/master-card.png') }}" alt="">
                            <?php }elseif ($post_data['card_name'] == 'amex') { ?>
                                <img class="img-fluid p-2 card-pay-border card_image" height="30px" width="10px" src="{{ asset('assets/website/image/card/Amex.png') }}" alt="">
                            <?php }elseif ($post_data['card_name'] == 'diners') { ?>
                                <img class="img-fluid p-2 card-pay-border card_image" src="{{ asset('assets/website/image/card/Diners_Club.png') }}" alt="">
                            <?php }elseif ($post_data['card_name'] == 'discover') { ?>
                                <img class="img-fluid p-2 card-pay-border card_image" src="{{ asset('assets/website/image/card/Disciver.png') }}" alt="">
                            <?php }elseif ($post_data['card_name'] == 'jcb') { ?>
                                <img class="img-fluid p-2 card-pay-border card_image" src="{{ asset('assets/website/image/card/jcb.png') }}" alt="">
                            <?php }elseif ($post_data['card_name'] == 'unionpay') { ?>
                                <img class="img-fluid p-2 card-pay-border card_image" src="{{ asset('assets/website/image/card/unionpay-svgrepo-com.png') }}" alt="">
                            <?php }else{?>
                                <img class="img-fluid p-2 card-pay-border card_image" src="{{ asset('assets/website/image/visa-card.png') }}" alt="">
                            <?php } ?>

                            @if(isset($api_data['data']) && !empty($api_data['data']['account_number_last4']))
                            <span class="card-pay-color ms-md-3 ms-0">**** **** **** {{ $api_data['data']['account_number_last4'] }}</span>
                            @else
                            <span class="card-pay-color ms-md-3 ms-0">**** **** **** {{ trim($post_data['last_four_number']) }}</span>
                            @endif
                        </div>
                        <img class="img-fluid" src="{{ asset('assets/website/image/stripe.png') }}" alt="">
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-5 p-2">
            <div class="col-lg-5 col-md-8 col-11 px-0">
                @if($post_data['payment_method'] == 'paypal')
                    <?php if($post_data['plan_type'] == 1){ $plan_id = $plan_detail['paypal_monthly_subscription_id']; }else{ $plan_id = $plan_detail['paypal_yearly_subscription_id']; } ?>
                <div id="paypal-button-container-{{$plan_id}}" class="paypal_div"></div>
                @else
                

                <form class="row justify-content-center" id="stripe_plan_form" action="{{ route('stripe.return.response')}}" method="post" >
                    @csrf()

                    <input type="hidden" name="selected_card" value="{{ $post_data['selected_card'] }}">
                    <input type="hidden" name="plan_type" value="{{ $post_data['plan_type'] }}">
                    <input type="hidden" name="plan_id" value="{{ $post_data['plan_id'] }}">
                    <input type="hidden" name="name" value="{{ $post_data['name'] }}" >
                    <input type="hidden" name="number" value="{{ $post_data['number'] }}">
                    <input type="hidden" name="exp_month_year" value="{{ $post_data['exp_month_year'] }}">
                    <input type="hidden" name="cvv" value="{{ $post_data['cvv'] }}" >
                    
                    <button class="col-lg-4 col-md-6 col-12 btn text-white font-date bg-color box-radius p-md-4 fw-bold" type="submit"><?php if(Session::get('app_string')){ echo Session::get('app_string.subscription.confirm'); }else{ echo CommonHelper::multi_language('subscription','confirm')->multi_language_value->language_value; } ?></button>

                </form>
                @endif
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

    var plan_id = "{{!empty($plan_id)?$plan_id:''}}";  
    paypal.Buttons({
          style: {
              shape: 'rect',
              color: 'gold',
              layout: 'vertical',
              label: 'subscribe'
          },
          createSubscription: function(data, actions) {
            return actions.subscription.create({
              plan_id: plan_id
            });
          },
          onApprove: function(data, actions) {
            console.log(data);
            console.log(actions);

            if(data.subscriptionID != ''){
                paypal_return_response(data.subscriptionID);
            }else{
                Notiflix.Notify.Failure("Payment is not successfull");
            }
            //alert(data.subscriptionID); // You can add optional success message for the subscriber here
            //$('#subscription_id').empty();
            //$('#subscription_id').html(data.subscriptionID);

            //$('#status_msg').empty();
            //$('#status_msg').html(" Sucess, You have sucessfully actived subscription.");

          },
          onCancel: function (data) {
              console.log(data);

            $('#status_msg').empty();
            $('#status_msg').html(" Sorry,Your subscription not actived!");

          }
      }).render('#paypal-button-container-'+plan_id);



function paypal_return_response(subscription_id) {
    
    var plan_id = "{{ $post_data['plan_id'] }}";   
    var plan_type = "{{ $post_data['plan_type'] }}";  
    
    $.ajax({
        type: "get",
        dataType: "json",
        url: "{{ route('paypal.return.response') }}",
        data:{'plan_id':plan_id,"plan_type":plan_type,"subscription_id":subscription_id},
        success: function (returnData) {
            if(returnData.status == true){
                Notiflix.Notify.Success('Successfully');
                window.location.href = "{{ route('home','movie') }}";
            }
        },
        error: function (data) {
        },
    });
}

</script>
@endsection
