@extends('website.layouts.master')
@section('content')

    @include('website.include.flash-message')


<style type="text/css">
   .group1{
        background: transparent ;
        color: white ;
        width: 84px ;
        height: 84px ;
        font-size: 33px ;
    }
</style>
    <section>
    <div class="container">
        <div class="mt-md-5 py-5 mb-md-4">
            <a href="{{ route('forgot.password') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a>
        </div>
        <form method="post" action="{{ route('verify.otp') }}" class="position-relative verify_form">
            @csrf()
            <input type="hidden" name="phone" value="{{ $_GET['phone'] }}">
            <input type="hidden" name="country_code"  value="{{ $_GET['country_code'] }}">
            <input type="hidden" name="country_iso2"  value="{{ !empty($_GET['country_iso2'])?$_GET['country_iso2']:'' }}">
            <input type="hidden" name="type"  value="{{ !empty($_GET['type'])?$_GET['type']:'' }}">

            <div class="text-center">
                <img class="img-fluid" src="{{ asset('assets/website/image/verify-email.png') }} " alt="">
                <div class="text-white font-22 my-md-4 pt-3 pb-md-0 pb-3"><?php if(Session::get('app_string')){ echo Session::get('app_string.forgot_password.verify_mobile_otp'); }else{ echo CommonHelper::multi_language('forgot_password','verify_mobile_otp')->multi_language_value->language_value; } ?></div>
                <div class="d-flex justify-content-center">
                    <div class="text-forgot font-verify col-md-3"><?php if(Session::get('app_string')){ echo Session::get('app_string.forgot_password.mobile4digit'); }else{ echo CommonHelper::multi_language('forgot_password','mobile4digit')->multi_language_value->language_value; } ?></div>
                </div>
            </div>
            <div class="d-flex justify-content-center gap-md-4 gap-3 pt-1 mt-5">
               
                <div class="otp-vrification" id="otp" style="display: contents;">

                    <input type = 'password' class="form-control text-center group1 fw-bold font-18" data-parsley-type="digits" name="verify_otp_1" maxlength="1" id="first" data-parsley-errors-container="#pin_error" pattern="[0-9]*" inputmode="numeric"
                     oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');" >
                    
                    <input type = 'password' class="form-control text-center group1 fw-bold font-18" data-parsley-type="digits" name="verify_otp_2" maxlength="1" id="second" data-parsley-errors-container="#pin_error"pattern="[0-9]*" inputmode="numeric"oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');">

                    <input type = 'password' class="form-control text-center group1 fw-bold font-18" data-parsley-type="digits" name="verify_otp_3" maxlength="1" id="third" data-parsley-errors-container="#pin_error" pattern="[0-9]*" inputmode="numeric" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');">

                    <input type = 'password' class="form-control text-center group1 fw-bold font-18" data-parsley-type="digits" name="verify_otp_4" maxlength="1" id="fourth" data-parsley-errors-container="#pin_error"pattern="[0-9]*" inputmode="numeric" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');">

                </div>
            </div>
        </form>

            <input type="hidden" name="mobile_otp" value="{{ Session::get('mobile_otp') }}">
            <input type="hidden" name="phone" value="{{ $_GET['phone'] }}">
            <input type="hidden" name="country_code"  value="{{ str_replace('+', '', $_GET['country_code']) }}">
            <div class="text-center mt-md-4 pt-3">
                <button style="border:none;" onclick="resend_mobile_otp();" class=" bg-transparent text-yellow font-18 text-decoration-none" type="button"><?php if(Session::get('app_string')){ echo Session::get('app_string.register.resend_otp'); }else{ echo CommonHelper::multi_language('register','resend_otp')->multi_language_value->language_value; } ?></button>
            </div>


            <div class="text-center mt-4">
                <button class="btn profile-save-btn text-white bg-color font-18 fw-bold p-3 my-4 box-radius verify_continue_btn" type="button"><?php if(Session::get('app_string')){ echo Session::get('app_string.forgot_password.verify_continue'); }else{ echo CommonHelper::multi_language('forgot_password','verify_continue')->multi_language_value->language_value; } ?></button>
            </div>
        
    </div>
</section>
    
    <script type="text/javascript">
        $('.verify_continue_btn').click(function(){
            var otp = $('input[name=mobile_otp]').val();

            var verify_otp =  $('input[name=verify_otp_1]').val()+$('input[name=verify_otp_2]').val()+$('input[name=verify_otp_3]').val()+$('input[name=verify_otp_4]').val();
            
            if(otp == verify_otp){
                $('.verify_form').submit();
            }else{
                var msg = `<?php if(Session::get('app_string')){ echo Session::get('app_string.register.valid_otp'); }else{ echo CommonHelper::multi_language('register','valid_otp')->multi_language_value->language_value; } ?>`;
                Notiflix.Notify.Failure(msg);
            }
        });


function resend_mobile_otp() {

    var phone =  $('input[name="phone"]').val(); 
    var country_code =  $('input[name="country_code"]').val(); 
    //var base_url = window.location.origin;
    var base_url = `<?php if(isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] =='forgot-password'){ echo url('/').'/resend-otp-forgot-password'; }else{ echo url('/').'/resend-otp'; } ?>`;


    $('input[name=verify_otp_1],input[name=verify_otp_2],input[name=verify_otp_3],input[name=verify_otp_4]').val(''); 
        
    $.ajax({
        type: "get",
        dataType: "json",
        url: base_url+"?mobile="+phone+"&code="+country_code,
        success: function (returnData) {

            if(returnData.status == true){
                $('input[name=mobile_otp]').val(returnData.data.otp)
                Notiflix.Notify.Success(returnData.message);
                //Notiflix.Notify.Success('Mobile OTP : '+returnData.data.otp);
            }else{
                Notiflix.Notify.Failure(returnData.message);
            }
        },
        error: function (data) {
        },
    });
}

    function pin_validation() {

        var pin1 = $('#first').val();
        var pin2 = $('#second').val();
        var pin3 = $('#third').val();
        var pin4 = $('#fourth').val();

        if(pin1 == '' || pin2 == '' || pin3 == '' || pin4 == ''){        
            $('#pin_error').css('color','red');
            $('#pin_error').html('This value is required.');
            return false;
        }else{
            $('.profile_lock').submit();
        }
            
    }

    function OTPInput() {
  const inputs = document.querySelectorAll('#otp > *[id]');
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener('keydown', function(event) {
      if (event.key === "Backspace") {
        inputs[i].value = '';
        if (i !== 0)
          inputs[i - 1].focus();
          event.preventDefault();
      } else {
        if (i === inputs.length - 1 && inputs[i].value !== '') {
          return true;
        } else if (event.keyCode > 47 && event.keyCode < 58) {
          inputs[i].value = event.key;
          if (i !== inputs.length - 1)
            inputs[i + 1].focus();
          event.preventDefault();
        }else if (event.keyCode > 95 && event.keyCode < 112) {
          inputs[i].value = event.key;
          if (i !== inputs.length - 1)
            inputs[i + 1].focus();
          event.preventDefault();
        }

      }
    });
  }
}
OTPInput();
</script>


@endsection
