@extends('website.layouts.master')
@section('content')
    @include('website.include.flash-message')

    @include('website.layouts.head')

<style type="text/css">
    .bg-body {
      background: linear-gradient(rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.89));
    }
</style>
    <section class="mt-5 pt-5">
        <div class="container">
            <div class="mt-4 pt-lg-4 font-28 text-white text-center text-sm-start fw-bold">
                <?php if (!empty($view_more['data']['data']) && count($view_more['data']['data']) > 0) {
                    foreach ($view_more['data']['data'] as $key => $value) {
                        foreach ($value['genres'] as $gkey => $gvalue) {
                            if ($gvalue['id'] == base64_decode($category_type)) {
                                $genre_name = $gvalue['name'][app()->getLocale()];
                            }
                        }
                    }
                }
                echo $genre_name;
                ?>
            </div>

            <div class="about-grid-selction mt-3 py-3">

                <?php if(!empty($view_more['data']['data']) && count($view_more['data']['data'])>0){ 
					foreach ($view_more['data']['data'] as $key => $value) { ?>

                <div class="hover-img-list position-relative">
                    <?php if($value['is_free'] == 1){ ?>
                        @switch(Session::get('locale'))
                            @case("sp")
                                <img src="{{ asset('assets/website/image/card/Spanish.png') }}" class="img-fluid free_tag_img">
                            @break
                            @case("md")
                                <img src="{{ asset('assets/website/image/card/Mandarin.png') }}" class="img-fluid free_tag_img">
                            @break
                            @case("hi")
                                <img src="{{ asset('assets/website/image/card/hindi.png') }}" class="img-fluid free_tag_img">
                            @break
                            @case("fr")
                                <img src="{{ asset('assets/website/image/card/French.png') }}" class="img-fluid free_tag_img">
                            @break
                            @default
                                <img src="{{ asset('assets/website/image/card/free.png') }}" class="img-fluid free_tag_img">
                        @endswitch
                    <?php } ?>
                    <img src="{{ $value['poster'] }}" class="img-fluid img-height box-radius">
                    <a href="{{ route('content-details', $value['slug']) }}"
                        class="btn play-hover-list w-100 h-100 position-absolute"><img
                            src="{{ asset('assets/website/image/play.png') }}" class="img-fluid"></a>
                </div>

                <?php } } ?>

            </div>

            @if($view_more['data']['last_page']>1)    
            <nav aria-label=".." class="mt-5">
                <ul class="pagination justify-content-center">
                   

                    <?php for ($i=1; $i <= $view_more['data']['last_page']; $i++) { 
				    	if(!empty(request()->get('page'))){
				    		$page = request()->get('page');
				    	}else{
				    		$page = 1;
				    	}
				    	if($page == $i){ $url = 'JavaScript:void(0)"'; $active = "active"; }else{ $url = URL('/view-more')."/".$type."/".$category_type.'?type=web&page='.$i; $active=''; }
				    ?>
					@if($i==1)
					<li class="page-item  mx-2">
                        <a class="page-link bg-transparent text-white border-0" href="{{ $url }}" tabindex="-1"
                            aria-disabled="true">
                            <</a>
                    </li>
					@endif
                    <li class="page-item mx-2 {{ $active }}"><a class="page-link"
                            href="{{ $url }}">{{ $i }}</a></li>
                    <?php } ?>

                    <li class="page-item mx-2">
                        <a class="page-link bg-transparent text-white border-0" href="{{ $url }}">></a>
                    </li>
                </ul>
            </nav>
            @endif

        </div>

        <?php /*{{ $view_more->links() }}*/ ?>
    </section>




    @include('website.layouts.footer')
@endsection
