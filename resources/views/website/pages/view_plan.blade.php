@extends('website.layouts.master')
@section('content')

@include('website.include.flash-message')

<section>
    <div class="container">
        <div class="mt-md-5 py-5">
            <a href="{{ route('profile') }}"><img src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span class="text-white font-18"><?php if(Session::get('app_string')){ echo Session::get('app_string.subscription.subscription_label'); }else{ echo CommonHelper::multi_language('subscription','subscription_label')->multi_language_value->language_value; } ?></span>
            <a href="{{ route('all.plans') }}" class="float-end text-yellow font-18 fw-bold mt-1"><?php if(Session::get('app_string')){ echo Session::get('app_string.subscription.see_all_plans'); }else{ echo CommonHelper::multi_language('subscription','see_all_plans')->multi_language_value->language_value; } ?></a>
        </div>
        <div class="row pb-4 justify-content-center">
            <div class="col-xxl-4 col-lg-5 col-md-6 col-12 bg-subscrip">
                <div class="p-sm-3 p-2">
                    <div class="font-23 text-yellow"><?php if(Session::get('app_string')){ echo Session::get('app_string.account_profile.your_current_plan'); }else{ echo CommonHelper::multi_language('account_profile','your_current_plan')->multi_language_value->language_value; } ?></div>
                    <?php if(!empty($user) && !empty($plan['data'])){
                        
                            foreach ($plan['data'] as $key => $value) { 

                                if($value['id'] == $user['plan_id']){ 
                                    if($user['plan_type'] == 1){ $plan_price = $value['monthly_price']; }else{ $plan_price = $value['yearly_price']; }
                                    if($user['plan_type'] == 1){ $plan_type = CommonHelper::multi_language('payment_method','month')->multi_language_value->language_value; }else{ $plan_type = CommonHelper::multi_language('payment_method','year')->multi_language_value->language_value; }
                            ?>
                                <p class="fw-light text-white opactiy-80 mt-2">{{ $value['description'] }}</p>
                                <div class="text-yellow font-23">{{ $value['currency_sign'] }} {{ $plan_price }}<span class="text-white fw-light font-21 opactiy-80"> / {{ $plan_type }}</span></div>
                                <hr class="subscription-bottom my-4">
                                <div class="font-23 text-forgot fw-light"><?php if(Session::get('app_string')){ echo Session::get('app_string.account_profile.your_next_bill'); }else{ echo CommonHelper::multi_language('account_profile','your_next_bill')->multi_language_value->language_value; } ?></div>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="font-date text-white fw-light">{{ $value['expiration_date_formatted'] }}</div>
                                    <button class="text-yellow remove-btn-color py-2 px-md-5 px-4 font-18 box-radius border-0 view_plan_btn">{{ $value['name'] }}</button>
                                </div>            
                    <?php } } } ?>
                    
                </div>
            </div>
            <div class="mt-4 pe-md-5 text-center">
                <button class="col-lg-4 col-md-6 col-12 btn text-white font-20 bg-color box-radius p-3"  data-bs-toggle="modal" data-bs-target="#staticBackdrop"><?php if(Session::get('app_string')){ echo Session::get('app_string.account_profile.cancel_plans'); }else{ echo CommonHelper::multi_language('account_profile','cancel_plans')->multi_language_value->language_value; } ?></button>
                <div class="modal fade" id="staticBackdrop" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content text-center p-4">
                            <div class="modal-header">
                                <h5 class="modal-title" id="staticBackdropLabel"><?php if(Session::get('app_string')){ echo Session::get('app_string.subscription.are_you_sure_cancel_plan'); }else{ echo CommonHelper::multi_language('subscription','are_you_sure_cancel_plan')->multi_language_value->language_value; } ?></h5>
                            </div>
                            <div class="justify-content-center mt-4 mb-2">
                                <a href="javascript:void(0)"><button type="button" class="btn font-18 text-yellow border-color col-4 box-radius" data-bs-dismiss="modal"><?php if(Session::get('app_string')){ echo Session::get('app_string.logout.cancel'); }else{ echo CommonHelper::multi_language('logout','cancel')->multi_language_value->language_value; } ?></button></a>
                                <a href="{{ url('/cancel-plan/'.$user['plan_id'].'/'.$user['plan_type']) }}"><button type="button" class="btn font-18 bg-color text-white col-4 box-radius ms-4" data-bs-dismiss="modal"><?php if(Session::get('app_string')){ echo Session::get('app_string.subscription.confirm'); }else{ echo CommonHelper::multi_language('subscription','confirm')->multi_language_value->language_value; } ?></button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
