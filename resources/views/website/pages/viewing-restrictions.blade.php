@extends('website.layouts.master')
@section('content')
    @include('website.include.flash-message')

    <section>
        <div class="container">
            <div class="mt-md-5 py-5">
                <a href="{{ route('edit-profile', base64_encode($id)) }}"><img
                        src="{{ asset('assets/website/image/left-arrow.png') }}"></a> <span
                    class="text-white font-18"><?php if (Session::get('app_string')) {
                        echo Session::get('app_string.edit_profile.viewing_restrictions');
                    } else {
                        echo CommonHelper::multi_language('edit_profile', 'viewing_restrictions')->multi_language_value->language_value;
                    } ?></span>
            </div>
            <form action="{{ route('viewing-restrictions', base64_encode($id)) }}" method="post">
                <input type="hidden" name="user_id" value="{{ $id }}">
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-md-10 px-md-0">
                        <div>
                            <div class="row about-viewing gap-lg-5 gap-md-3 gap-0">
                                <div class="col-md-5 ms-md-5 col-11 ms-4">
                                    <h5 class="text-yellow mt-5 mb-md-5 py-3 fw-normal"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.viewing_restriction.title_restriction_for');
                                    } else {
                                        echo CommonHelper::multi_language('viewing_restriction', 'title_restriction_for')->multi_language_value->language_value;
                                    } ?>
                                        {{ $user['data']['full_name'] }}</h5>
                                    <div class="title-bert">

                                        <?php if(!empty($user_restriction['data']['age_ratings'])){
                                		foreach ($user_restriction['data']['age_ratings'] as $key => $value) { 
                                			if($value['selected'] == true){ $checked = 'checked="checked"'; }else{ $checked = ''; }
                                	?>

                                        <div class="d-flex align-items-center justify-content-between mb-md-4 pb-2">
                                            <div class="d-flex align-items-center">
                                                <div class="play-checkbox">
                                                    <input id="play-checkbox" type="checkbox" {{ $checked }}
                                                        name="age_rating_id[]" value="{{ $value['id'] }}">
                                                    <label for="play-checkbox"></label>
                                                    <svg width="15" height="14" viewBox="0 0 15 14" fill="none">
                                                        <path d="M2 8.36364L6.23077 12L13 2"></path>
                                                    </svg>
                                                </div>
                                                <span class="text-white font-25 ms-4">{{ $value['name'] }}</span>
                                            </div>
                                            <a href="javascript:void(0)" class="float-end px-lg-5 px-md-3 px-5"
                                                data-bs-toggle="popover" data-bs-trigger="focus" data-bs-placement="bottom"
                                                data-content="Content" data-bs-content="{{ $value['description'] }}">

                                                <img class="img-fluid"
                                                    src="{{ asset('assets/website/image/about-more.png') }}" alt="">
                                            </a>
                                        </div>

                                        <?php } } ?>


                                    </div>
                                </div>
                                <div class="col-md-5 ms-md-4 col-11 ms-4">
                                    <h5 class="text-yellow mt-5 mb-md-5 pt-3 fw-normal"><?php if (Session::get('app_string')) {
                                        echo Session::get('app_string.viewing_restriction.block_movies');
                                    } else {
                                        echo CommonHelper::multi_language('viewing_restriction', 'block_movies')->multi_language_value->language_value;
                                    } ?></h5>
                                    <div class="block-movie position-relative ">
                                        <input class="form-control font-20 bg-transparent ps-4 pe-5 py-3 text-white "
                                            type="text" name="" placeholder="<?php if (Session::get('app_string')) {
                                                echo Session::get('app_string.viewing_restriction.Enter_movie_or_series_name');
                                            } else {
                                                echo CommonHelper::multi_language('viewing_restriction', 'Enter_movie_or_series_name')->multi_language_value->language_value;
                                            } ?>"
                                            id="search_filter">
                                        <a id="clear_search"></a>
                                        <ul id="searchResult" class="list-unstyled bg-white book-movie text-break"
                                            style="margin-top: 5px;"></ul>

                                    </div>
                                    <div class="pt-2 add_content_div my-5">

                                        <?php if(!empty($user_restriction['data']['user_content'])){
                                		
                                		$content_id = array_column($user_restriction['data']['user_content'], 'content_id');

                                		foreach ($user_restriction['data']['user_content'] as $key => $value) { 
                                	?>
                                        <div class="d-flex justify-content-between align-items-center mb-5 "
                                            id="restricted_content_div{{ $value['content_id'] }}">
                                            <label class="text-white font-25">{{ $value['name'] }}</label>
                                            <img class="img-fluid me-5"
                                                src="{{ asset('assets/website/image/cancel-icon.png') }}" alt=""
                                                onclick="return remove_content_div({{ $value['content_id'] }})">
                                        </div>
                                        <?php } } ?>

                                        <input type="hidden" name="content_id" id="content_ids"
                                            value="{{ !empty($content_id) ? implode(',', $content_id) : '' }}">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="my-5 py-2 text-center">
                        <button class="btn text-white font-20 fw-bold profile-save-btn bg-color p-3 box-radius"
                            type="submit"><?php if (Session::get('app_string')) {
                                echo Session::get('app_string.account_profile.save');
                            } else {
                                echo CommonHelper::multi_language('account_profile', 'save')->multi_language_value->language_value;
                            } ?></button>
                    </div>
                </div>
                @csrf()
            </form>
        </div>
    </section>



    <script type="text/javascript">
        $("#clear_search").click(function() {
            document.getElementById('search_filter').value = "";
           $('#clear_search').html('');
           $("#searchResult").empty();
        });
         $(document).ready(function() {
           
            var typingTimer; //timer identifier
            var doneTypingInterval = 3000; //time in ms, 5 seconds for example
            var $input = $('#search_filter');

            //on keyup, start the countdown
            $input.on('keyup', function() {
                if($(this).val().length > 0) {
                    $('#clear_search').html('<img class="img-fluid movie-close-icon restrict"src="{{ asset('assets/website/image/cancel-icon.png') }}">');                             
                } else {
                    $('#clear_search').html('');
                }
                clearTimeout(typingTimer);
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            });

            //on keydown, clear the countdown 
            $input.on('keydown', function() {
                clearTimeout(typingTimer);
            });

            function doneTyping() {
                //do something
                var search = $("#search_filter").val();

                if (search != "" && search.length >= 3) {

                    $.ajax({
                        url: "{{ route('viewing-restrictions-search') }}",
                        type: 'get',
                        data: {
                            search: search
                        },
                        dataType: 'json',
                        success: function(response) {

                            $("#searchResult").empty();
                            var html = '';

                            var len = response.data.length;

                            for (var i = 0; i < len; i++) {
                                var id = response['data'][i]['id'];
                                var name = response['data'][i]['name'];

                                html += '<li class="text-yellow font-18" value="' + id +
                                    '" onclick="add_content_div(' + id + ',`' + name + '`);">' + name +
                                    '</li>';
                            }

                            $("#searchResult").html(html);
                        },
                        error: function(response) {
                            var html = '<li class="text-yellow font-18 text-center">' + response
                                .responseText + '</li>';
                            $("#searchResult").empty();
                            $("#searchResult").html(html);
                        }
                    });
                } else {

                    $("#searchResult").empty();
                }

            }

        });



        function add_content_div(id, name) {

            var content_ids = $('#content_ids').val();
            var is_exist_id = content_ids.search(id);

            if (is_exist_id < 0) {
                var html =
                    '<div class="d-flex justify-content-between align-items-center mb-5" id="restricted_content_div' + id +
                    '"><label class="text-white font-25">' + name +
                    '</label><img class="img-fluid me-5" src="{{ asset('assets/website/image/cancel-icon.png') }}" alt="" onclick="return remove_content_div(' +
                    id + ')"></div>';

                $('.add_content_div').prepend(html);

                if (content_ids == null || content_ids == '') {
                    var ids_array = [];

                } else {
                    var ids_array = content_ids.split(",");
                }
                ids_array.unshift(id);


                var string_ids = ids_array.toString();
                $('#content_ids').val(string_ids);

                $("#searchResult").empty();
                $("#search_filter").val('');

            }

        }

        function remove_content_div(id) {
            $("#restricted_content_div" + id).remove();

            var content_ids = $('#content_ids').val();
            var ids_array = content_ids.split(",");

            var string_ids = $.grep(ids_array, function(value) {
                return value != id;
            });

            $('#content_ids').val(string_ids);
        }
    </script>
@endsection
