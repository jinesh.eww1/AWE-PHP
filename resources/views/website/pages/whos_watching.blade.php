@extends('website.layouts.master')
@section('content')
@include('website.include.flash-message')

<style type="text/css">
    .lock-icon{
        height: 20%;
        width: 23%;
    }
    .awe-img-width{
        height: 110px;
        width: 110px;
        object-fit: cover;
    }
</style>
<section>
    <div class="container">
        <div class="text-center pt-md-5 mt-5">
            <img src="{{ asset('assets/website/image/awe-home.png') }}" class="img-fluid awe-img-width box-radius">
            <div class="text-white font-device"><?php if(Session::get('app_string')){ echo Session::get('app_string.movie_details.who_watching'); }else{ echo CommonHelper::multi_language('movie_details','who_watching')->multi_language_value->language_value; } ?></div>
        </div>
        <div class="text-center mt-md-4 pt-md-2">
            <?php /*<input type="hidden" name="latitude" id="latitude" value="{{ Session::get('latitude') }}">
            <input type="hidden" name="longitude" id="longitude" value="{{ Session::get('longitude') }}">*/ ?>
            <input type="hidden" name="ip" id="device_ip">
            <input type="hidden" name="device_name" id="device_name">
            <input type="hidden" name="device_token" id="device_token" value="{{ Session::get('device_token') }}">

            <?php if(isset($data) && !empty($data)){ 
                    foreach ($data as $key => $value) { 

                    if(!empty($value['lock_password'])){
                        $url = route('lock-profile',base64_encode($value['id']))."?type='watching'";
                        $onclick = '';
                    }else{
                        $url = 'javascript:void(0)';
                        $onclick = 'return generate_token('.$value['id'].');';
                    }
            ?>
                    
                <div class="btn bg-white m-lg-5 m-3 box-radius" style="height:175px; width: 175px;">
                    <a href="{{ $url }}" class="text-decoration-none" onclick="{{$onclick}}" >
                        <p class="font-18 text-yellow fw-bold text-start mb-2 mt-1">{{ strlen($value['first_name']) > 10 ? substr($value['first_name'],0,10)."..." : $value['first_name'] }} 
                            <?php if($value['is_child'] == 1){ ?>
                                <span class="text-end" style="float: right; color:#444444;"><?php if(Session::get('app_string')){ echo Session::get('app_string.home.kids'); }else{ echo CommonHelper::multi_language('home','kids')->multi_language_value->language_value; } ?></span>
                            <?php } ?>
                        </p>
                        <img class="img-fluid mb-2" id="whos_img" src="{{ $value['profile_picture_full_url'] }}"onerror="this.src='{{ asset('/assets/website/image/awe-home.png') }}'" alt="">
                        <?php if(!empty($value['lock_password'])){ ?>
                            <img class="img-fluid p-0 lock-icon" src="{{ asset('assets/website/image/card/lock.png') }}">
                        <?php } ?>
                    </a>
                </div>

            <?php } } ?>

        </div>

        <?php if(!empty($data) && count($data)<5){ ?>
            <div class="text-center my-3">
                <a href="{{ route('account.add') }}" class="btn text-white font-20 fw-bold bg-color p-3 profile-save-btn box-radius"><?php if(Session::get('app_string')){ echo Session::get('app_string.settings.add_profile'); }else{ echo CommonHelper::multi_language('settings','add_profile')->multi_language_value->language_value; } ?></a>
            </div>
        <?php } ?>
    </div>
</section>

<script type="text/javascript">
/*$(document).ready(function(){
    getLocation();
});*/
function generate_token(id) {
    
    var id = id;
    //var latitude = $('#latitude').val();
    //var longitude = $('#longitude').val();
    var ip = $('#device_ip').val();
    var device_name = $('#device_name').val();
    var device_token = $('#device_token').val();
    
    if(device_token == ''){
        Notiflix.Notify.Failure(`<?php if (Session::get('app_string')) { echo Session::get('app_string.app_titles_and_messages.allow_notification_permission'); } else { echo CommonHelper::multi_language('app_titles_and_messages', 'allow_notification_permission')->multi_language_value->language_value;} ?>`);
        
        return false;
    }
    

    $('#loader').show();
    
    $.ajax({
        type: "get",
        dataType: "json",
        url: "{{ route('generate.token') }}",
        data:{'id':id,'ip':ip,'device_name':device_name,'device_token':device_token},
        success: function (returnData) {
            if(returnData.status == true){
               // Notiflix.Notify.Success('{{ __("message.api.login_success") }}');
                window.location.href = "{{ route('home','movie') }}";
            }else{
                Notiflix.Notify.Failure(returnData.message);
            }
        },
        error: function (data) {
        },
    });
}

</script>
@endsection
