<?php

// Home
Breadcrumbs::for('dashboard', function ($trail) {
    $trail->push('Dashboard', route('admin.dashboard'));
});

Breadcrumbs::for('users', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Users', route('admin.users.index'));
});

Breadcrumbs::for('edituser', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('User', route('admin.users.index'));
    $trail->push('Edit', route('admin.users.index'));
});

Breadcrumbs::for('aprofile', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Profile', route('admin.profile'));
});

Breadcrumbs::for('viewuser', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Users', route('admin.users.index'));
    $trail->push('View', route('admin.user.show',''));
});

/*Admin Orders */
Breadcrumbs::for('order', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Order', route('admin.orders.index'));
});
Breadcrumbs::for('vieworder', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Order', route('admin.orders.index'));
    $trail->push('view', route('admin.orders.show',''));
});

/*Admin Contactus */
Breadcrumbs::for('Contact Us', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Contact Us', route('admin.contactus.index'));
});

Breadcrumbs::for('viewcontactus', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Contact Us', route('admin.contactus.index'));
    $trail->push('View', route('admin.contactus.show',''));
});

/* iSSUE */
Breadcrumbs::for('issue', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Issue', route('admin.issue.index'));
});
Breadcrumbs::for('reportedissue', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Reported Issue', route('admin.reported.issue'));
});

Breadcrumbs::for('addissue', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Issue', route('admin.issue.index'));
    $trail->push('Add', route('admin.issue.create'));
});

Breadcrumbs::for('editissue', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Issue', route('admin.issue.index'));
    $trail->push('Edit', route('admin.issue.edit',''));
});

Breadcrumbs::for('admingenre', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Genre', route('admin.genre.index'));
});

Breadcrumbs::for('adminaddgenre', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Genre', route('admin.genre.index'));
    $trail->push('Add', route('admin.genre.create'));
});

Breadcrumbs::for('admineditgenre', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Genre', route('admin.genre.index'));
    $trail->push('Edit', route('admin.genre.create'));
});

Breadcrumbs::for('adminviewgenre', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Genre', route('admin.genre.index'));
    $trail->push('View', route('admin.genre.show',''));
});

/*Admin CastType */

Breadcrumbs::for('admincasttype', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Cast Type', route('admin.cast.index'));
});

Breadcrumbs::for('adminaddcasttype', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Cast Type', route('admin.cast.index'));
    $trail->push('Add', route('admin.cast.create'));
});

Breadcrumbs::for('adminviewcasttype', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Cast Type', route('admin.cast.index'));
    $trail->push('View', route('admin.cast.show',''));
});

Breadcrumbs::for('admineditcasttype', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Cast TYpe', route('admin.cast.index'));
    $trail->push('Edit', route('admin.cast.create'));
});

/*Admin Artists */

Breadcrumbs::for('adminartist', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Artists', route('admin.artist.index'));
});

Breadcrumbs::for('adminaddartist', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Artists', route('admin.artist.index'));
    $trail->push('Add', route('admin.artist.create'));
});

Breadcrumbs::for('adminviewartist', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Artists', route('admin.artist.index'));
    $trail->push('View', route('admin.artist.show',''));
});

Breadcrumbs::for('admineditartist', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Artists', route('admin.artist.index'));
    $trail->push('Edit', route('admin.artist.create'));
});

/*Admin Artists */

Breadcrumbs::for('admintags', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Tags', route('admin.tags.index'));
});

Breadcrumbs::for('adminaddtags', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Tags', route('admin.tags.index'));
    $trail->push('Add', route('admin.tags.create'));
});

Breadcrumbs::for('adminedittags', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Tags', route('admin.tags.index'));
    $trail->push('Edit', route('admin.tags.create'));
});

Breadcrumbs::for('adminviewtags', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Tags', route('admin.tags.index'));
    $trail->push('View', route('admin.tags.show',''));
});

/*Admin multi language */

Breadcrumbs::for('multilanguage', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Multi Language', route('admin.multilanguage.index'));
});

Breadcrumbs::for('addmultilanguage', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Multi Language', route('admin.multilanguage.index'));
    $trail->push('Add', route('admin.multilanguage.create'));
});

Breadcrumbs::for('viewmultilanguage', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Multi Language', route('admin.multilanguage.index'));
    $trail->push('View', route('admin.multilanguage.show',''));
});

Breadcrumbs::for('editmultilanguage', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Multi Language', route('admin.multilanguage.index'));
    $trail->push('Edit', route('admin.multilanguage.create'));
});

/*Admin Age Rating */

Breadcrumbs::for('adminagerating', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Age Restriction', route('admin.ratings.index'));
});

Breadcrumbs::for('adminaddagerating', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Age Restriction', route('admin.ratings.index'));
    $trail->push('Add', route('admin.ratings.create'));
});

Breadcrumbs::for('adminviewagerating', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Age Restriction', route('admin.ratings.index'));
    $trail->push('View', route('admin.ratings.show',''));
});

Breadcrumbs::for('admineditagerating', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Age Restriction', route('admin.ratings.index'));
    $trail->push('Edit', route('admin.ratings.create'));
});

/*Admin Content */
Breadcrumbs::for('admincontent', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Content', route('admin.content.index'));
});

Breadcrumbs::for('adminaddcontent', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Content', route('admin.content.index'));
    $trail->push('Add', route('admin.content.create'));
});


Breadcrumbs::for('adminviewcontent', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Content', route('admin.content.index'));
    $trail->push('View', route('admin.content.show',''));
});

Breadcrumbs::for('admineditcontent', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Content', route('admin.content.index'));
    $trail->push('Edit', route('admin.content.create'));
});



/*Admin Video */
Breadcrumbs::for('adminvideo', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Video', route('admin.video.index'));
});

Breadcrumbs::for('adminaddvideo', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Video', route('admin.video.index'));
    $trail->push('Add', route('admin.video.create'));
});


Breadcrumbs::for('adminviewvideo', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Video', route('admin.video.index'));
    $trail->push('View', route('admin.video.show',''));
});

Breadcrumbs::for('admineditvideo', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Video', route('admin.video.index'));
    $trail->push('Edit', route('admin.video.create'));
});

/*Admin feedback */
Breadcrumbs::for('adminfeedback', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Feedback', route('admin.feedback.index'));
});

Breadcrumbs::for('adminviewfeedback', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Feedback', route('admin.feedback.index'));
    $trail->push('View', route('admin.feedback.show',''));
});

/*Admin ask to us */
Breadcrumbs::for('adminasktous', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Ask to us', route('admin.ask-to-us.index'));
});

Breadcrumbs::for('adminviewasktous', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Ask to us', route('admin.ask-to-us.index'));
    $trail->push('View', route('admin.ask-to-us.show',''));
});

/*Admin faq */
Breadcrumbs::for('adminfaq', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('FAQ', route('admin.faq.index'));
});

Breadcrumbs::for('adminaddfaq', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('FAQ', route('admin.faq.index'));
    $trail->push('Add', route('admin.faq.create'));
});


Breadcrumbs::for('adminviewfaq', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('FAQ', route('admin.faq.index'));
    $trail->push('View', route('admin.faq.show',''));
});

Breadcrumbs::for('admineditfaq', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('FAQ', route('admin.faq.index'));
    $trail->push('Edit', route('admin.faq.create'));
});


/*Admin Season */

Breadcrumbs::for('adminseason', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Season', route('admin.season.index'));
});

Breadcrumbs::for('adminaddseason', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Season', route('admin.season.index'));
    $trail->push('Add', route('admin.season.create'));
});


Breadcrumbs::for('adminviewseason', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Season', route('admin.season.index'));
    $trail->push('View', route('admin.season.show',''));
});

Breadcrumbs::for('admineditseason', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Season', route('admin.season.index'));
    $trail->push('Edit', route('admin.season.create'));
});

/*Admin Plan */

Breadcrumbs::for('adminplan', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Plan', route('admin.plan.index'));
});

Breadcrumbs::for('adminaddplan', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Plan', route('admin.plan.index'));
    $trail->push('Add', route('admin.plan.create'));
});


Breadcrumbs::for('admineditplan', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Plan', route('admin.plan.index'));
    $trail->push('Edit', route('admin.plan.create'));
});

Breadcrumbs::for('adminviewplan', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Plan', route('admin.plan.index'));
    $trail->push('View', route('admin.plan.show',''));
});

Breadcrumbs::for('admincurrency', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Currency', route('admin.currency.index'));
});

Breadcrumbs::for('adminaddcurrency', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Currency', route('admin.currency.index'));
    $trail->push('Add', route('admin.currency.create'));
});


Breadcrumbs::for('admineditcurrency', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Currency', route('admin.currency.index'));
    $trail->push('Edit', route('admin.currency.create'));
});

Breadcrumbs::for('adminviewcurrency', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Currency', route('admin.currency.index'));
    $trail->push('View', route('admin.currency.show',''));
});

Breadcrumbs::for('admincountry', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Country', route('admin.country.index'));
});

Breadcrumbs::for('adminaddcountry', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Country', route('admin.country.index'));
    $trail->push('Add', route('admin.country.create'));
});


Breadcrumbs::for('admineditcountry', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Country', route('admin.country.index'));
    $trail->push('Edit', route('admin.country.create'));
});

Breadcrumbs::for('adminviewcountry', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Country', route('admin.country.index'));
    $trail->push('View', route('admin.country.show',''));
});

Breadcrumbs::for('adminprice', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Price', route('admin.price.index'));
});

Breadcrumbs::for('adminaddprice', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Price', route('admin.price.index'));
    $trail->push('Add', route('admin.price.create'));
});


Breadcrumbs::for('admineditprice', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Price', route('admin.price.index'));
    $trail->push('Edit', route('admin.price.create'));
});

Breadcrumbs::for('adminviewprice', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Price', route('admin.price.index'));
    $trail->push('View', route('admin.price.show',''));
});

Breadcrumbs::for('adminattribute', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Features', route('admin.attribute.index'));
});

Breadcrumbs::for('adminaddattribute', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Features', route('admin.attribute.index'));
    $trail->push('Add', route('admin.attribute.create'));
});


Breadcrumbs::for('admineditattribute', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Features', route('admin.attribute.index'));
    $trail->push('Edit', route('admin.attribute.create'));
});

Breadcrumbs::for('adminviewattribute', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Features', route('admin.attribute.index'));
    $trail->push('View', route('admin.attribute.show',''));
});

Breadcrumbs::for('adminpayment_history', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Payment History', route('admin.payment_history.index'));
});

Breadcrumbs::for('adminviewpayment_history', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Payment History', route('admin.payment_history.index'));
    $trail->push('View', route('admin.payment_history.show',''));
});

Breadcrumbs::for ('config', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('System Configuration', route('admin.system.config'));
});
Breadcrumbs::for ('app_config', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Application Configuration', route('admin.system.config'));
});