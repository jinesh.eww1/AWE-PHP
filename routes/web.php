<?php

use Illuminate\Support\Facades\Route;

// use App\Http\Controllers\Admin\UserController;
// use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Admin\SendMailController;
use App\Http\Controllers\Admin\CMSController;
    use App\Http\Controllers\Admin\ExportController;
use App\Http\Controllers\Website\HomeController;
use App\Http\Controllers\SubscriptionController;
  
use App\Http\Controllers\Website\SocialLoginController;
use App\Http\Controllers\Website\SettingController;
use App\Http\Controllers\Website\AppleSocialController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


Route::post('/webhook_response', ['as'=>'home','uses'=>'PaypalWebHookController@webhook_response'])->name('webhook_response');
Route::post('/webhook_response/paypal', ['as'=>'home','uses'=>'PaypalWebHookController@webhook_paypal_response'])->name('webhook_paypal_response');

Route::get('/login',[HomeController::class,'index']);
Route::any('/',[HomeController::class,'index'])->name('index');
Route::get('/signup',[HomeController::class,'signup'])->name('signup');
Route::post('/signup',[HomeController::class,'signup'])->name('signup');
Route::get('/signup/email-otp/{email}',[HomeController::class,'send_email_otp']);
Route::get('/signup/mobile-otp',[HomeController::class,'send_otp_mobile']);
Route::any('/verify/otp',[HomeController::class,'verify_otp'])->name('verify.otp');
Route::any('/forgot/password',[HomeController::class,'forgot_password'])->name('forgot.password');
Route::get('/language/change',[HomeController::class,'language_change'])->name('language.change');
Route::get('/resend-otp',[HomeController::class,'resend_mobile_otp']);
Route::get('/resend-otp-forgot-password',[HomeController::class,'resend_mobile_otp_forgot']);
Route::get('help', [SettingController::class,'help'])->name('help');
Route::get('feedback', [SettingController::class,'feedback'])->name('feedback');
Route::post('ask-to-us', [SettingController::class,'ask_to_us_add'])->name('ask-to-us');


Route::group(['middleware' => ['website']], function () {
    
    Route::any('/preferred-category',[HomeController::class,'preferred_category'])->name('preferred.category');
    Route::get('/home/{type}',[HomeController::class,'home'])->name('home');
    Route::get('/my-list',[HomeController::class,'my_list'])->name('my-list');
    Route::get('/view-more/{type}/{category_type}',[HomeController::class,'view_more'])->name('view.more');
    
    Route::get('/choose-plan',[App\Http\Controllers\Website\PlanController::class,'choose_plan'])->name('all.plans');
    Route::any('/choose-payment-method',[App\Http\Controllers\Website\PlanController::class,'choose_payment_method'])->name('payment.method');
    Route::any('/subscription-plan-overview',[App\Http\Controllers\Website\PlanController::class,'subscription_plan_overview'])->name('subscription.plan.overview');
    Route::get('/paypal/return/response',[App\Http\Controllers\Website\PlanController::class,'paypal_return_response'])->name('paypal.return.response');
    Route::post('/stripe/return/response',[App\Http\Controllers\Website\PlanController::class,'stripe_return_response'])->name('stripe.return.response');
    Route::any('/preferred-language',[HomeController::class,'preferred_language'])->name('preferred.language');
    Route::get('/user-profiles',[HomeController::class,'whos_watching'])->name('whos.watching');
    Route::get('/generate/token',[HomeController::class,'generate_token'])->name('generate.token');
    Route::any('/account-add',[HomeController::class,'account_add'])->name('account.add');
    Route::get('/view-plan',[HomeController::class,'view_plan'])->name('view.plan');
    Route::get('/logout',[HomeController::class,'logout'])->name('logout');
    Route::get('/cancel-plan/{plan_id}/{plan_type}',[HomeController::class,'cancel_plan'])->name('cancel.plan');
    Route::get('/confirm-cancel-plan',[HomeController::class,'confirm_cancel_plan'])->name('confirm.cancel.plan');

    Route::get('/profile',[SettingController::class,'profile'])->name('profile');
    Route::get('/allow-notification',[SettingController::class,'allow_notification'])->name('allow.notification');
    Route::any('/account-profile',[SettingController::class,'account_profile'])->name('account.profile');
    Route::get('/register-device',[SettingController::class,'register_device'])->name('register-device');
    Route::post('/remove-device',[SettingController::class,'remove_device'])->name('remove-device');

    Route::get('/share-activity',[SettingController::class,'share_activity'])->name('share-activity');
    Route::get('/manage-profile',[SettingController::class,'manage_profile'])->name('manage-profile');
    Route::any('/edit-profile/{id}',[SettingController::class,'edit_profile'])->name('edit-profile');
    Route::any('/lock-profile/{id}',[SettingController::class,'lock_profile'])->name('lock-profile');
    Route::any('/profile-lock/{id}',[SettingController::class,'profile_lock'])->name('profile-lock');
    Route::any('/playback-setting/{id}',[SettingController::class,'playback_setting'])->name('playback-setting');
    Route::any('/preferred-categories/{id}',[SettingController::class,'preferred_categories'])->name('preferred-categories');
    Route::any('viewing-restrictions/{id}', [SettingController::class,'viewing_restrictions'])->name('viewing-restrictions');
    Route::get('viewing-restrictions-search', [SettingController::class,'viewing_restrictions_search'])->name('viewing-restrictions-search');
    Route::any('change-password', [SettingController::class,'change_password'])->name('user-change-password');
    Route::get('popular-search', [HomeController::class,'popular_search'])->name('popular-search');
    Route::get('search', [HomeController::class,'search'])->name('search');
    Route::get('payment-history', [SettingController::class,'payment_history'])->name('payment-history');
    Route::get('account-remove/{id}', [SettingController::class,'account_remove'])->name('account-remove');
    Route::any('forgot-pin/{id}', [SettingController::class,'forgot_pin'])->name('forgot-pin');
    
    Route::get('content/details/{slug}', [HomeController::class,'content_detail'])->name('content-details');
    Route::get('chat-support', [SettingController::class,'chat_support'])->name('chat-support');
    Route::get('content-liked', [HomeController::class,'content_liked'])->name('content-liked');
    Route::get('content-disliked', [HomeController::class,'content_disliked'])->name('content-disliked');
    Route::get('content-my-list', [HomeController::class,'content_my_list'])->name('content-my-list');
    Route::get('/notifications',[HomeController::class,'notifications'])->name('notifications');
});

    

//social login
Route::get('/facebook', [SocialLoginController::class, 'redirectToFacebook'])->name('facebook');
Route::get('/facebook/callback', [SocialLoginController::class, 'handleFacebookCallback']);
Route::get('auth/google', [SocialLoginController::class, 'redirectToGoogle'])->name('google');
Route::get('google/callback', [SocialLoginController::class, 'handleGoogleCallback']);


// apple login 
Route::get('auth/apple', [AppleSocialController::class, 'redirectToApple'])->name('apple');
Route::any('apple/callback', [AppleSocialController::class, 'handleCallback'])->name('apple.callback');

Route::get('/subscription/create', ['as'=>'home','uses'=>'SubscriptionController@index'])->name('subscription.create');
Route::post('order-post', ['as'=>'order-post','uses'=>'SubscriptionController@orderPost']);

Route::get('about-us', 'Api\DocumentController@about_us');
/*Route::get('contact-us', 'Api\DocumentController@contact_us');
Route::get('privacy-notice', 'Api\DocumentController@privacy_notice');
Route::get('term-of-service', 'Api\DocumentController@term_of_service');*/


Route::get('streaming', 'CommonController@streaming');

Route::post('add-channel-id', 'CommonController@add_channel_id');
Route::get('account-activation/{id}', 'CommonController@account_activation');
Route::any('create/password/{id}/{token}', 'CommonController@craete_password')->name('create.password');
Route::get('auth-account-activation/{id}', 'CommonController@auth_account_activation');

//Route::post('forgot/password', 'CommonController@forgot_password')->name('forgot_password');

Route::get('terms-conditions', 'CommonController@terms_conditions');
Route::get('privacy-policy', 'CommonController@privacy_policy');
Route::get('loyalty-reward-terms-condition', 'CommonController@loyalty_reward_terms_condition');
Route::get('logs/{id}', 'CommonController@activity_log');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'admin']], function () {

    Route::get('profile', 'ProfileController@view')->name('profile');
    Route::post('profile-update/{id}', 'ProfileController@update')->name('profile.update');

    Route::post('change-password/{id}', 'ProfileController@changePassword')->name('change.password');

    Route::get('/contenttags/{type}', 'ExportController@contenttags')->name('contenttags.export');
    Route::get('/users/{type}', 'ExportController@users')->name('users.export');
    Route::get('/genres/{type}', 'ExportController@genres')->name('genres.export');
    Route::get('/casting/{type}', 'ExportController@casting')->name('casting.export');
    Route::get('/artists/{type}', 'ExportController@artists')->name('artists.export');
    Route::get('/age/{type}', 'ExportController@age')->name('age.export');
    Route::get('/plan_managment/{type}', 'ExportController@plan_managment')->name('plan_managment.export');
    Route::get('/plan_features/{type}', 'ExportController@plan_features')->name('plan_features.export');
    Route::get('/payment_records/{type}', 'ExportController@payment_records')->name('payment_records.export');
    Route::get('/contents/{type}', 'ExportController@contents')->name('contents.export');
    Route::get('/seasons/{type}', 'ExportController@seasons')->name('seasons.export');
    Route::get('/videos/{type}', 'ExportController@videos')->name('videos.export');
    Route::get('/faqs/{type}', 'ExportController@faqs')->name('faqs.export');
    Route::get('/feedbacks/{type}', 'ExportController@feedbacks')->name('feedbacks.export');
    Route::get('/multi_language_management/{type}', 'ExportController@multi_language_management')->name('multi_language_management.export');
    
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('s3bucket', 'DashboardController@s3bucket')->name('s3bucket');
    Route::get('support', 'DashboardController@support_chat')->name('support');
    Route::get('get_chat_history_data', 'DashboardController@get_chat_history_data')->name('get_chat_history_data');   
    Route::post('reply_chat', 'DashboardController@reply_chat')->name('reply_chat');   
    Route::get('users', 'UserController@index')->name('users.index');
    Route::get('users/show/{id}', 'UserController@show')->name('user.show');
    Route::get('users/edit/{id}', 'UserController@edit')->name('user.edit');
    Route::put('users/update/{id}', 'UserController@update')->name('user.update');
    Route::get('users/destroy/{id}', 'UserController@edit')->name('user.destroy');
    Route::post('active_deactive','UserController@active_deactive')->name('active_deactive');
    Route::post('restore_account','UserController@restore_account')->name('restore_account');

    Route::post('active_deactive_genre','CategoryController@active_deactive_genre')->name('active_deactive_genre');
      
    Route::resource('category',CategoryController::class);
    Route::resource('season',SeasonController::class);
    Route::resource('ratings',AgeRatingController::class);
    Route::resource('tags',TagController::class);
    
    Route::resource('artist',ArtistController::class);
    Route::resource('genre',GenreController::class);
    Route::resource('cast',CastTypeController::class);
    Route::resource('plan',PlanController::class);  
    Route::resource('currency',CurrencyController::class);  
    Route::resource('country',CountryController::class);  
    Route::resource('price',PriceController::class);  
    Route::resource('attribute',AttributeController::class);  
    Route::resource('contactus',ContactUsController::class);    
    Route::resource('issue',IssueController::class);    
    Route::resource('content',ContentController::class);    
    Route::resource('video',VideoController::class);    
    Route::resource('faq',FaqController::class);    
    Route::resource('feedback',FeedbackController::class);    
    Route::resource('ask-to-us',AskToUsController::class);    
    Route::resource('multilanguage',MultiLanguageKeyController::class);    
    Route::resource('payment_history',PaymentHistoryController::class);    

    // Route::get('tags', TagController::class,['names' => 'tags.export'])->only(['tagsExport']);
    // Route::get('tags/{type}', TagController::class,['names' => 'tags.export'])->only(['tagsExport']);
        
    Route::post('import', 'ContentController@import')->name('import');
    Route::any('adv_content_change', 'ContentController@adv_content_change')->name('adv_content_change');
    Route::any('movie_adv_content_change', 'ContentController@movie_adv_content_change')->name('movie_adv_content_change');
    Route::get('get_season_data', 'VideoController@get_season_data')->name('get_season_data');   
    Route::post('trailer_file_upload', 'ContentController@trailer_file_upload')->name('trailer_file_upload');   
    Route::post('chunk_file_upload', 'VideoController@chunk_file_upload')->name('chunk_file_upload');   
    Route::post('active_deactive_faq','FaqController@active_deactive_faq')->name('active_deactive_faq');

    Route::get('plan/manageattr/{id}', 'PlanController@manageattr')->name('plan.manageattr');   
    Route::post('plan/attr_update/{id}', 'PlanController@attr_update')->name('plan.attr_update');   
    
    Route::post('product/addstore/{id}', 'ProductController@addstore')->name('product.addstore');   
    Route::get('remove_product_store/{id}', 'ProductController@remove_product_store')->name('remove_product_store');   
    Route::post('save_product_store', 'ProductController@save_product_store')->name('save_product_store');   
    Route::post('active_deactive_product','ProductController@active_deactive_product')->name('active_deactive_product');
    Route::post('get_image','ProductController@get_image')->name('get_image');

    Route::post('order_notification_customer','OrderController@order_notification_customer')->name('order_notification_customer');
    
    Route::get('reported/issue','IssueController@reported_issue_index')->name('reported.issue');
    Route::get('bulk/notification',[NotificationController::class,'index'])->name('bulk.index');
    Route::post('bulk/notification',[NotificationController::class,'store'])->name('bulk.store');
    
    Route::get('system/config','SystemConfigController@index')->name('system.config');
    Route::post('system/config/submit','SystemConfigController@update')->name('system.config.submit');
    Route::post('system/config/backup','SystemConfigController@update')->name('system.config.backup');
    Route::post('system/config/download','SystemConfigController@update')->name('system.config.download');
    Route::post('system/config/delete_backup','SystemConfigController@update')->name('system.config.delete_backup');
    
    Route::get('app/config','SystemConfigController@app')->name('app.config');
    Route::post('app/config/submit','SystemConfigController@submit_app')->name('app.config.submit');

    Route::get('report/all_report','ReportController@index')->name('report.all');

    Route::post('refund','OrderController@refund_test')->name('refund');
    Route::any('send-mail-customer',[SendMailController::class,'customer'])->name('mail.customer');
    Route::any('send-notification-customer',[SendMailController::class,'customer_notification'])->name('notification.customer');

    Route::any('terms-conditions',[CMSController::class,'terms_conditions'])->name('terms_conditions');
    Route::any('privacy-policy',[CMSController::class,'privacy_policy'])->name('privacy_policy');
    Route::any('loyalty-reward-terms-condition',[CMSController::class,'loyalty_reward_terms_condition'])->name('loyalty_reward_terms_condition');
    
});

require __DIR__ . '/auth.php';
